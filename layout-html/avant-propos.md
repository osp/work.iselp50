<div class="cartouche">   

<p class="cartouche-titre">Avant-propos</p>
<p class="cartouche-signature">
<span class="book-index" data-book-index="Van der Stichele, Michel">Michel Van der Stichele</span> </p>
<p class="cartouche-role">
Président du conseil d’administration
</p>

</div> 

<div id="my-toc-content" style="display:none;"></div>

> *À chaque siècle son art, à l’art sa liberté*

> > %%Klimt, Gustave|Gustave Klimt%%

L’ISELP&#x202F;: cinquante années déjà de médiation culturelle&#x202F;! Instituée en 1971
par %%Brys-Schatan, Gita|Gita Brys-Schatan%%, la pépite ne cesse de croître et de faire lien
entre public et production artistique.

L’art aujourd’hui est plus que jamais une question de dialogue entre
l’artiste et le public. La médiation est la façon la plus appropriée de
créer ce contact. Elle n’impose pas ses réponses mais suscite et
nourrit le débat&#x202F;; elle accompagne le visiteur dans ses questionnements
et lui donne les outils lui permettant de forger sa perception
personnelle.

Merci à tous ces publics, de tous âges et de tous horizons, qui durant
ces décennies se sont prêtés à l’exercice et ont enrichi ce dialogue.
Ce public continue plus que jamais à répondre présent, que ce soit une
présence physique aux cours, conférences ou expositions ou plus
virtuelle au travers des capsules que vous êtes nombreux à écouter.

Merci aux nombreux artistes qui ont permis et accepté ces rencontres
avec enthousiasme et conviction. Merci aux directrices et directeurs qui
se sont succédé à la tête de l’Institut, créant le cadre et les
conditions de cette médiation. Merci à tout le personnel qui y a
travaillé et qui y travaille aujourd’hui&#x202F;: commissaires d’exposition,
techniciens du bâtiment, chercheurs, médiateurs, responsable du centre
de documentation, graphistes, personnel de l’accueil, responsables de
la communication, gestionnaires administratifs et financiers, bref à
toutes celles et à tous ceux qui apportent leur pierre à l’édifice et
qui excellent à le faire rayonner.

Permettez-moi de formuler des sentiments de gratitude à l’égard des
administratrices et des administrateurs qui ont soutenu et guidé notre
association durant toutes ces années. Leur apport efficace permet à
l’ISELP d’envisager son avenir de façon sereine et pérenne.

Aux pouvoirs publics, nous adressons aussi nos remerciements. Dans
l’ordre chronologique, la %%Commission communautaire française%%, la
%%Fédération Wallonie-Bruxelles%% et la %%Région de Bruxelles-Capitale/Région bruxelloise|Région Bruxelles-Capitale%% à
l’intervention d’%%Actiris%% ont aidé l’ISELP et lui permettent
d’assurer plus que jamais son rôle de médiation.

En cinquante ans, le paysage bruxellois de l’art contemporain s’est
fortement densifié et diversifié. On ne peut que s’en réjouir. Ce
foisonnement bienvenu d’initiatives et de réalisations rend plus que
jamais nécessaire la mission spécifique de l’ISELP, à savoir le
discours critique sur la création contemporaine et la sensibilisation
des publics.

Puisse ce dialogue ouvert, franc, dense et riche en émotions se
poursuivre durant les cinquante prochaines années&#x202F;!
