Colophon
{: .cartouche}

Recherche en cours. Un parcours dans l’histoire de l’ISELP

<div id="my-toc-content" style="display:none;"></div>


Conception éditoriale et rédactionnelle
:   Adrien Grimmeau
:   Mélanie Rainville
:   Les textes de cette publication sont sous license CC BY-NC-SA.

Design graphique
:    OSP (Ludi Loiseau, Sarah Magnan)
:    Tous les fichiers de mise en page sont disponibles sur http://osp.kitchen/work/iselp50/ et sont sous la license Free Art License version 1.3
:    Cette publication est conçue avec des logiciels libres ou open source. Elle a été mise en page en CSS et en HTML avec Paged.js — www.pajedjs.org
:    Les cahiers d'images et la couverture ont été mis en page avec Scribus.
:    L’ensemble de ce livre a été composé avec les typographies suivantes&#x202F;: Comic Neue — SIL Open Font, U001 — Aladdin Free Public License et Wremena — Domaine public.
:    Les différentes formes du logo ISELP reprises dans la partie historique sont issues des supports graphiques de l’Institut. Extraites des communications imprimées, les formes de l’acronyme sont reprises dans chacun des chapitres en fonction de leurs années d’usage.

Photographies et images
:    Tout droits réservés, dans les conditions générales internationales de propriété intellectuelle. Aucune photographie de ce livre ne peut-être reproduite ou diffusée sous aucune forme ni par aucun moyen électronique, mécanique ou d’autre nature, sans autorisation écrite de l’éditeur et des auteurs.
:   Tous les efforts nécessaires ont été entrepris afin de contacter les ayants droit des documents reproduits. Merci de contacter l’ISELP pour toute demande.
:   Les scans des documents originaux repris dans le troisième cahier d'images, pages 65 à 80, apparaissent à deux échelles. Un tiers pour les affiches et deux tiers pour les dépliants ou cartons d’invitations.

Impression et reliure
:    Graphius
:    Hemelstraat 2
:    B-1651 Beersel

Papiers
:    Brossulin XT (couverture)
:    Munken Kristall
:    Symbol Freelife Gloss (cahiers photos)

Relecture
:   Jean-François Caro

ISELP
:    31-B, boulevard de Waterloo
:    1000 Bruxelles
:    Belgique
:    L’ISELP est un centre de recherche où publics, artistes et chercheurs se rencontrent autour d’œuvres et d’enjeux inhérents à la culture contemporaine. Ce livre est publié à l’occasion de son cinquantième anniversaire.


Éditeur responsable
:    Adrien Grimmeau
:    31-B, boulevard de Waterloo
:    1000 Bruxelles
:    Belgique

ISBN 
:   2-87373035-8
:   D/2021/87373/1    

Soutien
:    Cette publication a bénéficié d’un soutien spécifique de la Commission communautaire française et de Brussels International. L’ISELP remercie de leur appui continu la Fédération Wallonie-Bruxelles, la Commission communautaire française et Actiris.

<figure><img src="https://cloud.osp.kitchen/s/8F7Zfk5N637t3Sj/preview"/></figure>
