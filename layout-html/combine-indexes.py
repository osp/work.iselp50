import glob
import json
import sys
import locale
locale.setlocale(locale.LC_ALL, "")

index = {}

inpatt = sys.argv[1] if len(sys.argv) > 1 else 'index-*.json'
outpath = sys.argv[2] if len(sys.argv) > 2 else 'index.json'
outpath_html = outpath + '.html'

for path in glob.glob(inpatt):
  local_index = json.load(open(path, 'r'))

  for k in local_index:
    if k in index:
      index[k] = sorted(set(index[k] + local_index[k]))
    else:
      index[k] = sorted(set(local_index[k]))


with open(outpath, 'w') as out:
  json.dump(index, out, ensure_ascii=False)

print('Wrote {}'.format(outpath))

######
# Templates for generated html

global_template = """
  <div id="my-toc-content" style="display:none;"></div>
  <div class="cartouche">
  <p class="cartouche-titre">Index</p>
  </div>
  <section class="index-body">
     <p class="cartouche-role">Cet index inclus les noms propres cités dans la publication, hormis ceux de la liste de cours, conférences et colloques et de la liste des expositions.</p>
    {index}
  </section>
"""

index_letter_template = """
  <div id="{letter}" class='lettre'><h3>{letter}</h3><span></span></div>
  <section class="index-letter-section">
    {index}
  </section>
"""

index_row_template = """
  <section class="index-row">
    <span class="index-row-key"><p>{key}</p></span>
    <section class="index-row-pagenumbers"><p>{pagenumbers}</p></section>
  </section>
"""

page_number_template = """<span class="index-page-number">{number}</span>"""

page_number_separator = ', ' # also possible to make this empty and use an :after in CSS


######
# Constructing the index

last_letter = None
letter_index = {}
letter_index_html = ""
index_html = ""

indexKeys = sorted(index.keys(), key=locale.strxfrm )



for k in indexKeys:
  current_letter = k[0].lower()

  if last_letter != current_letter:
    if letter_index_html:
      index_html += index_letter_template.format(letter=last_letter, index=letter_index_html)
      letter_index_html = ""

  pagenumbers_html = page_number_separator.join([page_number_template.format(number=number) for number in index[k]])
  index_row_html = index_row_template.format(key=k, pagenumbers=pagenumbers_html)

  letter_index_html += index_row_html

  last_letter = current_letter

index_html += index_letter_template.format(letter=last_letter, index=letter_index_html)

body = global_template.format(index=index_html)

#######
# Write to an html file

with open("header.html", "r", encoding="utf-8") as input_file:
    h = input_file.read()
    # Insert id in header based on filename
    h = h.replace("{{ id }}",  "index")

with open("footer.html", "r", encoding="utf-8") as input_file:
    f = input_file.read()

with open(outpath_html, "w", encoding="utf-8") as o:
  o.write('{header} {body} {footer}'.format(header=h, body=body, footer=f))

print('Wrote {}'.format(outpath_html))