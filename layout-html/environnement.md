# Art<br/>espace<br/>public {: .cartouche-titre}

## D’%%Environnemental%% à *Being Urban*&#x202F;: l’ISELP et l’art public {: .sous-titre}
 
  
Adrien %%Grimmeau, Adrien|Grimmeau%%
{: .cartouche-signature}

<div id="my-toc-content" style="display:none;"></div>

Art, espace, public
{: .running-header}

<figure class="" markdown=1>
<img alt="" src="contenu-final/012-visuels-texte-environnement/1A.jpg" />
<figcaption>
<p>
Installation de Jean <span class="book-index" data-book-index="Glibert, Jean">Glibert</span> à la 
<span class="book-index" data-book-index="Bourse">Bourse</span> 
dans le cadre de l’exposition <em>La Couleur et la ville</em>, 1978. Photo&#x202F;: Jean Glibert
</p>
</figcaption>
</figure>

 <figure  class="elem-float-top"><img alt=""  src="contenu-final/012-visuels-texte-environnement/2A.jpg" /><figcaption>
<p><span class="book-index" data-book-index="Tout (Asbl)">TOUT</span> (Paul <span class="book-index" data-book-index="Gonze, Paul">Gonze</span>), <em>Rêve encadré</em>, installation au <span class="book-index" data-book-index="Mont des Arts">Mont des Arts</span> dans le cadre de l’exposition <em>La Couleur et la ville</em>, 1978. Photo&#x202F;: Paul Gonze</p>
</figcaption>
</figure>

<span class="lettrine">M</span>on récit commence avec l’exposition *La Couleur et la ville*. Organisée
du 14 novembre au 10 décembre 1978 à l’initiative du secrétaire d’État à
la Culture Française François <span class="book-index" data-book-index="Persoons, François">Persoons</span>, elle est pensée comme un projet
expérimental dont il confie l’organisation à deux ASBL bruxelloises&#x202F;:
l’ISELP (Gita <span class="book-index" data-book-index="Brys-Schatan, Gita">Brys-Schatan</span>) et %%Jeunesse et Arts Plastiques (JAP)| JAP%%[^1] (Michel <span class="book-index" data-book-index="Baudson, Michel">Baudson</span>). Elle rassemble
les artistes Jean <span class="book-index" data-book-index="Glibert, Jean">Glibert</span>, <span class="book-index" data-book-index="De Gobert, Philippe">Philippe De Gobert</span>, <span class="book-index" data-book-index="Jano">Jano</span>, <span class="book-index" data-book-index="Mees, André">André Mees</span>,
Jacqueline <span class="book-index" data-book-index="Mesmaeker, Jacqueline">Mesmaeker</span>, <span class="book-index" data-book-index="Tout (Asbl)">TOUT</span> (Paul %%Gonze, Paul|Gonze%%), Serge %%Vandercam, Serge|Vandercam%%, le
collectif %%50/04 (collectif)|50/04%% ainsi que le poète Maurice %%Maelderlick, Maurice|Maelderlick%% et le compositeur
%%Pierre, Alain|Alain Pierre%%.




<span class="lettrine">*L</span>a Couleur et la ville* amorce un tournant dans l’histoire de l’ISELP
pour au moins deux raisons. Cela fait alors cinq ans que l’Institut
organise des expositions, depuis *Art et ordinateur* en 1974, mais c’est
la première fois qu’il collabore directement avec des artistes
plasticiens. Avant 1978, les expositions n’étaient pas conçues comme une
expérience artistique. L’ISELP est alors avant tout un lieu de parole&#x202F;:
il organise principalement des cours et des rencontres, et s’il a
l’habitude d’inviter des artistes (depuis une première rencontre avec
Dennis %%Oppenheim, Dennis|Oppenheim%%  en 1972), c’est pour qu’ils parlent. Même les
expositions sont plutôt pédagogiques, voire directement basées sur un
livre. Pour la première fois, cette exposition proposera une expérience
esthétique par elle-même, et non guidée par le discours. D’ailleurs,
quasiment simultanément à *La Couleur et la ville*, l’Institut inaugure
son Espace Éphémère pour présenter des artistes contemporains. Cette
évolution n’est cependant pas une révolution, et pendant plusieurs
années encore les expositions principales ne seront pas strictement
plastiques.
{: .first}



<span class="lettrine">L</span>e deuxième élément nous intéresse plus particulièrement dans le cadre
de notre propos. *La Couleur et la ville* est conçue dans l’espace
public. Se faisant, l’exposition résout un problème récurrent de
l’Institut&#x202F;: son manque de place pour accueillir des œuvres (rappelons
qu’*Art et ordinateur* prit place à Forest National, faute de locaux).
Mais surtout, elle développe d’une façon nouvelle son rapport au public.
Les expositions présentées durant les premières années se référaient
plutôt à l’histoire culturelle, étudiant les formes visuelles liées à
des champs considérés comme périphériques de la création (informatique,
caricature, bande-dessinée, etc.), dans le but de connecter le langage
plastique au public le plus large. Cette fois, et toujours dans la même
optique, l’idée est de *placer* des formes artistiques dans un espace collectif.
{: .first}

<span class="lettrine">L</span>ier l’art et la ville n’est pas inattendu pour l’ISELP. Le lien
historique qu’entretient l’Institut avec l’école de %%École nationale supérieure d’architecture et des arts visuels de La Cambre (La Cambre)|La Cambre%% y est
certainement pour quelque chose&#x202F;: jusqu’en 1980, l’École nationale supérieure d'architecture et des arts visuels associe encore
l’enseignement de l’art et de l’architecture. Maurice %%Culot, Maurice|Culot%%, qui y
enseigne, est un des premiers à donner un cours à l’ISELP, en 1972, sur
«&#x202F;Le langage de la ville&#x202F;». Mais présenter de l’art contemporain dans la
ville est nouveau. Il est probable que Jean %%Glibert, Jean|Glibert%% ait joué un rôle de
premier plan à ce niveau. Peintre et sculpteur, il a créé à %%École nationale supérieure d’architecture et des arts visuels de La Cambre (La Cambre)|La Cambre%% en
1975 l’Atelier d’espaces urbains et ruraux dans l’optique de mêler art
et architecture. Depuis le début des années 1970, il développe une
pratique artistique de l’espace où ce sont des rapports de couleurs qui
organisent la perception. En témoigne en 1975 sa composition monumentale
pour la station de métro %%Mérode%%. En 1977, il est invité à parler de son
travail à l’ISELP. Dans la foulée, il fait partie des artistes
sélectionnés par Gita <span class="book-index" data-book-index="Brys-Schatan, Gita">Brys-Schatan</span> pour *La Couleur et la ville*. Il&nbsp;propose d’intervenir sur la <span class="book-index" data-book-index="Bourse">Bourse</span>&#x202F;: en réponse à la présence de bâches
de nettoyage, il appose de grandes surfaces colorées sur les colonnes
ainsi que devant ou derrière celles-ci, créant une composition spatiale
monumentale.
{: .first}

Dans la sélection d’artistes pour l’exposition, on trouve aussi une
étudiante de %%Glibert, Jean|Glibert%% : Jacqueline %%Mesmaeker, Jacqueline|Mesmaeker%% a fait forte impression lors
de son jury à %%École nationale supérieure d’architecture et des arts visuels de La Cambre (La Cambre)|La Cambre%% avec *Les Oiseaux*, une projection d’images de
mouettes sur des écrans de soie. L’installation est présentée dans le
%%Egmont, Parc d’|parc d’Egmont%%. Elle sera programmée l’année suivante par Jan %%Hoet, Jan|Hoet%% dans
*Aktuele Kunst in België* au %%SMAK%%, lançant la carrière de l’artiste.

Par ailleurs, Gita <span class="book-index" data-book-index="Brys-Schatan, Gita">Brys-Schatan</span> choisit Serge %%Vandercam, Serge|Vandercam%%, qui propose une
sculpture d’arbre dans le parc Royal, et, fidèle à sa volonté de
décloisonner, le poète Maurice %%Maelderlick, Maurice|Maelderlick%% et le compositeur Alain
Pierre, qui proposent une composition-spectacle synesthésique.

<figure class="elem-float-top petit table-ronde"><img class="" alt="" src="contenu-final/012-visuels-texte-environnement/3.jpg" /><figcaption>
<p>Affiche de la table ronde «&#x202F;Urbanistes et pouvoirs de décision&#x202F;» du cycle <span class="book-index" data-book-index="Environnemental">Environnemental</span>, 23 avril 1981.</p>
</figcaption>
</figure>

Pour %%Jeunesse et Arts Plastiques (JAP)|JAP%%, Michel <span class="book-index" data-book-index="Baudson, Michel">Baudson</span> choisit %%De Gobert, Philippe|Philippe De Gobert%%, qui propose de
transformer la rue de la %%Régence (rue de la)|Régence%% en rue cultivable, %%Jano%%, qui rythme la
%%Galerie Ravenstein|galerie Ravenstein%% par une grande installation en papier, %%Mees, André|André Mees%%,
qui projette des images monumentales sur la %%Cité administrative%% et le
<span class="book-index" data-book-index="Botanique">Botanique</span>, %%Tout (Asbl)|TOUT%% (Paul %%Gonze, Paul|Gonze%%), qui installe un cadre vide de 6 x 3 mètres
au %%Mont des Arts%%, tandis que le groupe %%50/04 (collectif)|50/04%% intervient dans le hall du
%%Palais des Beaux-Arts (BOZAR)|palais des Beaux-Arts%%.


<span class="lettrine">E</span>n choisissant de jeunes artistes, Michel <span class="book-index" data-book-index="Baudson, Michel">Baudson</span> et Gita <span class="book-index" data-book-index="Brys-Schatan, Gita">Brys-Schatan</span>
privilégient une lecture poétique de la ville. Pour l’ISELP,
l’expérience est inédite. Elle marque suffisamment les esprits pour que
trois mois plus tard, en mars 1979, y soit organisée une table ronde
rassemblant architectes et plasticiens dans le but de faire dialoguer
leurs approches. Dans la foulée sont créés les %%Midis de l’art urbain%%,
qui visent à «&#x202F;sensibiliser architectes et plasticiens à un problème
qui leur [est] commun&#x202F;: la construction de tissus urbains sensibles et
viables&#x202F;»[^2].
{: .first}


La même année, l’Institut accueille une exposition du %%Goethe Institut%%
basée sur un livre de l’artiste écologiste allemand Dieter %%Magnus, Dieter|Magnus%%,
*Umweltkunst gegen Kunstumwelt&#x202F;: Fassaden, Plaẗze, Landschaften* [L’art
de l’environnement contre l’environnement artificiel&#x202F;: façades,
places, paysages]. %%Magnus, Dieter|Magnus%% est régulièrement impliqué dans des projets
urbanistiques&#x202F;; il milite pour une approche nouvelle des espaces
publics, au sein desquels l’art ne serait pas monumental, mais
participerait à rendre la ville plus humaine. Il mêle dans sa réflexion
les jeux, les éléments décoratifs, les passages et les places&#x202F;: «&#x202F;Les
perceptions à partir de la vue, de l’ouïe et du toucher sont des
conditions préalables à l’épanouissement de l’individu et à son
identification à l’environnement urbain.&#x202F;»[^3] Même si cette exposition
n’émane pas de l’Institut, elle se connecte directement aux
questionnements qui viennent d’y émerger. Et ce n’est probablement pas
un hasard si, dès 1981, quand ces réflexions se poursuivent de façon
concrète, elles portent désormais le label *%%Environnemental%%*.


<span class="lettrine">S</span>ous ce nom prennent place tout d’abord quatre débats organisés en 1981,
conçus pour rassembler artistes, acteurs de la ville et public&#x202F;:
«&#x202F;Peinture urbaine&#x202F;?&#x202F;»[^4], «&#x202F;Les concours et le 1%&#x202F;»[^5], «&#x202F;Urbanistes et
pouvoirs de décision&#x202F;»[^6] et «&#x202F;Fibre et fil et l’intégration des
arts&#x202F;»[^7]. Deux autres débats étaient programmés mais n’ont pas eu
lieu&#x202F;: «&#x202F;L’état de la question à l’étranger&#x202F;» et «&#x202F;Matériaux et miroirs&#x202F;».
{: .first}


Suite à ces tables rondes, un atelier de recherche constitué de
plasticiens se réunit régulièrement pour approfondir les réflexions et
dégager des pistes d’action. Elles aboutiront en 1983 au Cahier n°1
d’*%%Environnemental%%*[^8], qui contient le «&#x202F;Manifeste pour
l’%%Environnemental%%&#x202F;». Signé par six artistes - %%Metallic Avau|Metallic%% Avau, Jean
%%Glibert, Jean|Glibert%%, Paul %%Gonze, Paul|Gonze%%, Yves %%Rahir, Yves|Rahir%%, Félix %%Roulin, Félix|Roulin%% et %%Tapta%% - ainsi que par
Gita <span class="book-index" data-book-index="Brys-Schatan, Gita">Brys-Schatan</span>, le texte s’ouvre sur le droit au plaisir. L’espace de
vie devrait quotidiennement provoquer notre perception ; or, la ville a
perdu cette capacité. En tant que porteur du lien entre individu et
société, l’artiste est à même de prendre en charge la poétisation de
l’environnement. Le manifeste revendique qu’il soit reconnu au même
titre que l’architecte ou l’ingénieur, et associé aux projets urbains
dès les premières discussions. Pour nourrir la réflexion, un tableau
classe plusieurs centaines de mots clés liés à l’art %%Environnemental%%,
sous trois catégories&#x202F;: les individus et le milieu (individus,
communautés, institutions), le site et son contexte (géographie,
urbanisme, situation, environnement, facteurs d’ambiance), le projet et
ses contraintes (problème, stratégie, économie, technologie).

Ce Cahier n°1 propose aussi des recommandations pour une loi relative à
l’intégration d’œuvres d’art dans les constructions publiques. Elles
favorisent une mise à disposition pour l’art de 2% du budget des
constructions, et encouragent l’organisation de concours pour la
sélection des projets. Une loi similaire sera adoptée un an plus tard
par la Communauté française, connue sous le nom de «&#x202F;loi du 1%&#x202F;» (même si le
pourcentage varie selon les contraintes, de 0,25 à 2%). Plutôt qu’un
concours, elle opte pour une commission artistique. La %%Communauté flamande%% adoptera un décret en 1986.

Il convient enfin de relever la mise en page du cahier, qui fait courir
dans le bas de chaque page une frise riche de plusieurs dizaines
d’images d’architecture antique autant que contemporaine, de nature,
d’art extra-européen, actuel, illégal, enfantin, public… Les images
sont organisées entre elles par des liens formels ou thématiques
implicites. Cette abondante iconographie véhicule immédiatement la
portée multiple du manifeste.

<figure class="elem-float-top petit manifeste"><img alt="" class="" src="contenu-final/012-visuels-texte-environnement/5A-coupe-gauche-g.jpg" /><figcaption>
<<<<<<< HEAD
<p>(Pages 203, 204 et 205)<br/>«&#x202F;Manifeste pour l’<span class="book-index" data-book-index="Environnemental">Environnemental</span>&#x202F;», publié dans <em>Environnemental</em>, cahier n°1, 1983.</p>
=======
<p>(Pages 203, 204 et 205)<br/>«&#x202F;Manifeste pour l’<span class="book-index" data-book-index="Environnemental">Environnemental</span>&#x202F;», publié dans <em>Environnemental</em>, Cahier n°1, 1983.</p>
>>>>>>> b800e4ac7d2020fa03b474e3605ef7421cf03caa
</figcaption>
</figure>
<figure class="elem-float-top petit manifeste"><img alt="" class="" src="contenu-final/012-visuels-texte-environnement/5A-coupe-g.jpg" /><figcaption>
<p></p>
</figcaption>
</figure>
<figure class="elem-float-top petit manifeste"><img alt="" class="" src="contenu-final/012-visuels-texte-environnement/5B-coupe.jpg" /><figcaption>
<p></p>
</figcaption>
</figure>

<span class="lettrine">L</span>a parution du manifeste est le premier résultat des tables rondes de
1981. Il est suivi de la création d’un Atelier environnemental
expérimental, qui propose aux étudiants d’écoles d’art et d’architecture
d’élaborer des projets d’interventions. Une exposition des résultats est
prévue à l’ISELP, mais aucune trace n’en a été retrouvée. Par contre,
deux expositions s’inscrivent dans la continuité d’%%Environnemental%%&#x202F;: *Néon, Fluor et Cie* (1984) et *<span class="book-index" data-book-index="Bombeur fou">Le Bombeur fou</span>* (1985).
{: .first}

*Néon, Fluor et Cie* est probablement l’exposition la plus ambitieuse de
l’histoire de l’Institut. Tandis que dans le bâtiment prennent place les
œuvres de vingt et un artistes ou studios belges et internationaux issues de
collections publiques et privées, le %%Egmont, Parc d’|parc d’Egmont%% accueille les
créations inédites de douze artistes ou groupes belges. Présenté un
peu moins d’un mois à un horaire décalé permettant une vision nocturne,
l’ensemble de plus de cinquante pièces a nécessité l’intervention de néonistes
pour donner formes aux propositions des artistes. Pour l’occasion,
Fernand %%Flausch, Fernand|Flausch%% crée une enseigne lumineuse placée sur la façade de
l’Institut, <span class="book-index" data-book-index="Waterloo (boulevard de)">boulevard de Waterloo</span>, dont elle souligne les arcs.
L’enseigne restera en place jusqu’en 1997, lorsque le chantier du
bâtiment nécessitera de la décrocher.

Par son ambition et l’originalité de son sujet, l’exposition marque les
esprits. Même si elle a tout d’une exposition muséale plutôt que d’une
expérience urbaine, elle s’inscrit dans la réflexion de l’ISELP sur la
ville&#x202F;: lors de sa première table ronde, consacrée à la peinture
urbaine, un des questionnements concernait l’élargissement du médium à
des technologies nouvelles (néon, laser, projection). Le néon étant l’un
des éléments récurrents de l’esthétique des métropoles, Gita
<span class="book-index" data-book-index="Brys-Schatan, Gita">Brys-Schatan</span> souhaite attirer l’attention des pouvoirs publics sur cette
forme nouvelle et ouvrir des collaborations. Ainsi, l’Association belge
de l’enseigne et de la décoration lumineuse salue dans le catalogue
l’initiative, à laquelle elle est ravie de prêter son expertise.

*<span class="book-index" data-book-index="Bombeur fou">Le Bombeur fou</span>*, quelques mois plus tard, fait aussi suite à la table
ronde sur la peinture urbaine. Plus particulièrement à la question des
démarches spontanées dans l’espace public, auxquelles %%Metallic Avau|Metallic%% Avau,
signataire du manifeste de 1983 et pionnier du graffiti bruxellois, est
sensible. <span class="book-index" data-book-index="Bombeur fou">Le Bombeur fou</span> est le surnom d’un graffeur anonyme dont les
œuvres apparaissent dans la ville en 1984-1985. La pratique urbaine
illégale n’est pas encore répandue dans la capitale à l’époque, et ses
peintures, aisément identifiables, retiennent l’attention&#x202F;: des
illustrations humoristiques appliquées en une couleur à la bombe
aérosol, en un trait rapide. L’équipe de l’ISELP décide de faire un
appel à l’artiste par médias et affiches interposés&#x202F;: qui qu’il soit, il
est invité à peindre dans l’Institut, couvert de rouleaux de papier
kraft pour l’occasion. Le peintre, qui se révèle être un étudiant en
art, se prête au jeu&#x202F;: «&#x202F;J’ai vu les affiches de l’ISELP à l’école. Je
suis arrivé le jour du vernissage. Il y avait des tentures dans la
grande salle, et j’ai peint en live avec %%Metallic Avau|Metallic%% Avau.&#x202F;»[^11]

<figure class="elem-float-top petit graffiti"><img class="" alt="" src="contenu-final/012-visuels-texte-environnement/4-haut.JPG" /><figcaption>
<p>Affiche de la table ronde «&#x202F;Le graffiti&#x202F;: degré zéro de l’art&#x202F;?&#x202F;», 27 novembre 1985.</p>
</figcaption>
</figure>


<figure class="elem-float-top couverture-env"><img alt="" src="contenu-final/012-visuels-texte-environnement/6.jpg" /><figcaption>
<p>Couverture du premier numéro de la revue <em><span class="book-index" data-book-index="Environnemental">Environnemental</span></em>, octobre 1989.</p>
</figcaption>
</figure>


<span class="lettrine">P</span>eu à peu, <span class="book-index" data-book-index="Bruxelles">Bruxelles</span> voit se développer quelques initiatives d’art public. La Fondation %%Fondation Roi Baudouin|Roi Baudouin%% lance plusieurs études et actions en la matière, auxquelles l’ISELP collabore parfois. Citons le Concours des Quatre Saisons, qui encourage en 1987 différents projets liés aux plantations, au mobilier et à la peinture[^12]. Mais il faut attendre 1989 et la régionalisation de la capitale pour que les choses changent.
Cette année-là, l’ISELP lance la revue *%%Environnemental%%*. Celle-ci
connaîtra vingt numéros en dix ans, entre 1989 et 1999, répartis en 14
publications. Le premier numéro annonce un programme ambitieux hérité du
manifeste de 1983&#x202F;: informer les artistes, urbanistes, pouvoirs publics et promoteurs privés des démarches (inter)nationales en art public, des réflexions théoriques et des appels et actualités, dans l’optique de favoriser le travail en commun entre artistes, architectes et preneurs de décisions. Le manifeste initial y est publié, suivi de plusieurs entretiens et articles de fond, ainsi qu’une présentation des
différentes lois belges en la matière, et un appel à la nouvelle Région
bruxelloise à légiférer.
{: .first}

<span class="lettrine">L</span>es trois premiers numéros maintiennent un rythme trimestriel. Le
troisième (juin 1990), comprend une interview de Charles %%Picqué, Charles|Picqué%%,
ministre-Président de la Région de <span class="book-index" data-book-index="Bruxelles">Bruxelles</span>-Capitale, ainsi que de
%%Thys, Jean-Louis|Jean-Louis Thys%%, ministre des Travaux publics et des Communications. Ce
dernier y décrit le fonctionnement de la future Commission artistique
des infrastructures de déplacement (%%Commission artistique des infrastructures de déplacement (CAID)|CAID%%), qui a pour vocation de
conseiller le ministre en matière d’intégrations artistiques liées au
métro et aux principaux axes routiers. Cette même année 1990, la %%Commission artistique des Infrastructures de Déplacement (CAID)|CAID%%
est officiellement créée&#x202F;: parmi les quatorze membres qui la composent
prend place Gita <span class="book-index" data-book-index="Brys-Schatan, Gita">Brys-Schatan</span>. À partir de ce moment, *%%Environnemental%%*
joue un double rôle entre communication des démarches de la %%Commission artistique des Infrastructures de Déplacement (CAID)|CAID%% et
réflexion sur les enjeux de l’art public, auxquels la %%Commission artistique des Infrastructures de Déplacement (CAID)|commission%% devrait être
sensible. Le passage d’observateur à acteur participe à faire entrer
l’ISELP dans une période nouvelle de son histoire. Par le passé Gita
<span class="book-index" data-book-index="Brys-Schatan, Gita">Brys-Schatan</span> insistait sur l’indépendance de l’Institut ; désormais, son
rôle en art public associe plus étroitement l’ISELP aux pouvoirs
publics. Ce sera plus prégnant encore quand, en 1996, la directrice de
l’ISELP prendra en parallèle la présidence de la %%Commission artistique des Infrastructures de Déplacement (CAID)|CAID%%, renouvelée après
une année d’interruption de la commission.
{: .first}

<figure class="elem-float-top"><img alt="" src="contenu-final/012-visuels-texte-environnement/7A©JJ-SEROL2114.JPG" /><figcaption>
<p>Vue de l’installation <em>La Vie errante – la vie immobile – la prison</em> de Tatiana <span class="book-index" data-book-index="Bohm, Tatiana">Bohm</span> dans le <span class="book-index" data-book-index="Egmont, Parc d’">parc d’Egmont</span>, 2012. Photo&#x202F;: JJ SEROL</p>
</figcaption>
</figure>
<figure class="elem-float-top"><img alt="" src="contenu-final/012-visuels-texte-environnement/8©jjserol_1429.jpg" /><figcaption><p>Vue de l’installation <em>Inside / OUT</em> de <span class="book-index" data-book-index="Chenut, Vincent">Vincent Chenut</span> et Alberto <span class="book-index" data-book-index="Ginepro, Alberto">Ginepro</span> dans le parc d'Egmont, 2013. Photo&#x202F;: JJ SEROL</p>
</figcaption>
</figure>

<figure class="elem-float-top"><img alt="" src="contenu-final/012-visuels-texte-environnement/10Colombi.jpg" />
<figcaption>
<p><span class="book-index" data-book-index="Colombi, Chiara">Chiara Colombi</span>, <em>Monument à la jeunesse</em>, campus de l’Institut <span class="book-index" data-book-index="Institut Émile Gryson">Émile Gryson</span>, 2017. Photo&#x202F;: ISELP, Antoine <span class="book-index" data-book-index="Debeauvais, Antoine">Debeauvais</span></p>
</figcaption>
</figure>

<span class="lettrine">E</span>n deux décennies de fonctionnement, la %%Commission artistique des Infrastructures de Déplacement (CAID)|CAID%% placera une trentaine
d’œuvres, principalement dans les stations de métro et prémétro. La
participation de Gita <span class="book-index" data-book-index="Brys-Schatan, Gita">Brys-Schatan</span> aux réflexions, à l’appui des
connaissances accumulées depuis plus de dix ans et que la revue continue
d’engranger, permet à la directrice de l’ISELP de mettre à l’épreuve les
préconisations du manifeste de 1983. La plupart des œuvres sélectionnées
répondent à une analyse du contexte propre à la démarche revendiquée par
*%%Environnemental%%*. Par contre, force est de constater que le
fonctionnement de la commission, limitée à un rôle consultatif et non
directement impliquée dans les aménagements des infrastructures, rend
particulièrement difficile un dialogue entre artistes et architectes, lequel prendra rarement forme. Par ailleurs, la complexité des lieux
impartis dirige la commission vers des techniques relativement
classiques (sculptures et revêtements muraux) plutôt que vers des
nouveaux médias tel que les tables rondes de 1981 le préconisaient.
L’expérience de la %%Commission artistique des Infrastructures de Déplacement (CAID)|CAID%% permet donc à l’ISELP de s’investir directement
dans les questions qui préoccupent l’Institut depuis une décennie,
confrontant ses recommandations à une réalité de terrain qui s’avère
difficile.
{: .first}


Notons que les membres de la %%Commission artistique des Infrastructures de Déplacement (CAID)|CAID%% font régulièrement savoir qu’ils
aimeraient proposer une meilleure médiation des œuvres aux usagers mais
leur mission ne leur permet pas de mener cet objectif à bien.

<span class="lettrine">E</span>n parallèle à ce travail de terrain, les publications
d’*%%Environnemental%%* continuent de développer à la fois une réflexion sur
l’art public, et un réseau d’acteurs de cette discipline. Trois numéros
plus conséquents méritent d’être évoqués.
{: .first}

En 1994 paraît «&#x202F;L’art urbain dans l’%%Europe%% des Douze&#x202F;» (numéros 6 à 9 de
la revue). Cette ambitieuse compilation rassemble des entretiens avec
une sélection d’experts et d’artistes des pays qui composent l’%%Europe%%, à
propos des enjeux de l’art public, de son fonctionnement, sa médiation,
et son avenir. Parmi les plasticiens, on peut lire les témoignages de
Stephan Balkenhol, %%Chillida, Eduardo|Eduardo Chillida%%, Luciano %%Fabro, Luciano|Fabro%%, Per %%Kirkeby, Per|Kirkeby%%, Ernest
%%Pignon-Ernest, Ernest|Pignon-Ernest%% ou Bernar %%Venet, Bernard|Venet%%. Le livre sera complété quelques mois plus
tard d’un tome II (n° 12 et 13) consacré aux nouveaux membres de
l’%%Europe%% (%%Norvège%%, %%Autriche%%, %%Finlande%%, %%Suède%%). Ce n’est certainement pas
un hasard si la parution de ces volumes a lieu en parallèle avec le
développement du %%Quartier européen%%, qui a un impact majeur sur
l’urbanisme bruxellois. La %%Commission artistique des Infrastructures de Déplacement (CAID)|CAID%% réfléchit à l’époque à une
intervention qui prendrait place sur le %%Rond-point Schuman|rond-point Schuman%%. Elle a
sélectionné quatre artistes qui remettent une proposition&#x202F;: Dani
%%Karavan, Dani|Karavan%%, Per %%Kirkeby, Per|Kirkeby%%, Aldo %%Rossi, Aldo|Rossi%% et Ulrich %%Rückriem, Ulrich|Rückriem%%. Le projet ne verra
pas le jour.

En 1996, un nouveau volume exceptionnel est consacré au «&#x202F;Répertoire
illustré de l’art %%Environnemental%%&#x202F;»&#x202F;: près de 200 fiches d’artistes y
sont rassemblées, reprenant biographie, choix d’œuvres et coordonnées,
faisant du livre un véritable outil professionnel, avant la
généralisation d’Internet.


Enfin, en 1999 sont publiés les deux derniers numéros de la revue, de
façon complémentaire. Le premier propose un guide de l’art public
bruxellois&#x202F;: il rassemble 81 œuvres postérieures à 1990, émanant de la
%%Commission artistique des Infrastructures de Déplacement (CAID)|CAID%%, des communes, des commissions communautaires ou de structures
privées, témoignant de la complexité du territoire institutionnel de la
ville. Le second expose un choix de 67 projets non réalisés dans la
capitale. Il offre un parcours alternatif dans la ville, et permet aussi
à Gita <span class="book-index" data-book-index="Brys-Schatan, Gita">Brys-Schatan</span> de témoigner de la difficulté et des frustrations
que la %%Commission artistique des Infrastructures de Déplacement (CAID)|CAID%% ressent dans ses missions. La publication, accompagnée d’une
exposition organisée dans les nouvelles salles de l’ISELP agrandi, est
pensée comme un coup de projecteur sur des projets qui pourraient
toujours voir le jour dans la ville. Le bâtiment fraîchement inauguré se
veut lui-même exemplaire&#x202F;: les anciennes écuries accueillent désormais
une intervention pérenne de Joseph %%Kosuth, Joseph|Kosuth%%.



<span class="lettrine">A</span>vec la publication de ce dernier volume d’*%%Environnemental%%* s’opère un
passage dans un nouveau chapitre de l’histoire de l’ISELP. Avec le
changement de direction en 2002 et le passage de flambeau de Gita
<span class="book-index" data-book-index="Brys-Schatan, Gita">Brys-Schatan</span>, l’Institut n’a désormais plus de liens avec la %%Commission artistique des Infrastructures de Déplacement (CAID)|CAID%%, et la
nouvelle équipe passe du rôle d’activateur à celui d’observateur. Grâce
à l’important centre de documentation lié à %%Environnemental%%, l’ISELP est
consulté pour deux publications inventaires. En 2004, l’%%Institut Bruxellois de l’Environnement (IBGE)/Bruxelles Environnement|IBGE%%[^13]
commande un *Parcours de sculptures dans les espaces verts de
<span class="book-index" data-book-index="Bruxelles">Bruxelles</span>*, qui témoigne du rôle actif de l’%%Institut Bruxellois de l’Environnement (IBGE)/Bruxelles Environnement|IBGE%% en la matière. En 2011
paraît l’*Inventaire des intégrations d’œuvres d’art 1986-2010 en
application du Décret de la Communauté française*. L’ouvrage, commandé
par la Communauté française (aujourd’hui Fédération Wallonie-<span class="book-index" data-book-index="Bruxelles">Bruxelles</span>),
se veut un examen des vingt-cinq premières années d’application du décret du 1%
pour lequel %%Environnemental%% s’était battu au début des années 1980.
L’enquête est dirigée par Anne-Esther %%Henao, Anne-Esther|Henao%%, qui a rejoint quelques
années plus tôt l’équipe de l’ISELP en tant que responsable de l’art
public. Outre l’examen de 45 œuvres *in situ*, 14 projets en cours et 21
projets abandonnés, elle propose une mise en contexte de l’évolution de
l’art public %%Belgique|en Belgique%% ainsi qu’une série de constats sur des
améliorations possibles. La plupart de ces observations sont déjà bien
connues&#x202F;: un dialogue étroit entre architecte et artiste, un bon
accompagnement de l’artiste dans son projet, une médiation accrue auprès
du public de l’œuvre, une attention aux nouvelles technologies. Cette
continuité montre la difficulté de faire évoluer les pratiques publiques
quant à la fonction de l’art.
{: .first}

Outre cette analyse critique, un nouveau questionnement se fait jour&#x202F;:
celui de la pérennité des œuvres. Faut-il encore penser les œuvres dans
une durée longue&#x202F;? Et faut-il systématiquement les intégrer à
l’architecture&#x202F;? Ce questionnement est le fruit d’une évolution de l’art
public autour des années 2000. Dans la capitale, l’événement <span class="book-index" data-book-index="Bruxelles 2000">Bruxelles 2000</span>
 a été le déclencheur de ce changement de paradigme, qui ne lierait
plus intimement l’art aux espaces, mais plutôt au public, et au temps.


<span class="lettrine">L’</span>analyse opérée par Anne-Esther %%Henao, Anne-Esther|Henao%% accompagne un regain d’intérêt de
l’ISELP pour l’art public. Les cinq années qui suivent verront plusieurs
projets se développer sous son impulsion.
{: .first}

C’est au même moment que je commence à travailler à l’Institut. J’y suis
d’abord conférencier extérieur. Mes recherches de l’époque sur
l’histoire du graffiti bruxellois (auquel je consacre un cours à
l’Institut et différentes visites guidées) recoupent les préoccupations
urbaines. J’ai ensuite l’occasion de rejoindre l’équipe et d’assister
Anne-Esther %%Henao, Anne-Esther|Henao%% dans le redéploiement de la recherche&#x202F;: son souhait
est de faire de l’ISELP l’observatoire des nouvelles tendances, celles
qui privilégient l’échange et la construction collective à l’œuvre
intégrée.

Le %%Egmont, Parc d’|parc d’Egmont%% sera l’un des lieux de cette expérimentation. À
l’occasion des 40 ans de l’Institut en 2011, une partie de l’exposition
*Célébration* y prend place, avec des œuvres d’Olivier <span class="book-index" data-book-index="Bovy, Olivier">Bovy</span>, Marion
%%Fabien, Marion|Fabien%%, Christophe %%Terlinden, Christophe|Terlinden%% et Bert %%Theis, Bert|Theis%%. En parallèle, des rencontres
sont organisées avec les artistes Jochen %%Gerz, Jochen|Gerz%%, Cécile %%Massart, Cécile|Massart%% ou Bert
%%Theis, Bert|Theis%%. Par la suite, des interventions seront régulièrement présentées
dans le parc, dues à de jeunes plasticiens tels que Tatiana <span class="book-index" data-book-index="Bohm, Tatiana">Bohm</span>, %%Caplet, Gaëlle|Gaëlle Caplet%%, %%Chenut, Vincent|Vincent Chenut%%, Alberto %%Ginepro, Alberto|Ginepro%% et Julien %%Khanh Vong, Julien|Khanh Vong%%.

En parallèle, les salles des anciennes écuries accueillent des artistes
dont le travail est largement lié à l’espace urbain. Deux expositions
monographiques en témoigneront en 2014&#x202F;: *<span class="book-index" data-book-index="Bonom">Bonom</span>, le singe boiteux*, et
*Christophe %%Terlinden, Christophe|Terlinden%%, No Sugar*. Toutes deux explorent sous formes
documentaires ou d’interventions plastiques les recherches de deux
artistes qui ont régulièrement œuvré dans <span class="book-index" data-book-index="Bruxelles">Bruxelles</span> dans des esthétiques
radicalement différentes.

Il me paraît opportun de pointer aussi deux projets qui croisent
art et médiation, soit des propositions qui se formulent avant tout en
fonction d’un public spécifique plutôt que d’un espace. Le premier est
mis sur pied avec une classe de 6<sup>e</sup> secondaire de l’Institut %%Institut Saint-Louis|Saint-Louis%%&#x202F;:
suite à des discussions avec les élèves, les historiens de l’art de
l’ISELP invitent l’artiste Jean-François %%Octave, Jean-François|Octave%%  à créer une œuvre avec
eux. Le projet final, inauguré plusieurs années après, en 2014, mêle une
installation murale dans la cour de récréation à des projections vidéo
dans les couloirs de l’école. En
2017, l’artiste %%Colombi, Chiara|Chiara Colombi%% collabore avec les élèves de 5<sup>e</sup>
secondaire de l’Institut %%Institut Émile Gryson|Émile Gryson%% à la création d’un *Monument à la
jeunesse*, placé sur le campus.

Ces changements témoignent de la perméabilité nouvelle des enjeux de
l’art dans l’espace public. La question de la visibilité (et de la
pérennité) des œuvres trouve d’autres réponses, tandis que la
co-construction avec les participants au projet devient plus prégnante.
<figure class="elem-float-top"><img alt="" src="contenu-final/012-visuels-texte-environnement/9A.jpg" /><figcaption>
<p>Une des discussions menées dans le <em>Parlamento Flor</em> conçu par <span class="book-index" data-book-index="Zuloark">Zuloark</span> pour le laboratoire <span class="book-index" data-book-index="Being Urban"><em>Being Urban</em></span>, 2015. Photo&#x202F;: <span class="book-index" data-book-index="Destrument, Lionel">Lionel Destrument</span>. CC BY-SA 4.0</p>
</figcaption>
</figure>


<figure class="elem-float-top"><img alt="" src="contenu-final/012-visuels-texte-environnement/11.jpg" /><figcaption>
<p>Œuvre de Thierry <span class="book-index" data-book-index="Verbeke, Thierry">Verbeke</span> conçue pour le site <span class="book-index" data-book-index="RECY-K">RECY-K</span> de l’agence <span class="book-index" data-book-index="Bruxelles-Propreté">Bruxelles-Propreté</span>, installée en 2020. Photo&#x202F;: <span class="book-index" data-book-index="Nicolas Lambillon">Nicolas Lambillon</span></p>
</figcaption>
</figure>


<span class="lettrine">L</span>e projet *Being Urban* synthétise cette réflexion nouvelle. Il prend
place à l’ISELP en 2015 en tant qu’exposition, et sous forme de
publication l’année suivante. L’historienne et curatrice Pauline de La
%%La Boulaye, Pauline de|Boulaye%% et moi-même envisageons le projet comme un laboratoire&#x202F;: un
forum de rencontres entre les acteurs de l’art public bruxellois, le
public, et les artistes. Des débats sont organisés dans les salles tout
au long du projet, ainsi que des visites sur le terrain. Notre objectif
est de déplier quinze ans d’art public, depuis <span class="book-index" data-book-index="Bruxelles 2000">Bruxelles 2000</span>, pour
stimuler ces pratiques nouvelles dans les années à venir. Sept projets
sont présentés qui permettent une rencontre particulière entre artistes
et habitants&#x202F;: %%Celdran, Julien|Julien Celdran%% customise deux charrettes de la section
14B de <span class="book-index" data-book-index="Bruxelles">Bruxelles</span>-Propreté, %%Denicolai & Provoost%% négocient pour
l’installation d’une armoire en co-propriété dans l’espace public
(contrairement aux étapes françaises, l’installation à <span class="book-index" data-book-index="Bruxelles">Bruxelles</span> avortera
dans le contexte des attentats de l’époque), Xurxo %%Duran Sineiro, Xurxo|Duran Sineiro%% propose
des ventes aux enchères spontanées, %%Giller, Jérôme|Jérôme Giller%% organise une marche
collective sur le périmètre de la Région, Stephan %%Goldrajch, Stephan|Goldrajch%% fait
l’expérience de la figure du bouc émissaire qu’il place dans différents
endroits de la ville, Thomas %%Laureyssens, Thomas|Laureyssens%% organise un réseau ludique
d’oiseaux lumineux à activer par le sifflement, et The Mental %%Mental Masonry Lab (The)|Masonry%%
Lab propose une radio numérique à partir du centre de documentation en
art public.
{: .first}


<span class="lettrine">L</span>e projet *Being Urban* renoue, sans que nous en soyons conscients à
l’époque, avec les tables rondes d’%%Environnemental%%, en ce qu’il valorise
les expériences de terrain et encourage une pratique nouvelle. Les
nombreux échanges nous ont permis de publier *Being Urban. Pour l’art
dans la ville, <span class="book-index" data-book-index="Bruxelles">Bruxelles</span>*[^14], qui reprend l’histoire de l’art public
à partir de 2000, exactement là où s’arrêtait la revue *%%Environnemental%%*. Heureux
hasard. L’ouvrage prend la relève et témoigne à son tour de la nécessité
de médiation pour l’art public. Si on le compare aux premières
expériences des années 1970/1980, de *La Couleur et la ville* au manifeste
de 1983, on réalise comment <span class="book-index" data-book-index="Bruxelles">Bruxelles</span> a multiplié les initiatives,
spécialement au tournant du millénaire. L’art public se mue
progressivement du monumental à l’interstitiel, du pérenne au
temporaire, de l’espace au temps, de la mémoire collective à la
rencontre.
{: .first}

Depuis lors, l’Institut continue son action dans l’espace public.
L’équipe intervient régulièrement dans les discussions au sujet
d’interventions liées aux différents acteurs de l’urbanisme (communaux,
régionaux…). Depuis 2016, l’ISELP accompagne l’agence
%%Bruxelles%%-%%Propreté (agence)|Propreté%%</span> dans le choix de trois artistes appelés à intervenir
sur le site de %%Recy-K%%, qui regroupe diverses initiatives de recyclage à
Anderlecht. Joachim %%Coucke, Joachim|Coucke%%, Adrien 
%%Tirtiaux, Adrien|Tirtiaux%% et Thierry %%Verbeke, Thierry|Verbeke%%
poursuivent actuellement leur travail étroitement lié au contexte du
site.

<span class="lettrine">A</span>u moment d’écrire ces lignes, nous expérimentons un nouveau projet. À l’occasion des cinquante ans de l’Institut, nous avons eu l’occasion
d’en repenser l’accueil. En concertation avec la cellule Architecture de
la Fédération Wallonie-<span class="book-index" data-book-index="Bruxelles">Bruxelles</span>, et nourris de ces décennies de réflexion, nous avons décidé de faire un appel restreint à des équipes
composées d’architectes, d’artistes et de graphistes. L’équipe qui a
emporté le projet est composée autour des artistes %%Denicolai & Provoost%%,
qui se sont entourés des architectes de %%Nord%% et des graphistes d’%%Open Source Publishing asbl (OSP)|OSP%%.
Leur projet insiste sur l’imbrication complète des trois entités, au
point de rendre indissociable leur apport respectif. Dès que le nouvel
accueil sera inauguré, il sera bien difficile de pointer une œuvre d’art
dans l’aménagement de l’espace, tant la réflexion artistique est au cœur
du propos, indissociable de sa forme finale. En cela, nous répondons aux
souhaits émis dès les débuts d’%%Environnemental%%. Par ailleurs, ce projet
s’élabore en discussion avec l’équipe de l’ISELP, et ces mois d’échanges
montrent la complexité de la construction collective. Riche de cette
expérience, je me pose des questions nouvelles sur l’autonomie de
l’œuvre d’art et la nécessité de la co-construction, qu’il me faudra du
temps pour assimiler. La question est complexe, les réponses
certainement multiples, et surtout évolutives.
{: .first}

[^1]: %%Jeunesse et Arts Plastiques (JAP)|Jeunesse et%% Arts Plastiques est une ASBL créée en 1959 pour
    développer l’initiation à l’art moderne et contemporain.

[^2]: Affiche de présentation de %%Environnemental%%, 1981 [archives de
    l’ISELP].

[^3]: Dieter %%Magnus, Dieter|Magnus%%, «&#x202F;Les processus urbains et la fonction de l’art »,
    in *Dieter %%Magnus, Dieter|Magnus%%. Umweltkunst gegen Kunstumwelt*, Munich,
    Goethe-Institut, 1979, p.35.

[^4]: Les participants sont <span class="book-index" data-book-index="Baucher, Lucien">Lucien Baucher</span>, Laurent <span class="book-index" data-book-index="Busine, Laurent">Busine</span>, Michel
    %%Ceder, Michel|Ceder%%, François %%De Cugnac, François|De Cugnac%%, Paul %%De Gobert, Paul|De Gobert%%, Jean %%Glibert, Jean|Glibert%%, Paul %%Gonze, Paul|Gonze%%,
    %%Metallic Avau|Metallic%% Avau, Luc %%Van Malderen, Luc|Van Malderen%%, %%Dubois, Géraldine|Dubois%%, Arthur|Arthur %%Dubois, Géraldine|Dubois%%%% (échevin de la
    culture à %%Quaregnon%%) et Gita <span class="book-index" data-book-index="Brys-Schatan, Gita">Brys-Schatan</span>. Organisé le 22 janvier.

[^5]: Les participants sont <span class="book-index" data-book-index="Baucher, Lucien">Lucien Baucher</span>, Charles %%Derouck, Charles|Derouck%%, Jean-Claude
    %%Dessart, Jean-Claude|Dessart%%, Jean-Paul %%Laenen, Jean-Paul|Laenen%%, %%Léonard, René|René Leonard%%, Roger %%Somville, Roger|Somville%%, Yves %%Rahir, Yves|Rahir%%,
    %%Vienne, Michel|Michel Vienne%%, Paul %%Zimmer, Paul|Zimmer%% et Gita <span class="book-index" data-book-index="Brys-Schatan, Gita">Brys-Schatan</span>. Organisé le 19
    février.

[^6]: Les participants sont Jacques %%Aron, Jacques|Aron%%, Jean-Pierre <span class="book-index" data-book-index="Blondel, Jean-Pierre">Blondel</span>, Vincent
    %%Carton de Tournai, Vincent|Carton de Tournai%%, Etienne %%De Rouck, Etienne|De Rouck%%, Jean %%De Salle, Jean|De Salle%%, Claude %%Javeau, Claude|Javeau%%,
    Lucien %%Kroll, Lucien|Kroll%%, Pierre %%Laconte, Pierre|Laconte%%, Serge %%Moureaux, Serge|Moureaux%%, JL %%Kaulen, JL|Kaulen%%, Félix
    %%Roulin, Félix|Roulin%%, %%Thiry, Jean-Pierre|Jean-Pierre Thiry%%, Edmond %%Radar, Edmond|Radar%% et Gita <span class="book-index" data-book-index="Brys-Schatan, Gita">Brys-Schatan</span>.
    Organisé le 23 avril.

[^7]: Les participants sont <span class="book-index" data-book-index="Bastin, Roger">Roger Bastin</span>, Yves <span class="book-index" data-book-index="Bical, Yves">Bical</span>, Jacques %%De Maet, Jacques|De Maet%%,
    Jacqueline %%Fabbri-Daure, Jacqueline|Fabbri-Daure%%, Jean-Paul %%Jourdan, Jean-Paul|Jourdan%%, %%Léonard, René|René Leonard%%, Maurice
    %%Vandevelde, Maurice|Vandevelde%%, Serge %%Vandercam, Serge|Vandercam%%, Christian %%Varese, Christian|Varese%%, %%Tapta%% et Gita
    <span class="book-index" data-book-index="Brys-Schatan, Gita">Brys-Schatan</span>. Organisé le 14 mai.

[^8]: Qui ne sera pas suivi d’autres cahiers.

[^11]: <span class="book-index" data-book-index="Bombeur fou">Le Bombeur fou</span>, cité in Adrien %%Grimmeau, Adrien|Grimmeau%%, *Dehors ! Le graffiti à
    <span class="book-index" data-book-index="Bruxelles">Bruxelles</span>*, <span class="book-index" data-book-index="Bruxelles">Bruxelles</span>, CFC Éditions, 2011, p.34.

[^12]: Voir *Ville colorée, espace meublé*, <span class="book-index" data-book-index="Bruxelles">Bruxelles</span>, Secrétariat
    d’État à la %%Région de Bruxelles-Capitale/Région bruxelloise|Région bruxelloise%%, 1987.

[^13]: %%Institut Bruxellois de l’Environnement (IBGE)/Bruxelles Environnement|Institut bruxellois%% pour la gestion de l'environnement, l’actuel
    <span class="book-index" data-book-index="Bruxelles">Bruxelles</span> Environnement.

[^14]: Pauline de La %%La Boulaye, Pauline de|Boulaye%% et Adrien %%Grimmeau, Adrien|Grimmeau%% (dir.), *Being Urban.
    Pour l’art dans la ville, <span class="book-index" data-book-index="Bruxelles">Bruxelles</span>*, <span class="book-index" data-book-index="Bruxelles">Bruxelles</span>, ISELP/CFC-Éditions, 2016.

<!-- 
<div id="book-index"></div> -->
