<div id="my-toc-content" style="display:none;"></div>
 
 
Membres de l'équipe de l’ISELP depuis 1971
{: .cartouche-titre}

Directeurs et directrices
:   %%Brys-Schatan, Gita|Gita Brys-Schatan%% (1971-2002), 
:   %%Lemonnier, Arlette|Arlette Lemonnier%% (2002-2010), 
:   Éric Van Essche (Directeur scientifique : 2002-2010, Directeur général&#x202F;: 2010-2015), 
:   %%Vissault, Maïté|Maïté Vissault%% (2016-2018), 
:   Adrien Grimmeau (2018 jusqu’à aujourd’hui)



Employés
:   %%Ackermans, Christiane|Christiane&nbsp;Ackermans%%
:   %%Ackermans, Christophe|Christophe&nbsp;Ackermans%%
:   %%Anciaux, Myriam|Myriam&nbsp;Anciaux%%
:   %%Arese, Pierre|Pierre&nbsp;Arese%%
:   %%Baix, Alain|Alain&nbsp;Baix%%
:   %%Bandin, Marion|Marion&nbsp;Bandin%%
:   %%Baquet, Hélène|Hélène&nbsp;Baquet%%
:   %%Bivier, Évelyne|Évelyne&nbsp;Bivier%%
:   %%Boulares, Sami|Sami&nbsp;Boulares%%
:   %%Bouyaux, Bernadette|Bernadette&nbsp;Bouyaux%%
:   %%Castaigne, Joël|Joël&nbsp;Castaigne%%
:   %%Charpentier, Guy|Guy&nbsp;Charpentier%%
:   %%Cheval, Florence|Florence&nbsp;Cheval%%
:   %%Cicilloni, Silvana|Silvana&nbsp;Cicilloni%%
:   %%Collette, Sophie|Sophie&nbsp;Collette%%
:   %%Coppée, Marilyne|Marilyne&nbsp;Coppée%%
:   %%Courtens, Laurent|Laurent&nbsp;Courtens%%
:   %%Dauchot, Alain|Alain&nbsp;Dauchot%%
:   %%Debeauvais, Antoine|Antoine Debeauvais%%
:   %%Debod, Grégory|Grégory&nbsp;Debod%%
:   %%Defosse, Daphné|Daphné&nbsp;Defosse%%
:   %%La Boulaye, Pauline de|Pauline&nbsp;de&nbsp;La&nbsp;Boulaye%%
:   %%De Mesmaeker, Solange|Solange&nbsp;De&nbsp;Mesmaeke%%
:   %%Detandt, Corinne|Corinne&nbsp;Detandt%%
:   %%De Vrieze, Murielle|Murielle&nbsp;De&nbsp;Vrieze%%
:   %%D’hond, Anna|Anna&nbsp;D’hond%%
:   %%Dior, Jean-François|Jean&nbsp;François&nbsp;Dior%%
:   %%Dubois, Géraldine|Géraldine&nbsp;Dubois%%
:   %%Dupont, Olivier|Olivier&nbsp;Dupont%%
:   %%Géronnez, Alain|Alain&nbsp;Géronnez%%
:   %%Godefroid, Jean-Louis|Jean-Louis&nbsp;Godefroid%%
:   %%Godts, Sophie|Sophie&nbsp;Godts%%
:   %%Grimmeau, Adrien|Adrien&nbsp;Grimmeau%%
:   %%Hanon, Frédéric|Frédéric&nbsp;Hanon%%
:   %%Hatzigeorgiou, Pauline|Pauline&nbsp;Hatzigeorgiou%%
:   %%Henao, Anne-Esther|Anne-Esther&nbsp;Henao%%
:   %%Henkinet, Catherine|Catherine&nbsp;Henkinet%%
:   %%Van Hoa, Pham|Pham&nbsp;Van&nbsp;Hoa%%
:   %%Illah Bendahman, Abdel|Abdel&nbsp;Illah&nbsp;Bendahman%%
:   %%Lenouvel, Sébastien|Sébastien&nbsp;Lenouvel%%
:   %%Leroy, Kim|Kim&nbsp;Leroy%%
:   %%Lurquin, Christine|Christine&nbsp;Lurquin%%
:   %%Marchal, Géraldine|Géraldine&nbsp;Marchal%%
:   %%Frédérique|Frédérique&nbsp;Margraff%%
:   %%Monniez, Valérie|Valérie&nbsp;Monniez%%
:   %%Muylaert, Léo|Léo&nbsp;Muylaert%%
:   %%Nerincx, Olivia|Olivia&nbsp;Nerincx%%
:   %%Penailillo, Raul|Raul&nbsp;Penailillo%%
:   %%Peremans, Françoise|Françoise&nbsp;Peremans%%
:   %%Piérart, Béatrice|Béatrice&nbsp;Piérart%%
:   %%Piron, Pascale|Pascale&nbsp;Piron%%
:   %%Rainville, Mélanie|Mélanie&nbsp;Rainville%%
:   %%Randolet Violaine|Violaine&nbsp;Randolet%%
:   %%Rigaut, Aurélie|Aurélie&nbsp;Rigaut%%
:   %%Romanelli, Fabrizio|Fabrizio&nbsp;Romanelli%%
:   %%Rulens, Jean|Jean&nbsp;Rulens&nbsp;(dit Rosier)%%
:   %%Salembier, Maud|Maud&nbsp;Salembier%%
:   %%Schuyten, Monique|Monique&nbsp;Schuyten%%
:   %%Sanchez, Ivan Suntaxi|Ivan&nbsp;Suntax&nbsp;Sanchez%%
:   %%Santocono, Adèle|Adèle&nbsp;antocono%%
:   %%Stanescu, Michèle|Michèle&nbsp;Stanescu%%
:   %%Tiberghien, Septembre|Septembre&nbsp;Tiberghien%%
:   %%Vandevelde, Hélène|Hélène&nbsp;Vandevelde%%
:   %%Van Essche, Éric|Éric&nbsp;Van&nbsp;Essche%%
:   %%Vidal, Nicole|Nicole&nbsp;Vidal%%
:   %%Wanet, Sophia|Sophia&nbsp;Wanet%%
:   %%Xhafa, Bardul|Bardul&nbsp;Xhafa%%
:   %%Yerlès, Pierre-Paul|Pierre-Paul&nbsp;Yerlès%%
:   %%Ysabeaux, Terry|Terry&nbsp;Ysabeaux%% 
:   <span class="logos">e</span>

<div id="membres-ca" markdown=1>

Membres du Conseil d’administration depuis 1971 
{: .cartouche-titre } 
 
Présidents
:   %%Urvater, Joseph-Berthold|Joseph-Berthold Urvater (1970-1971)%%
:   %%Delevoy, Robert L.|Robert L. Delevoy (1972-1980)%%
:   %%Prigogine, Ilya|Ilya Prigogine (1980-2003)%%
:   %%Van der Stichele, Michel|Michel Van der Stichele%% (2003 à aujourd’hui)

Administrateurs et administratrices
:   %%Argan, Giulio Carlo|Giulio Carlo Argan%%
:   %%Berger, René|René Berger%%
:   %%Berlanger, Marcel|Marcel Berlanger%%
:   %%Borel, France|France Borel%%
:   %%Bussers, Marcel|Marcel Bussers%%
:   %%Brys-Schatan, Gita|Gita Brys-Schatan%%
:   %%Chaponan-Garcia, Anne|Anne Chaponan-Garcia%%
:   %%Coquelet, Jean|Jean Coquelet%%
:   %%Culot, Maurice|Maurice Culot%%
:   %%Daled, Herman|Herman Daled%%
:   %%Debouverie, Patrick|Patrick Debouverie%%
:   %%De Lulle, Francis|Francis De Lulle%%
:   %%Delevoy, Robert L.|Robert L. Delevoy%%
:   %%Delvaux, André|André Delvaux%%
:   %%Dewit, Jacques|Jacques Dewit%%
:   %%Dumont, Georges-Henri|Georges-Henri Dumont%%
:   %%Goffart, Albert|Albert Goffart%%
:   %%Grosjean, Catherine|Catherine Grosjean%%
:   %%Herman, Ariane|Ariane Herman%%
:   %%d’Huart, Nicole d’|Nicole d’Huart%%
:   %%Jaffé, Hans L. C.|Hans L. C. Jaffé%%
:   %%Jakou, Raoul|Raoul Jakou%%
:   %%Jenard, Laurence|Laurence Jenard%%
:   %%Kao, Wen Li|Wen Li Kao%%
:   %%La Haye, Martine|Martine La Haye%%
:   %%Lassaigne, Jacques|Jacques Lassaigne%%
:   %%Lasserre, Christian|Christian Lasserre%%
:   %%Lefébure, Jean|Jean Lefébure%%
:   %%Legrand, Luc|Luc Legrand%%
:   %%Hodey, Philippe le|Philippe le Hodey%%
:   %%Lemesre, Marion|Marion Lemesre%%
:   %%Léonard, René|René Léonard%%
:   %%Liebaers, Herman|Herman Liebaers%%
:   %%Mesot, Jean-Émile|Jean-Émile Mesot%%
:   %%Muyle, Johan|Johan Muyle%%
:   %%Pierlet, Robert|Robert Pierlet%%
:   %%Poupko, Jean-Pierre|Jean-Pierre Poupko%%
:   %%Remiche, Jean|Jean Remiche%%
:   %%Van Hout, Georges|Georges Van Hout%%
:   %%Van Roye, Michel|Michel Van Roye%%
:   %%Hassel-Szternfeld, Barbara|Barbara Hassel-Szternfeld%%
:   %%Urvater, Joseph-Berthold|Joseph-Berthold Urvater%%
:   %%Verschueren, Bob|Bob Verschueren%%
:   %%Warche, Robert|Robert Warche%%  
<span class="logos">f</span>


</div>
