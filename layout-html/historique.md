 <div id="my-toc-content"></div>

  
# Entre démocrati&shy;sation de la culture et construction collective {: .cartouche-titre}
# Historique de l’ISELP {: .sous-titre}

Mélanie %%Rainville, Mélanie|Rainville%%
{: .cartouche-signature}

Historique de l’ISELP
{: .running-header}

<div class="header2">
<h2> L’art&#x202F;: pour qui&#x202F;? pour quoi&#x202F;?</h2>
<p class="dates">1971-1981</p>
</div>

### Décoder le langage de l’art pour saisir le monde

Bruxelles, 1970. Le secteur des galeries commerciales est effervescent.
Fernand %%Spillemaeckers, Fernand|Spillemaeckers%% a ouvert %%MTL%% (1966) et démontre son engagement
artistique, social et politique à travers des publications qui traitent
de l’art et de ses enjeux. %%New Smith Gallery%%, %%Galerie Aujourd’hui%%,
%%Galerie Cogeime%%, %%Villa Hermosa%%, %%Galerie Paul Maenz%%, Les contemporains et
divers espaces ouverts par %%Baronian, Albert|Albert Baronian%% sont autant d’endroits où
l’on peut entrer en contact avec des œuvres contemporaines. Des
galeristes – principalement bruxellois – ont créé l’%%Association des galeries d’art actuel|Association des%%
galeries d’art actuel en Belgique (1967-2001) pour défendre leurs
intérêts et concurrencer les autres capitales à proximité. Pour se doter
d’un organe de visibilité, ils créent La %%Foire d’art actuel%% en
Belgique[^1]  (1968, aujourd’hui %%Art Brussels%%). L’univers marchand
promeut des esthétiques qui ne sont pas toujours synchrones avec les
tendances officielles. Il a l’avantage de la diversité et attire les
amateurs. Il n’y a pas de %%Musée d’Art moderne de Bruxelles|musée d’art moderne%% à %%Bruxelles%% et les jeunes
créateurs bénéficient de peu de visibilité au sein des institutions
artistiques. Depuis un peu moins de cinq ans, des artistes et des
intellectuels manifestent leur mécontentement envers les politiques en
vigueur. Le %%Palais des Beaux-Arts (BOZAR)|palais des Beaux-Arts%% a été occupé. Marcel %%Broodthaers, Marcel|Broodthaers%% a
présenté son *Musée d’Art moderne, Département des Aigles, Section XIX<sup>e</sup>
siècle* chez lui. Un nombre croissant d’artistes cherche à dépasser les
limites des anciennes disciplines, notamment en recourant à la
performance et au film. La dématérialisation de l’objet d’art est
enclenchée. Les œuvres sont iconoclastes. Décriés ou acclamés, les
concepts qui se cachent derrière elles sont bien en vue. Ils répondent à
un désir de s’extraire des dynamiques commerciales et engendrent un
usage accru du langage verbal dans la production artistique.

La création artistique émergente bénéficie également de peu de
visibilité au sein des canaux de transmission de l’histoire de l’art.
Quelques initiatives prises en 1959 améliorent la situation. Le palais
des Beaux-Arts crée %%Jeunesse et Arts Plastiques (JAP)|Jeunesse et%% Arts Plastiques (JAP). On y écoute des
historiens de l’art, organisateurs d’expositions, conservateurs,
artistes et autres personnalités du monde artistique actuel. Philippe
%%Roberts-Jones, Philippe|Roberts-Jones%% devient conservateur aux %%Musées royaux des Beaux-Arts de Belgique%%. À titre de chargé de cours, il fonde la chaire d’histoire de
l’art contemporain ainsi qu’un cours sur l’art contemporain à
l’Université libre de %%Bruxelles%%.[^2] Cette nouveauté est positive pour
le secteur, bien que ce que l’on qualifie de contemporain dans le
monde académique de l’époque n’est peut-être pas si récent. Les écoles
d’art telles que l’Académie royale des Beaux-Arts de Bruxelles
(%%Académie royale des Beaux-Arts de Bruxelles (ArBA — ESA)|ArBA%%-EsA), l’ESA %%Saint-Luc — ESA|Saint-Luc%% et l’École nationale supérieure
d’architecture et des arts visuels de %%École nationale supérieure d’architecture et des arts visuels de La Cambre (La Cambre)|La Cambre%% forment de
jeunes artistes. Elles offrent des cours d’histoire de l’art, mais
ceux-ci sont subsidiaires par rapport aux cours pratiques. Robert L.
%%Delevoy, Robert L.|Delevoy%%, directeur de %%École nationale supérieure d’architecture et des arts visuels de La Cambre (La Cambre)|La Cambre%%, est toutefois en contact étroit avec
une jeune professeure qui reconnaît l’importance du rôle social de l’art
contemporain&#x202F;: %%Brys-Schatan, Gita|Gita Brys-Schatan%%.

%%Brys-Schatan, Gita|Gita Brys-Schatan%% a deux enfants dans les années 1950 et réalise
peu à peu que la vie de mère au foyer ne lui convient pas à long terme.
Elle retourne aux études et achève un doctorat en histoire de l’art à la %%Sorbonne%%, à %%Paris%%, à la fin des années 1960. Elle
produit une thèse sur Jean %%Brusselmans, Jean|Brusselmans%%, dirigée par René %%Jullian, René|Jullian%%, s’installe à %%Bruxelles%% et enseigne à la %%Chapelle Reine Élisabeth|Chapelle Reine%%
Élisabeth. À la fin des années 1960, elle installe régulièrement son
projecteur de diapositives au cinquième étage de l’Innovation pour
dévoiler des tranches d’histoire de l’art aux publics qui s’y
présentent. Face à la loyauté de ses auditeurs, elle observe un
véritable désir d’éducation continue. Celui-ci souligne l’absence
d’institutions bruxelloises offrant aux adultes la possibilité
d’entamer, consolider ou approfondir des connaissances en histoire de
l’art. Elle imagine une association sans but lucratif dédiée à
l’éducation artistique. De passage à %%Paris%%, elle présente son projet au baron Joseph-Berthold %%Urvater, Joseph-Berthold|Urvater%%, un diamantaire anversois et
collectionneur d’œuvres d’art. Il soutient d’emblée sa mise en œuvre.
L’*Institut supérieur pour l’étude du langage plastique. Fondation J.B. %%Fondation Joseph-Berthold Urvater|Urvater%%* (ISELP) est créé le 20 juillet 1970. Une publication annonce la
création de l’ASBL au moniteur belge le 11 février 1971. Le conseil
d’administration de l’&#57344; est composé d’éminentes personnalités
incluant des professeurs universitaires d’autres métropoles européennes
(Rome, %%Lausanne%%, Amsterdam), ainsi que des conservateurs liés au musée
d’Ixelles, aux Musées royaux d’Art et d’Histoire et au musée d’Art
moderne de %%Paris%%. La dimension internationale de sa composition, même
dans le contexte de mondialisation actuel, étonne. %%Brys-Schatan, Gita|Gita Brys-Schatan%%
sait s’entourer. Certains administrateurs constituent des acteurs
déterminants dans les orientations idéologiques de l’&#57345;. Ils
représentent d’ailleurs, dans certains cas, son premier corps
professoral. Le milieu culturel bruxellois est doté d’un nouvel
organisme qui propose des cours et des rencontres avec des artistes dans
le but d’élargir la compréhension des arts de tous les temps. Il entame
ses activités huit mois plus tard, à la fin octobre, dans un local
appartenant à Robert %%Warche, Robert|Warche%%, membre de la %%Chambre des Antiquaires%%[^3].

Le nom est bien réfléchi, chaque mot est choisi avec attention.
*Institut*. La structure en devenir est un lieu dédié à la recherche. Ni
école, ni centre d’exposition à proprement parler, il s’agit d’un organe
éducatif souhaitant indépendance et autonomie. *Supérieur*. L’absence de
structure similaire dans le paysage culturel bruxellois impose un
positionnement infaillible. Il importe de construire et démontrer une
valeur, de faire preuve de crédibilité. La valorisation induite par
cet adjectif est l’indice d’une quête de reconnaissance sociale,
culturelle et politique. *Pour l’étude*. Dans la foulée de 1968, on
souhaite que la culture soit accessible à tous. En art contemporain,
l’initiation et la démocratisation sont à l’ordre du jour. Pop art, land
art, art conceptuel, hyperréalisme, les courants artistiques se
succèdent rapidement à l’aube des années 1970. %%Brys-Schatan, Gita|Gita Brys-Schatan%% estime
important d’offrir des clés de lecture pour aborder l’art de son temps.
La volonté de rassembler des historiens de l’art, des artistes et des
publics est effective. L’étude se produit en amont et en aval de cours
d’histoire de l’art, souvent au contact les uns des autres. Il y a
innovation dans les méthodes d’étude, d’analyse et de transmission de
connaissances. %%Brys-Schatan, Gita|Gita Brys-Schatan%% tente une approche de l’art
contemporain par les sciences humaines. Elle considère les œuvres en
fonction de l’individu et de la société, et non pas de leurs qualités
formelles. Leur composition, par exemple, est relayée au second plan.
*Du langage plastique*. La sémiotique visuelle se développe dans les
années 1960.[^4] Cette approche se structure autour de signifiants et de
signifiés. Dans cette optique, des formes esthétiques et plastiques
deviennent *communication*. À la même époque, le mouvement d’art
conceptuel entraîne la dématérialisation de l’œuvre d’art et laisse plus
d’espace au langage verbal. La fondatrice et directrice de l’&#57344;
conçoit les arts plastiques comme un langage à apprendre, à développer,
à décoder, à transmettre. Dans cette perspective, l’artiste est un être
sensible qui a l’aptitude de reconnaître les points forts du monde dans
lequel il vit et de les traduire à travers ses œuvres. Il incarne un
visionnaire que les publics doivent comprendre pour saisir l’essence de
leur époque. L’artiste se positionne au cœur des activités de l’&#57346;.
L’inflexion pédagogique mise en avant est essentiellement sémiologique
et sociologique. Les œuvres sont décodées en fonction du contexte social
qui les a vues émerger.

Après un an de présidence du conseil d’administration, le baron %%Urvater, Joseph-Berthold|Urvater%%
cède sa place à Robert L. %%Delevoy, Robert L.|Delevoy%% (en poste jusqu’en 1980). Ce dernier
souligne la pertinence de l’approche pédagogique tournée vers la
sémiologie&#x202F;: «&#x202F;La transformation des moyens d’expression entraîne ou
provoque nécessairement de nouveaux modes d’approche de l’art et parmi
eux il me semble que des analyses dérivées de la linguistique sont
susceptibles de renouveler non seulement la manière d’appréhender l’art
d’aujourd’hui, mais plus profondément encore les méthodes
d’investigation traditionnelles de l’Histoire de l’Art en général. Dès
le moment où l’on accepte d’assimiler l’art au langage ou de considérer
l’art comme langage c’est-à-dire comme système de signes, des
perspectives nouvelles et exaltantes s’ouvrent pour en accuser la portée
comme phénomène de communication.&#x202F;»[^5] Cet intérêt envers la sémiologie
dont témoignent %%Brys-Schatan, Gita|Gita Brys-Schatan%% et Robert L. %%Delevoy, Robert L.|Delevoy%% se manifeste dans
le programme de cours. Daniel %%Peraya, Daniel|Peraya%% contribue au rapprochement entre la
linguistique et l’art par l’intermédiaire d’interventions régulières
dans le contexte des cours «&#x202F;Sémiologie générale&#x202F;» (1971), «&#x202F;Quelques
approches des faits de communication&#x202F;» (1972), «&#x202F;Sémiologie, langage
cinématographique et critique idéologique. Lecture de film&#x202F;» (1972),
«&#x202F;Linguistique générale&#x202F;: notions fondamentales&#x202F;» (1973), «&#x202F;Langages&#x202F;:
communication et/ou signification&#x202F;?&#x202F;» (1973), «&#x202F;Sémiologie graphique
élémentaire et portative&#x202F;» (1973). D’autres cours se distinguent en la
matière&#x202F;: «&#x202F;Le langage de l’affiche&#x202F;» par %%Brys-Schatan, Gita|Gita Brys-Schatan%% (1971),
«&#x202F;L’image comme communication visuelle&#x202F;» par Abraham %%Moles, Abraham|Moles%% (1973), «&#x202F;La
mutation des signes&#x202F;» par %%Berger, René|René Berger%% (1973) et «&#x202F;Sémiologie graphique
élémentaire et portative&#x202F;», repris par Luc Van Malderen (1973), entre
autres.

%%Brys-Schatan, Gita|Gita Brys-Schatan%% fait également valoir l’importante imbrication entre
l’art et son contexte social par un nombre conséquent de cours. Dès
1972, elle dispense elle-même un cours intitulé «&#x202F;L’aventure provocante
de l’art contemporain&#x202F;» et anime un débat avec Michel %%Ragon, Michel|Ragon%% au palais
des Beaux-Arts intitulé «&#x202F;L’art pourquoi faire&#x202F;?&#x202F;». Dans le même ordre
d’esprit, Jean-Pierre Van Tieghem offre le cours «&#x202F;Approche sociologique
de l’art actuel&#x202F;» (1972). Ces initiatives font écho au symposium «&#x202F;L’art
et son contexte culturel&#x202F;», que Fernand %%Spillemaeckers, Fernand|Spillemaeckers%% organise à La
Cambre en 1973. L’&#57347; est en phase avec son époque. Par la suite,
entre autres, Claude %%Javeau, Claude|Javeau%% propose «&#x202F;Sociologiquement, l’art et
l’image&#x202F;» (1976) et Daniel Vander Gucht propose «&#x202F;Initiation à la
sociologie&#x202F;: la société, l’art dans la société, la société dans l’art&#x202F;»
(1983). Ce dernier est d’ailleurs le premier sociologue belge à tisser
et théoriser des liens entre l’histoire de l’art et la sociologie. Gita
Brys-Schatan est sensible à l’intérêt de l’interdisciplinarité. À
l’&#57346;, d’autres professeurs traitent du sujet sous des intitulés de
cours plus discrets. Ces quelques interventions démontrent de manière
assez exemplaire l’adéquation entre la création de l’&#57345; et le
contexte de démocratisation de l’art et de l’enseignement, en cours à
l’époque.

À l’instar de Robert L. %%Delevoy, Robert L.|Delevoy%%, l’architecte et urbaniste Maurice
%%Culot, Maurice|Culot%%, enseignant à %%École nationale supérieure d’architecture et des arts visuels de La Cambre (La Cambre)|La Cambre%%, semble également introduire un champ de
spécialisation à l’&#57347;. %%Culot, Maurice|Culot%% s’intéresse à une nouvelle théorie du
langage de la ville en 1971&#x202F;: «&#x202F;De tout temps et aujourd’hui davantage
encore, on constate une extraordinaire contradiction entre les
nécessités de l’urbanisme et la réalité de l’aménagement des villes.
C’est le résultat de cette politique des ‘théories urbaines écartées’
dues à des décisions politiques négligeant le besoin réel pour des
intérêts immédiats. (…) L’étude du langage de la ville permet de
déceler les facteurs déterminants de son caractère, son visage actuel et
son futur.&#x202F;»[^6] Bien qu’il ne dispense qu’un seul cours à l’&#57345;
(«&#x202F;L’architecture contre&#x202F;», 1972), sa sensibilité et ses préoccupations
envers la ville font écho à de nombreuses activités ultérieures.

Le programme de cours des deux premières années d’activité de l’&#57344;
trace une ligne éditoriale assez claire qui persiste longtemps dans
l’esprit de %%Brys-Schatan, Gita|Gita Brys-Schatan%% et l’orientation de sa structure. Les
sciences sociales, spécialement la sémiologie et la sociologie, servent
de filtre pour examiner les productions artistiques. À partir de l’année
académique 1972-1973, le programme est divisé en fonction de plusieurs
axes. Parmi ceux-ci, les rencontres-débats %%Inforart%% animées par
Jean-Pierre Van Tieghem. Chaque mois[^7], le journaliste et critique des
arts visuels de la %%RTBF/RTB|RTBF%% reçoit et interroge un artiste belge ou étranger
dans l’auditorium de l’&#57346;. De nombreux artistes participent à ces
dynamiques évènements, dont Dennis %%Oppenheim, Dennis|Oppenheim%% [^8], %%Le Parc, Julio|Julio Le Parc%%,
Jean-Pierre %%Ghysels, Jean-Pierre|Ghysels%%, %%Gilli%%, %%Acconci, Vito|Vito Acconci%% (films, exclusivement), %%Meurant, Serge|Serge%% et Georges %%Meurant, Georges|Meurant%%, Georges %%Miezanagora, Georges|Miezanagora%%, Pierre %%Cordier, Pierre|Cordier%%, Joël %%Kermarrec, Joël|Kermarrec%%,
%%Cadere, André|André Cadere%%, Juan %%d’Oultremont, Juan|d’Oultremont%%, Jacques %%Lennep, Jacques|Lennep%%, Félix %%Roulin, Félix|Roulin%%, Henri
%%Ronse, Henri|Ronse%%, %%Forest, Fred|Fred Forest%%, %%Bourgeois, Jean-Jacques|Jean-Jacques Bourgeois%%, Jacques %%’T Kindt, Jacques|’T Kindt%%, Eddy
%%Devolder, Eddy|Devolder%%, Michel %%Thomas, Michel|Thomas%%, Alain %%Fleig, Alain|Fleig%%, Colette %%Deble, Colette|Deble%%, Marie-Jo %%Lafontaine, Marie-Jo|Lafontaine%%.
Les rencontres %%Inforart%% cessent en 1981.

Progressivement, ces axes sont structurés au sein d’un programme qui
englobe quatre profils de cours. Un profil d’initiation et
d’apprentissage culturels intitulé «&#x202F;Après le travail avant la soirée&#x202F;»
est prévu pour ceux qui travaillent en journée. Un deuxième profil
compile un cycle de cours fondamentaux et un cycle d’approfondissement
visant la consolidation de connaissances. Un troisième profil prévoit le
recyclage des enseignants[^9] et un quatrième l’approfondissement et la remise en question des connaissances. Ces
quatre profils comptabilisent plus de 150 heures de cours répartis les
lundis, mardis, mercredis, jeudis dans des horaires impartis entre 14&#x202F;h
et 23&#x202F;h[^10]. Une impressionnante diversité de sujets sont abordés.
L’année 1978, à elle-seule, regroupe les cycles de cours suivants&#x202F;:
«&#x202F;Initiation et apprentissage culturels&#x202F;», «&#x202F;Initiation à l’histoire de
la peinture et de la sculpture&#x202F;», «&#x202F;Initiation à l’histoire de
l’architecture&#x202F;», «&#x202F;Panorama des arts plastiques&#x202F;», «&#x202F;XX<sup>e</sup>  –
Métamorphes&#x202F;», «&#x202F;Enquêtes&#x202F;: Renaissance, Inde&#x202F;», «&#x202F;Enquêtes&#x202F;: XX<sup>e</sup>
siècle&#x202F;», «&#x202F;La logique des espaces urbains&#x202F;», «&#x202F;Les styles et leur
symbolique&#x202F;», «&#x202F;Psychanalyse et langage&#x202F;», «&#x202F;Le jazz&#x202F;? Pour quoi&#x202F;? Par
qui&#x202F;? Comment&#x202F;?&#x202F;», «&#x202F;Pour une nouvelle lecture de l’œuvre plastique&#x202F;»,
«&#x202F;Séminaire sur l’image électronique&#x202F;». Si les premières années
académiques comprenaient majoritairement des cours liés à l’art
contemporain, ce n’est plus nécessairement le cas par la suite.
Cherchant à toucher un large éventail de sensibilités et de niveaux de
connaissance, %%Brys-Schatan, Gita|Gita Brys-Schatan%% diversifie la nature de ses activités et
les sujets abordés.

### Professeurs, auditeurs, membres et artistes

%%Brys-Schatan, Gita|Gita Brys-Schatan%% constitue un corps professoral francophone qu’elle
sollicite ponctuellement. Les professeurs ont la responsabilité de
trouver le *ton juste* pour s’adresser aux différents groupes
d’auditeurs. Ni trop spécialisé, ni trop basique, le langage employé
joue un rôle important. Cette recherche représente un défi de taille qui
résiste aux années. Il y a peu d’opportunités d’enseigner l’histoire de
l’art à %%Bruxelles%% dans les années 1970. Elles sont encore plus rares
lorsqu’il s’agit d’art contemporain. L’&#57345; invite des historiens de
l’art spécialisés ou généralistes à s’adresser à ses auditeurs. Un
passage dans l’auditoire de l’&#57347; caractérise
le parcours professionnel de plusieurs acteurs ayant fait carrière dans les mondes
académique, muséal et artistique. Claude %%Javeau, Claude|Javeau%%, Pierre %%Sterckx, Pierre|Sterckx%%, Rouve
%%Hauser, Rouve|Hauser%%, Henri %%Van Lier, Henri|Van Lier%%, Ignace %%Vandevivere, Ignace|Vandevivere%%, Pierre %%Lenain, Pierre|Lenain%%, Bernard
%%Marcelis, Bernard|Marcelis%%, Daniel %%Peraya, Daniel|Peraya%%, pour ne donner que quelques exemples, sont
aujourd’hui des personnalités connues. Porteuse de crédibilité, cette
expérience permet notamment de tester la prise de parole publique et de
confronter ses idées.

L’&#57346; s’apparente à un club social. Les auditeurs doivent payer un
abonnement annuel et avoir une carte de membre avant de s’inscrire aux
cours[^11]. Ils reçoivent un certificat de fréquentation lorsqu’ils sont
assidus, un diplôme lorsqu’ils présentent un travail agréé par
l’ensemble des enseignants[^12]. Calqué sur un modèle académique
répandu, le mode de fonctionnement de l’&#57345; permet la formation de
spécialistes du langage plastique.

Les gains obtenus grâce aux abonnements et aux inscriptions aux cours
sont insuffisants pour permettre la viabilité de la structure. Gita
Brys-Schatan fait preuve d’imagination pour pallier le manque de budget
et de soutien logistique. Elle lance un programme de vente de
sérigraphies intitulé «&#x202F;500 à 500 F&#x202F;», consistant à produire 500
sérigraphies et à les vendre 500 francs belges (aujourd’hui 12,39 euros)
chacune (ce projet devient assez rapidement «&#x202F;500 à 1 500&#x202F;»). Jan
%%Dibbets, Jan|Dibbets%%, Jo %%Delahaut, Jo|Delahaut%%, %%Loge, Bernard|Bernard Loge%%, Jean-Michel %%Folon, Jean-Michel|Folon%%, %%Mara, Pol|Pol Mara%%, entre
autres, participent à cette stratégie de financement indépendante. Grâce
aux membres de l’&#57347; qui s’emparent de l’opportunité d’acquérir une
œuvre contemporaine à prix abordable, elle renfloue les caisses et
maintient un peu de liquidités.[^13] Ce projet prend forme grâce au
concours de Robert L. %%Delevoy, Robert L.|Delevoy%%, qui mobilise un atelier de %%École nationale supérieure d’architecture et des arts visuels de La Cambre (La Cambre)|La Cambre%% pour
la production des premières impressions. Dès le printemps 1972,
cependant, %%Brys-Schatan, Gita|Gita Brys-Schatan%% mène une enquête sur les meilleurs ateliers
de sérigraphie et de lithographie au profit des artistes qui
participeront à ce projet.

### *Art et ordinateur*

%%Brys-Schatan, Gita|Gita Brys-Schatan%% commence à organiser des expositions en réponse aux
sollicitations répétées des auditeurs assistant aux cours. Son premier
projet est ambitieux&#x202F;: *Art et ordinateur* prend place à
%%Forest National%%[^14] du 18 avril au 18 mai 1974. Une version réduite est
présentée à la Maison de la %%Maison de la culture de Namur|culture de Namur%% l’automne suivant. Cette
exposition organisée en collaboration avec le %%Goethe Institut%%[^15]
aborde une thématique susceptible de toucher des publics qui ne
s’intéressent pas forcément aux arts plastiques. Elle offre une
exploration, une excursion dans un domaine méconnu. Il ne s’agit ni de
rétrospective, ni d’explication. À l’époque, le terme *ordinateur* fait
partie du lexique francophone depuis une vingtaine d’années. Depuis
1960, des artistes s’emparent graduellement de ce nouvel outil,
continuellement perfectionné. L’exposition *Art et ordinateur* rassemble
plus de 200 œuvres *programmées* par des créateurs travaillant en
France, en Allemagne, en Italie, en Hollande, aux États-Unis, en
Belgique, etc. Mathématicien et philosophe allemand, Georg Nees
(1926-2016) est le premier artiste au monde à présenter publiquement ses
travaux produits à l’aide d’un programme d’ordinateur en 1965. Il
profite d’algorithmes pour générer ses dessins mécaniques et présente
*Corner Graphics* dans *Art et ordinateur* près de dix ans plus tard.
Peter Struycken, Yves %%Kodratoff, Yves|Kodratoff%% et le Groupe de %%Vincennes, Groupe de|Vincennes%% sont également
de la partie. Leurs travaux démontrent différents usages de la machine
et témoignent de préoccupations diverses, allant de la recherche de
mécanisation ou de processus aléatoires à la sollicitation d’une
participation active de la part des visiteurs et la réalisation
d’enquêtes conceptuelles. Outre les œuvres exposées, la présentation
publique inclut un montage audiovisuel de 35 minutes, une section
didactique, des projections de films, une séance d'écoute de musique par
ordinateur et une publication réunissant des écrits de spécialistes.
L’apparence artisanale de cette publication produite à l’aide d’un
ordinateur rassemble des recherches fouillées. Le ministère de la
%%Ministère de la Culture française|Culture française%%, la Commission française de la Culture de
l’Agglomération de Bruxelles, %%École nationale supérieure d’architecture et des arts visuels de La Cambre (La Cambre)|La Cambre%% et le %%Goethe Institut%% lui
offrent soutien et collaboration.

Dans son ouvrage *Computer Graphics – Computer Art*, Herbert W. %%Franke, Herbert W.|Franke%%,
pionnier du graphisme informatique et auteur de science-fiction, cite
des dizaines d’expositions rapprochant l’art et l’ordinateur présentées
entre 1968 et 1980[^16]. Celle de %%Brys-Schatan, Gita|Gita Brys-Schatan%% y est mentionnée comme la
première manifestation présentant des graphiques informatiques en
Belgique[^17]. Abraham %%Moles, Abraham|Moles%%, auteur et professeur à l’université de
Strasbourg, publie une importante étude intitulée *Art et ordinateur*
aux éditions Casterman en 1971. Il y pose des questions cruciales&#x202F;:
l’artiste est-il remplacé par l’ordinateur ou simplement déplacé&#x202F;? Que
sera une nouvelle critique d’art&#x202F;? Quelles sont les bases de la nouvelle
culture en symbiose avec la machine&#x202F;? De passage à l’&#57344; deux ans plus
tard pour une conférence intitulée «&#x202F;L’image comme communication
visuelle&#x202F;» (1973), il nourrit sans doute la réflexion de Gita
Brys-Schatan. *Art et ordinateur* est traversée par ces questions.
L’&#57346; répond à sa mission fondamentale&#x202F;: explorer la notion de langage
dans les productions culturelles. Il est question d’arts plastiques,
mais également de musique et de films. Un essai d’Abraham %%Moles, Abraham|Moles%% intitulé
‘L’art permutationnel&#x202F;: la conscience du jeu et ses règles’, issu de son
ouvrage de référence de 1971, est reproduit dans la publication qui
accompagne l’exposition. Manfred %%Mohr, Manfred|Mohr%%, Yves %%Kodratoff, Yves|Kodratoff%% (‘Pour un art par
ordinateur’), Jean-Claude %%Halgand, Jean-Claude|Halgand%% (‘Peinture ordinateur’), Hervé %%Huitric, Hervé|Huitric%%
et Monique %%Nahas, Monique|Nahas%% (‘Distance, texture et vieilles dentelles’) ainsi que
%%Arveiller, Jacques|Jacques Arveiller%% (‘L’informatique musicale, qu’est-ce que c’est&#x202F;?’),
pionniers en ce qui concerne l’art digital, signent un essai dans cette
publication. %%Brys-Schatan, Gita|Gita Brys-Schatan%% y ajoute sa contribution&#x202F;: ‘Au point de
vue de l’histoire de l’art’.

### Des locaux permanents

Depuis sa fondation, l’&#57345; occupe ponctuellement des auditoriums situés
à %%École nationale supérieure d’architecture et des arts visuels de La Cambre (La Cambre)|La Cambre%%, ainsi qu’au 4 rue aux %%Laines (rue aux)|Laines%% et au 116 rue de %%Livourne (rue de)|Livourne%%.
Face au succès des activités programmées, la nécessité d’un espace de
travail plus confortable s’impose.

La Commission française de la Culture de l’Agglomération de Bruxelles
(la %%Commission communautaire française (COCOF)|COCOF%% – Commission communautaire française, en est l’héritière et
se dénomme aujourd’hui le %%Service public francophone bruxellois%%) voit le
jour en 1972. Créée pour développer une politique culturelle originale,
complémentaire à celle du %%Ministère de la Culture française|ministère de la Culture française%%, elle a pour
mission de contribuer à la mise en place de structures et de services
d’éducation permanente qui manquent à %%Bruxelles%%. L’initiative de Gita
Brys-Schatan trouve un soutien majeur au sein de cette entité
politique[^18]. Jean-Pierre %%Poupko, Jean-Pierre|Poupko%%, son président (1972-1989), ainsi que
Maurice %%Culot, Maurice|Culot%%, architecte-urbaniste engagé dans le conseil
d’administration de l’&#57346;, soutiennent sa recherche de locaux
permanents. Ils identifient le potentiel des espaces du 31 %%Waterloo (boulevard de)|boulevard de Waterloo%%. Ceux-ci correspondent aux anciennes écuries du %%Egmont, palais d’|palais d’Egmont%%
et appartiennent à la Ville de %%Bruxelles%%. Ils jouxtent le %%Egmont, Parc d’|parc d’Egmont%%
et sont classés au registre du Patrimoine par la Commission royale des
%%Commission royale des Monuments et Sites|Monuments et Sites%%. L’octroi de bâtiments de la Ville à l’&#57347;
correspond à l’agenda politique de Jean-Pierre %%Poupko, Jean-Pierre|Poupko%%, qui souhaite
œuvrer à la préservation du patrimoine et au soutien d’une structure
culturelle francophone. Il permet en outre à la Commission française de
participer à l’Année européenne du patrimoine architectural (1975).

L’histoire du %%Egmont, palais d’|palais d’Egmont%% remonte au milieu du XVI<sup>e</sup> siècle. Il
s’agit d’abord de deux hôtels achetés par la famille %%D’Arenberg (Famille)|d’Arenberg%% et
rassemblés lors d’importants travaux d’architecture en 1752. Le palais
%%Palais d’Egmont|d’Egmont%% est rénové en 1838 et cette intervention donne lieu à la
construction de nouvelles écuries de part et d’autre du passage de
Milan, qui relie le %%Egmont, Parc d’|parc d’Egmont%% et le %%Waterloo (boulevard de)|boulevard de Waterloo%%. Ces
nouvelles écuries accueillent&#x202F;: «&#x202F;[…] les chevaux (aile gauche depuis
le Palais), des voitures (aile droite), la cuisine des palefreniers,
ainsi qu’une habitation particulière de service côté boulevard, et une
autre côté parc.&#x202F;»[^19] Les bâtiments néoclassiques, composés de seize
arches, sont construits selon les plans de l’architecte Tilman-François
%%Suys, Tilman-François|Suys%%. À l’issue de la Première Guerre mondiale, la famille %%D’Arenberg (Famille)|d’Arenberg%%
quitte la Belgique et cède le %%Egmont, palais d’|palais d’Egmont%% à la Ville de %%Bruxelles%%.
Indépendantes du reste du site, les écuries ont plusieurs fonctions au
fil des années. Une intercommunale, sous-station d’électricité de la
Ville de Bruxelles, cloisonne les pièces en fonction de ses besoins en
1930. «&#x202F;Le courant haute tension y est transformé et distribué à basse
tension. Le pont roulant, emblématique de la grande salle d’exposition
actuelle, porté par une succession de portiques en béton armé qui
rythment l’espace, est un héritage de cette époque.&#x202F;»[^20] Le sculpteur
Georges %%Dobbels, Georges|Dobbels%% loue également à la Ville de Bruxelles, de 1953 à 1987, un espace de 180 mètres carrés de la
partie du bâtiment située à proximité du %%Waterloo (boulevard de)|boulevard de Waterloo%%. Il y établit son atelier.
Par ailleurs, des ateliers de danse, de théâtre et d’escrime s’y
installent temporairement.

En 1975, le positionnement de ce bâtiment dans cette partie de la ville
représente un atout pour l’&#57347;. Situé au carrefour de boutiques de
luxe et d’un canal de circulation internationale (le %%Egmont, palais d’|palais d’Egmont%%,
cédé par la Ville de %%Bruxelles%% au ministère des %%Ministère des Affaires étrangères|Affaires étrangères%%), il
connecte le bas et le haut de la ville. Le %%Egmont, Parc d’|parc d’Egmont%% ainsi que ses
accès, classés parmi les sites patrimoniaux de la Ville de %%Bruxelles%% par
arrêté royal depuis le 20 juillet 1972, contribuent également à
l’intérêt du site. Les écuries nécessitent d’importants travaux de
rénovation suite à des occupations et inoccupations successives. Les
espaces sont redessinés en fonction des besoins de l’Institut. Mené par
les architectes Daniel %%Lelubre, Daniel|Lelubre%% et Brigitte %%Libois, Brigitte|Libois%%, le chantier se
déploie dès 1974-1975. Au bout du %%Milan, passage de|passage de Milan%%, à proximité du parc,
la partie du bâtiment développée en hauteur, à droite du portique, est
réaménagée au profit de fonctions administratives (secrétariat,
direction, salle de séminaire, foyer, bibliothèque). Le foyer et la
bibliothèque sont fermés par des portes vitrées qui donnent sur une
salle polyvalente. Créée grâce au réaménagement d’un vaste espace des
écuries, cette salle d’une centaine de mètres carrés,
jouxtant l’atelier du sculpteur Georges %%Dobbels, Georges|Dobbels%%, est dédiée à des
conférences, des expositions, etc.

L’inauguration publique des nouveaux espaces du 31, %%Waterloo (boulevard de)|boulevard de Waterloo%% est combinée au vernissage de *Patrimoine bruxellois&#x202F;:
reportages* le 15 octobre 1975. Présenté dans la nouvelle salle
polyvalente, ce projet marque la reconnaissance de %%Brys-Schatan, Gita|Gita Brys-Schatan%%
envers Jean-Pierre %%Poupko, Jean-Pierre|Poupko%% pour l’octroi de locaux exceptionnels. Il se
décline en trois reportages photographiques documentant les travaux de
rénovation des anciennes écuries du %%Egmont, palais d’|palais d’Egmont%%, un inventaire du
patrimoine architectural de la culture de l’Agglomération et une enquête
menée dans les rues de %%Bruxelles%%. Intimement lié à la ville, à la
société et à l’histoire, ce projet souligne l’importance des problèmes
posés par la préservation et la restauration de l’immobilier bruxellois.
Jean-Pierre %%Poupko, Jean-Pierre|Poupko%% profite de l’occasion pour souligner la nécessité
d’agir en faveur de ce patrimoine. Lors de l’événement, ce dernier
annonce la mise sur pied d’une campagne d’éducation, dès 1976, visant la
promotion de tous les musées. L’&#57347; n’attend pas cette campagne pour
poursuivre ses activités. Il continue à organiser des programmes de
cours et des expositions. Suivant une périodicité assez irrégulière
compte tenu de la nature polyvalente de la salle où elles prennent
place, ces expositions durent environ un mois. Hormis les expositions
individuelles présentées dans l’Espace Éphémère dont il sera question
plus loin, par exemple, %%Brys-Schatan, Gita|Gita Brys-Schatan%% organise deux expositions en
1975, une en 1976, une en 1977, trois en 1978, quatre en 1979, deux en
1980, etc. Plutôt hétérogènes au premier abord, elles confirment la
sensibilité annoncée lors de la fondation de l’Institut&#x202F;: elles
témoignent, en alternance ou simultanément, d’approches sémiologique et
sociologique de l’art. Si l’&#57346; est précurseur en la matière, cette
tendance s’ancre graduellement dans le milieu artistique au cours des
années 1970. Appelé à commenter le petit nombre de visiteurs ayant vu la
célèbre exposition du %%Palais des Beaux-Arts (BOZAR)|palais%% des Beaux-Arts %%Andre, Carl|*Carl Andre*%% / %%Broodthaers, Marcel|*Marcel Broodthaers*%% / %%Buren, Daniel|*Daniel Buren*%% / %%Burgin, Victor|*Victor Burgin*%% / %%Gilbert & George|*Gilbert & George*%% / %%Kawara, On|*On Kawara*%% / %%Long, Richard|*Richard Long*%% / %%Richter, Gerhard|*Gerhard Richter*%% en
1974, Pierre %%Sterckx, Pierre|Sterckx%% réagit d’ailleurs en ce sens&#x202F;: «&#x202F;L’art actuel n’est
valable que s’il interroge, non pas tellement l’esthétique, mais la
politique, la technologie, la sociologie, la sémiologie. L’exposition
mise sur pied par Yves %%Gevaert, Yves|Gevaert%%, n’aurait-elle eu que ce
retentissement-là, qu’elle se justifierait.&#x202F;»[^21] L’écrivain, critique
d’art et enseignant belge résume les préoccupations qui continuent
d’animer la fondatrice de l’&#57344; trois ans après la création de son
ASBL.

Quelques expositions organisées ou accueillies par %%Brys-Schatan, Gita|Gita Brys-Schatan%%
sont particulièrement représentatives de son approche sémiologique de
l’art. Trois d’entre elles articulent l’image et le texte de manière
similaire. *Héros-Tics* retrace l’évolution du concept de héros dans les
bandes dessinées. Des planches de dessin originales invitent les
lecteurs de bandes dessinées à côtoyer les amateurs d’œuvres d’art.
Outre une rétrospective %%Tillieux, Maurice|Tillieux%%, cette exposition de mars 1978 inclut
des planches originales de %%Druillet, Philippe|Druillet%%, %%Moebius%%, %%Tardi, Jacques|Tardi%%, %%Pratt, Hugo|Pratt%%, %%Gir|Gir%%, %%F’Murr%%, %%Forest, Jean-Claude|Forest%%, %%Franquin%%, %%Gottlieb%%, %%Hergé%%, %%Macherot%%, %%Vance, William|Vance%%, etc. Présentée du 28
novembre au 7 décembre 1978, l’exposition *Les livres d’enfants ne sont
plus ce qu’ils étaient* traite du rapport entre l’enfance, l’image et le
texte. %%Brys-Schatan, Gita|Gita Brys-Schatan%% estime que les dessins produits à destination
des enfants peuvent être analysés dans le champ de l’histoire de l’art
et intellectualise le sujet dans une publication éponyme. Plutôt
didactiques, ces deux projets rapprochant art et langage esquissent
l’histoire de deux formes de littératures. L’*Exposition internationale
de tampons d’artistes*[^22] prolonge les recherches entamées sur le
langage plastique. Du 1<sup>er</sup> mars au 3 avril 1981, %%Brys-Schatan, Gita|Gita Brys-Schatan%%
rassemble des tampons d’une centaine d’artistes venus du monde entier
sur des documents-témoins[^23]. Grâce à un formulaire de participation
bilingue (français, anglais) envoyé par courrier postal à tous les
artistes de son carnet d’adresses, la poste lui livre le contenu de son
exposition. À l’époque, %%Brys-Schatan, Gita|Gita Brys-Schatan%% conçoit le tampon d’artiste
comme le signe d’une singularisation similaire à la signature. Ce
projet, vraisemblable&shy;ment ludique et humoristique, lui permet de creuser
la relation entre le texte et l’image présents sur les traces imprimées.

Durant les années 1970, l’&#57346; présente également un ensemble
d’expositions qui soulignent ou révèlent des phénomènes de société. *Art
en série provenant d’intérieurs ouvriers du pays de la %%Ruhr%%*
(19 novembre-13 décembre 1975), organisée en collaboration avec le
%%Goethe Institut%% dans le contexte des Journées de la %%Ruhr%%, constitue un
exemple probant. Cette exposition atypique est liée à un phénomène
socioculturel propre à %%Bergkamen%%, une petite ville ouvrière. Tous les
deux ans, une foire aux tableaux y est organisée. Dans ce contexte, les
ouvriers peuvent, s’ils le souhaitent, échanger gratuitement une œuvre
qu’ils ont en leur possession contre une gravure moderne de leur choix.
L’ensemble des œuvres abandonnées est acquis par la municipalité de
%%Bergkamen%%, intégré à une collection et, éventuellement, exposé à
l’étranger. Le titre de l’exposition décrit le contenu présenté à
l’&#57346;&#x202F;: reproductions de représentations de cygnes et d’angelots, de
Jésus en bon pasteur, de buveurs de bière, de cerfs, entre autres,
classées par thèmes. %%Brys-Schatan, Gita|Gita Brys-Schatan%% s’intéresse à ces œuvres et à ce
phénomène pour des raisons sociologiques, indépendamment de leur qualité
esthétique. Elle présente également dans la salle d’exposition, sous
forme de cartes postales, les gravures modernes offertes en échange des
œuvres exposées. Cette juxtaposition souligne l’incontestable différence
entre les deux esthétiques, voire les deux idéologies. Du 4 au 27
décembre 1976, l’exposition *Humour/Caricature/Miroir du monde* regroupe
une sélection de deux cents dessins représentant des personnalités ou
des faits divers liés aux actualités de l’époque. L’&#57347; accueille un
volet de l’exposition collective *Musée de poche. Inventaire permanent 1
– Milieu rural*.[^24] Cette exposition itinérante établit l’inventaire
des œuvres récentes produites par des artistes de la Communauté
française de Belgique qui abordent un ou plusieurs aspects du milieu
rural. Elle permet notamment à Jacques %%Lennep, Jacques|Lennep%% de présenter le cinquième
personnage de son *Musée de l’Homme*&#x202F;: %%Six, Madame Paul|Madame Paul Six%% (fermière
collectionnant des autographes). Véhicule de l’esthétique relationnelle
formulée par l’artiste dès 1972, ce *Musée* décline les trois types de
relation synthétisant sa théorie&#x202F;: la relation structurale, la
relation au récit et la relation sociale. Jacques %%Lennep, Jacques|Lennep%% et Gita
Brys-Schatan partagent une volonté de positionner l’homme au centre de
l’art et d’un carrefour de relations, de s’interroger sur la nature et
le rôle de l’art. Cette volonté est spécialement prégnante au sein de
l’exposition *Les bâtisseurs de l’imaginaire*[^25], présentée à l’&#57346;
en 1979. Il s’agit d’une exposition didactique regroupant des ensembles
de documents constitués par %%Prévost, Claude|Claude%% et %%Prévost, Clovis|Clovis%% Prévost pour rendre compte
du travail de créateurs singuliers&#x202F;: des créateurs inclassables, souvent
autodidactes et obnubilés par leur entreprise, qui se sont appropriés un
lieu pour y vivre, construire ou transformer des formes architecturales.
On parle alors d’architecture *brute*, comme on dit *art brut*. Pour ces
artistes, la création en devenir devient simultanément un mode de vie et
une raison de vivre. Raymond %%Isidore, Raymond|Isidore%% (surnommé %%Picassiette%% parce qu’il
emploie des éclats de vaisselle pour créer ses mosaïques) décore
l’intérieur et l’extérieur d’une maison-jardin à Chartres. Irial %%Vets, Irial|Vets%%
achète une chapelle désaffectée près de Broglie, en Normandie, et y
reproduit le plafond de la chapelle %%Chapelle Sixtine|Sixtine%%. %%Vidal, Camille|Camille Vidal%% produit près
de 150 sculptures en ciment représentant des personnalités de la
télévision, des hommes politiques, des personnages de fiction, des
animaux, etc. Dispersées dans son jardin, ces sculptures forment un
ensemble intitulé *L’Arche de Noé*. %%Monsieur G%% construit une maison en
Seine-et-Marne qu’il conçoit comme un sanctuaire le protégeant des
lasers du gouvernement russe. Cette maison est également décorée de
fresques et de bas-reliefs.

Quelques années auparavant, le sociologue et professeur bruxellois
Claude %%Javeau, Claude|Javeau%% publiait la première édition de son ouvrage *L’Enquête par
questionnaire. Manuel à l’usage du praticien* (1972), aux Éditions de
l’Université de %%Bruxelles%%. Celui-ci ne passe certainement pas inaperçu
aux yeux de %%Brys-Schatan, Gita|Gita Brys-Schatan%%. Dès 1975, elle intègre l’enquête dans son
approche pédagogique en organisant un cycle de cours de spécialisation
intitulé «&#x202F;Enquête sur l’image, ses structures, ses finalités&#x202F;»,
reposant sur le principe d’une enquête interdisciplinaire. Quelques
années plus tard, elle organise deux expositions reposant sur de
véritables enquêtes. Une première est entamée en 1979 en collaboration
avec l’Institut interuniversitaire de l’opinion publique (%%Institut interuniversitaire de l’opinion publique (INUSOP)|INUSOP%%). Gita
Brys-Schatan sonde l’opinion publique sur son rapport aux arts
plastiques et souhaite, entre autres, mesurer l’impact visuel que
certains lieux artistiques bruxellois ont sur elle. Les questions sont
intimement liées aux préoccupations ayant mené à la fondation de
l’&#57347;&#x202F;: «&#x202F;Attachez-vous de l’importance à l’éducation visuelle&#x202F;?&#x202F;»,
«&#x202F;L’art est-il intégré à votre vie quotidienne&#x202F;?&#x202F;», «&#x202F;Jugez-vous les
tendances de l’art actuel, en cette fin de siècle, comme étant
’décadentes’&#x202F;? Quelle en serait la raison&#x202F;?&#x202F;», «&#x202F;L’artiste a-t-il un
rôle à jouer dans notre société&#x202F;? Si oui lequel&#x202F;?&#x202F;», «&#x202F;L’art dans la
rue, dans le contexte urbain, dans le circuit quotidien, vous
semble-t-il&#x202F;: dangereux, enrichissant, intéressant, éducatif, décoratif,
subversif&#x202F;?&#x202F;», «&#x202F;À votre avis, une incompréhension s’est-elle installée
aujourd’hui entre les artistes et le public&#x202F;? Dans l’affirmative,
pourquoi&#x202F;? », «&#x202F;Achetez-vous (aimeriez-vous acheter) de l’art
contemporain&#x202F;? Si vous achetez, quel est votre but&#x202F;?&#x202F;», «&#x202F;Pour vous, que
serait une œuvre de ‘mauvais goût’&#x202F;?&#x202F;», «&#x202F;Pour apprécier une œuvre
contemporaine, faut-il une connaissance particulière&#x202F;?&#x202F;», «&#x202F;Auriez-vous
une idée originale pour réaliser un projet à Bruxelles&#x202F;?&#x202F;», «&#x202F;Quelle
serait la question importante à laquelle vous auriez aimé répondre et
qui manque à cette enquête&#x202F;?&#x202F;» L’enquête de %%Brys-Schatan, Gita|Gita Brys-Schatan%%
s’apparente à une étude de marché cherchant à valider la pertinence des
champs d’activité de l’&#57344;, à l’aube de son dixième anniversaire.
L’exposition *1&#x202F;000 Bruxellois et l’art* (7-20 décembre 1979) rend compte
des réponses obtenues. Bien qu’on connaisse peu de choses sur son
contenu réel, elle apporte certainement une précieuse matière à
réflexion à l’historienne de l’art. L’année suivante, elle réalise une
autre enquête, essentiellement par correspondance. Cette fois-ci, des artistes contemporains sont interrogés sur la place qu'ils accordent à la photographie dans leur travail. L’exposition *La photographie et la peinture aujourd’hui* (13 mars-4 avril 1980), toujours aussi
documentaire et didactique, révèle le fruit de ses recherches à travers des interventions écrites. On y retrouve des contributions %%D’Alessandro|d’Alessandro%%,
%%Alluin, Monique|Monique Alluin%%, %%Ben|Ben%%, %%Forest, Fred|Fred Forest%%, %%Collins, Hannah|Hannah Collins%%, %%Delay, Alexandre|Alexandre Delay%%, %%Dubit, Philippe|Philippe Dubit%%, %%Erró%%, %%Gerz, Jochen|Jochen Gerz%%, %%Le Gac, Jean|Jean Le Gac%%, %%Lennep, Jacques|Jacques Lennep%%, %%Lizène, Jacques|Jacques Lizène%%, %%Mariani, Umberto|Umberto Mariani%%, %%Messager, Annette|Annette Messager%%, %%Urban%%, %%Untel%%, %%Viallat, Claude|Claude Viallat%% et %%Warhol, Andy|Andy Warhol%%, entre autres.

Ces quelques exemples d’expositions présentées à l’&#57346; durant sa
première décennie d’activité partagent un point commun&#x202F;: elles ouvrent
et décloisonnent des mondes considérés distincts par ailleurs au profit
de l’examen d’un phénomène social. Des œuvres graphiques, musicales et
filmiques produites par ordinateur, par exemple, sont placées sur le
même plan que des photographies documentaires, des représentations
picturales reproduites à grande échelle, des œuvres modernes, des
dessins de bandes dessinées, des livres pour enfants. Par conséquent, la
définition de l’œuvre d’art s’ouvre à toutes formes de productions
culturelles.

### Un Espace Éphémère pour l’art contemporain

Outre quelques expositions intimement liées à des préoccupations
sociologiques et sémiologiques, %%Brys-Schatan, Gita|Gita Brys-Schatan%% s’attache également à
présenter des œuvres contemporaines. L’Espace Éphémère[^26], existant
entre octobre 1978 et 1985, offre de la visibilité aux œuvres d’artistes
émergents. C’est d’abord l’occasion d’exposer des photographies
d’artistes belges et étrangers&#x202F;: « L’&#57347; désire être un centre vivant
de recherches dans les domaines d’expression actuelle (…). (Il) veut
donner au public la possibilité de comprendre les nouvelles directions
dans lesquelles s’engage la photographie actuelle. Les expositions ont
été réalisées pour donner à voir et à penser en éliminant les sujets
faciles et commerciaux.&#x202F;»[^27] À partir de l’été 1983, l’Espace Éphémère
est détaché de la photographie au profit du cycle «&#x202F;Dessins et autres
desseins&#x202F;». L’&#57346; donne libre cours à des artistes employant la plume
et le crayon «&#x202F;afin de montrer la différence et la richesse des diverses
disciplines qui dessinent&#x202F;». Ce cycle d’expositions présente alors des
œuvres de Myriam %%Kervyn, Myriam|Kervyn%% (photographe), Jacques %%Richez, Jacques|Richez%% (graphiste),
Jean-François %%Octave, Jean-François|Octave%%  (architecte), José %%Roland, José|Roland%% (peintre) et Félix %%Roulin, Félix|Roulin%%
(sculpteur). Compositeurs, dessinateurs de BD, scientifiques et
designers sont également exposés. Par la suite et jusqu’en 1999, l’ISELP
délaissera considérablement l’exposition d’œuvres, contemporaines ou non,
et concentrera davantage ses efforts sur ses cours et ses activités de
recherche et d’édition autour de l’art environnemental.

Cette première décennie d’activité est marquée par un changement majeur
au sein du conseil d’administration&#x202F;: Robert L. %%Delevoy, Robert L.|Delevoy%% (1980) cède la présidence à Ilya %%Prigogine, Ilya|Prigogine%%. Le célèbre physicien, chimiste et
philosophe belge d’origine russe reçoit des distinctions prestigieuses
depuis quelques années, dont un prix Nobel de chimie en 1977. Il
accompagne %%Brys-Schatan, Gita|Gita Brys-Schatan%% et son équipe, quelques mois plus tard, à
travers les célébrations du dixième anniversaire de l’&#57347;. Des
évènements sont organisés&#x202F;: une rencontre entre le professeur Abraham
%%Moles, Abraham|Moles%% et le professeur et auteur Henri van Lier intitulée «&#x202F;Demain l’art
», une conférence dispensée par le philosophe %%Kostas, Axelos|Axelos Kostas%% intitulée
«&#x202F;Le problème de la fin de l’art&#x202F;» et une table-ronde intitulée «&#x202F;Présent
et avenir&#x202F;» rassemblant Jacques %%Meuris, Jacques|Meuris%%, Georges %%Vercheval, Georges|Vercheval%%, Carole
%%Naggar, Carole|Naggar%%, Pierre %%Cordier, Pierre|Cordier%% et Alain %%Fleig, Alain|Fleig%%. Par la suite, une publication
dressant le bilan de ce qui a été accompli est produite. Elle se décline
en plusieurs essais et listes qui présentent des manifestations
culturelles, des cours, des professeurs. Elle représente un précieux
effort d’archivage témoignant de la profonde vivacité de l’ASBL.


<div class="header2">
<h2>Croissance et profession&shy;nalisation</h2>
<p class="dates">1982-1986</p>
</div>

### Diversifier les activités

Dix ans auparavant, l’Institut proposait trois séminaires et accueillait
une cinquantaine d’auditeurs par année académique. À présent, il
développe un programme compilant vingt heures de séminaires et de cours
par semaine au profit de près de cinq cents auditeurs par année
académique. %%Brys-Schatan, Gita|Gita Brys-Schatan%% a dorénavant besoin de plus d’espace pour
déployer le spectre d’activités qu’elle souhaite mener.

L’&#57349; jouit d’une plus grande visibilité à partir de décembre 1983. Il
s’agrandit en occupant désormais une nouvelle salle du bâtiment des
anciennes écuries du %%Egmont, palais d’|palais d’Egmont%% qui s’ouvre sur le %%Waterloo (boulevard de)|boulevard de Waterloo%%.[^28] Les deux espaces ne sont pas connectés entre eux, mais
l’entrée initiale, accessible depuis le %%Milan, passage de|passage de Milan%%, n’est plus le
seul point d’accès à ses activités. Une boutique est installée dans
cette zone d’accueil. Les visiteurs y retrouvent des sérigraphies
d’artistes, des livres d’art, des guides des monuments de Belgique et des ouvrages de photographie. En complément de cette activité commerciale, l’&#57348; ouvre
une %%Foire du livre d’art%% en septembre 1985, un mois avant la rentrée
académique. Cet événement est reconduit durant quelques années.

En cherchant à identifier et transmettre un langage artistique (plus ou
moins) universel, %%Brys-Schatan, Gita|Gita Brys-Schatan%% semble œuvrer à une forme de
cohésion sociale, à une quête de légitimité ou de reconnaissance des
artistes, comme si elle participait à une démocratisation à travers la
reconnaissance des créateurs de tout acabit. L’ajout de cours à
destination des enfants dans la programmation de l’&#57351;, à raison d’une
heure par semaine pour un groupe d’enfants de 8 à 11 ans et un groupe de
12 à 15 ans, semble également s’inscrire dans un projet sociétal. Dans
l’annonce du cours d’initiation des enfants à l’histoire de l’art dans
le programme de deuxième cycle de 1986-1987, une nomenclature justifie
la pertinence de cet ajout à la grille horaire. Parmi les « bonnes
raisons de faire courir ses enfants à l’&#57351;&#x202F;», quelques points attirent
l’attention&#x202F;: «&#x202F;- Parce qu’il est grand temps de pallier la carence de
plus en plus accusée des enfants et des futurs adultes en matière
artistique ; - Parce que si l’on ne court pas quand on est jeune, cela
devient plus difficile à rattraper après ; - Parce qu'à l’&#57349; il y a
un cours rien que pour eux… Faites les courir pour qu’ils puissent
discourir de peinture, de sculpture, de publicité de manière autre (…)
». En filigrane, ces trois motivations semblent traiter du fossé
entre la société et l’art contemporain – régulièrement sujet à
polémiques – qui a probablement toujours fait partie de la réalité
citoyenne. %%Brys-Schatan, Gita|Gita Brys-Schatan%% propose d’y pallier par l’éducation&#x202F;: un
citoyen ayant développé un esprit critique par rapport à l’art durant sa
jeunesse a effectivement plus de chances de *discourir* du sujet de
*manière autre*.

### Champs de spécialisation et récurrences

Le rapprochement entre l’art et la société opéré par %%Brys-Schatan, Gita|Gita Brys-Schatan%%
prend corps au sein d’une forme précise au fil des années&#x202F;: l’art
intégré dans l’espace urbain[^29].  Bien qu’il propose des cours liés à
l’architecture, à l’urbanisme et à l’art dans l’espace social depuis sa
fondation, ce n’est qu’à partir de 1983 que l’Institut donne une forme
officielle et concrète à ce champ d’intérêt. Il crée alors le domaine
de recherche, de réflexion et d’investigation «&#x202F;%%Environnemental%%&#x202F;». Un
cycle de sept entretiens sur les grands problèmes urbains (1978-1979)
intitulé les «&#x202F;%%Midis de l’art urbain%%&#x202F;», ainsi que le colloque
«&#x202F;Architectes + plasticiens = une ville différente » (1979), en
représentent les premières manifestations tangibles.[^30] À partir de ce
moment, l’&#57351; favorise des contacts réguliers entre plasticiens et
architectes pour traiter de l’intégration des arts plastiques à
l’architecture et à l’urbanisme. Ce faisant, il éveille le public aux
possibilités de rendre la ville plus *sensible* en y intégrant des
œuvres d’art. «&#x202F;Plus qu’un champ de recherche, même, cette sphère
d’investigation prolonge une perception de l’art, un credo, une
conviction&#x202F;: l’art doit être intégré à la ville, voire à la vie
quotidienne des citoyens. L’art nous apprend beaucoup sur le monde qui
nous entoure.&#x202F;»[^31]

Depuis la fondation de l’&#57348;, les programmes de cours développés
soulignent une curiosité marquée envers les arts extra-européens. Durant
les années 1980, ce champ d’investigation est spécialement prégnant. En
1981, un cycle de cours intitulé «&#x202F;Découvertes&#x202F;: culture, art, site&#x202F;» se
décline en différents volets&#x202F;: l’Inde, la Chine, l’Égypte, la Grèce. En
outre, un cours abordant la civilisation islamique est mis au programme.
L’année suivante, le même cycle de cours se décline différemment&#x202F;: la
Russie et l’art byzantin, le Japon, les pays indianisés du Sud-Est
asiatique, les États-Unis et leurs grandes collections. La civilisation
grecque est mise à l’étude. Une impressionnante liste de cultures et de
civilisations est étudiée au fil des années grâce à l’engagement de
différents professeurs, choisis en fonction de leur champ de
spécialisation. Cette tentative de présenter des productions culturelles
issues d’autres pays fait notamment écho à la célèbre exposition *Les magiciens de
la terre*[^32], qui est présentée au %%Centre Georges-Pompidou%% en 1989.
Dans ce contexte, %%Martin, Jean-Hubert|Jean-Hubert Martin%% développe un discours interculturel
en rapprochant des œuvres occidentales d’œuvres non occidentales,
généralement considérées distinctes. %%Brys-Schatan, Gita|Gita Brys-Schatan%% ne se spécialise
pas à proprement parler en arts extra-européens mais s’y intéresse
tôt et multiplie les opportunités de s’y confronter&#x202F;: voyages culturels
dédiés aux membres, séminaires menant à la découverte de cultures et
sites étrangers, expositions dévoilant des créations d’autres pays[^33],
stages. C’est dans cet esprit international que l’artiste allemand Jörg
%%Immendorff, Jörg|Immendorff%% est invité à diriger un atelier durant trois matinées et
trois soirées en 1984-1985. Étudiants et enseignants se réunissent
autour du praticien et participent à un échange artistique, pédagogique,
interfacultaire, interculturel. L’objectif de %%Brys-Schatan, Gita|Gita Brys-Schatan%% est
clair&#x202F;: «&#x202F;(…) afin de donner aux étudiants des diverses écoles d’art
la possibilité de travailler avec un plasticien étranger de renom, nous
inaugurons cette année une formule workshop&#x202F;». Dans les faits, cet
atelier constitue l’heureuse prolongation du séminaire «&#x202F;Le pinceau –
l’aérographe – le tableau – le graffiti&#x202F;», produit par Dr %%Becker, Wolfgang|Wolfgang%%
Becker (conserva&shy;teur de la %%Neue Galerie%%, Aix-la-Chapelle).
L’intervention pratique de Jörg %%Immendorff, Jörg|Immendorff%% advient donc, en toute
logique, après une analyse de la peinture des *nouveaux fauves* du début
des années 1980. En 1987, peut-être dans une dynamique similaire, Tony
%%Cragg, Tony|Cragg%% dispense à son tour un atelier de sculpture.

L’&#57349; propose plusieurs événements consacrés au film et à la vidéo, en
parallèle avec l’évolution de la production artistique.[^34] À la fin
des années 1970, tous les jeudis soirs d’août à septembre, il offre
gratuitement des projections de films consacrés aux arts plastiques dans
le %%Egmont, Parc d’|parc d’Egmont%%. Peut-être précurseur du premier festival de films sur
l’art qui voit le jour à l’automne 2000 (aujourd’hui le BAFF – %%BAFF – Brussels Art Film Festival|Brussels Art Film Festival%%), ce programme estival souligne un intérêt marqué pour
les films produits *par* et *sur* les artistes. En novembre 1978, un
séminaire de recherches intitulé «&#x202F; Cinéma non-narratif&#x202F;» s'inscrivant dans un cycle sur le cinéma indépendant, entre autres, paraît à la grille de cours. Du 11 au 18
juin 1983, l’&#57348; accueille l’un des deux volets de l’exposition
itinérante *%%Méliès, Georges|Méliès%% l’enchanteur*.[^35] La figure de Georges %%Méliès, Georges|Méliès%%,
illusionniste et pionnier du spectacle cinématographique, fait écho à
l’intérêt de %%Brys-Schatan, Gita|Gita Brys-Schatan%% pour l’interdisciplinarité et les
pratiques artistiques fédératrices. D’autant plus que son œuvre incarne
un véritable objet de recherche – une recherche technique et
conceptuelle pour lui d’abord, une recherche historique et
intellectuelle pour sa petite-fille Madeleine Malthête-%%Méliès-Malthête, Madeleine|Méliès%% et de
nombreux autres chercheurs ensuite. Quelques mois plus tard, des films
d’artistes américains et anglais sont projetés dans le contexte de «&#x202F;Les
belles heures du film d’art&#x202F;». George %%Segal, George|Segal%%, Andy Warhol, Ed %%Kienholz, Ed|Kienholz%%,
David %%Hockney, David|Hockney%% et %%Bacon, Francis|Francis Bacon%% sont à l’honneur. Des expériences
d’artistes sont présentées. *%%Gilbert & George|Gilbert%% & George par %%Gilbert & George|Gilbert%% & George* et
*KI-I, respiration espace/temps au Japon* de Toshio %%Matsumoto, Toshio|Matsumoto%% sont
également à l’affiche. Pour clore le programme, un deuxième volet
présente à nouveau le film de %%Matsumoto, Toshio|Matsumoto%%, ainsi que *%%Bouts, Dirk|Dirk Bouts%%*
d’André %%Delvaux, André|Delvaux%%. Le 18 janvier 1984, l’&#57351; invite l’historien de l’art
et professeur Alain %%Vanderhofstadt, Alain|Vanderhofstadt%% à présenter *France, tour, détour,
deux enfants*, un film de Jean-Luc %%Godard, Jean-Luc|Godard%% et Anne-Marie %%Miéville, Anne-Marie|Miéville%% produit
en 1978. Durant les douze épisodes de 26 minutes composant cette
création, on suit deux enfants d’une dizaine d’années dans leur routine
quotidienne. Dans le contexte du séminaire «&#x202F;Comme si elle (la
photographie) était née de la peinture&#x202F;», ils servent d’objet
analytique, de filtres pour déconstruire la photographie et étudier le
langage – les *dits* et les *non-dits* – de l’image. Au printemps
1984, %%Berckmans, Jean-Pierre|Jean-Pierre Berckmans%%, producteur et réalisateur de vidéo clips,
associé à la %%Dream Factory%%, propose un séminaire de trois séances. À
cette époque, la chaîne de télévision américaine %%MTV%% diffuse des vidéo
clips en continu depuis deux ans et demi, la musique est imagée au sein
des foyers privés. Les enchaînements d’images en mouvement qui
accompagnent dorénavant la musique représentent un sujet d’étude par
excellence pour l’&#57349;.

Sur le programme de l’année académique 1983-1984, %%Brys-Schatan, Gita|Gita Brys-Schatan%%
demande&#x202F;: «&#x202F;Hier… et ailleurs… fallait-il aussi être ainsi dans le
vif du vent&#x202F;?&#x202F;» Elle soulève le plaisir et la difficulté – toujours
d’actualité aujourd’hui – de saisir les tendances du moment, de se
renouveler, d’*être de son temps*. Elle enchaîne&#x202F;: «&#x202F;Des séminaires nous
apportent l’éclaircissement (ou la complexité) d’une problématique qui
nous cerne et nous concerne. Mais déjà nous jetons les ‘Pleins feux
sur…’ des moments de culture fascinants. Qui nous enseignent et nous
renouvellent. Tout comme – fidèles à nous-mêmes – nous entrons à
nouveau dans une polémique de l’actualité de l’art.&#x202F;» Est-ce que
l’examen d’images associées à des supports musicaux, au début des années
1980, peut faire partie des polémiques évoquées&#x202F;? Du 18 mai au 3 juin
1983[^36], l’&#57351; présente l’exposition *Pochettes de disques,
enluminures du XX<sup>e</sup> siècle*. Le rassemblement de pochettes de disques
dans le contexte d’une exposition est l’occasion d’accorder plus
d’attention à l’impact visuel et plastique de l’emballage de cet objet
du quotidien.[^37] Objet familier des citoyens de toutes classes sociales
et générations confondues, la pochette de disque sert un projet culturel
et pédagogique visant à sensibiliser le public le plus large possible à
l’importance du langage plastique. Fait assez rare à l’&#57349; dans les
années 1980, la recherche est déployée au sein d’une publication. Cette
dernière est divisée en trois parties. Une première partie intitulée
«&#x202F;L’œil écoute&#x202F;» regroupe des textes de %%Brys-Schatan, Gita|Gita Brys-Schatan%%, %%Borgers, Marc|Marc Borgers%%,
Luc %%Van Malderen, Luc|Van Malderen%%, Henri %%Pousseur, Henri|Pousseur%%, Clément %%Dailly, Clément|Dailly%% et Gilles %%Verlant, Gilles|Verlant%%. À la
lecture de plusieurs essais, on sent que l’image de la pochette de
disque est un sujet d’étude récent, que les auteurs écrivent leurs
observations et demeurent dans la subjectivité. Une deuxième partie
consiste en un répertoire de pochettes de disques reproduites en noir et
blanc, composé en fonction de leur qualité visuelle&#x202F;: «&#x202F;Il tend à
souligner la volonté d’impact, produite par l’usage de différents moyens
culturels et psychologiques&#x202F;: ce sont ces allusions, ces archétypes, ces
chocs affectifs ou ces agressions volontaires qui sont ici mis en
valeur. À chacun ensuite de poursuivre son enquête. »[^38] Enfin, une
troisième partie propose un historique intitulé «&#x202F;L’art d’habiller le
disque, de An %%Girard, An|Girard%% à Hipgnosis&#x202F;», signé par Jean-Patrick %%Duchesne, Jean-Patrick|Duchesne%%,
alors aspirant au Fonds de la Recherche Scientifique (%%Fonds (national) de la recherche scientifique (FNRS)|FNRS%%). Ce
dernier essai témoigne d’une véritable recherche et, à juste titre,
l’auteur est identifié comme un collaborateur scientifique dans le
colophon de l’ouvrage. Suite à cette exposition, la musique est abordée
à quelques reprises au sein du programme de cours de l’&#57348;&#x202F;: opéra,
rock des années 1950, 1960, 1970, les %%Beatles%%, %%Cage, John|John Cage%% et Pierre
%%Boulez, Pierre|Boulez%%, le jazz, etc.

L’&#57351; occupe un créneau spécifique dans le monde artistique. Il est
complémentaire par rapport aux autres structures de diffusion
culturelle. Il est avant tout un institut. Un regroupement de
professeurs, chercheurs, artistes et autres penseurs qui tentent de
dégager les enjeux culturels de leur époque au profit de différents
publics, des néophytes aux érudits. Cours d’initiation,
approfondissements de connaissances, séminaires, remises en question&#x202F;:
les activités s’adressent à tous. Il s’agit d’un lieu de formation
continue ni trop académique, ni trop expérimentale. Le principal public
visé concerne ceux qui font partie de la vie active. Des individus
détenteurs d’une formation qui souhaitent élargir leur spectre de
connaissances. Certains par intérêt sincère envers l’art et la culture,
d’autres par désir de culture générale ou par simple loisir. Ainsi
retrouve-t-on dans la grille-horaire de l’&#57349; des activités portant
sur toutes sortes de sujets. La spécialisation ne se situe pas
véritablement dans les choix de sujets, mais dans la manière de les
approcher. Indépendamment de son support, le langage plastique constitue
le centre des préoccupations. Surtout lorsqu’il imprègne le quotidien
des citoyens. La vie quotidienne intéresse %%Brys-Schatan, Gita|Gita Brys-Schatan%%. En 1981,
le sociologue Claude %%Javeau, Claude|Javeau%% dispense le cours «&#x202F;Au degré zéro de la vie
quotidienne&#x202F;: les symboles de la banalisation&#x202F;». Ils parlent alors
d’organiser une exposition ensemble, mais le projet ne voit
malheureusement pas le jour.

%%Brys-Schatan, Gita|Gita Brys-Schatan%% manifeste clairement ce positionnement. Sa structure
accueille des historiens de l’art qui peuvent s’exprimer sur l’art
contemporain du temps présent, mais elle ne fait pas partie de
l’effervescence liée au microcosme qui l’anime. Avec les avantages et
les inconvénients que cela peut apporter, elle s’inscrit en marge des
réseaux officiels. Ainsi l’&#57348; peut-il se permettre de présenter des
films expérimentaux de Georges %%Méliès, Georges|Méliès%%, Andy Warhol, %%Gilbert & George|Gilbert%% & George et
Jean-Luc %%Godard, Jean-Luc|Godard%% avant ou après une exposition didactique sur les
masques, par exemple. Présentée du 22 au 29 avril 1982, *Masque et
autres masques* se décline en trois séquences&#x202F;: le masque comme objet
ethnologique, le masque en tant qu’objet d’art et rituel, le
comportement quotidien dans nos sociétés. Bien qu’on annonce des œuvres
anciennes et contemporaines, des animations et des films, la dimension
anthropologique et sociologique du projet prend le dessus. Il est
surtout question de commedia dell’arte, d’uniformes, de folklore, de
sociétés sans écriture et de rituels, alors que le langage plastique du
masque est à l’ordre du jour.

### Une approche inclusive de l’exposition

%%Brys-Schatan, Gita|Gita Brys-Schatan%% se donne pour principale mission *d’éduquer le regard
et de sensibiliser au langage plastique.* Elle sous-entend une
hiérarchie, mais la déconstruit constamment au fil des années en
sollicitant ses publics de différentes manières. Les expositions *1&#x202F;000
bruxellois et l’art* (1979) et *La Photographie et la peinture
aujourd’hui* (1980) donnent une dimension publique à la parole de
citoyens. D’autres initiatives confondent les rôles et les
responsabilités, rapprochent les artistes, les visiteurs et les
organisateurs.

L’appel de dossiers qui est à l’origine de l’*Exposition internationale
de tampons* illustre déjà une forme d’ouverture par rapport aux
hiérarchies artistiques. L’&#57349; reçoit plus d’une centaine de
propositions d’artistes. Dans le texte d’introduction du catalogue
associé à cette exposition, %%Brys-Schatan, Gita|Gita Brys-Schatan%% écrit&#x202F;: «&#x202F;Nous avons été
surpris par la diversité, la quantité et la spontanéité des envois&#x202F;:
Australie, Océanie, Vénézuela, États-Unis, Pologne, Italie, Allemagne,
Hollande, France… Belgique, etc… Certains auteurs réputés pour avoir
exposé leurs œuvres dans les galeries les plus célèbres… d’autres
vraisemblablement peu connus.&#x202F;» Ainsi ce projet semble-t-il dévoiler une
envie de voir ce qui se passe au-delà des sphères d’artistes établis.
Il élargit la prise de parole de l’artiste.

Mené au début de l’année 1983, le projet *Ne vous mettez pas en boule,
mettez-nous en boîte* intrigue en ce sens. On sait peu de choses sur le
résultat de ce projet, mais nous en connaissons tout de même les
modalités. Une feuille divisée en douze carrés, visiblement destinée à
être découpée et assemblée en cube, invite à contribuer à une entreprise
collective&#x202F;: «&#x202F;À vous de jouer, et avec nous&#x202F;: faites des faces, faites
des cubes, faites des boîtes… imaginez, fantasmez, colorez, dessinez,
découpez, riez, photographiez, bricolez, ajustez, collez… osez&#x202F;!
Renvoyez-nous (monté ou non) le ou les cubes. Exposition, prix et verre
de l’amitié&#x202F;: jeudi 20 janvier à 18h30.&#x202F;»[^39] Cette initiative
collégiale, possiblement dédiée à nourrir les liens sociaux entre
l’équipe de l’&#57351;, ses membres, ses auditeurs et ses visiteurs, est
peut-être le prétexte d’une simple célébration de la nouvelle année
entre pairs, peut-être une occasion de remercier tout le monde. Le
caractère «&#x202F;organisé&#x202F;» de l’événement laisse croire que, soit on lui a
accordé une véritable importance, soit on singe les codes de
l’exposition. Réalisé avec un sens de l’humour et de l’autodérision
sous-entendu par son titre, ce projet sollicite la participation, le
volontarisme des visiteurs, des auditeurs, des artistes, des pairs.

Dans le même esprit, l’&#57349; annonce l’exposition *José %%Roland, José|Roland%%. Carnets,
petits cahiers…*[^40] par une affiche à découper et plier. Il invite
les visiteurs à reconstituer un carnet permettant de voir des dessins de
l’artiste et de lire des extraits de presse&#x202F;: «&#x202F;Pour reconstituer le
carnet, pliez cette face (vers l’intérieur) suivant le pointillé, puis
coupez les pages.&#x202F;» Un peu plus d’un an plus tard, l’&#57348; présente
*Travaux en cours* qui, du 10 au 20 septembre 1985, regroupe des travaux
d’étudiants inscrits dans les écoles d’art bruxelloises. Dans ce cas-là,
des inscriptions imprimées sur le carton d’invitation de l’événement
proposent au lecteur de le découper suivant des lignes pointillées et
d’y fixer une épingle au dos. L’assemblage vise la création d’un badge
permettant de «&#x202F;pénétrer sans danger dans les *Travaux en cours*&#x202F;». Ces
deux derniers exemples où le visiteur est invité à bricoler avant
d’entrer dans l’espace d’exposition sont significatifs. Faisant office
de *préparation à la visite*, les deux bricolages encouragent une
responsabilisation du visiteur envers ce qu’il s’apprête à découvrir.
Malgré leur caractère ludique et humoristique, ces initiatives peuvent
sous-entendre une réflexion sur les dispositions nécessaires à la
confrontation d’une exposition. Et si elles ne représentent en réalité
que de simples invitations à mener un geste créatif ensemble, on peut
toujours y percevoir une approche inclusive et active de l’exposition.

Dans le même esprit, les trois projets évoqués brouillent les
responsabilités des organisateurs, des artistes et des visiteurs.
Invité à produire un objet pouvant prendre place *dans* ou *autour*
des ensembles d’œuvres, le visiteur profite d’une plus grande marge de
manœuvre pour investir l’espace de monstration et peut même, dans
certains cas, accorder une dimension publique à sa *production*. Les
publics ont dorénavant la possibilité de quitter leur rôle *passif* et
de se positionner sur le même plan que *ceux qui savent*. Le travail
linéaire réalisé entre les historiens de l’art et les artistes au profit
d’un public se transforme graduellement en triangulation ; un désir de
co-construction prend forme.


<div class="header2">
<h2>L’histoire de l’art est-elle finie&#x202F;?</h2>
<p class="dates">1987-2001</p>
</div>

### Une entrée dans la postmodernité

Dès l’automne 1987[^41], l’&#57350; prépare un appel à dossiers et
l’expédie par la poste à tous les artistes[^42] de son carnet
d’adresses. C’est une invitation à prendre part à un important projet
éditorial&#x202F;: *Mémoire de l’histoire de l’art*. Un document rassemblant
des reproductions en couleurs accompagnées de leurs informations
techniques fait partie de l’envoi.[^43] «&#x202F;Avez-vous été concerné dans un
moment de votre vie par l’histoire de l’art ou par une œuvre plus
précise (parmi les 150 chefs-d’œuvre cités ci-après)&#x202F;? » Les artistes
peuvent choisir une œuvre qui résonne en eux et créer, à partir de cette
référence, une nouvelle proposition artistique. L’initiative remporte un
succès dépassant toutes les attentes. L’&#57352; sélectionne trente-six
propositions parmi les 350 candidatures reçues.

*Mémoire de l’histoire de l’art* est un ouvrage imposant par sa taille
et son contenu. Ses 317 pages cumulent, en trois parties, des essais
d’historiens de l’art, de sociologues et autres chercheurs, une
introduction à ce que signifie l’histoire de l’art, une partie où sont
regroupés d’importants chefs-d’œuvre («&#x202F;Les chefs-d’œuvre des
chefs-d’œuvre&#x202F;») et leur réactualisation par les trente-six artistes
actuels sélectionnés («&#x202F;Création et recréation&#x202F;»).[^44] Pour chacune des
citations, on retrouve une notice écrite par %%Brys-Schatan, Gita|Gita Brys-Schatan%%, parfois
un texte de l’artiste, ainsi que des photos en noir et blanc qui
positionnent la nouvelle œuvre, reproduite en couleur, dans le contexte
de la pratique de l’artiste concerné. L’œuvre citée, qualifiée d’«&#x202F;œuvre
mémoire&#x202F;», est systématiquement annoncée. Nul jeu de devinette, la
filiation est rendue évidente. Par conséquent, la prise de parole
individuelle de l’artiste se transforme en véritable dialogue ; les
subjectivités se superposent.

Cet ouvrage est la première publication de l’&#57351; à ne pas entretenir
un lien direct avec sa programmation. Il ne s’agit pas d’un catalogue,
mais d’un véritable projet éditorial. Il constitue une mise en abyme de
la vocation de l’Institut qui veille à varier les méthodes pour
transmettre la *mémoire*[^45] de l’histoire de l’art. Cours, colloques,
expositions, ateliers et centre de documentation, entre autres,
répondent à ce désir de reconstituer, ajuster et partager des pans
d’histoire de l’art. Ils génèrent en outre des points de rencontre entre
individus, cultures et pratiques. Si la copie est un mode
d’apprentissage historique pour l’artiste, la citation est une occasion
de réaliser une transaction identitaire avec un autre artiste en
s’appropriant et réinterprétant l’une de ses formes artistiques dans sa
propre pratique. Une possibilité, pour l’artiste-citant, de s’inscrire
dans une généalogie historique et artistique. Les époques et les
subjectivités se mélangent. Cela fait partie de l’ADN de l’&#57352;&#x202F;:
«&#x202F;(…) les périodes et les cultures d’un passé proche ou lointain
enracinent notre savoir et notre vision contemporains&#x202F;», écrit Gita
Brys-Schatan à l’automne 1981. Vincent %%Heymans, Vincent|Heymans%%, dans le contexte d’une
« Petite anthologie », traverse cinq siècles de littérature dédiée à l’art
avant de poser une question qui est dans l’air du temps à l’époque&#x202F;:
l’histoire de l’art est-elle finie&#x202F;? Quelques paragraphes plus loin,
l’auteur termine son historique relativement linéaire en se référant à
%%Belting, Hans|Hans Belting%% pour annoncer un passage à la postmodernité. Ce
qualificatif arrive à point nommé à la fin de l’ouvrage puisque tout ce
qui le précède est résolument *postmoderne*. L’histoire de l’art – le
passé – devient un répertoire de formes au sein duquel les artistes
contemporains peuvent puiser pour faire émerger une nouvelle création.
La subjectivité du créateur devient un critère de jugement valable et
reconnu tandis que l’hétérogénéité règne. Les artistes créent des liens
entre le passé et le présent au détriment d’une chronologie continue,
tout en opérant une mise à distance de leur œuvre-source. La citation,
l’ironie, la parodie, le pastiche et l’imitation sont d’ailleurs des
procédures artistiques postmodernes par excellence. En outre, la
question de la valeur, indissociable du postmodernisme, est également
abordée à travers l’essai de Raymonde %%Moulin, Raymonde|Moulin%%, «&#x202F;Le marché et le musée, la
constitution de la valeur artistique contemporaine&#x202F;»[^46]. Entre les
créations, aucun autre fil conducteur que le concept originel
(l’invitation à *citer* une œuvre à travers une nouvelle œuvre) dont
elles découlent. Les lecteurs sont invités à naviguer entre les époques,
les territoires et les esthétiques. Ils expérimentent le temps
relationnel et relatif entre les œuvres et les artistes.

Qu’est-ce que l’histoire de l’art&#x202F;? Comment s’écrit-elle, se
transforme-t-elle et se transmet-elle&#x202F;? À l’aube des années 1990, Gita
Brys-Schatan s’interroge&#x202F;: «&#x202F;La décade ‘90’ entre en vigueur… et les
disciplines artistiques, fidèles à leur spécificité – la
macro-sensibilité – explorent et constatent. L’&#57351; fidèle lui aussi,
mais aux exigences qu’il s’est donné dès sa création, va cette année
poursuivre à nouveau sa voie&#x202F;: ouvrir les domaines des arts visuels, en
former une trame comme un langage que l’on offre, une langue que l’on
traduit. Nous sommes toujours attentifs aux points forts de ce langage,
ainsi qu’aux nécessités exprimées par nos publics en nos
auditoires.&#x202F;»[^47] L’&#57350; est une structure où l’on fait confiance aux
artistes pour déterminer les aspects du monde qui méritent attention.
%%Brys-Schatan, Gita|Gita Brys-Schatan%% et son équipe reprennent une interrogation déjà
entamée à la fin des années 1970 en invitant Jan %%Hoet, Jan|Hoet%% à
présenter une conférence intitulée «&#x202F;L’art actuel – Présences pour
l’avenir&#x202F;», en 1990. Quel est le sens de l’art&#x202F;? Pour %%Brys-Schatan, Gita|Gita Brys-Schatan%%,
il semble qu’il n’ait jamais véritablement changé et qu’il vise toujours
à créer et révéler des liens entre les personnes, indépendamment des
époques et des territoires.


### Chantier et politique éditoriale

En 1981 déjà, %%Brys-Schatan, Gita|Gita Brys-Schatan%% introduisait une demande d’extension de
ses locaux. Elle reçoit finalement une réponse positive en 1988. Après
des années d’attente, le ministre Valmy %%Féaux, Valmy|Féaux%% accorde officiellement le
budget de rénovation et d’extension revendiqué. Peu de temps après, la
Ville de %%Bruxelles%% manifeste toutefois son intention de vendre une
partie de ses locaux et toute l’extension attendue à des promoteurs
privés&#x202F;: un marchand voisin de l’&#57351; souhaite agrandir ses espaces,
avec la complicité de l’échevin de la Ville de Bruxelles, Michel
%%Demaret, Michel|Demaret%%, tenté par le prix plus élevé du loyer.[^48] En opposition à ces
démarches, %%Brys-Schatan, Gita|Gita Brys-Schatan%% lance une pétition pour faire valoir ses
missions d’éducation permanente. Elle récolte 6&#x202F;000 signatures et les
médias relaient largement l’information. Un an plus tard, la signature
d’un bail emphytéotique entre la Ville de %%Bruxelles%% et la Communauté
française permet à l’&#57352; de conserver et d’agrandir les bâtiments
qu’il occupe déjà.

De profondes transformations structurelles et immobilières marquent la
troisième décennie de l’&#57351;. Dès 1992, %%Brys-Schatan, Gita|Gita Brys-Schatan%% et ses
collaborateurs suivent des démarches administratives concernant
l’immobilier&#x202F;: établissements et réajustements de plans, choix des
architectes entérinés par le ministre de la Communauté française,
commande de la première esquisse aux architectes, soumission de
l’esquisse à la Commission royale des %%Commission royale des monuments et sites|monuments et sites%% pour avis et
accord, approbation de l’esquisse, etc. Le bureau d’architectes %%Lelubre, Daniel|Lelubre%%
et %%Libois, Brigitte|Libois%% est sollicité. Il s’agit de moderniser l’espace, de doubler sa
superficie (1&#x202F;000 mètres carrés) et d’adapter les espaces intérieurs aux
activités qui se sont décuplées entre 1971 et 1992.

L’équipe cherche des locaux disponibles pour la location afin de
poursuivre les cycles de cours durant l’archivage et le déménagement
interne. L’auditorium est encombré. On délocalise les cours dans les
salles du %%Palais des Congrès de Bruxelles|palais des Congrès%%  de %%Bruxelles%% et du centre des
%%Centre des Riches-Claires|Riches-Claires%% en 1992-1993[^49], et dans une salle de la Maison de la
Francité en 1993-1994. Les cours sont suspendus à partir de mars
1994 en vue du début des travaux. Ce n’est toutefois que trois ans plus
tard, le 6 mars 1997, que Charles %%Picqué, Charles|Picqué%%, ministre de la Culture de la
Communauté française, fait un discours pour marquer l’ouverture du
chantier de rénovation de l’&#57350;.

Les cinq années durant lesquelles les bureaux administratifs sont
délocalisés, d’abord dans des locaux provisoires à l’&#57354; (1994-1997),
puis dans l’hôtel néoclassique %%Puccini, Hôtel|Puccini%%, un bâtiment de la Communauté
française hérité de feu la province du Brabant et situé au 294, rue
%%Royale (rue)|Royale%% (printemps 1997 – printemps 1999), marquent un ralentissement
bien relatif compte tenu de l’important travail éditorial réalisé durant
cette période. À cette époque, en plus de %%Brys-Schatan, Gita|Gita Brys-Schatan%% qui
orchestre tout, une équipe de trois personnes réalise un travail
journalistique, de recherche et d’écriture sur l’art urbain. Plus d’une
dizaine de numéros de la revue *%%Environnemental%%* sont publiés entre 1989
et 1999. Cette revue est le principal organe d’un vaste champ de
réflexion continuellement développé au fil des années.

Conscient de ce champ d’expertise développé par l’&#57351;, Charles %%Picqué, Charles|Picqué%%
annonce un concours lors de son discours d’ouverture des travaux&#x202F;: des
jeunes créateurs sont invités à concevoir une bâche pour couvrir la
façade de l’&#57352; durant le chantier. La bâche sollicitée mesure 8,5
mètres de large sur 7,2 mètres de haut. Les élèves des classes
terminales des écoles d’art de la %%Communauté française de Belgique%%,
ainsi que les jeunes artistes sortis depuis moins de cinq ans d’une de
ces écoles, peuvent participer. Un prix de 75&#x202F;000 FB (environ 1&#x202F;800
euros) est alloué à l’auteur du meilleur projet retenu par le jury. Un
trio composé de Berta %%Truyols, Berta|Truyols%%, %%Baltus, Michaël|Michaël Baltus%% et Charles %%Kaisin, Charles|Kaisin%% remporte
le concours et accroche sur la façade des milliers de petites
bandelettes de plastique de différentes couleurs en 1997. Les travaux,
devant initialement durer 200 jours ouvrables, prennent fin en janvier
1998. Le 16 janvier 1998, %%Brys-Schatan, Gita|Gita Brys-Schatan%% prononce un discours qui
marque la fin du chantier. Elle en profite pour annoncer le redémarrage
des activités un an plus tard, après emménagement et mise en place d’une
programmation.

### Extension

La fondatrice et directrice de l’&#57350;, négociatrice hors-pair, réussit
à doubler le nombre de ses collaborateurs en obtenant un certain nombre
de postes liés au statut d’agent contractuel subventionné (ACS). Le
deuxième mandat (1995-1999) de Charles Picqué, en qualité de
ministre-Président de la %%Région de Bruxelles-Capitale/Région bruxelloise|Région bruxelloise%%, aura permis à l’&#57352; de
prendre de l’ampleur. %%Brys-Schatan, Gita|Gita Brys-Schatan%% remercie son équipe le soir du
vernissage de l’exposition *Liberté, libertés chéries ou l’art comme
résistance… à l’art&#x202F;: un regard posé sur dix années d’acquisitions de
la %%Communauté française de Belgique%%. 1989-1998*. Durant les trois années de délocalisation des
bureaux, cette équipe se consacre à des publications (revue
*%%Environnemental%%*), restructure le centre de documentation, la diathèque
et la gestion de fichiers. Elle prépare également des programmes de
cours et des expositions pour anticiper son retour dans ses espaces
rénovés. Au sein de l’équipe renforcée, cette période de transition ne
se fait pas sans remous. %%Brys-Schatan, Gita|Gita Brys-Schatan%% est une femme passionnée.
Elle ne compte pas ses heures de travail et n’établit pas de frontière
claire entre sa vie professionnelle et sa vie personnelle. On ne crée
pas une structure du tout au tout comme on accepte un emploi de salarié.
Or, l’effervescence engendrée par la création de politiques culturelles
au début des années 1970 – notamment grâce à la création de la
Commission française de la Culture de l’Agglomération de %%Bruxelles%% –
s’amenuise à l’aube des années 2000. D’autant plus que la quantité de
structures culturelles bruxelloises augmente. Les métiers artistiques
s’institutionnalisent[^52] et se professionnalisent continuellement. Du
côté du monde de l’emploi, à l’&#57350; comme au sein d’autres institutions
culturelles, une prise de conscience modifie le rapport au travail.
L’absence de règlement du travail devient un sujet sensible. L’histoire
des trente premières années de l’&#57354; raconte, outre l’évolution d’un
microcosme artistique, les difficultés inhérentes à la gestion d’un
espace de diffusion du secteur non marchand. Le jour de l’emménagement
dans les nouveaux locaux arrive malgré de nombreux reports. La tâche est
colossale, mais l’équipe s’y affaire sans soutien extérieur.
Une fois encore, empaquetage, location de camionnette, déplacement des boîtes,
emménagement. L’équipe de professionnels a des airs de famille. Les
activités reprennent au 31 %%Waterloo (boulevard de)|boulevard de Waterloo%%, dans des locaux
transformés, en janvier 1999.

Dorénavant, l’équipe de l’&#57351; bénéficie d’une salle de cours, d’un
double espace d’exposition, d’un centre de documentation, d’une
galerie d’art (%%Galerie-Découverte%%), d’une vitrine d’objets et de livres
d’art (Rayon Art), d’un café-restaurant (*%%Au Passage de Milan%%*), de
nouveaux bureaux, d’une salle de réunion et de nombreux espaces
techniques. La superficie des espaces, atteignant maintenant 1&#x202F;000 mètres
carrés, a littéralement doublé grâce à l’installation d’une passerelle
qui relie les espaces initiaux du bâtiment (attenants au %%Egmont, Parc d’|parc d’Egmont%%)
et l’espace anciennement occupé par le sculpteur Georges %%Dobbels, Georges|Dobbels%% (à
proximité du %%Waterloo (boulevard de)|boulevard de Waterloo%%). %%Brys-Schatan, Gita|Gita Brys-Schatan%% profite du
réaménagement et de l’agrandissement de ses espaces pour réactualiser
ses activités. La %%Galerie-Découverte%% et %%Galerie Découverte%% et %%Rayon Art/Objets|Rayon Art/Objets%% se
substituent à l’Espace Éphémère. Les noms sont différents, mais les
principes sont similaires&#x202F;: on y découvre des œuvres d’artistes
contemporains belges et étrangers qui ne sont pas encore connus du grand
public. La %%Galerie-Découverte%% se concentre sur les beaux-arts, tandis
que %%Galerie Découverte%% et %%Rayon Art/Objets|Rayon Art/Objets%% se concentre sur les arts décoratifs (bijoux et
autres petits objets produits à partir d’arts du feu – céramique,
verre, porcelaine, etc.). Indépendamment de leur statut, les œuvres sont
mises en vente. %%Rayon Art/Librairie%%, par ailleurs, est un tout nouveau
concept pour l’&#57350;. Il s’agit d’une librairie spécialisée en art
dirigée par Éric %%Van Essche, Éric|Van Essche%%[^54] et installée dans l’espace d’entrée du
%%Waterloo (boulevard de)|boulevard de Waterloo%%. Mille cinq cents livres d’art – et sur l’art – sont
répartis sur deux longues bibliothèques murales. Depuis trente ans,
l’&#57352; est un lieu de rencontre. Il mélange artistes et professeurs
d’ici et d’ailleurs avec des publics en tous genres autour d’enjeux
artistiques, culturels et sociétaux. Le réaménagement des espaces permet
de renforcer la convivialité de l’Institut puisque les visiteurs peuvent
désormais flâner dans un espace polyvalent où ils peuvent se restaurer,
bouquiner et découvrir une œuvre. Un concept inédit à Bruxelles, à
l’époque.

À l’issue de cette deuxième phase de travaux, le bâtiment de l’&#57350; est
bonifié d’une œuvre de Joseph %%Kosuth, Joseph|Kosuth%% grâce au décret relatif à
l’intégration d’œuvres d’art dans les bâtiments publics.[^55] En
contact avec l’artiste depuis 1995, %%Brys-Schatan, Gita|Gita Brys-Schatan%% propose l’œuvre *A
Map with 13 Points* à la Commission artistique de la Communauté
française, qui l’accepte et la commande. Un assistant de %%Kosuth, Joseph|Kosuth%% se rend
à l’&#57351; et grave treize citations d’artistes célèbres sur les vitres
du bâtiment, le long du %%Milan, passage de|passage de Milan%%. Ces citations traitent d’art
et de relation à l’art.[^56] Elles font écho à la vocation pédagogique,
artistique et relationnelle de l’&#57352;. Elles rappellent en outre le
profond intérêt envers le langage et le discours qui anime Gita
Brys-Schatan. L’intégration de cette œuvre dans le bâtiment de l’&#57351;
est cohérente en regard de son histoire, même avec vingt ans de recul.
Les citations de paroles d’artistes font d’ailleurs écho aux citations
artistiques reproduites dans la publication *Mémoire de l’histoire de
l’art* qui, dix ans auparavant, faisait entrer l’&#57350; dans la
postmodernité.

L’&#57352; reprend ses cours à l’automne 1999. Nouveaux espaces, nouveau
site internet[^57] (novembre 2000), nouveau logo. Un design graphique
légèrement différent accompagne un programme académique qui véhicule des
motivations intactes. «&#x202F;C’est en s’intéressant de près à l’œuvre des
artistes que l’on apprend de nouvelles manières de regarder le monde,
que l’on aiguise une perception rabotée par la routine quotidienne… On
se donne ainsi une immense forme de liberté.&#x202F;»[^58], écrit Gita
Brys-Schatan. Elle réitère son parti-pris. Les cours divisés en trois
niveaux (initiation, approfondissement, remise en question) traversent
toujours un large éventail de sujets et de temporalités. Une séance
académique de réouverture entame l’année académique 1999-2000. Le 24
septembre 1999, exceptionnellement au %%Egmont, palais d’|palais d’Egmont%%, divers
intervenants prennent la parole. Le président du conseil
d’administration de l’&#57351;, Ilya %%Prigogine, Ilya|Prigogine%%, situe l’Institut dans le
contexte de la «&#x202F;culture culturante&#x202F;». Guy %%Haarscher, Guy|Haarscher%% (doyen de la
faculté de philosophie et lettres de l’%%Université libre de Bruxelles (ULB)|ULB%%) prononce une conférence
intitulée «&#x202F;Vers une véritable université des droits de l’homme&#x202F;».
Joseph %%Kosuth, Joseph|Kosuth%% témoigne de l’art en situation et Johan %%Muyle, Johan|Muyle%% évoque l’art
actuel interculturel. Enfin, Thierry %%Genicot, Thierry|Genicot%%, journaliste à la %%RTBF/RTB|RTBF%%,
évoque l’action de l’&#57350;. Ces cinq interlocuteurs définissent
l’Institut directement ou indirectement&#x202F;: une école ouverte à tous où
l’on peut fréquenter des œuvres, et spécialement réfléchir à l’art
urbain et l’art extra-européen, et à leurs enjeux. Cette séance
académique se prolonge par une collation dinatoire dans les locaux
rénovés de l’&#57352;, ainsi que par le vernissage de l’exposition *Côté
parc, côté boulevard, l’ISELP, un chemin pour les arts*. À l’aube du
trentième anniversaire, ce projet ouvertement commémoratif rappelle les
points forts de l’institution culturelle bruxelloise. Du 25 septembre au
9 octobre 1999, les visiteurs peuvent voir des documents extraits des
archives de l’&#57351; qui témoignent de son passé. On y voit des
photographies prises lors des cours, des reproductions de pages
couvertures de la revue *%%Environnemental%%*, des cartons d’invitation et
des affiches d’expositions telles que *Art et ordinateur* (1974) ou
*Masque et autres masques* (1982), entre autres, mis en espace par Winston %%Spriet, Winston|Spriet%%. Il
n’est d’ailleurs pas anodin que Paul %%Gonze, Paul|Gonze%% ait réinstallé son
œuvre *Rêve de voyance ou de cécité* dans le %%Egmont, Parc d’|parc d’Egmont%% à cette
occasion. Son rayon laser animant la statue du prince de Ligne produite
par John %%Cluysenaar, John|Cluysenaar%% en 1935, rappelle un moment clé :
l’exposition *Néon, fluor et Cie.* (1984).[^59]

*Le Réservoir d’idées*, exposition et publication, est un projet imaginé
par %%Brys-Schatan, Gita|Gita Brys-Schatan%% pour accorder une visibilité publique à des idées
qui sont restées à l’état de projet. Une soixantaine d’artistes belges
et internationaux exposent des plans, des dessins, des maquettes et des
photomontages de projets d’art environnemental du 21 octobre au 13
novembre 1999. Cette exposition marque la fin d’une tendance didactique
et documentaire qui imprègne les expositions organisées par la
fondatrice de l’&#57352; depuis *Art et ordinateur*, en 1974.

### Une nouvelle approche de l’exposition

Durant les années 1970, le %%Palais des Beaux-Arts (BOZAR)|palais des Beaux-Arts%% est l’une des rares
structures publiques bruxelloises présentant des œuvres contemporaines.
Yves %%Gevaert, Yves|Gevaert%%, d’abord comme collaborateur de la %%Société des expositions%%
(1968-1972) puis comme codirecteur aux côtés de %%Bertouille, Suzanne|Suzanne Bertouille%%
(1972-1975), introduit au sein de l’institution une pléiade de jeunes
pratiques, souvent conceptuelles. Karel %%Geirlandt, Karel|Geirlandt%%, à titre de directeur
de la %%Société des expositions%% (1974-1986) inscrit et maintient le palais
des Beaux-Arts dans un réseau de circulation d’œuvres actuelles et
internationales.[^60] Sous l’impulsion de Philippe %%Roberts-Jones, Philippe|Roberts-Jones%%, les
%%Musées royaux des Beaux-Arts de Belgique|Musées royaux des Beaux-Arts%% inaugurent le %%Musée d’Art moderne de Bruxelles|musée d’Art moderne%%
(1962-1978, 1984-2011) qui, tout comme le %%Musée d’Ixelles|musée d’Ixelles%%, inclut
ponctuellement des œuvres d’artistes émergents dans son programme
d’expositions. Jean-Louis %%Godefroid, Jean-Louis|Godefroid%% fonde l’ASBL bruxelloise %%Contretype%%
en 1978. Cette initiative le mène à exposer les travaux de jeunes
photographes dans son appartement, jusqu’à ce qu’il investisse l’hôtel
de maître Art Nouveau d’Édouard %%Hannon, Édouard|Hannon%% en 1988. Face à ces différentes
entités, l’&#57350; a une identité distincte et complémentaire, bien que la
création de %%Contretype%% rappelle celle de son Espace Éphémère.

La scène bruxelloise évolue durant les années 1980. Le Centre d’art
contemporain, ouvert par la %%Fédération Wallonie-Bruxelles|Fédération Wallonie-Bruxelles %% en 1983 et
dédié à la présentation d’expositions d’art contemporain, est actif
jusqu’en 2004. Sa directrice, %%Dumont, Fabienne|Fabienne Dumont%%, s’intéresse au milieu
artistique belge à travers l’organisation d’expositions collectives et
d’échanges d’artistes internationaux. Elle crée notamment le cycle
d’expositions *Parcours*, destiné à donner de la visibilité aux jeunes
artistes qui sortent des écoles d’art bruxelloises %%Erg — ESA|(Erg,%% %%Le 75 — ESA|le 75%%, l’%%Académie royale des Beaux-Arts de Bruxelles (ArBA — ESA)|ArBA%%,
%%Saint-Luc — ESA|Saint-Luc%%, à tour de rôle). Le centre culturel du %%Botanique%% propose
également par moments des productions émergentes à partir de sa création
en 1984. %%ARGOS ASBL|ARGOS%% à partir de 1989. Les artistes Alec %%De Busschère, Alec|De Busschère%%,
%%Bedel, Delphine|Delphine Bedel%%, Christophe %%Draeger, Christophe|Draeger%% et Patrick %%Everaert, Patrick|Everaert%% créent l’espace
de diffusion %%Établissement d’en face|Établissement%% d’en face en 1991. La création de ce premier
centre d’artistes autogéré (*artist-run space*) bruxellois,
non institutionnel et non commercial, contribue à la visibilité des
jeunes artistes. Tandis qu’on assiste à l’élargissement, la
multiplication et la diversification des réseaux voués à la création
artistique contemporaine dans plusieurs villes du monde occidental,
%%Bruxelles%% entame lentement sa prise de position. À défaut de profiter
d’un musée dédié à l’art contemporain, les créateurs trouvent des
alternatives. L’évènement %%Bruxelles|*Bruxelles*%% *2000, capitale culturelle européenne*
engage un important investissement financier dans une rénovation
urbaine. Cette valorisation rayonne sur le secteur culturel qui, à cette
occasion, présente 1&#x202F;500 projets en plus de la %%Zinneke Parade%%. La
manifestation semble agir comme une étincelle au sein d’un foyer de
créateurs en tous genres, dont les initiatives métamorphosent
durablement le tissu urbain et le secteur culturel. Le collectif de
curateurs %%Komplot%% est créé en 2002. Un nouveau modèle de structure voit
le jour et diversifie le secteur. La dynamique est en train de changer,
un nouvel équilibre est à trouver.

L’&#57354; est profondément affecté par la transformation du milieu de
l’art contemporain bruxellois. Bien qu’elle se déploie lentement, cette
mutation l’oblige à renforcer son positionnement pour faire face à de
nouveaux enjeux. L’année académique 1999-2000 marque une prise
d’importance des expositions. Pour la première fois, elles sont
annoncées dans le programme de cours. Ce même programme annonce en
outre&#x202F;: «&#x202F;Nos expositions se positionnent déjà en 99 et augmenteront
encore leur impact dans les temps qui viennent.&#x202F;» L’approche
documentaire et didactique de %%Brys-Schatan, Gita|Gita Brys-Schatan%% est substituée par une
sensibilité accrue envers la singularité des démarches artistiques.
L’exposition quitte la sphère du livre et de l’étude pour celle de
l’expérience esthétique. Un nouveau paradigme de l’exposition est
d’ailleurs annoncé lors de *Find*, qui rassemble des créations de design
finlandais du 2 au 29 février 2000&#x202F;: «&#x202F;Par cette démarche volontariste,
*Find* veut donner une nouvelle impulsion au concept même d’exposition&#x202F;:
l’exposition doit faire avancer la création.&#x202F;» Que ce soit grâce aux
nouveaux espaces de l’&#57352; ou à une tendance générale observée dans le
milieu artistique, %%Brys-Schatan, Gita|Gita Brys-Schatan%% modifie son approche de
l’exposition. Elle accorde dorénavant plus de place aux expositions
individuelles, qu’elle avait délaissées depuis l’abandon de l’Espace
Éphémère, en 1985.

Les projets durent toujours aussi peu de temps, un mois tout au plus.
Une hiérarchie s’établit entre les espaces suivant l’entrée du 31
%%Waterloo (boulevard de)|boulevard de Waterloo%% et les grandes salles visibles depuis les
mezzanines et la passerelle, un étage plus bas, au niveau du passage de
Milan. Plus expérimental, l’espace du haut, la %%Galerie-Découverte%%,
accueille des œuvres d’artistes émergents. Les salles inférieures
accueillent plutôt des œuvres d’artistes connus, du moins établis. Elles
sont inaugurées par une exposition d’œuvres d’%%Axell, Evelyne|Evelyne Axell%% (1935-1972)
initiée par le réalisateur et époux de l’artiste, %%Antoine, Jean|Jean Antoine%%. Organisée
par %%Brys-Schatan, Gita|Gita Brys-Schatan%%, avec la collaboration du %%Musée d’Ixelles|musée d’Ixelles%%,
*Mémoire de Bacchante* est présentée du 9 au 30 juin 2000.

Quelques mois auparavant, l’&#57351; accueillait une dizaine de
conférenciers francophones dans le contexte d’un cycle de cours de
quarante heures intitulé «&#x202F;Messagerie de l’art contemporain. Dakar -
Haïti / %%Lausanne%% - Ouagadougou / Lyon - Le Caire / Yaoundé - Montréal /
Cotonou - Alger&#x202F;: le train de l’art contemporain roule en francophonie,
1<sup>ère</sup> étape&#x202F;». Ils avaient la mission d’analyser les particularités des
œuvres produites par des artistes de différentes nationalités
s’exprimant en français. Historiens de l’art, artistes et journalistes se
succédaient pour examiner la corrélation entre le langage verbal et la
création artistique. Ce cycle de cours incarne la base scientifique
d’une exposition présentée à la fin de l’année&#x202F;: *Messagerie de l’art
contemporain. Le train de l’art contemporain roule en francophonie. 2<sup>e</sup>
étape*[^61], présentée du 9 novembre au 6 décembre 2000. Cette
entreprise représente un véritable tour de force compte tenu du budget
disponible pour l’organiser. Elle rassemble des œuvres trouvées en
Belgique, bien qu’elles soient généralement produites par des artistes
de nationalité étrangère, par exemple. Paradoxalement locale et
internationale, cette exposition interroge le rôle de la langue
véhiculaire dans la culture d’un pays, soulève des pistes de réflexion
sur la spécificité culturelle et la globalisation de la création
contemporaine. Elle s’inscrit dans l’événement « Le tour du monde
francophone en 80 villes&#x202F;», organisé par la Commission communautaire
française, à l’occasion de *&#x202F;%%Bruxelles 2000%%&#x202F;*. Avec cette exposition,
%%Brys-Schatan, Gita|Gita Brys-Schatan%% réaffirme plus directement son intérêt pour les
cultures non occidentales et leurs pratiques artistiques et remarque que
c’est dans l’air du temps&#x202F;: «&#x202F;Auparavant, certains n’hésitaient pas à
classer l’art non occidental dans le tiroir de l’anthropologie plutôt
que dans celui de l’art contemporain. Aujourd’hui, les créateurs
contemporains non européens commencent à être appréciés à leur juste
valeur, au même titre que les artistes occidentaux. Une nouvelle preuve
nous en est donnée au travers de cette exposition.&#x202F;»[^62]

En 2001, %%Brys-Schatan, Gita|Gita Brys-Schatan%% initie un cycle de six expositions et
publications monographiques intitulées «&#x202F;Avoir lieu – Être au fond&#x202F;».
Elle sélectionne six artistes qui recourent à l’installation artistique
et les invite à créer des œuvres autour du sens profond des verbes
*avoir* et *être*. Les artistes Bob %%Verschueren, Bob|Verschueren%%, Johan %%Muyle, Johan|Muyle%% et Philippe
%%Decelle, Philippe|Decelle%% sont associés au sous-titre «&#x202F;Avoir lieu&#x202F;», tandis que Paul
%%Gonze, Paul|Gonze%%, Michel %%Mouffe, Michel|Mouffe%% et %%Carez, Christian|Christian Carez%%, sont associés au sous-titre
«&#x202F;Être au fond&#x202F;». Remises en contexte au sein des publications, les œuvres
offrent une expérience sensible dans les salles d’exposition.

Respectant profondément les publics auxquels elle s’adresse, Gita
Brys-Schatan consulte régulièrement les auditeurs et les visiteurs.
Sollicitations de commentaires sur la programmation, acceptations de
propositions de sujets d’étude, enquêtes collectives menant à des études
sociologiques&#x202F;: les méthodes sont variées. Dans le contexte de la série
«&#x202F;Avoir lieu – Être au fond&#x202F;», elle se met à risque en sollicitant des
avis sur son concept d’exposition. «&#x202F;Que vous inspirent ces deux
concepts&#x202F;? Avoir lieu… Être, au fond. Ces deux concepts vont
successivement stimuler la création de six artistes ; que peuvent-ils
inspirer au public, à ceux qui investiront le lieu de création&#x202F;?&#x202F;»[^63]
Les témoignages et les opinions recueillis sont intégrés dans le contenu
des publications ; les publics ont un rôle actif. À travers cette
démarche, %%Brys-Schatan, Gita|Gita Brys-Schatan%% manifeste son attachement pour l’être
humain et son sens critique.

Présentée à l’&#57350; du 26 octobre au 24 novembre 2001, l’exposition *Le
Clonage d’Adam* rassemble une cinquantaine d’œuvres cernant des
problématiques liées au corps humain. Les représentations et les
métamorphoses du corps se structurent selon trois axes&#x202F;: l’automate, la
poupée, le clone. On y retrouve notamment des œuvres d’%%Orlan%%, de
Philippe %%Druillet, Philippe|Druillet%%, de %%Gilbert & George|Gilbert%% & George, de %%Lang, Fritz|Fritz Lang%% et de Niki de
%%de Saint-Phalle, Niki|Saint-Phalle%%. Pour l’une des premières fois, les communications autour
de l’exposition sont intimement liées à un programme de conférences et
de performances. Le 15 novembre, Dr Robert %%Schoysman, Robert|Schoysman%%, spécialiste en
génétique, propose une conférence, et les jumelles hermaphrodites
berlinoises %%Eva & Adele%% réalisent une performance. Ces dernières se
retrouvent également autour d’une table ronde intitulée «&#x202F;Art et
clonage&#x202F;», en compagnie de %%Durant, Ben|Ben Durant%%, Michel %%Clerbois, Michel|Clerbois%%, Gita %%Brys-Schatan, Gita|Brys-Schatan%% (les commissaires de l’exposition), et du sociologue Daniel %%Vander Gucht, Daniel|Vander Gucht%%.


<div class="header2">
<h2>De l’initiative privée à l’institution publique</h2>
<p class="dates">2002-2009</p>
</div>

### Expositions et programme scientifique

Durant trente ans, la personnalité et les convictions de Gita
Brys-Schatan imprègnent toutes les activités menées *à*, *par* et *avec*
l’&#57353;. Trois décennies durant lesquelles elle maintient la structure à
bout de bras. Elle négocie durement lorsque son projet nécessite des
ressources supplémentaires pour s’épanouir. Elle sait travailler en
réseau et identifier les soutiens potentiels. Le maintien d’une toute
petite équipe de travail jusqu’à la fin de années 1990 permet une
gestion collégiale et une cohérence dans la programmation.

Lors de son départ en 2002, la fondatrice et directrice lègue un
héritage conséquent. La petite structure privée est devenue une
véritable institution publique. Des subsides (Communauté française,
%%Commission communautaire française (COCOF)|COCOF%%, %%Actiris%%, %%Loterie nationale%%, etc.), des mécènes (%%Eeckman Art & Insurance|Eeckman%% Art &
Insurance) et des philanthropes (de nombreux membres) sont engagés dans
le projet, et ces revenus doivent être justifiés par une programmation
conséquente en termes de quantité et qualité d’événements. Le duo formé
par Arlette %%Lemonnier, Arlette|Lemonnier%% et Éric %%Van Essche, Éric|Van Essche%% fait face à un défi. Il est à
la tête d’un puissant outil mais doit se l’approprier et le définir
en fonction d’un monde en constante évolution.

### Une nouvelle génération s’engage

Arlette %%Lemonnier, Arlette|Lemonnier%% prend la direction de l’&#57354; le 1<sup>er</sup> août 2002.
Sociologue agrégée et collectionneuse d’art, elle a plusieurs types
d’expériences professionnelles à son actif. Entre autres, dix ans de
recherche démographique à l’%%Université libre de Bruxelles (ULB)|ULB%%, dix-huit ans d’implication au sein de
Présence et action culturelles (%%Présence et action culturelles (PAC)|PAC%%), un mouvement d’éducation
permanente. Autodidacte, elle a une formation de terrain en art
contemporain, nourrie d’histoire de l’art, de visites d’ateliers
d’artistes, de vernissages, ainsi que de lectures d’essais. Elle est
membre de la %%Commission consultative des arts plastiques%% durant deux ans
et critique d’art pour *%%Art et Culture%%*.

Éric %%Van Essche, Éric|Van Essche%% s’est joint à l’&#57353; trois ans auparavant avec son
concept %%Rayon Art/Librairie%%. En dotant %%Bruxelles%% d’une structure qui
existe au cœur d’autres villes, il inscrit l’&#57354; dans un mouvement
novateur. L'Institut devient un organisme culturel polyvalent où l’on peut
se restaurer, bouquiner, voir des œuvres.

Le binôme identifie rapidement un mode de travail viable. Directrice
générale, Arlette %%Lemonnier, Arlette|Lemonnier%% assure la gestion financière du bâtiment et
du personnel – alors composé de treize employés –, ainsi que des
expositions. Directeur scientifique, Éric %%Van Essche, Éric|Van Essche%% gère la
programmation des cours donnés par une quinzaine de professeurs, les
colloques, les grandes conférences et les voyages culturels. Cette
direction bicéphale apporte quelques modifications au fonctionnement de
l’&#57353;. Elle établit un règlement de travail et installe une pointeuse
pour un comptage des heures de travail plus transparent. Restructuré, le
travail est distribué en cellules correspondant aux champs d’activités&#x202F;:
cellule Communication, cellule Internet, cellule %%Galerie-Découverte%% et
%%Galerie Découverte%% et %%Rayon Art/Objets|Rayon Art/Objets%%, cellule %%Rayon Art/Librairie%%, cellule
Voyages-découvertes. Les employés sont associés à des tâches
spécifiques mais se retrouvent lors des montages et des démontages pour
des tâches communes. Parfois, une graphiste se retrouve en salopette
pour peindre, un historien de l’art recoud le textile d’une œuvre, une
secrétaire emballe ou déballe une œuvre. Grâce à une clause inscrite
dans le contrat des employés, les expositions constituent un vecteur de
cohésion au sein de l’équipe. En outre, la liste des membres du
personnel de l’&#57354; est publiée sur le programme annuel.
L’individualité des membres du personnel ne s’efface plus derrière la
vision d’une direction, on assiste à l’émergence d’une somme de
personnalités contribuant au développement d’un projet commun.

%%Brys-Schatan, Gita|Gita Brys-Schatan%% quitte la direction le 31 juillet 2002 mais demeure
membre du conseil d’administration de l’ASBL. Elle poursuit la
programmation des expositions prévues jusqu’à la fin de l’automne 2002.
Un an plus tard, elle signe le document attestant la succession d’Ilya
%%Prigogine, Ilya|Prigogine%% par Michel Van der Stichele à la présidence du conseil
d’administration. Inspecteur général des Finances puis directeur général
à la %%Région de Bruxelles-Capitale/Région bruxelloise|Région de Bruxelles-Capitale%%, Michel Van der Stichele s’est
toujours intéressé à la gestion des institutions culturelles. À
l’époque, il préside notamment les conseils d’administration du
%%Botanique%% %%Botanique|Centre Culturel de la Fédération Wallonie-Bruxelles%%, et du
%%Cirque Royal%%. Le baron %%Urvater, Joseph-Berthold|Urvater%%, qui est administrateur de l’&#57353; depuis
le tout début, décède la même année. Une nouvelle génération s’engage.

### L’art contemporain à travers le spectre de l’exposition

L’&#57354; se repositionne au sein d’un milieu artistique en profonde et
continuelle transformation depuis 2000&#x202F;: de plus en plus perçue comme un
*médium*, l’organisation d’expositions prend de l’importance. Tristan
%%Trémeau, Tristan|Trémeau%% aborde cette nouvelle réalité lors d’une conférence intitulée
«&#x202F;De la naissance de la valeur d’exposition à la substitution de l’œuvre
par l’exposition&#x202F;: questions esthétiques posées par une mutation, de
%%Benjamin, Walter|Walter Benjamin%% à aujourd’hui&#x202F;»[^64], en 2003. Il exprime en partie ces
changements dans la capitale. À partir de 2005, le concours annuel %%ArtContest%%[^65]
encourage les artistes de moins de 35 ans. La Ville de Bruxelles,
souhaitant avoir son propre organisme pour organiser des expositions,
crée la %%Centrale électrique%% en 2006. Cette structure accueille trois
expositions temporaires par année. Six ans plus tard, elle précise son
identité sur la scène culturelle belge et internationale pour s’adapter
au microcosme de l’art contemporain. Elle devient la Centrale for Contemporary Art en 2012. Ce changement se traduit par une nouvelle
identité graphique et une programmation mettant les artistes belges à
l’honneur, tout en les plaçant dans une perspective internationale. Le
Wiels ouvre ses portes en 2007. Cette structure entame ses activités
avec des méthodes de travail professionnelles de pointe, adaptées aux
enjeux du secteur. La Loge ouvre ses portes en 2012 et diversifie encore
davantage l’offre d’expositions d’art contemporain. Les
*artist-run spaces* se multiplient, de nombreuses galeries commerciales
ouvrent leurs portes ou installent une antenne à %%Bruxelles%%. La Fondation
%%Fondation Thalie|Thalie%% voit le jour en 2014, la %%Patinoire royale%% et %%Société|Société en 2015%%. Le
rythme s’accélère. La signature des organisateurs d’exposition devient de plus en plus importante, la singularité des
projets est recherchée. Entretemps, face à la forte concurrence tant
pour les lieux de diffusion qui peinent à maintenir leur singularité et
leur complémentarité que pour les artistes qui tentent de se démarquer,
il devient aussi important de savoir présenter son travail que de le
réaliser.

Si les expositions constituent une activité subsidiaire par rapport aux
cours dispensés par l’&#57353; entre 1974 et 2002, ce n’est définitivement
plus le cas par la suite. Arlette %%Lemonnier, Arlette|Lemonnier%% se joint à l’équipe avec
l’intention de programmer et développer des expositions d’art
contemporain. Face à l’irrégularité qui régnait auparavant dans ce champ
d’activité, notamment due au manque de locaux disponibles, elle a carte
blanche. Ce faisant, elle insuffle une énorme dynamique à l’&#57353;. Les
espaces et la hiérarchie entre «&#x202F;les grandes salles&#x202F;»,
la %%Galerie-Découverte%% et le %%Galerie Découverte%% et %%Rayon Art/Objets|Rayon Art/Objets%% sont maintenus sous sa
direction. À partir de 2003, le programme d’expositions qui concerne les
grandes salles de l’&#57354; se structure en faveur de trois modalités,
même si celles-ci connaissent des exceptions. Chaque année, il se
compose désormais d’une exposition individuelle, d’une exposition
collective et d’une exposition liée à un cycle de cours, une actualité
artistique ou une collaboration. Dans ces trois cas, une périodicité de
deux mois est adoptée et les jeunes artistes de la Communauté française
sont privilégiés.

Les grandes salles accueillent une exposition monographique par année.
Un artiste y rassemble des œuvres inédites, produites en fonction de
l’architecture intérieure. L’&#57353; n’est pas un *cube blanc*. Les murs
sont coupés à intervalles réguliers par des pilastres qui multiplient
les recoins. Les fenêtres, les verrières et les portes constituent
autant d’obstacles aux murs pleins. Traversée par une passerelle et
bordée de mezzanines de part et d’autre, la salle principale peut
accueillir de très grandes pièces compte tenu de sa hauteur
impressionnante. Arlette %%Lemonnier, Arlette|Lemonnier%% choisit de tirer parti de ce
magnifique espace, exempt de neutralité.

En 2002, elle découvre le travail de Guillaume %%Liffran, Guillaume|Liffran%% lors d’une exposition à la %%Galerie Gabriel Brachot%%. Le jeune artiste, alors âgé de
27 ans, partage son intention de créer des sculptures qui déclinent la forme du pylône électrique. Séduite par son projet, la nouvelle
directrice lui propose une collaboration et développe *Guillaume %%Liffran, Guillaume|Liffran%%&#x202F;: Electric* (21 février au 5 avril 2003). Sculptures et dessins sont
dispersés dans les salles. La pièce maîtresse est ambitieuse&#x202F;: une tour dont la base carrée repose sur le sol de la grande salle et le faîte
surplombe largement la passerelle. Trois façades sur quatre sont fermées par des peaux. Créée spécifiquement pour l’exposition, *Enfermement I*
impressionne par ses 5,70 mètres de hauteur. Outre ce que l’artiste souhaite y insuffler comme sens, la référence physique et graphique au
pylône électrique rappelle que les murs de l’&#57353; ont déjà abrité une sous-station d’électricité de la Ville de %%Bruxelles%%. Un passé qui se
manifeste par la conservation d’un pont roulant qui traverse une partie des salles d’exposition. Elle rappelle en outre
l’intérêt que l’&#57354; manifeste envers l’espace et le mobilier publics dans toutes leurs facettes. Par la suite, %%Cardoen, Philippe|Philippe Cardoen%% (*Gises*,
2004) expose une trentaine de sculptures en acier brut (corrodées partiellement et traitées au noir), pesant entre 10 et 150 kg chacune,
au centre de la salle&#x202F;; Bénédicte %%Henderick, Bénédicte|Henderick%% (*Lætitia B&#x202F;: autopsie*, 2006) installe une immense sculpture tubulaire qui occupe une bonne
partie de la salle et s’élève pratiquement jusqu’au plafond. Christian %%Rolet, Christian|Rolet%%, Michel %%Clerbois, Michel|Clerbois%%, %%Bornain, Alain|Alain Bornain%%, Didier %%Mahieu, Didier|Mahieu%%, Bernard %%Gaube, Bernard|Gaube%%,
Sylvie %%Macías Díaz, Sylvie|Macías Díaz%% et %%Carez, Christian|Christian Carez%%, avec la collaboration d’Arlette %%Lemonnier, Arlette|Lemonnier%%, composent à leur tour avec cette architecture qui fait
dorénavant partie de l’identité de l’&#57353;.

Des expositions collectives s’ajoutent également au programme annuel
régulier de l’&#57354;. La sélection collégiale d’un thème mène à
l’organisation de réunions où la directrice, les historiens de l’art de
l’&#57353; et des artistes invités affinent une réflexion ensemble. La
première occurrence s’intitule *Du diaphane et de l’illusion&#x202F;: une
pluralité d’apparences*, présentée du 16 mai au 19 juillet 2003. Dans ce
contexte, quinze artistes femmes exposent des œuvres abordant la
suggestion, la perception, le visible, l’invisible. %%Amathéü, Catherine|Catherine Amathéü%%, %%Bertrand, Lucile|Lucile Bertrand%%, %%Cambruzzi, Marie-Ange|Marie-Ange Cambruzzi%%, Marie %%Delfosca, Marie|Delfosca%%, Laurence %%Dervaux, Laurence|Dervaux%%,
Bénédicte %%Henderick, Bénédicte|Henderick%%, Aïda %%Kazarian, Aïda|Kazarian%%, Niki %%Kokkinos, Niki|Kokkinos%%, Sylvie %%Pichrist, Sylvie|Pichrist%%,
Marianne %%Ponlot, Marianne|Ponlot%%, Aude %%Renault, Aude|Renault%%, Isabelle %%Rousseau, Isabelle|Rousseau%%, Léopoldine %%Roux, Léopoldine|Roux%%, Ela
%%Stasiuk, Ela|Stasiuk%% et Reiko %%Takizawa, Reiko|Takizawa%% profitent de l’exercice. Le parti pris de la
jeune création locale est maintenu, et les femmes y sont visibles.

Le projet collectif suivant, *Le Dessus des cartes&#x202F;: art et
cartographie* (2004), donne la parole à vingt artistes belges et
étrangers. Ces derniers livrent des visions personnelles de la
cartographie de la Terre par l’intermédiaire d’œuvres existantes ou de
nouvelles productions. La matière exposée traite du territoire. Par
conséquent, elle souligne des enjeux politiques, militaires,
économiques, sociaux et écologiques qui traitent de la distribution du
pouvoir. Françoise %%Schein, Françoise|Schein%% dénonce des injustices sociales typiquement
brésiliennes en pointant l’absence des favelas sur les cartes de la
mégalopole de Rio de Janeiro. Le militant écologiste Peter %%Fend, Peter|Fend%% souligne
l’importance des ressources naturelles en brouillant la traditionnelle
hiérarchie entre l’océan et la terre ferme sur des cartes d’un nouveau
genre. Certains artistes, dont la démarche est structurée autour du
voyage et du déplacement, réfèrent à des expériences vécues plus
directement. Parmi ceux-ci, Christophe %%Fink, Christophe|Fink%%, Marie-Christine %%Katz, Marie-Christine|Katz%%,
Capucine %%Levie, Capucine|Levie%%, Jessica %%Vaturi, Jessica|Vaturi%%, Angel Vergara se distinguent. D’autres
abordent la cartographie de manière plus poétique. Dans tous les cas,
cette exposition invite à adopter de nouveaux points de vue – ceux des
artistes – pour examiner le fonctionnement du monde privé ou collectif.

Lors d’un entretien mené par Christine %%Jamart, Christine|Jamart%% et publié dans le magazine
*%%L’Art même%%*, Arlette %%Lemonnier, Arlette|Lemonnier%% affirme&#x202F;: «&#x202F;Le but des expositions à
sujet générique de l’&#57353; est de convier artistes confirmés et jeunes
plasticiens afin que ces derniers retirent un enrichissement de cet
échange et de cette confrontation. C’est là la mission de service
public d’une institution culturelle comme la nôtre et c’est un risque
volontiers assumé.&#x202F;»[^66] La commissaire de l’exposition cherche à
susciter une forme de collégialité et d’émulation. Elle ne travaille
d’ailleurs pas seule. Sa plus proche collaboratrice est assurément la
jeune historienne de l’art Catherine %%Henkinet, Catherine|Henkinet%% (%%Université libre de Bruxelles (ULB)|ULB%%), qui s’est jointe à
l’équipe de l’&#57353; en 2003. Celle-ci soutient la production de chacune
des expositions.

Les expositions développées sous la responsabilité d’Arlette %%Lemonnier, Arlette|Lemonnier%%
sont pérennisées par une publication. Le contenu du *Dessus des
cartes&#x202F;: art et cartographie*, comme celui des autres projets, est
archivé et approfondi dans un ouvrage illustré qui présente les œuvres
exposées et le contexte historique dans lequel elles ont émergé.
Marie-Ange %%Brayer, Marie-Ange|Brayer%%, %%Brys-Schatan, Gita|Gita Brys-Schatan%%, Kim %%Leroy, Kim|Leroy%%, Claudine %%Marlaire, Claudine|Marlaire%% et
Pierre %%Sterckx, Pierre|Sterckx%%[^67] y apportent leur contribution critique. Dans ce
catalogue comme dans l’entretien publié dans *%%L’Art même%%*, Arlette
%%Lemonnier, Arlette|Lemonnier%% ne fait pas l’économie du contexte global dans lequel son
projet s’inscrit. Elle traite d’une prise de conscience générale, rendue
perceptible par la 50<sup>e</sup> %%Biennale de Venise%% (2003) et la %%Documenta%% XI
(2002), liée à la distribution du pouvoir sur la planète. Et pour cause&#x202F;! En 2002, Okwui %%Enwezor, Okwui|Enwezor%% devient le premier directeur non européen de la
%%Documenta%% XI, à Cassel. Il développe une stratégie pour décloisonner
l’approche eurocentrique habituellement associée à la célèbre
manifestation culturelle&#x202F;: des antennes sont distribuées à Vienne,
Berlin, New Delhi, Sainte-Lucie et Lagos afin de répartir l’évènement au
cœur de nouveaux espaces. Il adjoint en outre un fond conséquent à cette
forme ambitieuse tandis que les œuvres sélectionnées comprennent
majoritairement des photos, des films et des vidéos de nature
documentaire cernant des problématiques liées à la migration, à
l’urbanisation et au postcolonialisme. Plus de vingt ans après *Les
Magiciens de la Terre* (1989) de %%Martin, Jean-Hubert|Jean-Hubert Martin%%, %%Enwezor, Okwui|Enwezor%% profite
d’une manifestation internationale de grande envergure pour souligner
l’importance d’une nouvelle cartographie du monde de l’art contemporain
qui tarde à s’établir. En février 2003, l’historien de l’art
Jean-Philippe %%Theyskens, Jean-Philippe|Theyskens%% en aborde les enjeux dans le contexte de «&#x202F;Onze
éditions&#x202F;: bilans pluriels et états des lieux de la %%Documenta%%&#x202F;», un
séminaire de recherche et de spécialisation dispensé à l’&#57354;. Grâce au
concours des historiens de l’art qui y travaillent régulièrement et des
professeurs qui y sont embauchés ponctuellement, l’Institut s’attache à
développer un discours critique sur l’actualité artistique.

Par la suite, l’&#57353; présente les expositions *Picto(s)* (2006), *Paysages/Visions
paradoxales* (2007), *De Narcisse à Alice&#x202F;: miroirs et reflets en
question *(2008), *Dress Code* (2009), *Flesh* (2009), *En quelques
traits, un cabinet de dessin* (2010). Ces expositions ne sont pas reliées
par un fil rouge évident, mais chacune d’entre elles représente une
recherche au *temps présent*. Leur sujet et leur contenu, devenus objets
d’études, sont explorés et nourris par des artistes, des historiens de
l’art et d’autres auteurs.

Les grandes salles accueillent un troisième type d’exposition, d’abord
placé sous le signe de la transversalité. Arlette %%Lemonnier, Arlette|Lemonnier%% et Éric Van
Essche en profitent pour dresser des parallèles avec des cycles de cours
et des colloques. L’exposition *Apparition(s)*, développée en
collaboration avec les ASBL Ad Libitum (%%Chamboisier, Anne-Laure|Anne-Laure Chamboisier%%) et Transcultures (%%Frank, Philippe|Philippe Frank%%), présentée du 14 novembre au 6
décembre 2003, rassemble des interventions médiatiques de Bertrand
%%Gadenne, Bertrand|Gadenne%% et Alexandra %%Dementieva, Alexandra|Dementieva%%. Cette dernière, par exemple, propose un
dispositif composé d’un écran de projection faisant miroir&#x202F;: en léger
décalage par rapport à la réalité, le visiteur y observe son image
projetée sur les images d’une vidéo incluant parfois d’autres personnes.
Il s’agit d’une occasion de conscientiser sa présence dans la salle
d’exposition de l’&#57354;, de même que sa réception d’une œuvre
contemporaine au moment même. Cette œuvre performative, au sens propre
où l’entend %%Austin Langshaw, John|John Langshaw Austin%%[^68], opère une mise en abyme qui
soulève, entre autres, des enjeux inhérents à la considération des
publics. En parallèle, Éric %%Van Essche, Éric|Van Essche%% organise un colloque
international intitulé «&#x202F;Exposer l’image en mouvement&#x202F;? De la grammaire
spatiale à la production de contenus&#x202F;» qui se tient les 28 et 29
novembre 2003. Des chercheurs et des artistes, belges et étrangers,
dispensent des communications, deux tables rondes et des projections.
Ils profitent du festival Netd@ys Wallonie-Bruxelles pour creuser les
pistes de réflexion amorcées par l’exposition *Apparition(s)*. Les actes
de ce colloque, co-édités avec %%La Lettre Volée%%, paraissent en septembre
2004.

Difficile à organiser dans la durée, ce croisement systématique entre
cours, colloque et monstration d’œuvres est graduellement
abandonné[^69]. Le troisième type d’exposition présenté dans les grandes
salles de l’&#57353;, généralement au printemps, se cristallise plutôt
autour d’enjeux actuels et de collaborations avec d’autres organismes
culturels. Il s’agit d’une mise à disposition des salles où la
collaboration se joue à différents niveaux selon les cas, incluant ceux
de la scénographie, du montage et du démontage, du gardiennage, de
l’accueil des visiteurs, des visites guidées et des relations de presse.
Hormis l’accueil annuel d’expositions de photographies produites et
organisées par Leica au tournant de chaque année entre décembre 2003 et
janvier 2009, l’&#57354; accueille, en présence d’Arlette
%%Lemonnier, Arlette|Lemonnier%%, *Demain, j’irai mieux* (2004), *Le Bijou contemporain en
Communauté française* (2004), *%%Mass moving%%. Un aspect de l’art
contemporain en Belgique 1969-1975* (2004), *Les Grands Moments de vie*
(Véronique %%Ellena, Véronique|Ellena%%, 2006), *Artistes pour Amnesty* (2006), *Hommage à
Marthe %%Wéry, Marthe|Wéry%%* (2007), *White Noise. Carte blanche à la céramiste Caroline
%%Andrin, Caroline|Andrin%%* (2008), *Singuliers/Pluriels. Huit artistes s’installent. Carte
blanche à %%Chantelot, Marie|Marie Chantelot%%* (2010), *Sonopoetics&#x202F;: De la parole à
l’image, de la poésie au son* (2010), *Ceci n’est plus Beyrouth.
Photographies de Fouad %%Elkoury, Fouad|Elkoury%%* (2011). Assez courtes de prime abord,
ces expositions soulignent néanmoins un fondement de son ADN&#x202F;: la
rencontre et la collaboration.

Outre les expositions déployées dans les grandes salles, une grande
quantité de projets prennent également place au sein de Rayon
Art/Objets[^70] et de la %%Galerie-Découverte%%. Plus expérimentaux, ces
deux espaces liés à une modeste activité économique permettent la
découverte de nombreux artistes. %%Galerie Découverte%% et %%Rayon Art/Objets|Rayon Art/Objets%% est le premier espace
traversé lorsque l’entrée publique de l’&#57353; donne sur le %%Waterloo (boulevard de)|boulevard de Waterloo%%. Attenant à %%Rayon Art/Librairie%%, cet espace présente des
œuvres artistiques de petites dimensions : sculptures, bijoux contemporains, objets en
porcelaine, en émail, etc. Le travail de deux ou trois artistes y cohabite le temps
d’une exposition qui dure en moyenne deux mois. La %%Galerie-Découverte%%
est un espace d’exposition consacré aux jeunes artistes de la Communauté
française. Entre sa création en 2000, sa transformation en 2006[^71] et
sa mutation en 2011, cet espace de diffusion apporte à lui seul  de la
visibilité au travail de 70 artistes.

### Une direction scientifique et des prises de parole

La fondation de l’&#57353; découle d’une volonté d’étudier et d’enseigner
différents pans de l’histoire de l’art. Il s’agit de sa principale
activité entre 1971 et 1994, avant qu’il se consacre entièrement à
l’édition entre 1994 et 1999. %%Brys-Schatan, Gita|Gita Brys-Schatan%% choisit l’indépendance
et écarte tout système susceptible de transformer l’&#57354; en école
officielle, notamment en diversifiant les activités qui y sont menées.
Sur ce plan également, l’&#57353; doit se repositionner au sein d’un
secteur en transformation. Ses cours d’histoire de l’art, contemporain
ou non, sont dorénavant disséminés parmi les initiatives d’universités,
d’écoles supérieures d’art ou d’autres lieux de diffusion artistique.

Éric %%Van Essche, Éric|Van Essche%%, à titre de directeur scientifique de l’&#57353;, développe
une programmation qui permet d’explorer des problématiques propres à
l’art contemporain au profit de publics variés. En marge des cours
structurés en fonction du statut de leur contenu – initiation,
fondamental, recherche –, les grandes conférences offrent un beau
panorama des préoccupations de l’&#57354;, tant par la qualité des
interlocuteurs que par celle des sujets abordés. Entre 2004 et 2010,
Éric %%Van Essche, Éric|Van Essche%% accueille plusieurs grands noms associés à la réflexion
sur l’art, dont Bernard %%Stiegler, Bernard|Stiegler%%, Alain %%Fleischer, Alain|Fleischer%%, Paul %%Ardenne, Paul|Ardenne%%, Régis
%%Cotentin, Régis|Cotentin%%, Christian %%Ruby, Christian|Ruby%%, Marc %%Jimenez, Marc|Jimenez%%, Catherine %%Millet, Catherine|Millet%%, %%Michaud, Yves|Yves Michaud%%,
Georges %%Didi-Huberman, Georges|Didi-Huberman%%, Norbert %%Hillaire, Norbert|Hillaire%%, Thierry %%Davila, Thierry|Davila%%, %%Michaud, Éric|Éric Michaud%%,
Bernard %%Marcelis, Bernard|Marcelis%%, Jacques %%Donguy, Jacques|Donguy%%, %%Bastin, Olivier|Olivier Bastin%%, %%Audi, Paul|Paul Audi%%. Ces
penseurs partagent l’état de leurs recherches sur le statut et la
fonction de l’objet d’art, la théâtralisation, l’enrobage et les limites
du corps, l’esthétique, les nouvelles technologies et l’espace public,
entre autres. Autant de sujets qui confirment l’inscription de l’&#57353;
dans un réseau de circulation des savoirs.


À travers les activités du directeur scientifique, l’&#57353; se démarque
surtout par ses colloques internationaux, dont les actes sont publiés en
collaboration avec la maison d’édition %%La Lettre Volée%%. Ces évènements
de réflexion de deux jours, généralement reconduits tous les deux ans,
permettent d’approfondir des aspects de l’art contemporain. Assez
descriptifs, les titres des colloques à eux seuls révèlent les sujets
examinés&#x202F;: «&#x202F;Exposer l’image en mouvement&#x202F;: de la grammaire spatiale à
la production de contenu&#x202F;» (2003), «&#x202F;Le sens de l’indécence&#x202F;: la question
de la censure des images à l’âge contemporain&#x202F;» (2004), «&#x202F;Les formes
contemporaines de l’art engagé&#x202F;: de l’art contextuel aux nouvelles
pratiques documentaires&#x202F;» (2006), «&#x202F;Spéculations spéculaires&#x202F;: le reflet
du miroir dans l’image contemporaine&#x202F;» (2008), «&#x202F;Aborder les bordures :
l’art contemporain et la question des frontières&#x202F;» (2010), «&#x202F;Hors-cadre&#x202F;:
peinture, couleur et lumière dans l’espace public contemporain&#x202F;» (2012).
Ces colloques voient le jour sous la direction d’Éric %%Van Essche, Éric|Van Essche%%, mais
sont le fruit de précieuses collaborations. «&#x202F;Les formes contemporaines
de l’art engagé&#x202F;» bénéficie du concours de l’École de recherche
graphique (Erg), %%Culture et démocratie%%, la fondation %%Fondation Marcel Hicter|Marcel Hicter%% et
Vox/Polymorfilms. Les actes de ce colloque se divisent en deux parties
nourries par près de trente auteurs&#x202F;: «&#x202F;Engagement et positions
théoriques&#x202F;», «&#x202F;Engagement et pratique artistique&#x202F;». On y perçoit une
articulation entre la théorie et la pratique qui est dans l’air du
temps&#x202F;: deux ans auparavant, en mars 2004, la création du décret de
%%Bologne%% entraînait l’alignement de tous les types d’enseignement de la
Fédération Wallonie-Bruxelles (universités, hautes écoles, écoles
supérieures d'art, établissements d’enseignement de promotion
sociale) sur un modèle commun à l’échelle européenne. Ce modèle
structure l’enseignement supérieur en trois cycles distincts, soit le
bachelier, le master et le doctorat. Cette réforme de l’enseignement
supérieur mène à la création d’un doctorat en art et sciences de l’art,
développé conjointement entre les écoles supérieures d’art et les
universités. Les pratiques artistiques et scientifiques se rapprochent
au sein d’un parcours académique, lequel est largement commenté par les
critiques, historiens et théoriciens de l’art, les artistes, les
enseignants et autres acteurs du milieu culturel. L’&#57354; évolue en
dehors du système académique et n’est donc pas affecté directement par
cette transformation du système éducatif belge. Son statut d’institut de
recherche indépendant lui épargne ces complications. Toutefois, ce
phénomène socioculturel ne tarde pas à se refléter dans la
programmation de l’&#57353;.

La recherche prend majoritairement deux formes au sein des activités
menées à l’&#57354;. D’une part, on invite différents acteurs à présenter
leurs travaux de recherche personnels dans le contexte de cours qui y
sont dispensés. Liés à l’Institut ou indépendants, ces acteurs évoluent
dans le milieu de l’histoire de l’art, de la culture au sens large, ou
d’une autre science humaine. D’autre part, on soutient la recherche à
travers les services d’un centre de documentation depuis l’année
académique 1976-1977. Au départ, le centre de Documentation des arts
visuels actuels (DAVA) contient un département d’information, une
diathèque, une sonothèque et une bibliothèque. Cette dernière regroupe
des ouvrages de base sur les arts anciens et extra européens, ainsi que
des ouvrages généraux, encyclopédies, monographies, écrits d’artistes,
catalogues d’expositions, revues d’art internationales, dossiers
d’artistes portant sur les arts visuels contemporains. Durant les années
1980, le centre est régulièrement promu dans le programme annuel. On
cherche à l’enrichir en sollicitant des dons de catalogues, de livres et
de revues traitant des arts visuels contemporains. Pour remercier la
générosité des donateurs, on propose même de créer un fonds à leur nom.
Par ailleurs, les auditeurs prenant part au programme d’activités sont
invités à s’y rendre pour s’approprier et approfondir les savoirs
acquis. À la fin des années 1990, les historiens de l’art liés à l’&#57353;
restructurent l’organisation de son contenu. Le centre de documentation
traverse une importante étape de son développement en 2004 lorsqu’il est
bonifié par la %%Communauté française du fonds documentaire du Centre d’art contemporain|Communauté française du fonds documentaire%% du %%Centre d’art contemporain%% (63 avenue des %%Nerviens, avenue des|Nerviens%%, Bruxelles) suite à sa
fermeture. Par la suite, un historien de l’art de l’&#57354; gère et
développe une collection de documents spécialisés en «&#x202F;art
environnemental&#x202F;» en ses locaux, et Michèle %%Stanescu, Michèle|Stanescu%% gère une collection
de documents consacrés à l’art moderne et contemporain conservée dans le
bâtiment de l’avenue des %%Nerviens, avenue des|Nerviens%%. Un tri des ouvrages et une fusion des
catalogues sont opérés, mais la gestion en deux lieux est maintenue
jusqu’en 2011. Michèle %%Stanescu, Michèle|Stanescu%% est documentaliste au centre de
documentation du %%Centre d’art contemporain%% de 1984 à 2004 avant
d’intégrer l’équipe de l’&#57353; en 2005. Elle y apporte un savoir et une
expérience conséquents en collaborant avec les historiens de
l’art engagés dans l'Institut, et en contribuant à la gestion de ses
ressources documentaires. Documentaliste de formation, elle développe
des dossiers d’artistes (tâche initiée par le Service des arts
plastiques de la Communauté française), des mallettes pédagogiques et
soutient activement ses lecteurs.


<div class="header2">
<h2>Médiation et co&#8209;construction de&nbsp;sens</h2>
<p class="dates">2010-2020</p>
</div>

Longtemps assimilé à une école, l’&#57355; a toujours eu le souci
d’introduire l’art contemporain auprès du plus grand nombre de publics
possibles afin de rétrécir le fossé historique qui les tient à distance,
de leur donner des clés pour aborder le monde autrement. Le projet
social annoncé dans les années 1970, «&#x202F;étudier les œuvres d’art pour
mieux comprendre son époque&#x202F;», se décline en cycles de cours au fil de
ses quarante premières années. Au départ, il faut *initier*, *éduquer*
et *démocratiser*. Le mode de fonctionnement de l’Institut est calqué
sur le modèle académique. À partir de 2010, après quelques essais et
erreurs, la volonté de *faire école* prend corps à travers des activités
pédagogiques et des collaborations directes avec des écoles existantes.

Au fil des années, des historiens de l’art de l’&#57355; plus ou moins
sensibles au contact avec les publics prennent des initiatives. Les
activités pédagogiques visent de nouveaux publics et s’engagent auprès
d’écoles et d’associations pour développer des programmes d’activités
qui correspondent à leurs besoins. C’est ainsi qu’en 2006, un atelier de
sensibilisation à l’art public conduit une classe de sixième secondaire
à rédiger un appel d’offres pour la réalisation d’une œuvre à l’Institut
%%Institut Saint-Louis|Saint-Louis%%. L’œuvre, conçue par l’artiste Jean-François %%Octave, Jean-François|Octave%%, est
réalisée en concertation avec les élèves. Les activités pédagogiques
commencent à quitter l’auditorium et à faire l’objet de partenariats.
Des visites guidées d’expositions – à l’&#57355; ou en d’autres lieux
culturels – sont toujours offertes mais ne constituent plus une
forme unique. Ce secteur d’activité gagne beaucoup d’importance en
2009&#x202F;: «&#x202F;L’art contemporain continue à susciter méfiance et scepticisme
auprès du grand public. Pourtant les créateurs d’aujourd’hui vivent dans
le même contexte que nous et posent, avec les moyens actuels, des
questions qui nous concernent tous et toutes. Pour permettre au plus
grand nombre d’aborder les enjeux de l’art contemporain, l’&#57355; propose
aux écoles, associations et entreprises des conférences illustrées
conçues sans hermétisme dans un souci d’échange et de dialogue.&#x202F;»[^73]
La mise en place de ce nouveau champ d’activité, de plus en plus
affirmé, accompagne le changement de statut d’Éric %%Van Essche, Éric|Van Essche%% qui
devient directeur général suite au départ d’Arlette %%Lemonnier, Arlette|Lemonnier%%. À ce
moment-là, il réorganise le travail en *pôles* («&#x202F;Recherche&#x202F;»,
«&#x202F;Expositions&#x202F;», «&#x202F;Médiation&#x202F;»). Les activités pédagogiques sont
rassemblées dans le pôle «&#x202F;Médiation&#x202F;», qui inclut essentiellement des
cours, des séminaires, des conférences, des conversations et des
projections. De manière plus ponctuelle, on y ajoute des colloques, des
festivals, des visites guidées, des excursions et des voyages culturels.
Ce pôle est assurément celui qui englobe la plus grande diversité
d’activités. À travers celui-ci, Laurent %%Courtens, Laurent|Courtens%% – intégré à l’équipe
de l’&#57356; en 2003 – et Adrien %%Grimmeau, Adrien|Grimmeau%% – qui rejoint l'Institut en 2010 – s’engagent à développer des projets qui tissent des
liens avec les publics, notamment avec les écoles.

À cette époque, les cours ne sont plus identifiés en fonction du statut
de leur contenu (initiation, approfondissement, remise en question),
mais en fonction de leur horaire. La durée des cycles rétrécit et le
terme «&#x202F;cours&#x202F;» est parfois remplacé par «&#x202F;conférence&#x202F;».
L’abandon des catégories historiques joue en faveur d’une
réactualisation des modes de communication et permet un décloisonnement
salutaire. Dorénavant, les auditeurs n’ont plus à positionner leur
niveau de connaissances en découvrant le programme de l’&#57355;. À l’aube
du quarantième anniversaire, l’expression «&#x202F;&#57356;, faculté ouverte&#x202F;» est
toujours d’actualité. D’autant plus qu’il ne faut désormais plus être
membre de l’Institut pour y suivre un cours.

Ces modifications dans la programmation s’accompagnent d’une tentative
de redéfinition identitaire. Une expression fait son apparition sur les
communications à la fin des années 2000&#x202F;: «&#x202F;Histoire de l’art et art
contemporain&#x202F;». L’adoption de cette « maxime » remet, d’une certaine
manière, le long nom de l’&#57355; au goût du jour. L’expression «&#x202F;langage
plastique&#x202F;», légitime et justifiée dans les années 1970, jure désormais.
La nouvelle génération – visée par la programmation – manque de
référents pour interpréter le sens de cet archaïsme. On réussit à
réactualiser le fond et la forme du programme d’activité, mais on
choisit de conserver l’intitulé «&#x202F;Institut supérieur pour l’étude du
langage plastique&#x202F;», qui porte quarante ans d’histoire. Le slogan «&#x202F;Voir
et comprendre l’art contemporain&#x202F;» complète le sous-titre. «&#x202F;En montrant
et en expliquant la création actuelle, l’&#57356; contribue à la diffusion
de la culture contemporaine et à la compréhension de notre présent à
l’aide du regard porté par les artistes sur le monde
d’aujourd’hui.&#x202F;»[^74]

Plusieurs changements adviennent à cette époque. Par exemple, le
Festival du film sur l’art, réservé jusqu’alors aux membres, est ouvert
au grand public à l’occasion de son dixième anniversaire, en 2010.
Adrien %%Grimmeau, Adrien|Grimmeau%% en reprend la coordination et l’associe, dès l’année
suivante, à la remise de deux prix&#x202F;: le Prix découverte (1&#x202F;250 euros) et
le Prix du Film sur l’art (1&#x202F;750 euros). Cet événement rassembleur prend
beaucoup d’importance par la suite, grâce à une collaboration fertile
avec le %%Centre du film sur l’art%%. Bientôt, le festival, renommé BAFF –
%%BAFF – Brussels Art Film Festival|Brussels Art Film Festival%%, sera accueilli à Bozar, à %%Cinematek%%, voire à la %%Koninklijke Academie voor Schone Kunsten (KASK)|KASK%% à Gand et au %%Plaza|Plaza%% à Mons.

### Un généreux 40<sup>e</sup> anniversaire

L’&#57355; se replonge dans les travaux de rénovation à la fin de la
décennie grâce au soutien de la Fédération Wallonie-Bruxelles,
emphytéote des bâtiments. Ces travaux donnent lieu à une extension et
une modification d’une aile du bâtiment des anciennes écuries du palais
%%Palais d’Egmont|d’Egmont%% d’après les plans initiaux des mêmes architectes, Brigitte
%%Libois, Brigitte|Libois%% et Daniel %%Lelubre, Daniel|Lelubre%%. Les deux bâtiments situés de part et d’autre
du %%Milan, passage de|passage de Milan%%, reliés au-dessus du porche attenant au parc
d’Egmont, permettent à l’équipe de l’&#57356; de réaménager ses bureaux et
de redéployer sa grille d’activités au sein de nouveaux espaces après
une fermeture publique de quatre mois.

À l’automne 2011, l’&#57355; profite de son quarantième anniversaire pour
dévoiler ses locaux. Parmi les changements majeurs, on note que le
centre de documentation est installé dans la nouvelle aile. Deux niveaux
supérieurs accueillent les étagères et la salle de lecture. Les fonds
précédemment conservés à l’&#57356; et sur l’avenue des %%Nerviens, avenue des|Nerviens%% sont enfin
rassemblés en un seul endroit, et bonifiés à nouveau. On leur ajoute un
fonds traitant d’arts plastiques offert par la Fédération
Wallonie-Bruxelles, et un fonds développé par %%Contretype%% et déposé à
long terme à l’&#57355;[^75]. Grâce aux efforts conjugués d’Éric %%Van Essche, Éric|Van Essche%%
et de ses collaborateurs, le centre de documentation regroupe dorénavant
quatre collections de documents et représente un organe de recherche
majeur. Le niveau inférieur de cette nouvelle aile est dégagé pour
constituer une nouvelle salle d’exposition&#x202F;: l’atelier. Celui-ci est
dédié à accueillir des plasticiens, mais également des artistes du monde
du spectacle, de la performance, de la vidéo, de la danse et de la
musique. Il est propice à l’expérimentation, tant sur le plan de la
production que sur celui de la diffusion. Il accueille des workshops
et des répétitions, mais surtout des artistes en résidences. Ces
derniers l’investissent durant des périodes de trois mois, le temps de
développer un projet et d’expérimenter son rapport avec un public lors
d’une ouverture publique. Dès le printemps 2012, sous la direction
d’Éric %%Van Essche, Éric|Van Essche%%, le programme de résidences artistiques permet de
soutenir le travail de Sébastien %%Reuzé, Sébastien|Reuzé%%, Fabrice %%Samyn, Fabrice|Samyn%%, Cie
%%Cie t.r.a.n.s.i.t.s.c.a.p.e|t.r.a.n.s.i.t.s.c.a.p.e%%, Emmanuel Van der Auwera, Aurore %%Dal Mas, Aurore|Dal Mas%% &
Sandrine %%Morgante, Sandrine|Morgante%%, Adrien %%Lucca, Adrien|Lucca%%, Anne %%Penders, Anne|Penders%%, %%Chenut, Vincent|Vincent Chenut%%, Priscilla
%%Beccarri, Priscilla|Beccarri%%, %%Bara, Manon|Manon Bara%%, Loïc %%Desroeux, Loïc|Desroeux%%, Robert %%Milin, Robert|Milin%%, Les %%Disparues, Les|Disparues%%
(Aleksandra %%Chaushova, Aleksandra|Chaushova%%, Judith %%Duchêne, Judith|Duchêne%%, Teodora %%Cozman, Teodora|Cozman%%, Clara %%Sobrino, Clara|Sobrino%%),
Yogan %%Muller, Yogan|Muller%%. Dans le même esprit, cet espace voit également défiler des
jurys de fin d’année des écoles supérieures artistiques telles que
%%Erg — ESA|l’Erg%%, %%Saint-Luc — ESA|Saint-Luc%%, %%Le 75 — ESA|le 75%%, l’%%Académie royale des Beaux-Arts de Bruxelles (ArBA — ESA)|ArBA%%, %%École nationale supérieure d’architecture et des arts visuels de La Cambre (La Cambre)|La Cambre%%. En cela, cet atelier touche
définitivement l’émergence. Cet espace de diffusion de projets en cours
de développement enclenche un cercle vertueux&#x202F;: il fournit un
laboratoire et une vitrine aux jeunes artistes et constitue une bonne
source de découvertes pour les historiens de l’art de l’&#57356; qui
s’intéressent aux nouvelles tendances, aux nouvelles démarches
artistiques. Le pôle «&#x202F;Exposition&#x202F;», essentiellement représenté par
Catherine %%Henkinet, Catherine|Henkinet%% et Adèle %%Santocono, Adèle|Santocono%%, gère dorénavant cinq espaces
d’exposition&#x202F;: la galerie, le studio, l’atelier, %%Galerie Découverte%% et %%Rayon Art/Objets|Rayon Art/Objets%%,
l’espace de restauration *%%Au Passage de Milan%%*. À ces différents
espaces s’ajoutent parfois des projets dans le %%Egmont, Parc d’|parc d’Egmont%% et
*extra-muros*.

L’Institut organise une panoplie d’activités pour marquer son
quarantième anniversaire. Durant l’automne, il présente l’exposition
*Célébration(s)* dans l’ensemble de ses salles&#x202F;: «&#x202F;l’&#57355; est traversé
par le thème de la célébration et ses déclinaisons allant de la fête à
l’anniversaire, du divertissement à la cérémonie, de l’enthousiasme
d’une liesse collective à la mémoire et au souvenir.&#x202F;» Les membres
de l’équipe de l’&#57355;, les artistes et les visiteurs célèbrent la
pérennité de la structure durant quatre journées festives suivant le
vernissage. Les initiatives se multiplient&#x202F;: quarante textes sur l’art
sont lus lors d’un marathon, quarante films sur la célébration sont
projetés. En outre, une installation vidéo présentant quarante portraits
d’artistes, de conférenciers et de visiteurs, réalisés par André
%%Goldberg, André|Goldberg%% à partir d’entretiens individuels, révèle des réflexions
critiques sur l’art contemporain.

Hormis cette programmation spéciale qui constitue un temps fort pour
l’&#57355;, la création du programme de résidences artistiques et d’un
concours visant l’intégration d’une œuvre dans l’espace de restauration
constituent des initiatives plus pérennes. Effectif de 2011 à 2015, ce
concours permet à cinq jeunes artistes de profiter d’un an de visibilité
à l’&#57356;&#x202F;: Frédéric %%Penelle, Frédéric|Penelle%%, %%Obêtre%%, Sofi %%Van Saltbommel, Sofi|Van Saltbommel%%, Patrick %%Guns, Patrick|Guns%%
et %%Bouvier, Amélie|Amélie Bouvier%%.

Épaulé par une coordination générale et des soutiens logistiques, Éric
%%Van Essche, Éric|Van Essche%% se donne le rôle d’ambassadeur des missions et des projets de
l’&#57355; auprès des pouvoirs publics. Il veille sur tous les aspects qui
permettent la programmation. Il valide les projets proposés et assure
leur suivi et leur développement, mais offre une grande liberté à ses
collaborateurs. Les cinq historiens de l’art se concentrent sur leur
pôle (médiation, exposition, recherche) et la programmation. À cette
époque, les historiens de l’art liés à l’&#57356;[^77] deviennent des
*programmateurs* au sens événementiel&#x202F;: ils programment des conférences,
des rencontres, des ateliers, des journées d’étude, des visites guidées
et des stages pour enfants, diverses activités pédagogiques et des
expositions. Un changement majeur advient&#x202F;: chacun d’entre eux est
invité à proposer et organiser des projets d’exposition.

À partir du moment où ils ont l’occasion de délaisser un peu leur pôle
pour organiser une exposition, les historiens de l’art travaillant à
l’&#57355; en profitent. Ils y révèlent leur personnalité, leurs
préoccupations et leurs champs d’intérêt. Catherine %%Henkinet, Catherine|Henkinet%%, impliquée
dans la production d’expositions depuis 2003, demeure responsable de ce
pôle. Elle organise dorénavant ses propres projets dont *Et le monde est
plongé dans la pénombre* (2013 – Lise %%Duclaux, Lise|Duclaux%%), *Failure Falling
Figure* (2015 - Agnès %%Geoffray, Agnès|Geoffray%%), *L’Image qui vient* (2016, en collaboration avec Laurent
%%Courtens, Laurent|Courtens%%), *\#Institut. Table of Content* (2017), *SYNC !* (2017), *Babel*
(2019, en collaboration avec Mélanie %%Rainville, Mélanie|Rainville%%). Laurent %%Courtens, Laurent|Courtens%%
organise *Flesh* (2009/2011), *Exonymie* (2010), *ARCHIVES/déplier
l’histoire* (2014) et *A Forest* (2018). Adrien %%Grimmeau, Adrien|Grimmeau%% organise
*%%Bonom%%, le singe boiteux* (2014), ainsi que *Being Urban. Laboratoire
pour l’art dans la ville* (2015), en collaboration avec Pauline de La
%%La Boulaye, Pauline de|Boulaye%%. Anne-Esther %%Henao, Anne-Esther|Henao%% organise *Christophe %%Terlinden, Christophe|Terlinden%%. No Sugar*
(2014). %%Cheval, Florence|Florence Cheval%% organise *Hostipitalité* (2015) et *%%Balcou, Béatrice|Béatrice Balcou%% / Kazuko %%Miyamoto, Kazuko|Miyamoto%%* (2016).[^78] L’historien de l’art qui présente
*son* projet veille dorénavant sur tous ses aspects, du concept de base
au programme d’évènements qui l’accompagne. Il profite parfois d’une
certaine collégialité, mais il a le dernier mot sur les choix. Ces
expositions rivalisent d’originalité.

Cette vague d’enthousiasme reflète la reconnaissance institutionnelle de
la figure du curateur perceptible dans le milieu artistique. Les
formations et les discours critiques sur l’exposition se développent&#x202F;:
le Postgraduate Curatorial Programme (1999) à la %%KASK|KASK%% (Gand), le Master en
design urbain avec une spécialité en design d’exposition – %%MASDEX%%[^79]
(2014) à %%Arts²%%, le Master en pratiques de
l’exposition – %%CARE%% (2015) à l’%%Académie royale des Beaux-Arts de Bruxelles (ArBA — ESA)|ArBA%%-EsA (Bruxelles). Ces formations, qui
existent depuis longtemps au sein d’autres pays, sont à la fois l’indice
et l’effet d’un engouement pour les métiers entourant la mise en public
d’œuvres. Elles forment des spécialistes de l’exposition qui tentent de
se frayer un chemin dans un monde professionnel relativement hostile
compte tenu des conditions d’emploi précaires qu’il offre, notamment en
raison du petit nombre de postes permanents disponibles dans les lieux
de diffusion existants. Elles développent un secteur professionnel
fragile, mais encouragent également une précieuse créativité dans la mise
en exposition.

En 2013, l’Institut s’associe au réseau transfrontalier d’art
contemporain %%50 degrés Nord%%, qui fédère des lieux de diffusion d’art
contemporain distribués de part et d’autre de la frontière franco-belge.
Par cette association, l’&#57356; entérine sa présence au sein d’un réseau
d’organisateurs d’expositions. Il accepte qu’il y ait comparaison,
compétition et course vers l’augmentation du nombre annuel de visiteurs
d’expositions. Devant rendre des comptes à ses subventionnaires,
d’ailleurs, l’&#57355; commence à rigoureusement compter ses visiteurs. On
sait dorénavant qu’une exposition collective attire en moyenne 1&#x202F;500
visiteurs, tandis qu’une exposition individuelle attire en moyenne 1&#x202F;400
visiteurs[^80]. Malgré ces moyennes, une exposition individuelle bat
tous les records de fréquentation en 2014&#x202F;: *%%Bonom%%, le singe boiteux*
attire 14&#x202F;000 visiteurs. Les statistiques de fréquentation sont
considérées dans l’organisation des activités, les prises de risques
sont mesurées&#x202F;: l’&#57356; commence à soutenir plus régulièrement des
artistes établis.

Les expositions organisées par les programmateurs de l’&#57355; sont
ambitieuses, mais elles ne résument pas à elles seules les missions
poursuivies. Au-delà de cette activité publique qui s’inscrit dans une
durée plus importante que les cours ou les conférences, entre autres,
plusieurs initiatives ciblées sont invisibles aux yeux des publics. Les
nombreuses activités menées auprès de groupes d’élèves constituent de
bons exemples. Entre 2012 et 2014, l’&#57355; entretient un partenariat
avec %%Indications ASBL%%, qui se consacre à la sensibilisation artistique
et à la sollicitation de l’esprit critique des jeunes. Dans ce contexte,
Laurent %%Courtens, Laurent|Courtens%% et Adrien %%Grimmeau, Adrien|Grimmeau%% rendent visite à des groupes
scolaires pour discuter d’œuvres contemporaines et de leur portée. Les
élèves sont ensuite invités à se confronter à des expositions
présélectionnées et à remettre un prix à celle qui leur semble la plus
percutante. Ce projet donne lieu à des échanges stimulants pour tous.
Dans le même ordre d’esprit, l’atelier attenant au centre de
documentation représente un laboratoire de production et de diffusion
important pour les jeunes artistes. Il permet une programmation souple
et accueille aussi bien des artistes en résidence que des projets
d’étudiants. En 2013, Laurent %%Courtens, Laurent|Courtens%%, Jérôme %%Piron, Jérôme|Piron%%, Arnaud %%Hoedt, Arnaud|Hoedt%%[^81]
et Olivier %%Cornil, Olivier|Cornil%% (respectivement programmateur à l’&#57356;, professeurs
et photographe) partent pour un voyage d’une semaine dans la %%Ruhr%% avec
deux classes de 7<sup>e</sup> technique de l’Institut %%Institut Don Bosco|Don Bosco%%. Les étudiants
découvrent une région industrielle, sont initiés aux techniques de prise
de vue photographiques et visitent des expositions. Ils échangent
quotidiennement au sujet des images capturées, d’une sélection et de ses
potentielles mises en forme. Le fruit de leurs efforts prend place dans
l’atelier de l’&#57355; à travers une exposition&#x202F;: *Die Welt ist
Schön*[^82] (*Le monde est beau*). Cette intervention polymorphe menée
par Laurent %%Courtens, Laurent|Courtens%% et ses collaborateurs confond trois champs
d’activités de l’&#57356;, soit les ateliers dans les écoles, les voyages
et les expositions. En élargissant l’horizon des étudiants, elle répond
à la mission première de l’Institut&#x202F;: établir une connexion entre l’art
– ou l’histoire de l’art – et des publics et agir comme intermédiaire au
besoin.

En mars 2015, tandis qu’Adrien %%Grimmeau, Adrien|Grimmeau%% se consacre à l’organisation du
Festival du film sur l’art et à l’art public, Éric %%Van Essche, Éric|Van Essche%% embauche
Pauline %%Hatzigeorgiou, Pauline|Hatzigeorgiou%% (%%Université libre de Bruxelles (ULB)|ULB%%, %%Université catholique de Louvain (UCL)|UCL%%) à titre d’historienne de l’art et
programmatrice associée à la médiation. Son arrivée à l’&#57355; donne une
nouvelle impulsion à la médiation et aux liens avec les écoles. Elle
conçoit notamment le projet %%Passerelle%%. Il s’agit d’une importante
collaboration entre les programmes d’agrégation des écoles supérieures
d'art francophones de %%Bruxelles%% (%%Erg — ESA|Erg%%, ESA-%%Saint-Luc — ESA|Saint-Luc%%,
%%Académie royale des Beaux-Arts de Bruxelles (ArBA — ESA)|ArBA%%-ESA, %%École nationale supérieure d’architecture et des arts visuels de La Cambre (La Cambre)|La Cambre%%), plusieurs enseignants au sein de sections non artistiques
d’écoles secondaires qualifiantes et le pôle Médiation de l’&#57356;. Ce
projet invite des étudiants en agrégation des arts plastiques à réaliser
un stage avec un groupe d’étudiants qui ne rencontrent pas de questions
culturelles dans leur parcours scolaire habituel. Dans le contexte de ce
programme, ces derniers sont régulièrement plongés au cœur de
manifestations artistiques et d’échanges d’idées portant sur la culture
durant une année entière. Pauline %%Hatzigeorgiou, Pauline|Hatzigeorgiou%% conçoit ce projet pour
provoquer des points de rencontre et d’échange entre la pédagogie, la
médiation culturelle et la pratique artistique. Elle souhaite pointer
les points communs entre ces trois sphères d’activité, à savoir «&#x202F;(…)
la transmission et les problématiques associées aux réceptions des
objets de savoir comme de culture, par les sujets auxquels elle
s’adresse&#x202F;».[^83] Plusieurs années après sa création, le projet
%%Passerelle%% rencontre chaque année les besoins et les envies de
participants.

L’historienne de l’art participe également à des événements critiques
par rapport à l’éducation artistique. Le 5 novembre 2016, elle se joint
à Sébastien %%Marandon, Sébastien|Marandon%%[^84] et Laurent %%Courtens, Laurent|Courtens%% pour proposer une lecture
de l’ouvrage *Le Maître ignorant, cinq leçons sur l’émancipation
intellectuelle* (1987), de Jacques %%Rancière, Jacques|Rancière%%, qui est tout à fait en
phase avec ses réflexions. Les trois interlocuteurs examinent la
réflexion philosophique et politique que l’auteur développe sur
l’éducation à travers la biographie du pédagogue révolutionnaire Joseph
%%Jacotot, Joseph|Jacotot%%. Quelques jours plus tard, ses champs d’activités sont également
sollicités lors d’une «&#x202F;Mise au point / Table ronde&#x202F;» intitulée «&#x202F;Ils ne
savent pas ce qu’ils font&#x202F;» (16 novembre 2016). %%Barbery, Géraldine|Géraldine Barbery%%,
Sébastien %%Marandon, Sébastien|Marandon%% et Marie-Émilie %%Ricker, Marie-Émilie|Ricker%% confrontent leurs conceptions de
la médiation artistique et discutent de problématiques qui la concernent
; ils ont la difficile tâche de dresser un état des lieux de la
médiation.

L’&#57355; continue d’offrir des projets de médiation sur mesure aux
écoles. Les professeurs sont invités à formuler leurs souhaits
concernant la temporalité, le sujet et la forme des interventions, à
accueillir les médiateurs au sein de leur groupe, en classe ou à
l’&#57356;. Ce service à la carte donne lieu, entre autres, à plusieurs
ateliers. Le «&#x202F;Monument à la jeunesse » (*Nous commun*),
installé au %%CERIA%% et inauguré le 23 juin 2017, découle de ce service «&#x202F;à
la carte&#x202F;». Dans ce contexte, des élèves de 5<sup>e</sup> secondaire de
l’Institut %%Institut Émile Gryson|Émile Gryson%% travaillent avec l’artiste Chiara Colombi, sous
la responsabilité de Pauline %%Hatzigeorgiou, Pauline|Hatzigeorgiou%%, pendant plusieurs mois. Au
terme de ce projet, une sculpture est installée sur le campus de
%%Bruxelles%%.

### Réactualisation d’une identité institutionnelle

Éric %%Van Essche, Éric|Van Essche%% quitte la direction de l’&#57355; en décembre 2015. Ses
quinze années de présence à l’Institut ont permis aux activités
scientifiques de prendre de l’ampleur. Il a réactualisé l’image et
l’identité de l’&#57356; dans la continuité de son histoire. Sous sa
direction, le modèle de gestion gagne en clarté et en démocratie.

Maïté %%Vissault, Maïté|Vissault%% est la quatrième directrice de l’&#57355; (janvier 2016).
Docteure en histoire de l’art, elle publie de nombreux articles au sein
de revues spécialisées, ainsi que *Der Beuys Komplex – l’identité
allemande à travers la réception de l’œuvre de %%Beuys, Joseph|Joseph Beuys%% (1945-1986)*
en 2010. Elle enseigne l’histoire de l’art et organise plusieurs
expositions avant de diriger l’%%Musée d’Art Contemporain de la Communauté Germanophone de Belgique (IKOB)|IKOB%%-Musée d’art contemporain de la
Communauté germanophone de Belgique de 2013 à 2015. Elle se joint à
l’équipe de &#57355; avec des intentions claires&#x202F;: elle souhaite l’imposer
au cœur d’un réseau de réflexion critique et de recherche en art
contemporain. Elle relève un problème de compréhension publique des
missions et des activités de l’&#57356; pouvant être dû à la diversité de
manifestations proposées, ainsi qu’à une étiquette d’*école* qui remonte
à ses premières années d’existence, mais persiste dans l’imaginaire
collectif. Selon elle, il importe de réduire et de clarifier les champs
d’action de l’Institut qui, à l’époque, comprennent aussi bien des cours
ou des conférences d’initiation à l’histoire de l’art (une forme
d’éducation permanente qui rappelle le passé de la structure) que des
conférences spécialisées en art contemporain. Une modification des
activités et de l’image de l’&#57355; semble nécessaire pour le
repositionner dans le monde de l’art contemporain. Elle se familiarise
avec le microcosme et organise des discussions collectives au sein de
son équipe pour travailler sur la définition identitaire de l’Institut.
Qu’est-ce que l’&#57356;&#x202F;? Quelles sont ses forces&#x202F;? Comment peut-il se
positionner dans le secteur de l’art contemporain actuel&#x202F;? Prévu pour
clarifier la nature de la structure, ce travail d’introspection
institutionnelle s’extériorise de diverses manières. Le chantier est
rendu public, notamment à travers des communications.

À partir du 1<sup>er</sup> septembre 2016, les changements se succèdent
rapidement. La brochure du premier quadrimestre de l’année académique
2016-2017 donne le ton. La page de couverture affiche désormais un
nouveau logo. Le cercle orangé contenant l’acronyme «&#x202F;&#57355;&#x202F;» et le
slogan «&#x202F;Voir et comprendre l’art contemporain&#x202F;» est substitué par
l’expression «&#x202F;#Institut&#x202F;», écrit en noir sur blanc. Ce nouveau logo
devient un *label*, indice et symbole du statut d’*institut* de l’&#57356;.
Ni école, ni centre d’exposition, il s’agit d’une plate-forme de
réflexion et de recherche, d’un laboratoire qui rend l’expérimentation
possible à travers des tentatives fructueuses ou non, des nouveaux
formats d’activités souhaités plus englobants. Un échantillonnage de
caractères d’imprimerie de différentes tailles et polices est réparti
autour du logo. On pourrait croire à un nuancier de graphiste. La charte
graphique est renouvelée. Nouveau format, nouvelle mise en page,
trilinguisme, nouvelle terminologie. Dorénavant, les vernissages cèdent
la place aux *activations* et le nom commun *médiation* se transforme en
verbe au besoin. L’art contemporain doit être *médié*. La nouvelle image
de l’&#57356; est accompagnée d’une tentative de clarifier son spectre
d’activités aux yeux des publics. Une thématique annuelle crée un fil
rouge entre les expositions, les conférences, les tables rondes, les
résidences artistiques, entre autres. Une synergie est recherchée.
Stimulant sur le papier, le projet bouscule des habitudes au sein de
l’équipe et des publics. Les activités relevant de l’initiation sont
écartées. La nouvelle grille horaire calquée sur les grands centres
d’art contemporains internationaux effrite une histoire
institutionnelle.

En concertation avec son équipe, Maïté %%Vissault, Maïté|Vissault%% associe la programmation
de l’année académique 2016-2017 à la notion de communauté. Ce choix
évoque l’intérêt de la directrice envers les réseaux d’acteurs de
terrain. Au contraire de ses prédécesseurs qui invitaient surtout des
historiens et des théoriciens de l’art à prendre la parole devant les
auditeurs, elle invite et collabore régulièrement avec des curateurs
d’expositions et des directeurs de lieux de diffusion, par exemple. De
nouveaux points de vue émergent grâce à ces mises en contact. Le cycle
d’expositions «&#x202F;#Institut&#x202F;» décline ses trois volets entre le 7 octobre
et le 24 décembre 2016. La mise en relation d’une quinzaine
d’œuvres dévoile les nouveaux contours éditoriaux de l’&#57355;. Dès
le début de l’année suivante, le collectif %%Messidor%%, premier lauréat du
prix %%Connectif, Prix|Connectif%%[^86], rassemble ses œuvres au sein de l’exposition *Some
Arguments Later* à l’issue de sa résidence à l’&#57356;. Les trois volets
de «&#x202F;SYNC&#x202F; !&#x202F;» réunissent des collectifs d’artistes invités à
dialoguer sur ce que génère la notion de collectivité. Réalisé en
collaboration avec %%ARGOS ASBL|ARGOS%%, «&#x202F;COM∩∪TIES&#x202F;» (23&nbsp;septembre – 17&nbsp;décembre 2017) prend place en deux lieux. Dans ce
contexte, Maïté %%Vissault, Maïté|Vissault%% et Ive %%Stevenheydens, Ive|Stevenheydens%% (ARGOS) sélectionnent des
films et des vidéos en fonction de quatre catégories intitulées
«&#x202F;Urbanité&#x202F;», «&#x202F;Sociabilité&#x202F;», «&#x202F;Politique&#x202F;», «&#x202F;Environnement&#x202F;». Ils
organisent leur diffusion au cœur de dispositifs d’exposition conçus par
le duo d’artistes belges %%Sarah & Charles%%, et installés dans leurs
institutions respectives. Le dispositif de l’&#57355; occulte
l’architecture intérieure et prend la forme d’un écran de cinéma
classique. Il évoque l’importance de l’espace communautaire engendré par
le cinéma au XX<sup>e</sup> siècle. Celui d’%%ARGOS ASBL|ARGOS%% réfère plutôt au salon du XIX<sup>e</sup>
siècle. Dans les deux cas, les sélections d’œuvres projetées changent
toutes les trois semaines. L’année suivante débute avec le colloque
« COM∩∪TIES. Seuils/Drempels/Thresholds » (25 – 28&nbsp;janvier 2018). À son issue, Béatrice %%Delcorde, Béatrice|Delcorde%%
rassemble les œuvres qu’elle a élaborées durant une résidence à l’&#57356; :
l’exposition *On sort en bande, Labor of Love, Salad jours* clôture les
investigations collectives sur la notion de communauté. Toutes ces
expositions sont accompagnées de programmes de conférences et de
performances liées à leur contenu.

Entre septembre 2016 et avril 2018, Maïté %%Vissault, Maïté|Vissault%% assiste à
l’élaboration, la coordination, au montage et au démontage de trois
expositions en trois ou quatre volets et deux expositions de forme
traditionnelle. Cela équivaut à un cumul de douze expositions en
dix-sept mois d’ouverture publique. Ces expositions s’ajoutent à
l’édition de la revue *Patch*, dont le premier numéro[^89], coédité par
%%Cheval, Florence|Florence Cheval%%, Franz %%Drakkar, Franz|Drakkar%% et Yoann Van Parys, synthétise les idées
émergeant *durant* et *autour* du cycle «&#x202F;#Institut&#x202F;». Durant la même
période, les modalités du programme de résidences artistiques mis en
place en 2011 sont revues et le prix Hors-d’œuvre est remplacé par le
prix %%Connectif, Prix|Connectif%%. Si la multiplication des projets provoque un brassage
d’idées foisonnant, elle a l’inconvénient d’essouffler ceux qui en sont
à l’origine. Dans ce contexte, l’initiation, la consignation et la
transmission d’idées sont affectées ; la notion de communauté ne
s’enracine pas dans la construction d’un savoir. Cette cadence était
peut-être nécessaire pour entraîner un changement de fond dans la
structure, mais personne n’y était véritablement préparé. Maïté %%Vissault, Maïté|Vissault%%
quitte l’&#57355; au printemps 2018.

### Œuvrer au bénéfice des publics

Arrivé à l’&#57356; à titre d’historien de l’art en 2010[^90], Adrien
%%Grimmeau, Adrien|Grimmeau%% connaît le passé et le présent de l’&#57356; avant d’en imaginer le futur. Directeur à partir du printemps 2018, il choisit d’abandonner
toutes velléités de programmation et de laisser beaucoup de liberté à
ses collaborateurs. Si son travail éditorial consiste à créer des ponts
entre les divers champs d’activité, il insuffle pourtant l’une de ses
principales préoccupations par rapport à l’art contemporain dans la
grille d’activités de l’&#57356;&#x202F;: la médiation. Le fil conducteur entre
les champs d’activités se trouve dans la manière de communiquer *durant*
et *autour* des différentes manifestations. Il réintègre des cours
d’initiation à l’histoire de l’art contemporain dans le programme.
Quelques mois à peine après son arrivée, il programme un projet du duo
%%Denicolai & Provoost|Denicolai & Provoost%% intitulé «&#x202F;Banquet des dix gâteaux&#x202F;» ou «&#x202F;Tien
Taarten&#x202F;». %%Denicolai, Simona|Simona Denicolai%% et %%Provoost, Ivo|Ivo Provoost%% travaillent en duo, à
Bruxelles, depuis 1997. Leur pratique multidisciplinaire privilégie les
processus, courts ou longs, et implique des acteurs étrangers au monde
de l’art. À l’&#57355;, le 26 juin 2018, ils présentent l’aboutissement
d’un projet initié et soutenu à Genk par la %%Kunstcel%% dans le contexte du
«&#x202F;Pilootprojecten Kunst in Opdracht&#x202F;», en 2015. Les artistes dévoilent dix
gâteaux. Leur forme est élaborée suite à des discussions approfondies
avec des habitants ou collectifs de Genk ayant fait la demande d’un
monument dans la ville. Chacun de ces gâteaux évoque un monument désiré,
lié à un événement majeur de l’histoire de la ville, de la résistance
durant la Seconde Guerre mondiale à la fermeture des mines puis de Ford
Genk. Offerts à la dégustation, ils sont rassembleurs. Ils permettent de
commémorer, de digérer des événements difficiles. À travers ce projet,
des perspectives communes émergent entre l’histoire de l’&#57356; et son
nouveau directeur&#x202F;: l’art qui informe sur l’être humain et ses mondes,
l’art comme vecteur de cohésion sociale, la médiation et l’espace
urbain.

La médiation s’introduit dans tous les interstices. Son omniprésence
ouvre une réflexion de fond au sein de l’équipe de l’&#57355; qui s’y est
pourtant confrontée deux ans auparavant, durant l’élaboration du cycle
d’expositions «&#x202F;\#Institut&#x202F;» à l’automne 2016&#x202F;: Qu’est-ce que la
*médiation* et quelles en sont les limites&#x202F;? L’historien de l’art Pierre
Arese, qui fait partie de l’équipe de l’&#57356; depuis la fin de l’été
2017, s’engage officiellement auprès des publics à partir de
juin 2019. Il hérite d’une grande responsabilité puisqu’un nombre
croissant d’activités se rattachent désormais à ce pôle. Pédagogue hors
pair, il est outillé pour la prendre à bras le corps. Ses heures de
travail ne lui permettant pas de développer la médiation comme il
l’entend, un poste de chargé des publics scolaires est ouvert.
L’historienne de l’art Géraldine %%Marchal, Géraldine|Marchal%% se joint à l’équipe. Le binôme
développe une brochure de vingt-cinq pages qui reprend toutes les
activités de médiation proposées par l’&#57356;. La nomenclature est
précédée d’un texte éditorial annonçant un objectif clair&#x202F;: «&#x202F;Faire
émerger, par la mise en commun d’expériences vécues, un propos donnant
du sens à des essais esthétiques multiples qui s’insèrent tantôt dans la
prospection de la réalité tangible, tantôt dans l’échappée
utopique.&#x202F;» Cette approche teinte toutes les visites guidées
d’expositions, les marches urbaines visant la découverte de l’art public
et du *street art* bruxellois, les workshops créatifs dans les
expositions, les stages pour enfants créés et encadrés par des artistes
durant les vacances scolaires, les séances de lecture dispensées par des
conteurs professionnels, les ateliers de lecture d’images, les ateliers
d’analyse d’œuvres d’art moderne et contemporain, etc. Souvent réalisées
à la carte pour des classes qui en font la demande, ces activités
demeurent plus ou moins invisibles dans la programmation officielle de
l’&#57355;. Or, elles attirent pourtant 25% de son public actuel.

En novembre 2019, %%Arese, Pierre|Pierre Arese%% et Géraldine %%Marchal, Géraldine|Marchal%% invitent l’artiste %%Bica, Lætitia|Lætitia Bica%% à entamer
un cycle d’ateliers intergénérationnels à l’&#57355;. Une dizaine d’élèves
en quatrième année de l’école primaire %%École Baron Steens|Baron Steens%% et de pensionnaires
de la maison de %%Maison de repos et de soins « Aux Ursulines »|repos et de soins%% «&#x202F;Aux Ursulines&#x202F;» se joignent à elle
pour partager au sujet de leur rapport à l’engagement, à ses indices et
ses symboles. Au fil des échanges, ils donnent des formes matérielles
à leurs réflexions. L’exposition *Point de croix*, présentée du 24
janvier au 21 mars 2020 dans l’%%Espace Satellite%% de l’&#57356;[^92], rend
compte de leurs rencontres. Ce projet représente une manifestation
concrète de la perméabilité croissante entre les secteurs scolaire et
artistique observée par Sébastien %%Marandon, Sébastien|Marandon%%. Selon lui, de plus en plus
d’artistes s’intègrent au sein d’équipes pédagogiques dans le but de
développer des résidences combinant recherche, expérience artistique et
pédagogie. À juste titre, il développe un cycle de rencontres et de
conférences pour examiner les conséquences de ce nouveau phénomène.
«&#x202F;Que disent ces convergences sur l’évolution de l’école aussi bien que
de l’art&#x202F;? Sur les conceptions changeantes de l’apprentissage de la
citoyenneté, de la construction de l’individu et du commun à l’école&#x202F;?
Sur la place renouvelée qu’occupe l’art dans ce domaine de «&#x202F;formation »
?&#x202F;»[^93] La première itération de ce cycle prend place à l’&#57355; sous
l’intitulé «&#x202F;L’artiste-pédagogue, pédagogue artiste&#x202F;?&#x202F;» (13 février
2020). Elle consiste en une rencontre avec %%Busine, Laurent|Laurent Busine%%, fondateur et
directeur du %%Musée des Arts contemporains de la Fédération Wallonie-Bruxelles (MAC’s)|MAC’s%%, Musée des arts contemporains de la Fédération
Wallonie-Bruxelles, entre 2002 et 2016.

Le lien avec les écoles n’est toutefois pas l’apanage exclusif du pôle
Médiation. La transmission des connaissances et l’apprentissage par la
pratique font partie intégrante de l’ADN de l’&#57356;. Le pôle Exposition
s’engage également auprès des étudiants. Outre l’encadrement de nombreux
stages et les prêts de locaux pour les jurys d’écoles supérieures, il offre
la totalité de ses espaces d’exposition à deux reprises à la fin des
années 2010. Au printemps 2018, l’&#57356; met ses espaces d’exposition à
la disposition des étudiants de deux écoles gantoises&#x202F;: le %%Hoger Instituut voor Schone Kunsten (HISK)|HISK%% et la
%%Koninklijke Academie voor Schone Kunsten (KASK)|KASK%%. Jeunes artistes et commissaires d’expositions s’associent pour
élaborer et réaliser l’exposition *State of Statelessness*. Un an plus
tard, les étudiants du programme de Master en pratiques de l’exposition –
%%CARE%%, de l’%%Académie royale des Beaux-Arts de Bruxelles (ArBA — ESA)|ArBA%%-EsA (Bruxelles), regroupent des interventions de leur
cru ou d’artistes invités au sein de *Sly Composition*.

En mars 2020, au début du confinement de la Belgique dû à la crise
sanitaire provoquée par le coronavirus, Ludovic %%Lamant, Ludovic|Lamant%% signe un article
intitulé «&#x202F;Biennales, fin de partie&#x202F;?&#x202F;» dans *Mediapart*.[^94] Sa
réflexion décrit clairement ce que de nombreux professionnels du milieu
artistique ressentent depuis longtemps&#x202F;: le monde de l’art doit entrer
dans un nouveau paradigme et s’intéresser davantage aux réseaux locaux,
à l’écologie, l’inclusion, la lenteur, etc. Cette tendance s’immisce
d’ailleurs dans la programmation de l’&#57355; depuis quelques années.
Au-delà du fait que les œuvres d’art contemporain reflètent des aspects
du monde dans lequel nous vivons, et qu’elles entraînent les acteurs
culturels à aborder ces aspects au sein d’événements divers, on pressent
une volonté plus affirmée d’aborder des enjeux liés à la société. En
témoignent les nombreuses collaborations, les liens avec les écoles, les
efforts pour établir un véritable contact avec les publics, les
tentatives renouvelées de révéler le pouvoir des œuvres et de l’art.
Quelques exemples sont particulièrement significatifs.

En s’engageant dans la direction artistique de l’exposition *Gisements*, présentée lors de la soirée d’inauguration de %%Recy-K%%[^95] (25 octobre 2016) et appelée à devenir pérenne quelques années plus tard, l’&#57356; contribue à l’expérimentation et au développement de nouvelles formes d’économies. Il invite les artistes Joachim %%Coucke, Joachim|Coucke%%, Adrien
%%Tirtiaux, Adrien|Tirtiaux%% et Thierry %%Verbeke, Thierry|Verbeke%% à s’imprégner du site de %%Recy-K%% pour réfléchir aux matériaux de production et aux relations sociales. Pour l’occasion, trois œuvres monumentales sont élaborées à partir de ressources recyclées, et accompagnées d’œuvres antérieures. Ce projet donne corps à une préoccupation écologique qui résonne au cœur d’autres projets menés par l’&#57355;, tels que l’exposition *L’Art d’accommoder les restes* (23 juin - 23 juillet 2017). Dans ce contexte, les trois mêmes artistes prolongent leurs réflexions en développant des propositions plastiques installées dans les salles de l’&#57356;.

Depuis quelques années, l’historien de l’art Laurent %%Courtens, Laurent|Courtens%% reconduit
une collaboration avec %%Collectif formation société-Insertion socio-professionnelle (CFS-ISP)|CFS-ISP%% (Collectif formation société – Insertion
socio-professionnelle) visant la production d’une formation de trois
jours intitulée « La fabrique de l’autre ». À chaque itération, il se
joint à des chercheurs pour tenter de défaire des représentations de
l’*autre* héritées du colonialisme.

En outre, plusieurs expositions d’œuvres contemporaines touchent
également à de profondes mutations du monde actuel, en cours ou
souhaitées. *A Forest* (2018) rassemble des artistes qui s’intéressent
aux écosystèmes du monde végétal. *Unexistant* (2019) est une
exposition-installation conçue par l’artiste belge Alec %%De Busschère, Alec|De Busschère%% qui
aborde la notion identitaire en parallèle avec les réseaux sociaux.
L’exposition collective *Babel* (2019) examine la question des langues
qui rapprochent ou divisent les citoyens du monde entier. L’installation
*Entre intérêts* (2019), produite par Maëlle %%Dufour, Maëlle|Dufour%%, traite des systèmes
de surveillance qui ponctuent nos déplacements quotidiens. *Some Things*
(2020) est une exposition individuelle des œuvres de l’artiste
belgo-islandaise Guðný Rósa %%Ingimarsdóttir, Guðný Rósa|Ingimarsdóttir%% qui invite à profiter du
silence pour développer une attention soutenue et entretenir un rapport
intime avec des œuvres sur papier. *Savoir-faire* (programmée au
printemps 2020, reportée en septembre 2021) révèle des gestes liés à la
production et la consommation capitaliste, ou à la persistance et la
résistance face à cette idéologie. Bien que non-exhaustifs, ces quelques
exemples, indices de dysfonctionnements de plus en plus abordés par les
artistes actuels, révèlent une forme de mobilisation face à des enjeux
cruciaux.

Il n’est pas anodin que l’&#57355; présente l’exposition *Inspire* (2020) à
l’occasion de son cinquantième anniversaire. Les œuvres rassemblées au
sein de ce projet véhiculent différentes conceptions du temps et, par
extension, invitent à décélérer pour prendre conscience de la valeur de
notre propre temps. Si l’heure était à la fête lors du quarantième
anniversaire de l’&#57356; en 2011, l’heure actuelle est plutôt à la prise
de conscience et à l’action collective. Peut-être dû au contexte
sanitaire actuel, ou au contexte géopolitique et climatique incertain
qui invite à un recentrement sur l’espace social, ce nouvel état
d’esprit rétablit une salutaire connexion entre l’art et la réalité
sociale qui brille par son absence dans certaines sphères du monde de
l’art. On aura au moins vu émerger une solidarité rassurante dans le
secteur des arts plastiques belges durant cette crise sanitaire.
Différentes initiatives ont vu le jour pour fédérer les artistes et
établir des réseaux de soutien s’adressant aux moins nantis, qui se sont
vus amputer des contrats de travail. Espérons qu’ils seront pérennes.

Si les expositions constituent l’interface publique la plus évidente de
l’&#57356;, elles s’inscrivent néanmoins dans une liste d’activités
régulières incluant des cours, des conférences et des tables rondes. La
dimension scientifique insufflée par Éric %%Van Essche, Éric|Van Essche%% n’a jamais quitté
l’&#57355;. L’historien de l’art Laurent %%Courtens, Laurent|Courtens%% la nourrit depuis près de
dix ans. Outre les conférences qu’il produit et dispense lui-même, il
invite de nombreux intervenants à présenter leurs travaux de recherche
aux auditeurs de l’Institut. Professeurs généralistes ou spécialisés,
collectionneurs passionnés, chercheurs et acteurs de terrain partagent
leurs savoirs à l’Institut. Ils greffent parfois leur intervention au
cœur d’un programme inhérent aux problématiques soulevées par une
exposition. Par exemple, Laurent %%Courtens, Laurent|Courtens%% a développé un ambitieux
programme autour de son exposition *A Forest*. Inauguré par une
conférence de Paul %%Sztulman, Paul|Sztulman%% («&#x202F;La Forêt nouvelle&#x202F;»), il se poursuivait
par des interventions de l’ingénieure-agronome Caroline %%Vinck, Caroline|Vinck%%
(«&#x202F;L’écosystème forestier, mode d’emploi&#x202F;»), l’actrice et dramaturge
%%Dumont, Isabelle|Isabelle Dumont%% («&#x202F;Hortus Minor&#x202F;»), l’écrivain et jardinier Marco
%%Martella, Marco|Martella%% («&#x202F;La forêt est un jardin&#x202F;»), etc. Il s’agit alors d’une
véritable occasion de construire un savoir polymorphe,
interdisciplinaire. Il n’est pas anodin non plus que Laurent %%Courtens, Laurent|Courtens%%
programme une rencontre entre Sébastien %%Marandon, Sébastien|Marandon%% et Christian %%Ruby, Christian|Ruby%%
intitulée «&#x202F;Comment le public advient-il&#x202F;?&#x202F;» pour la séance inaugurale
de l’année académique 2020-2021. Ces deux intervenants tentent d’arrimer
des expériences vécues à des théories et discutent, au passage, de la
notion de public comme construction sociale. Cet événement introduit une
perspective critique au sein d’un concept clé pour l’&#57355;.

### Quelles formes donner aux savoirs mis en circulation&#x202F;?

L’institutionnalisation de l’articulation entre la pratique et la
réflexion sur l’art au sein des écoles supérieures d’art modifie
lentement, mais durablement l’activité artistique. La prise d’importance
de la figure de l’artiste-chercheur découle certainement de cette
modification du système académique. À la croisée des domaines artistique
et académique se situe d’ailleurs un nouveau genre d’intervention qui
prend la forme de conférence-performance. Des artistes créent de
nouvelles approches discursives, de nouveaux formats de parole en
présentant oralement le fruit de leurs recherches devant le public.
Récemment invités à l’&#57356;, Marie %%Cantos, Marie|Cantos%%, Jean-Philippe %%Convert, Jean-Philippe|Convert%% et Clara %%Thomine, Clara|Thomine%%
sont à l’origine de ce type de pratique polyphonique de plus en plus
courante.

La nature et le contexte des résidences d’artistes se transforment
également au cours des dernières années. Si on assiste à une
augmentation du nombre de résidences artistiques dispensées en Belgique
et dans d’autres pays depuis le début des années 2000, on observe à
l’&#57356; que les tâches réalisées durant les résidences
changent. Depuis la création du programme en 2011 et surtout depuis sa
refonte en 2017, de plus en plus d’artistes choisissent d’employer ce
temps de travail privilégié pour cumuler des articles, lire, écrire,
chercher. Ils travaillent avec un ordinateur et donnent des formes
artistiques à leurs hypothèses et découvertes. Les matériaux qui se
retrouvaient généralement dans l’espace de travail sont de plus en plus
virtuels. On passe de la résidence de production à la résidence de
recherche. Que cherche l’artiste&#x202F;? Malgré de nombreuses tentatives de
définir et synthétiser les spécificités de la recherche en art, d'en cerner la nature dans les milieux artistique et académique, celle-ci se refuse à toutes catégories. En revanche, les
modalités pouvant rendre la recherche visible peuvent faire l’objet
d’essais concrets dans l’espace. C’est ce qu’a fait %%Barret, Pascale|Pascale Barret%%, en
compagnie d’%%Abrahams, Annie|Annie Abrahams%% et Alix %%Desaubliaux, Alix|Desaubliaux%% (%%3G/eneration%%), au sein
de l’installation *Constallations*, qui prenait place dans l’Espace
Satellite de l’&#57356; au printemps 2019. Un dispositif juxtaposant trois
vidéos et de nombreux extraits d’archives (images, extraits de
discussions, captures d’écrans d’ordinateurs, etc.) rendait visibles les modes de
recherche des trois protagonistes. Ainsi la recherche
prenait-elle une forme artistique sur tous les plans.

Si la recherche artistique est fertile et soutenue
par de nombreux espaces de diffusion à travers des programmes de
résidences, la recherche scientifique éprouve plus de difficultés à
s’émanciper du système académique. Où sont les chercheurs en histoire de
l’art&#x202F;? Quels moyens met-on à leur disposition pour qu’ils puissent
poursuivre leurs recherches à l’extérieur du cadre privilégié de
l’université&#x202F;? Sans historiens de l’art actifs, aucune histoire ne
s’écrit et ne se pérennise. Face à la récurrente difficulté de trouver
des historiens de l’art habilités à prendre la parole à l’Institut,
alors que des dizaines d’étudiants sont diplômés chaque année, il est
devenu évident que de nombreuses envies de recherche se dissipent face à
l’absence de structures de soutien. Pour réagir à ce manque et soutenir
de jeunes chercheurs, l’&#57355; entame un programme de résidences
scientifiques à l’automne 2020. Il souhaite ainsi contribuer à la
mobilisation des idées et des recherches touchant, de près ou de loin, à
l’écriture ou la réécriture de l’histoire de l’art, ou de certains de
ses enjeux. %%Vancampenhoudt, Lyse|Lyse Vancampenhoudt%%, dont les recherches portent
principalement sur la visibilisation du travail des femmes artistes,
devient la première chercheuse en résidence au cours de l’automne 2020.

Le confinement lié à la crise sanitaire, événement qui fera tristement
date, est une occasion pour le personnel de l’&#57356; de faire évoluer
son mode de fonctionnement par rapport à la programmation de ses
conférences. Entamée dès l’automne 2018 mais jamais systématisée, la
mise en ligne de ses conférences permet une importante visibilité et une
salutaire écoute tant pour les conférenciers, les historiens de l’art
affiliés à l’&#57356;, les chercheurs et tous les autres publics
s’intéressant à l’art. Si bien que %%Arese, Pierre|Pierre Arese%% et Laurent %%Courtens, Laurent|Courtens%%
développent leurs propres émissions, qui sont transmises sur la
plate-forme %%SoundCloud%% de l’&#57355; à partir de mai 2020. *L’Art au
poing* présente des temps forts de l’histoire récente de l’art, des
parcours d’artistes engagés et des événements marquants[^96]. Ce
programme, sous la responsabilité de %%Arese, Pierre|Pierre Arese%%, donne également la
parole à d’autres historiens de l’art. Dans *L’Art au poing \#4*, par
exemple, Pierre-Yves %%Desaive, Pierre-Yves|Desaive%% traite de la %%Documenta%% X, présentée en
1997. Par ailleurs, *Zoom – La parole à l’œuvre* est une rubrique
imaginée par Laurent %%Courtens, Laurent|Courtens%%. Dans ce contexte, un historien, un
curateur ou un critique d’art décrit, examine et analyse une œuvre d’art
de son choix. Laurent %%Courtens, Laurent|Courtens%% entame la série en traitant d’une œuvre
de Teresa %%Margolles, Teresa|Margolles%% intitulée *Pistas de Baile* (photographies, 2016),
avant de passer la parole à d’autres professeurs intervenant
régulièrement à l’&#57356;[^97].


### D’une approche sociologique de l’art à la co-construction de sens

La cinquième décennie de l’histoire de l’&#57355; est traversée par une
crise identitaire qui entraîne une profonde remise en question de ses
missions, de ses priorités, de la répartition du travail. Elle touche
son fonctionnement interne, mais également sa portée externe. Trois
personnes se succèdent à la direction et investissent la lourde
responsabilité de repenser ses fondements. Éric %%Van Essche, Éric|Van Essche%% (2010-2015),
Maïté %%Vissault, Maïté|Vissault%% (2016-2018) et Adrien %%Grimmeau, Adrien|Grimmeau%% (2018 jusqu’à aujourd’hui)
tentent, chacun à leur tour, de clarifier ses missions aux yeux des
publics. Comme l’&#57356; produit des expositions et propose des
conférences en auditorium – deux types d’activités qui résistent parmi
un éventail de possibilités depuis 50 ans –, on le compare rapidement à
un centre d’exposition. Or, sa spécificité ne se situe pas exclusivement
dans son recours à ces deux modes d’intervention. Ceux-ci ne constituent
que deux moyens de toucher la mission de l’&#57355;. En réalité, ils
occultent le véritable fondement identitaire de l’Institut, qui prend
corps à travers des remises en question et des échanges humains plutôt
qu’à travers un type d’activité en particulier. Si l’on compare
l’objectif initial de %%Brys-Schatan, Gita|Gita Brys-Schatan%% avec celui des historiens de
l’art qui se sont rassemblés pour la remplacer depuis vingt ans, on
observe qu’ils sont identiques malgré une formulation différente. Dans
l’optique de démocratiser l’accès à l’art contemporain et d’offrir une
éducation artistique aux citoyens en les incluant dans l’histoire
relatée, %%Brys-Schatan, Gita|Gita Brys-Schatan%% emploie une approche sociologique de l’art et
de la culture. Cinq décennies plus tard, le désir d’abolir le fossé
divisant l’art contemporain et les publics imprègne toujours le
personnel de l’&#57356;. À travers les personnalités d’Arlette %%Lemonnier, Arlette|Lemonnier%%,
Éric %%Van Essche, Éric|Van Essche%%, Maïté %%Vissault, Maïté|Vissault%%, et peut-être encore davantage celle
d’Adrien %%Grimmeau, Adrien|Grimmeau%%, ce désir trouve réponse dans un profond
investissement envers les publics et la médiation qui leur est adressée.
Le partage des connaissances devient dialogique. On parle dorénavant
d’art et d’histoire de l’art face aux œuvres. La méthode change, mais le
projet social demeure le même&#x202F;: conserver une mémoire de l’histoire de
l’art en l’arrimant à un contexte de production et aux phénomènes
sociaux qui l’imprègnent.


[^1]: Virginie %%Devillez, Virginie|Devillez%% a publié des textes sur le milieu de l’art belge
    de la fin des années 1960 et des années 1970. Elle souligne, dans
    l’un deux, qu’il s’agit d’une des plus anciennes foires dans
    l’histoire récente de ce type d'événements puisqu’elle précède %%Art Basel%% (1970)
    et la %%FIAC%% (1974). Virginie %%Devillez, Virginie|Devillez%%, «&#x202F;Galeriemodellen, Vroeger en
    Nu », *Veldanalyse Beeldende Kunst*, 2013, Instituut voor Beeldende,
    audiovisuele en mediakunst (%%Instituut voor Beeldende, audiovisuele en mediakunst (BAM)|BAM%%), 2013, p. 10. Une version française de ce texte, intitulée «&#x202F;D’hier à aujourd’hui,
    les nouveaux modèles de galerie&#x202F;», également disponible sur
    Internet, est publiée en collaboration avec %%Salon, Le|Le Salon%%.

[^2]: Dans une lettre datée du 4 mai 1959, Henri %%Janne, Henri|Janne%%, alors recteur de
    l’Université libre de %%Bruxelles%% (%%Université libre de Bruxelles (ULB)|ULB%%), annonce à Joseph-Berthold
    %%Urvater, Joseph-Berthold|Urvater%% que l’%%Université libre de Bruxelles (ULB)|ULB%% a créé un cours d’histoire de l’art contemporain
    suivant l’une ses recommandations advenue un an plus tôt. À la fin des années 1960, la chaire d’histoire de l’art contemporain s’est
    sensiblement développée puisqu’elle comporte désormais 150 heures
    de cours par année. Cette lettre est conservée aux %%Archives de l’Art
    contemporain en Belgique (AACB)%%.

[^3]: Suite à cette séance inaugurale, les premiers cours se font dans
    des locaux prêtés par %%École nationale supérieure d’architecture et des arts visuels de La Cambre (La Cambre)|La Cambre%%.

[^4]: Actif depuis 1967 en rhétorique, en sémiotique et en théorie de la
    communication linguistique et visuelle, le collectif
    interdisciplinaire liégeois %%Groupe µ%% (Jean-Marie %%Klinkenberg, Jean-Marie|Klinkenberg%%,
    Francis %%Edeline, Francis|Edeline%%) contribue à sa théorisation.

[^5]: *Environnement-Bruxelles*, n°9, 1971.

[^6]: *Idem*.

[^7]: Les archives de l’ISELP traduisent une organisation plutôt
    irrégulière des rencontres %%Inforart%% malgré la volonté d’une
    fréquence mensuelle.

[^8]: Dennis %%Oppenheim, Dennis|Oppenheim%%  expose des œuvres au %%Palais des Beaux-Arts (BOZAR)|palais des Beaux-Arts%% du 28
    mai au 29 juin 1975. La publication qui accompagne son intervention
    contient notamment un texte signé par Jean-Pierre Van Tieghem.

[^9]: Ce programme dispensé à partir de l’automne 1973 est mené par des
    spécialistes des sciences pédagogiques. Il s’adresse aux professeurs
    de dessin, d’histoire, de français et de morale, ou à tout autre
    enseignant du secondaire supérieur et de l’enseignement artistique
    susceptible d’introduire des données ayant trait à l’art dans ses
    cours. 

[^10]: Ces données réfèrent à l’année académique 1978-1979.

[^11]: En 1971, une cotisation annuelle de membre adhérent coûte 500
    francs belges (aujourd’hui 12,39 euros) et la qualité de membre à
    vie s’acquiert par le versement d’une cotisation égale à dix fois
    la cotisation annuelle, soit 5&#x202F;000 francs belges (123,95 euros). En
    plus de cet abonnement, les auditeurs doivent payer les cours,
    conférences et autres rencontres de manière individuelle&#x202F;: 3&#x202F;000
    francs belges (74 euros) le semestre, à raison d’un cours d’une
    heure par semaine, pour un total de vingt séances.

[^12]: Cette information figure sur un document conservé dans les
    archives de l’ISELP, mais n’est pas corroborée par des preuves
    concrètes.

[^13]: Collectif, *ISELP dix années. Institut supérieur pour
    l’étude du langage plastique*, Bruxelles, Commission française de la
    Culture de l’Agglomération de Bruxelles, 1981, p. 11.

[^14]: %%Forest National%% (palais des Sports et du Spectacle de Bruxelles)
    est inauguré le 8 octobre 1970 par les danseurs du Ballet du XX<sup>e</sup>
    siècle de %%Béjart, Maurice|Maurice Béjart%%. En 1974, son directeur artistique,
    Albert-André %%L’Heureux, Albert-André|L’Heureux%%, intègre les arts plastiques à son programme
    et permet à l’ISELP d’inaugurer un nouvel espace récemment ajouté à
    ses locaux.

[^15]: %%Brys-Schatan, Gita|Gita Brys-Schatan%% reprend un certain nombre d’œuvres présentées
    dans le contexte de l’exposition *Wege Zur Computer*, organisée par
    le %%Goethe Institut%%. Il s’agit d’œuvres de %%Besset, Klaus|Klaus Besset%%, Pierre
    %%Cordier, Pierre|Cordier%%, Hein %%Gravenhorst, Hein|Gravenhorst%%, Karl %%Holzaeuser, Karl|Holzaeuser%%, Gottfried %%Jaeger, Gottfried|Jaeger%%,
    Herbert W. %%Franke, Herbert W.|Franke%%, Manfred %%Mohr, Manfred|Mohr%%, Georg %%Nees, Georg|Nees%%, Freider %%Nake, Freider|Nake%%, Karl
    %%Siebig, Karl|Siebig%%, Eth %%Studenten, Eth|Studenten%% et Demetrios %%Yannakopoulos, Demetrios|Yannakopoulos%%. Elle invite Auro
    %%Lecci, Auro|Lecci%%, Peter %%Struycken, Peter|Struycken%%, Alexandre %%Vitkine, Alexandre|Vitkine%% et Edward %%Zajec, Edward|Zajec%% à se
    joindre à eux, ainsi que quelques artistes de %%MBB Computer Graphics%%
    et du Groupe Art et informatique de %%Vincennes, Groupe de|Vincennes%%.

[^16]: Herbert W. %%Franke, Herbert W.|Franke%%, *Computer Graphics – Computer Art* (deuxième
    édition), Berlin/Heidelberg, Springer-Verlag, 1985 (1971), pp.
    105-107. %%Brys-Schatan, Gita|Gita Brys-Schatan%% présente à son tour un historique des
    expositions portant sur le sujet réactualisé à la fin de sa
    publication *Art et ordinateur*. Elle y ajoute son projet.

[^17]: *Art et ordinateur* et *Artists Videotapes*, exposition organisée
    par %%Baudson, Michel|Michel Baudson%% et présentée au %%Palais des Beaux-Arts (BOZAR)|palais des Beaux-Arts%%  de
    %%Bruxelles%% en février 1975, abordent des sujets distincts. *Artists
    Videotapes* est un projet majeur qui mène à la constitution de la
    vidéothèque de %%Jeunesse et Arts Plastiques (JAP)|Jeunesse et%% Arts Plastiques, et à son enrichissement
    jusqu’au milieu des années 1980. Par contre, le palais de Beaux-Arts
    de %%Bruxelles%% (aujourd’hui Bozar) organise trois éditions du Festival
    international de %%Festival international de musique électronique, vidéo et Computer Art|musique électronique, vidéo%% et *Computer Art* en
    1980, 1981 et 1982. La deuxième édition se déroule du 28 octobre au
    10 novembre 1981 et investit plusieurs sites, dont le palais des
    Beaux-Arts, le %%Plan K%%, la %%Galerie Ravenstein|galerie Ravenstein%% et le %%Shell Auditorium%%. Il
    s’agit de l’édition la plus ambitieuse.

[^18]: La Commission française offre un subside régulier à l’ISELP à
    partir de 1975.

[^19]: Cette citation est extraite du texte «&#x202F;Le %%Milan, passage de|passage de Milan%%&#x202F;: des
    écuries du %%Egmont, palais d’|palais d’Egmont%% à l’ISELP&#x202F;», écrit par Adrien %%Grimmeau, Adrien|Grimmeau%% à
    l’occasion des Journées du patrimoine 2019. Les informations
    concernant l’histoire du bâtiment du %%Egmont, palais d’|palais d’Egmont%% et de ses
    nouvelles écuries sont majoritairement issues de ses recherches.

[^20]: *Idem*.

[^21]: Monique Verken, *50 ans. %%Palais des Beaux-Arts (BOZAR)|palais des Beaux-Arts%%  de Bruxelles*,
    Bruxelles, %%Palais des Beaux-Arts (BOZAR)|palais des Beaux-Arts%%  de Bruxelles, 1978.

[^22]: L’*Exposition internationale de tampons d’artistes* est le fruit
    d’une collaboration avec %%Benon, Jean-Pierre|Jean-Pierre Benon%%, la %%Galerie Stempelplaats|galerie Stempelplaats%%
    d’Amsterdam, Johan %%Van Geluwe, Johan|Van Geluwe%% et son «&#x202F;Museum of Museums&#x202F;»,
    ainsi qu'Hervé %%Fischer, Hervé|Fischer%%.

[^23]: %%Brys-Schatan, Gita|Gita Brys-Schatan%% met l’accent sur la provenance des envois
    postaux plutôt que sur le nom des artistes dans l’introduction du
    catalogue de l’exposition. Elle renchérit néanmoins plus loin&#x202F;:
    «&#x202F;Certains auteurs réputés pour avoir ’exposé leurs œuvres’ dans les
    galeries les plus célèbres… d’autres vraisemblablement peu
    connus.&#x202F;» La longue liste d’artistes suit le texte d’introduction du
    catalogue&#x202F;: %%Ben|Ben%%, Jacques %%Lennep, Jacques|Lennep%%, %%Metallic Avau|Metallic%% Avau, Luc %%Van Malderen, Luc|Van Malderen%%,
    etc.

[^24]: Le %%Palais des Beaux-Arts de Charleroi|palais des Beaux-Arts%% de %%Charleroi%% accueille un autre volet de
    l’exposition *Musée de poche. Inventaire permanent 1 – Milieu
rural*, organisée et mise en circulation par le Centre d’Action
    Culturelle d’Expression française
    (%%Centre d’action culturelle d’expression française (C.A.C.E.F.)|C.A.C.E.F.%%), la Commission
    française de la Culture de l’Agglomération de %%Bruxelles%% (%%Commission française de la Culture de l’Agglomération de Bruxelles (C.F.C)|C.F.C.%%), la
    Ville de Liège et le %%Palais des Beaux-Arts de Charleroi|palais des Beaux-Arts%% de %%Charleroi%%.

[^25]: %%Prévost, Claude|Claude%% et %%Prévost, Clovis|Clovis%% Prévost documentent les projets de nombreux créateurs inclassables, hors-normes, qui évoluent en marge des
    réseaux artistiques officiels. Leur documentation prend la forme d’articles, de photos et de vidéos. L’exposition *Les Bâtisseurs de
    l’imaginaire* a été présentée en divers lieux au fil des années. Le duo a publié ses recherches et ses
    documents au sein d’un livre augmenté et réédité à quelques reprises&#x202F;: Claude et Clovis Prévost, *Les Bâtisseurs de l’imaginaire*, %%Paris%%, Klincksieck, 2016.

[^26]: La création de l’Espace Éphémère advient la même année que la
    création de l’ASBL bruxelloise %%Contretype%%, également dédiée à
    l’exposition de photographies.

[^27]: Cette citation est extraite d’un document annonçant la création de l’Espace Éphémère.

[^28]: Cette information est issue de la rubrique «&#x202F;Centre d’accueil&#x202F;» d’un journal trimestriel publié par l'ISELP : *ISELP - Institut supérieur pour l'étude du langage plastique asbl*, n°4, décembre 1983.

[^29]: Adrien %%Grimmeau, Adrien|Grimmeau%% approfondit ce sujet dans un essai intitulé «&#x202F;Art Espace Public. D'%%Environnemental%% à Being Urban, l'ISELP et l'art public&#x202F;», publié dans le présent ouvrage.

[^30]: Cet intérêt envers l’urbanisme se manifeste également lors du
    premier colloque organisé par l’ISELP, en 1978, autour du thème de
    «&#x202F;La reconstruction de la ville européenne&#x202F;». Cet événement
    rassemble des spécialistes internationaux.

[^31]: %%Brys-Schatan, Gita|Gita Brys-Schatan%%, «&#x202F;Manifeste pour l’%%Environnemental%%&#x202F;»,
    *%%Environnemental%%*, cahier n°1, 1983, pp. 5-9.

[^32]: À titre d’organisateur d’exposition, %%Martin, Jean-Hubert|Jean-Hubert Martin%% revient
    sur ce projet dans le contexte d’un essai intitulé «&#x202F;L’art des
    villes et l’art des champs&#x202F;» et publié dans la publication *Mémoire
    de l’histoire de l’art*, deux ans plus tard. Cet essai semble
    confirmer l’attrait croissant envers l’art non européen,
    observable dans la programmation de l’ISELP tout au long des années
    1980.

[^33]: Par exemple, les expositions *La Roumanie salue l’%%Europe%% *(1988)
    et *Travaux d'architectes paysagistes danois contemporains* (1991) effectuent également des déplacements culturels
    favorisant la diversité. On y présente d’autres histoires, d’autres
    méthodes.

[^34]: L’ouverture d’%%ARGOS ASBL|ARGOS ASBL%% (aujourd’hui ARGOS. Centre for
    Audiovisual Arts) en 1989, à Bruxelles, cristallise l’intérêt
    croissant pour le film et la vidéo. Ce centre soutient toutes
    créations d’images en mouvement échappant au cinéma. À l’affût des
    développements du secteur, il distribue, archive, conserve, expose
    et projette les créations audiovisuelles artistiques belges.
    Surtout, il les promeut à l’étranger. Cette nouvelle entité permet
    le développement d’expertises liées à l’art vidéo, aux documentaires
    fictionnels et autres films expérimentaux réalisés par des artistes.

[^35]: %%Magie rouge%%, probablement un lieu de diffusion, situé à
    Bruxelles, au 52, rue Maes (Ixelles), accueille l’autre volet de
    l’exposition *%%Méliès, Georges|Méliès%% l’enchanteur*.

[^36]: La chanteuse Beverly Jo %%Scott, Beverly Jo|Scott%% (BJ %%Scott, Beverly Jo|Scott%%), accompagnée du groupe
    de musique %%By Chance%%, fait une prestation le soir du vernissage.

[^37]: %%Brys-Schatan, Gita|Gita Brys-Schatan%%, *Pochettes de disques&#x202F;: enluminures du XX<sup>e</sup>
    siècle*, Bruxelles, Éditions de l’ISELP, 1983, p. 4.

[^38]: *Ibid*., p. 18.

[^39]: Cette citation est imprimée sur le document en question, conservé dans les archives de l’ISELP.

[^40]: Cette exposition est présentée dans l’Espace Éphémère du 2 au 22
    mars 1984.

[^41]: Une lettre de l’artiste %%Albert%% adressée à %%Brys-Schatan, Gita|Gita Brys-Schatan%%,
    datée du 19 septembre 1987, est reproduite dans l’ouvrage. Produit
    quatre ans avant la publication de l’ouvrage, ce document permet de
    saisir l’ampleur du travail réalisé.

[^42]: Ce document a également été envoyé à des historiens de l'art, des chercheurs, des théoriciens,
    etc.

[^43]: Les 150 reproductions couleur faisant partie de ce document sont
    reproduites en noir et blanc et en ordre chronologique dans
    l’ouvrage *Mémoire de l’histoire de l’art*, de la page 120 à la page
    175. Cette liste d’œuvres de référence aurait été constituée à
    partir d’un principal critère de sélection&#x202F;: l’œuvre devait être
    reconnue par le plus grand nombre de regardeurs, et trouver
    résonnance même pour les publics non sciemment concernés par les
    arts plastiques. Qui plus est, les réactions et les émotions
    communiquées suite à la lecture de ce document ont également servi
    de baromètre pour évaluer la justesse de cette liste.

[^44]: Ces artistes sont&#x202F;: %%Albert%%, %%Bauweraerts, Jean-Jacques|Jean-Jacques Bauweraerts%%, Michel
    %%Cleempoel, Michel|Cleempoel%%, Luc %%Coeckelberghs, Luc|Coeckelberghs%%, Patrick %%Corillon, Patrick|Corillon%%, Anne %%Cuvelier, Anne|Cuvelier%%, Luc
    %%De Blok, Luc|De Blok%%, Laurence %%Dervaux, Laurence|Dervaux%%, Juan %%d’Oultremont, Juan|d’Oultremont%%, Evelyne %%Dubuc, Evelyne|Dubuc%%, Hugo
    %%Duchateau, Hugo|Duchateau%%, Francis %%Feidler, Francis|Feidler%%, Nadine %%Fievet, Nadine|Fievet%%, Claude %%Foubert, Claude|Foubert%%, Chantal
    %%Labeeu, Chantal|Labeeu%%, Lut %%Lenoir, Lut|Lenoir%%, Philippe %%Lipo, Philippe|Lipo%%, Jacques %%Lizène, Jacques|Lizène%%, %%Marcase|Marcase%%, Marc
    %%Melsen, Marc|Melsen%%, Jacqueline %%Mesmaeker, Jacqueline|Mesmaeker%%, Urbain %%Mulkers, Urbain|Mulkers%%, Veerle %%Pinckers, Veerle|Pinckers%%, %%Po$t Zoro$|Po$t%%
    Zozo$, Toma %%Roata, Toma|Roata%%, Marc %%Schepers, Marc|Schepers%%, %%Tapta%%, Joëlle %%Tuerlinckx, Joëlle|Tuerlinckx%%, Bob %%Van
    der Auwera, Bob|Van der Auwera%%, Franck %%Van Herck, Franck|Van Herck%%, Jan %%Vanriet, Jan|Vanriet%%, Liliane %%Vertessen, Liliane|Vertessen%%, Eva
    %%Visneyi, Eva|Visneyi%%, Franck %%Volders, Franck|Volders%%, Jonas %%Wille, Jonas|Wille%%, Léon %%Wuidar, Léon|Wuidar%%.

[^45]: Un hommage à la mémoire de Robert L. %%Delevoy, Robert L.|Delevoy%% constitue
    le tout premier texte de l’ouvrage.

[^46]: Raymonde %%Moulin, Raymonde|Moulin%%, «&#x202F;Le marché et le musée, la constitution de la
    valeur artistique contemporaine&#x202F;», *Mémoire de l’histoire de l’art*,
    Bruxelles, Éditions de l’ISELP, 1991, pp. 28-41. 

[^47]: Ce texte est extrait du programme de l’ISELP 1989-1990.

[^48]: *Le Soir*, 15 octobre 1989.

[^49]: Le programme est un peu plus modeste, il contient trois cours de
    quarante heures déployés entre octobre 1992 et mars 1993.

[^52]: L’%%Université libre de Bruxelles (ULB)|ULB%% crée sa filière Gestion culturelle en 1989.

[^54]: Éric %%Van Essche, Éric|Van Essche%% est l’auteur d’une thèse remarquée sur
    «&#x202F;l’écriture figurative&#x202F;» qui porte sur les rapports entre signes et images
    depuis l’Antiquité. Il enseigne l’esthétique à %%École nationale supérieure d’architecture et des arts visuels de La Cambre (La Cambre)|La Cambre%%,
    expérimente le monde de l’édition et fonde une société de diffusion
    du livre d’art (%%Sociétés%%). Il installe une librairie à l’ISELP à
    titre d’indépendant.

[^55]: La Fédération Wallonie-Bruxelles soutient une politique
    d’intégration d’œuvres d’art dans les bâtiments publics,
    conformément au décret du 10 mai 1984. Cela implique un processus de
    création artistique enchâssé au processus de création
    architecturale. Cette démarche vise autant à soutenir la création
    contemporaine et ses artistes, qu’à sensibiliser le public aux
    diverses formes d’interventions artistiques actuelles.

[^56]: «&#x202F;Je ne crois pas aux choses, je crois aux relations&#x202F;» (Georges
    %%Braque, Georges|Braque%%)&#x202F;; «&#x202F;L’art ne peut exister sans une condition permanente de
    remise en question&#x202F;» (Ad %%Reinhardt, Ad|Reinhardt%%), «&#x202F;Allons faire un %%Cézanne, Paul|Cézanne%%&#x202F;»
    (Paul %%Gauguin, Paul|Gauguin%% – Lorsqu’il commençait une nouvelle toile, il avait
    coutume de le dire) ; «&#x202F;Il n’appartient qu’à la pensée d’être
    ressemblante&#x202F;» (René %%Magritte, René|Magritte%%) ; «&#x202F;Le surréalisme a triomphé dans
    l’accessoire et échoué dans l’essentiel&#x202F;» (Luis %%Buñuel, Luis|Buñuel%%) ; «&#x202F;Ce
    n’est pas ce que vous voyez qui est l’art, l’art c’est la trouée&#x202F;»
    (Marcel %%Duchamp, Marcel|Duchamp%%) ; «&#x202F;L’habitude, sorte de mémoire, c’est un obstacle
    à la jouissance de la vie et de l’art&#x202F;» (Piet %%Mondrian, Piet|Mondrian%%) ; «&#x202F;Si
    quelque chose a un sens, peut-être en a-t-il encore plus répété dix
    fois&#x202F;» (Eva %%Hesse, Eva|Hesse%%) ; «&#x202F;Seules les choses inutiles sont
    essentielles&#x202F;» (Francis %%Picabia, Francis|Picabia%%) ; «&#x202F;Une vraie œuvre engendre un
    débat entre l’artiste et son public&#x202F;» (Rebecca %%West, Rebecca|West%%) ;
    «&#x202F;L’approbation du public est à fuir par-dessus tout&#x202F;» (André
    %%Breton, André|Breton%%) ; «&#x202F;Dans ce cas-là, quelle est la question&#x202F;?&#x202F;» (Gertrude
    %%Stein, Gertrude|Stein%%) ; «&#x202F;On ne finit que sur du fini&#x202F;» (Jean-Auguste Dominique
    %%Ingres, Jean-Auguste Dominique|Ingres%%).

[^57]: Internet vit ses balbutiements à partir de 1994-1995 mais se
    répand véritablement à partir de 1997. L’ISELP a un site Internet
    provisoire en date du 4 septembre 2000.

[^58]: Cette citation est extraite du texte d’introduction publié sur le
    programme du cycle d’initiation et d’apprentissage culturel «&#x202F;Après le
    travail, avant la soirée&#x202F;» de l’année académique 1999-2000.

[^59]: Cette œuvre de Paul %%Gonze, Paul|Gonze%% remporte le prix du public en 1984.

[^60]: %%Pen, Laurence|Laurence Pen%% a mené des recherches sur cette période de la scène
    artistique bruxelloise&#x202F;: %%Pen, Laurence|Laurence Pen%%, *Des Stratégies obliques&#x202F;:
    une histoire des conceptualismes %%Belgique|en Belgique%%*, Rennes, Presses
    universitaires de Rennes, 2015. Elle signait récemment un article
    dans le dossier «&#x202F;%%Bruxelles%%. Années 60/70&#x202F;» de *%%L’Art même%%*.
    %%Pen, Laurence|Laurence Pen%%, «&#x202F;Vers une scène conceptuelle internationale à
    %%Bruxelles%%. Des acteurs engagés&#x202F;», *%%L’Art même%%*, n°81, 2020, pp. 4-9. Dans le même numéro, voir
    également l’article de Joris %%D’Hooghe, Joris|D’Hooghe%% intitulé «&#x202F;1968-1975.
    Avant-garde à Bruxelles&#x202F;», pp. 10-13.

[^61]: L’ISELP s'associe à cinq structures culturelles pour ce projet&#x202F;:
    L’%%Association intercommunale Culturelle de Bruxelles|association intercommunale culturelle de Bruxelles%%, Le Centre de
    l’audiovisuel à Bruxelles, Le %%Centre du film sur l’art|Centre du film sur l’art%%, Le %%Fonds Henri Storck|Fonds Henri%%
    Storck, La %%Maison de la Francité|Maison de la Francité%%.

[^62]: Cette citation est extraite d’un article publié dans *La
    Lanterne*, le 16 novembre 2000.

[^63]: %%Brys-Schatan, Gita|Gita Brys-Schatan%%, *Avoir lieu/Johan %%Muyle, Johan|Muyle%%*, coll. « Livrets
    d’art », Bruxelles, Éditions de l’ISELP, 2001, pp. 34-35.

[^64]: Il s’agit d’une collaboration avec la revue *La Part de l’œil* (%%Académie royale des Beaux-Arts de Bruxelles (ArBA — ESA)|ArBA%%-EsA).

[^65]: ArtContest (ASBL), créé en 2005, est le concours annuel d’art
    contemporain de référence pour les jeunes artistes belges ou
    résidant en Belgique de 35 ans maximum, parrainé par Hans Op de
    %%Op de Beeck, Hans|Beeck%%. Ce concours, fondé et dirigé par %%Boucher, Valérie|Valérie Boucher%%, attachée à
    la scène artistique émergente, a pour vocation de révéler, de suivre
    et d’accompagner le travail des jeunes artistes contemporains sur le
    long terme.

[^66]: «&#x202F;Le dessus des cartes&#x202F;», entretien mené par Christine %%Jamart, Christine|Jamart%%,
    *%%L’Art même%%*, n°23, 2004, pp. 19-20. Ce numéro de *%%L’Art même%%* contient un dossier consacré au
    rapport entre l’art contemporain et la cartographie.

[^67]: Pierre %%Sterckx, Pierre|Sterckx%% propose le cycle «&#x202F;Histoires de cartes.
    Cartographier la peinture&#x202F;» en guise de grandes conférences les 27
    mai et 3 juin 2004. Ces conférences constituent une extension de sa
    contribution dans l’édition qui accompagne l’exposition.

[^68]: %%Austin Langshaw, John|John Langshaw Austin%%, *Quand dire, c’est faire*, %%Paris%%, Points, 1991 (1962).

[^69]: L’exposition *De Narcisse à Alice. Miroirs et reflets en
    question* (18 avril-21 juin 2008) concorde tout de même avec le
    colloque « Spéculations spéculaires&#x202F;: le reflet du miroir dans
    l’image contemporaine » (24-26 avril 2008). Dans le même ordre
    d’idées, le colloque « Aborder les bordures&#x202F;: l’art contemporain et
    la question des frontières » (22-24 avril 2010) est doublé d’un
    atelier (19-23 avril 2010) réalisé par l’artiste Éric
    Van Hove, en collaboration avec Nida %%Sinnokrot, Nida|Sinnokrot%% (%%Team Habibi%%), qui
    s’achève par une exposition. Présentée dans les grandes salles de
    l’ISELP du 24 avril au 29 mai 2010, l’exposition *Exonymie* consiste
    en la recomposition d’un rayonnage original de la %%Katholiek Universiteit Leuven (KUL)|Katholiek%%
    Universiteit Leuven (KUL) pour soulever des questions liées à la
    frontière linguistique et des questions communautaires qui
    écartèlent la Belgique depuis des années.

[^70]: Cet espace d’accueil où l’on expose des œuvres récentes s’appelle
    «&#x202F;Espace Éphémère&#x202F;» entre 1978 et 1994, «&#x202F;%%Galerie Découverte%% et %%Rayon Art/Objets|Rayon Art/Objets%%&#x202F;» entre
    2000 et 2011, «&#x202F;%%Espace Satellite%%&#x202F;» entre 2018 et 2020.

[^71]: L’apparence de Rayon Art est transformée par le bureau
    d’%%ADN|architecture ADN%% (David %%Henkinet, David|Henkinet%%, Nicolas %%Lacobellis, Nicolas|Lacobellis%%) en 2006.
    L’espace est réorganisé et un nouveau mobilier y est installé.
    L’objectif de cette transformation est de faciliter la compréhension
    des activités de l’ISELP dès l’entrée pour les nouveaux visiteurs.

[^73]: Cette citation est extraite d’un texte publié sur le site web de
    l’ISELP à l’époque.

[^74]: L’ISELP est défini ainsi au début des années 2010. Cette phrase
    est largement reprise par les médias qui communiquent autour de son
    quarantième anniversaire en 2011.

[^75]: Au moment où ces pages sont écrites, l’ISELP et %%Musée Art et marges|Art et marges%%
    musée (Bruxelles) développent une convention pour officialiser le
    dépôt du fonds de documents d’%%Musée Art et marges|Art et marges%% musée au centre de
    documentation de l’ISELP.

[^77]: 2000-2010&#x202F;: %%Bivier, Évelyne|Évelyne Bivier%%, Laurent %%Courtens, Laurent|Courtens%%, Sophie %%Godts, Sophie|Godts%%,
    Frédéric %%Hanon, Frédéric|Hanon%%, Anne-Esther %%Henao, Anne-Esther|Henao%%, Catherine %%Henkinet, Catherine|Henkinet%%, Kim %%Leroy, Kim|Leroy%%,
    Frédérique %%Margraff, Frédérique|Margraff%%, Olivia %%Nerincx, Olivia|Nerincx%%, Aurélie %%Rigaut, Aurélie|Rigaut%%, Adèle
    %%Santocono, Adèle|Santocono%%, Éric %%Van Essche, Éric|Van Essche%% ; 2010-2020&#x202F;: %%Arese, Pierre|Pierre Arese%%, %%Cheval, Florence|Florence Cheval%%, Laurent
    %%Courtens, Laurent|Courtens%%, Adrien %%Grimmeau, Adrien|Grimmeau%%, Pauline %%Hatzigeorgiou, Pauline|Hatzigeorgiou%%,
    Anne-Esther %%Henao, Anne-Esther|Henao%%, Catherine %%Henkinet, Catherine|Henkinet%%, Géraldine %%Marchal, Géraldine|Marchal%%, Mélanie
    %%Rainville, Mélanie|Rainville%%, Aurélie %%Rigaut, Aurélie|Rigaut%%, Adèle %%Santocono, Adèle|Santocono%%, Éric %%Van Essche, Éric|Van Essche%%.
    Septembre %%Tiberghien, Septembre|Tiberghien%% et Pauline de La %%La Boulaye, Pauline de|Boulaye%% se sont également
    jointes à l’équipe durant de plus courtes périodes.

[^78]: Des expositions hors les murs sont également développées durant
    cette période&#x202F;: *FLESH II* au musée %%Musée Ianchelevici|Ianchelevici%% de La Louvière
    (Laurent %%Courtens, Laurent|Courtens%%, 2011), *Duos d’artistes&#x202F;: un échange* au Musée
    %%Musée jurassien des Arts de Moutier, en Suisse|jurassien%% des arts de Moutier, en Suisse (Catherine %%Henkinet, Catherine|Henkinet%%, 2011),
    *Célébration(s)* au %%Centre Wallonie-Bruxelles|Centre Wallonie-Bruxelles%% à %%Paris%% (Catherine
    %%Henkinet, Catherine|Henkinet%%, 2013), *Corps commun* aux %%Anciens Abattoirs de Mons%%
    (Adrien %%Grimmeau, Adrien|Grimmeau%%, Laurent %%Courtens, Laurent|Courtens%%, 2013), *Transitions, Mira Sanders* à %%Été 78%%
    (Catherine %%Henkinet, Catherine|Henkinet%%, 2015).

[^79]: Le %%MASDEX%% ne fait plus partie des programmes d’études d'Arts²
    depuis juin 2020.

[^80]: Ces chiffres, qui datent de 2015, doivent être considérés comme
    des estimations.

[^81]: Le 7 mars 2015, l’ISELP accueille une conversation guidée par
    Arnaud %%Hoedt, Arnaud|Hoedt%% et Jérôme %%Piron, Jérôme|Piron%%. Les enseignants traitent de la langue,
    de l’instruction et de l’écriture en insistant plus spécialement sur
    le caractère arbitraire de certaines règles d’orthographe. Des
    effets néfastes, tels que la discrimination sociale, sont nommés. Au
    final, la conversation soulève le débat et prend une forme
    théâtrale. L’approche humoristique des deux interlocuteurs invite à
    développer un sens critique face à toutes les conventions
    orthographiques qui sont trop peu remises en question. Dans ce
    contexte, les publics de l’ISELP sont invités à développer un esprit
    critique, à remettre en doute ce qui fait autorité. Les deux
    linguistes remportent un grand succès. Leur intervention finit par
    prendre la forme d’un spectacle vivant intitulé *La Convivialité*,
    ainsi que d’un livre intitulé *La Faute de l’orthographe* (4 octobre
    2017). Il ne s’agit pas d’art à proprement parler, mais de culture
    et de remise en question.

[^82]: Les élèves ayant participé à ce projet sont&#x202F;: %%Avci, Ismaïl|Ismaïl Avci%%, Erwin
    %%Blaszczuk, Erwin|Blaszczuk%%, %%Carette, Lionel|Lionel Carette%%, Kevin %%Collinet, Kevin|Collinet%%, Sébastien %%Ergo, Sébastien|Ergo%%, Sébastien %%Firre, Sébastien|Firre%%, Morteza %%Khavand Ghassroldashti, Morteza|Khavand%%
    Ghassroldashti, Valentin %%Hanssens, Valentin|Hanssens%%, Kevin %%Lapage, Kevin|Lapage%%, Mario %%Marasco, Mario|Marasco%%,
    %%Petit, Guillaume|Guillaume Petit%%,Sébastien
    %%Rodriguez Escaleira, Sébastien|Rodriguez Escaleira%%, %%Sens, Antoine|Antoine Sens%%, Kevin %%Vandenbroeck, Kevin|Vandenbroeck%%.

[^83]: Pauline %%Hatzigeorgiou, Pauline|Hatzigeorgiou%%, «&#x202F;%%Passerelle%%&#x202F;: croiser les routes entre
    l’artiste et les élèves, l’art et la société&#x202F;», *Art, enseignement & médiation*, numéro 3,
    Bruxelles, %%Académie royale des Beaux-Arts de Bruxelles (ArBA — ESA)|ArBA%%-EsA, p. 12.

[^84]: Philosophe, professeur de français à l’école secondaire Institut
    %%Institut Sainte-Marie|Sainte-Marie%% et chercheur, Sébastien %%Marandon, Sébastien|Marandon%% s’intéresse aux
    croisements potentiels ou effectifs entre l’artiste et le pédagogue.
    Il participe à l’écriture de *Outside/Inside. Practices Between Art
    and Education* (%%Palais des Beaux-Arts (BOZAR)|BOZAR%% , Bruxelles, 2019) et à la publication des
    actes du colloque « Les places et les effets de l’artiste à l’école
    et sur les institutions culturelles » (*Art, enseignement &
    médiation*, n°4, %%Bruxelles%% / %%Paris%%, 2019).

[^86]: Maïté %%Vissault, Maïté|Vissault%% remplace le prix Hors-d’œuvre, financé par %%Eeckman Art & Insurance|Eeckman%%
    Art & Insurance, par le prix %%Connectif, Prix|Connectif%% en 2016. Par la suite, les
    récipiendaires sont %%Messidor%% (Sirah %%Foighel Brutmann, Sirah|Foighel Brutmann%%, Eitan %%Efrat, Eitan|Efrat%%,
    Pieter %%Geenen, Pieter|Geenen%%, Meggy %%Rustamova, Meggy|Rustamova%%), Béatrice %%Delcorde, Béatrice|Delcorde%%, Chloé %%Schuiten, Chloé|Schuiten%%
    et Clément %%Thiry, Clément|Thiry%%, Camille %%Lancelin, Camille|Lancelin%%.

[^89]: Le premier numéro de *Patch* est distribué à partir de janvier
    2017.

[^90]: Adrien %%Grimmeau, Adrien|Grimmeau%% est historien de l’art de formation (%%Université libre de Bruxelles (ULB)|ULB%%). Il
    enseigne, organise des expositions et publie des textes. Sa
    spécialisation porte sur divers enjeux liés à l’art public. Entre
    2011 et 2015, il présente dans ce cadre les expositions *%%Bonom%%, le
    singe boiteux* (2014), *Being Urban. Laboratoire pour l’art dans la
    ville* (2015), *YO. Brussels Hip Hop Generations* (%%Palais des Beaux-Arts (BOZAR)|BOZAR%% , 2017), et
    publie *Dehors&#x202F;! Le graffiti à Bruxelles* (CFC-Éditions, 2011).

[^92]: L’%%Espace Satellite%% correspond à la partie avant du bâtiment,
    située à proximité de l’entrée du %%Waterloo (boulevard de)|boulevard de Waterloo%%. Elle a
    porté d’autres noms par le passé&#x202F;: Espace Éphémère (1978-1985) et la
    %%Galerie-Découverte%% (2000-2011).

[^93]: Cette citation est extraite de l’encart promotionnel de
    l’événement publié sur le site web de l’ISELP.

[^94]: Ludovic %%Lamant, Ludovic|Lamant%%, «&#x202F;Biennales, fin de partie&#x202F;?&#x202F;», *Mediapart*, 28
    mars 2020.

[^95]: %%Recy-K%% est un bâtiment qui abrite diverses initiatives liées au
    domaine du recyclage.

[^96]: %%Arese, Pierre|Pierre Arese%% écrit quatre émissions de *L’Art au poing* entre
    mai et juin 2020&#x202F;: #1&#x202F;: «&#x202F;Destruction in Art Symposium, 1966&#x202F;», #2&#x202F;:  «&#x202F;%%Pavlenski, Piotr|Pavlenski%%, éclairage, 2017&#x202F;», #3&#x202F;: «&#x202F;Outrage au drapeau, 1989&#x202F;», #4&#x202F;: «&#x202F;Épié.e.s, 2013&#x202F;».

[^97]: Six émissions suivent la rubrique de Laurent %%Courtens, Laurent|Courtens%%&#x202F;:
    \#2&#x202F;: %%Baes, Rachel|Rachel Baes%%, *Le nœud* (1949) ; \#3&#x202F;: Mark %%Manders, Mark|Manders%%, *Shadow
    Study (2)*, 2010 ; \#4&#x202F;: Oriol %%Vilanova, Oriol|Vilanova%%, *Tête à tête* (2019 - en
    cours) ; \#5&#x202F;: Edouard %%Manet, Edouard|Manet%%, *Les Bulles de savon*, 1867 ;
    \#6&#x202F;: Emmanuel %%Van der Auwera, Emmanuel|Van der Auwera%%, *VideoSculpture XXI*, 2019 ; \#7&#x202F;:
    Sonia %%Delaunay, Sonia|Delaunay%%, *Couverture de berceau*, 1911. Véronique %%Bouillez, Véronique|Bouillez%%, Pierre-Yves
    %%Desaive, Pierre-Yves|Desaive%%, %%Florence, Delphine|Delphine Florence%%, Rosanna %%Gangemi, Rosanna|Gangemi%%, Lyse %%Vancampenhoudt, Lyse|Vancampenhoudt%% et
    Christophe %%Veys, Christophe|Veys%% partagent leurs connaissances dans ce contexte.  
