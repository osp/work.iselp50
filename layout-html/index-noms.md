# Index de l’historique

- Abrahams, Annie
- Académie royale des Beaux-Arts de Bruxelles (ArBA-EsA)
- Académie des Beaux-Arts de Tournai
- Acconci, Vito
- Ackermans, Christiane
- Ackermans, Christophe
- Actiris
- Albert
- Alluin
- Amathéü, Catherine
- Anciaux, Myriam
- Anciens Abattoirs de Mons
- Anderlecht
- Andre
- Andrin, Véronique
- Antoine, Jean
- Archives de l’Art contemporain en Belgique (AACB)
- Arese, Pierre
- Argan, Giulio Carlo
- ARGOS Asbl
- Aron, Jacques
- Art Contest
- Arts2 (Mons)
- Arveiller, Jacques
- Association Intercommunale Culturelle de Bruxelles
- Astman
- Austin Langshaw, John
- Autriche
- Avau, Metallic
- Avci, Ismaïl
- Axell, Evelyne
- Axelos, Kostas
- Ayoub, Pascale
- Bacon, Francis
- Baes, Rachel
- Bailet, Béatrice
- Baix, Alain
- Balcou, Béatrice
- Balkenhol, Stephan
- Baltus, Michaël
- Bandin, Marion
- Baquet, Hélène
- Bara, Manon
- Barbery, Géraldine
- Baronian, Albert
- Barret, Pascale
- Bastin, Roger
- Baucher, Lucien
- Baudson, Michel
- Bauer, Tessy
- Bauweraerts, Jean-Jacques
- Beatles
- Becker, Wolfgang
- Bedel, Delphine
- Beethoven, Ludwig van
- Béjart, Maurice
- Belting, Hans
- Ben
- Benjamin, Walter
- Benon, Jean-Pierre
- Berger, René
- Berckmans, Jean-Pierre
- Berlanger, Marcel
- Bertouille, Susanne
- Bertrand, Lucile
- Besset, Klaus
- Beuys, Joseph
- Bica, Laeticia
- Bical, Yves
- Biennale de Venise
- Bivier, Evelyne
- Blaszczuk, Erwin
- Blondel, Jean-Pierre
- Bologne
- Bohm, Tatiana
- Bombeur Fou
- Bonom
- Bonté, Isabelle
- Borel, France
- Borgers, Marc
- Bornain, Alain
- Botanique
- Boucher, Valérie
- Boulares, Sami
- Boulez, Pierre
- Boulevard de Waterloo
- Bourgeois, Jean-Jacques
- Bourse
- Bouteas, Yanis
- Bouts, Dirk
- Bouvier, Amélie
- Bouyaux, Bernadette
- Bovy, Olivier
- Brachot, Isy
- Braque, Georges
- Brayez, Marie-Ange
- Breton, André
- Brégeaut, Anne
- Brésil
- Brixhe, Valérie
- Broodthaers, Marcel
- Bruegel l’Ancien
- Brusselmans, Jean
- Brussels Art Film Festival (BAFF)
- Bruxelles
- Bruxelles 2000
- Bruxelles Propreté (Agence)
- Brys-Schatan, Gita
- Bucci, Ezio
- Bunuel, Luis
- Buren
- Burgering, Toni
- Burgin
- Buscarlet, Alain
- Busine, Laurent
- Bussers, Marcel
- By Chance
- Cage, John
- Cadere, André
- Cahun, Claude
- Cambruzzi, Marie-Ange
- Caplet, Gaëlle
- Cardoen, Philippe
- CARE
- Carette, Lionel
- Carez, Christian
- Carletta, Lisa
- Carton de Tournai, Vincent
- Castaigne, Joël
- Ceder, Michel
- Celdran, Julien
- Centrale Electrique
- CENTRALE for Contemporary Art
- Centre d’Art contemporain
- Centre Culturel de la Fédération Wallonie-Bruxelles
- Centre d’Action Culturelle d’Expression Française (C.A.C.E.F.)
- Centre de l’Audiovisuel à Bruxelles
- Centre de Documentation du centre d’art contemporain
- Centre de Documentation des Arts Visuels Actuels (D.A.V.A)
- Centre des Riches-Claires
- Centre du Film sur l’Art
- Centre Georges Pompidou
- Centre Wallonie-Bruxelles (Paris )
- Cézanne
- CERIA
- Chamboisier, Anne-Laure
- Chantelot, Marie
- Chaponan, Anne
- Charleroi
- Charpentier, Guy
- Chenu, Vincent
- Cheval, Florence
- Chillida, Eduardo
- Chryssa, Varda
- Cicilloni, Silvana
- Cinematek
- Cité administrative
- 50/04 (collectif)
- Cirque Royal
- Cleempoel, Michel
- Cluysenaar, John et Verbeyst
- Clerbois, Michel
- Coeckelberghs, Luc
- Cohen, Léonard
- Collectif Formation Société-Insertion socio-professionnelle (CFS-ISP)
- Collette, Sophie
- Collins
- Collinet, Kevin
- Coluche
- Colombi, Chiara
- Convert, Jean-Philippe
- Communauté Flamande
- Communauté Française de Belgique
- Communauté Française du fonds documentaire du Centre d’art contemporain
- Commission artistique des Infrastructures de Déplacement (CAID)
- Commission Française de la Culture de l’Agglomération de Bruxelles (C.F.C)
- Commission communautaire française (COCOF)
- Commission Royale des Monuments et Sites
- Commission consultative des arts
- Coney Island
- Contretype
- Copenhague
- Coppée, Marilyne
- Copers, Léo
- Coquelet, Jean
- Cordier, Pierre
- Corillon, Patrick
- Cornil, Olivier
- Cosine, Samuel
- Coucke, Joachim
- Courcelles, Pascal
- Courtens, Laurent
- Coutellier, Francis
- Cragg, Tony
- Culot, Maurice
- Culture et démocratie
- Cuvelier, Anne
- Dauchot, Alain
- Dailly, Clément
- D’Alessandro
- D’Arenberg (Famille)
- De Block, Luc
- De Busschère, Alec
- Debeauvais, Antoine
- Debod, Grégory
- Deble, Colette
- Debouverie, Patrick
- Decelle, Philippe
- De Cugnac, François
- Defosse, Daphné
- De Freyman, Christian
- De Gobert, Philippe
- Delaunay, Sonia
- Delcorde, Béatrice
- Delfosca, Marie
- De Maet, Jacques
- Denicolai & Provoost
- De Rouck, Etienne
- De Saint-Phalle, Niki
- De Salle, Jean
- Delahaut, Jo
- Delay
- Deller, Jeremy
- Delevoy, Robert L.
- De Lulle, Francis
- Delvaux, André
- Delvoye, Wim
- Demaret, Michel
- Dementieva, Alexandra
- Denicolai, Simona
- Dercon, Chris
- Derouck, Charles
- Dervaux, Laurence
- Desaubliaux, Alix
- Desaive, Pierre-Yves
- Descamps, Bernard
- Dessart, Jean-Claude
- Detandt, Corinne
- Devillez, Virginie
- Devolder, Eddy
- De Vrieze, Murielle
- De Winter, Jonathan
- De Witte, Jean-François
- D’Huart, Nicole
- D’Hooghe, Joris
- D’Hond, Anne
- Dialogist-Kantor
- Dibbets, Jan
- Diord, Jean-François
- Dobbels, Georges
- Documenta XI
- Dotremont, Christian
- D’Oultremont, Juan
- Draeger, Christophe
- Drakkar, Franz
- Dream Factory
- Druillet, Philippe
- Dubit
- Dubois, Arthur
- Dubois, Géraldine
- Dubuc, Evelyne
- Du Buisson, Georgette
- Duchamp, Marcel
- Duchateau, Hugo
- Duchesne, Jean-Patrick
- Duclaux, Lise
- Dufour, Maëlle
- Dumont, Fabienne
- Dumont, Georges-Henri
- Dumont, Isabelle
- Dupont, Olivier
- Dunkerque
- Duplain, Charles-François
- Durant, Ben
- Duran Sineiro, Xurxo
- Dusariez, Michel
- Ecole Baron Steens
- Ecole nationale supérieure d’architecture et des arts visuels de La Cambre (La Cambre)
- Edeline, Francis
- Eeckman Art & Insurance
- Efrat, Eitan
- Ellena, Véronique
- Elkoury, Fouad
- Ensor, James
- Environnemental
- Enwezor, Okwui
- Erasmus
- Ergo, Sébastien
- Erro
- ESA 75
- ESA Erg
- ESA Saint-Luc
- ESA ArBA
- Europe
- Even-Adin, Bernadette
- Everaert, Patrick
- Fabbri-Daure, Jacqueline
- Fabien, Marion
- Fabiola (Reine)
- Fabro, Luciano
- Fédération Wallonie-Bruxelles
- Féaux, Valmy
- Feidler, Francis
- Fend, Peter
- Fievet, Nadine
- Fink, Christophe
- Finlande
- Firre, Sébastien
- Fischer, Hervé
- Flausch, Fernand
- Flavin, Dan
- Fleig, Alain
- Florence, Delphine
- F’Murr
- Foighel Brutmann, Sirah
- Folon, Jean-Michel
- Fondation J.B. Urvater
- Fondation, Marcel Hicter
- Fondation Roi Baudouin
- Fondation Thalie
- Fonds (National) de la Recherche Scientifique (F.N.R.S.)
- Fonds Henri Storck
- Forest-National
- Forest, Fred
- Foubert, Claude
- Franck, Philippe
- Franke, Herbert W.
- Franquin
- Gadenne, Bertrand
- Galerie Aujourd’hui
- Galerie Cogeime
- Galerie Découverte et Rayon Art/Objets
- Galerie Gabriel Brachot
- Galerie Paul Maenz
- Galerie Ravenstein
- Galerie Stempelplaats
- Gangemi, Rosanna
- Gasparotto, Paolo
- Gaube, Bernard
- Gauguin, Paul
- Geenen, Pieter
- Geirlandt, Karel
- Génicot, Thierry
- Gentils, Vic
- Géronnez, Alain
- Gerz, Jochen
- Gevaert, Yves
- Ghyselen, Caroline
- Ghysels, Jean-Pierre
- Gide, André
- Gilbert & George
- Gilissen, Filip
- Giller, Jérôme
- Ginepro, Alberto
- Giraud, Fabien
- Girard, An
- Glibert, Jean
- Godefroid, Jean-Louis
- Godts, Sophie
- Godard, Jean-Luc
- Goethe Institut
- Goffart, Albert
- Goldrajch, Stephan
- Gomez, Bernard
- Gondry, Evelyne
- Gonze, Paul
- Gottlieb
- Gravenhorst, Hein
- Grimmeau, Adrien
- Groupe de Vincennes
- Guns, Patrick
- Haarscher, Guy
- Hainaut
- Halgand, Jean-Claude
- Hanon, Frédéric
- Hannon, Edouard
- Hanssens, Valentin
- Hatzigeorgiou, Pauline
- Hauser, Rouve
- Henao, Anne-Esther
- Henderick, Bénédicte
- Henkinet, Catherine
- Henkinet, David
- Hergé
- Herman, Ariane
- Hesse, Eva
- Heysel
- HISK (Gand)
- Hockney, David
- Hoedt, Arnaud
- Hoet, Jan
- Holzaeuser, Karl
- Hongrie
- Huitric, Hervé
- Hufkens, Xavier
- Illah Bendahman, Abdel
- Immendorff, Jörg
- Ingimarsdóttir, Guðný Rósa 
- Ingres, Jean-Auguste Dominique
- Institut Bruxellois de l’Environnement (IBGE)/Bruxelles Environnement
- Institut Don Bosco
- Institut Emile Gryson
- Institut Interuniversitaire de l’Opinion Publique (INUSOP)
- Institut Sainte-Marie
- Institut Saint-Louis
- Institut Supérieur pour l’Etude du Langage Plastique (ISELP)
- Instituut voor Beeldende, audiovisuele en mediakunst (BAM)
- Isidore, Raymond
- Jacotot, Joseph
- Jaffé, Hans L.C.
- Janne, Henri
- Jaeger, Gottfried
- Jamart, Christine
- Jano
- Jarrett, Keith
- Javeau, Claude
- Jakou, Raoul
- Jeunesse et Arts Plastiques (JAP)
- Jourdan, Jean-Paul
- Jullian, René
- Kaisin, Charles
- Kao, Wen Li
- Karavan, Dani
- KASK (Gand)
- Katholiek Universiteit Leuven (KUL)
- Katz, Marie-Christine
- Kaulen, JL
- Kazarian, Aïda
- Keguenne, Jack
- Kermarrec, Joël
- Kervyn, Myriam
- Khanh Vong, Julien
- Kienholz, Ed
- Kirkeby, Per
- Klimt, Gustave
- Klinkenberg, Jean-Marie
- Kodratoff, Yves
- Kokkinos, Niki
- Komplot
- Kosuth, Joseph
- Kowalski, Piotr
- Khavand Ghassroldashti, Morteza
- Kroll, Lucien
- Labeeu, Chantal
- La Boulaye, Pauline de
- La Lettre Volée
- Lacobellis, Nicolas
- Laconte, Pierre
- Laenen, Jean-Paul
- Lafontaine, Marie-Jo
- Lahaye, Martine
- Lamant, Ludovic
- Lambert, Henri
- Lancelin, Camille
- Lang, Fritz
- Lapage, Kevin
- Lassaigne, Jacques
- Lasserre, Christian
- Laureyssens, Thomas
- Laval, Antoine
- Lefébure, Jean
- Le Gac
- Lemonnier, Arlette
- Lenain, Pierre
- Lenoir, Lut
- Leonard, René
- L’Heureux, Albert-André
- Lecci, Auro
- Legrand, Luc
- Lemesre, Marion
- Lennep, Jacques
- Lenouvel, Sébastien
- Léonard, René
- Le Parc, Julio
- Lelubre, Daniel
- Leroy, Kim
- Levie, Capucine
- Libois, Brigitte
- Liebaers, Herman
- Liffran, Guillaume
- Lipo, Philippe
- Lizène, Jacques
- Loge, Bernard
- Loterie Nationale
- Lurquin, Christine
- Macherot
- Macias Diaz, Sylvie
- Maelderlick, Maurice
- Magnus, Dieter
- Magritte, René
- Maison de la Culture de Namur
- Maison de la Francité
- Maison de repos et de soins « Aux Ursulines »
- Mahieu, Didier
- Malevitch, Kasimir
- Manifeste pour l’Environnemental
- Manders, Mark
- Manet, Edouard
- Manhattan
- Mara, Pol
- Marandon, Sébastien
- Marasco, Mario
- Marcase
- Marcelis, Bernard
- Marchal, Géraldine
- Mariani
- Margolles, Teresa
- Margraff, Frédérique
- Marlaire, Claudine
- Martin, Jean-Hubert
- Mary, Xavier
- Mason
- Massart, Cécile
- MASDEX
- Matella, Marco
- Matsumoto, Toshio
- Matthys, Danny
- Mees, André
- Méliès, Georges
- Méliès-Malthête, Madeleine
- Melsen, Marc
- Mental Masonry Lab (The)
- Mèredieu, Florence de
- Mérode
- Merz, Mario
- Mesmaeker, Jacqueline
- Messager
- Metallic Avau
- Meurant, Georges
- Meurant, Serge
- Meuris, Jacques
- Midis de l’art urbain
- Miéville, Anne-Marie
- Miezanagora, Georges
- Ministère de la Culture Française
- Ministère des Affaires étrangères
- Miyamoto, Kazuko
- Moebius
- Moles, Abraham
- Mondrian, Piet
- Monniez, Valérie
- Mont des Arts
- Mohr, Manfred
- Molinier, Pierre
- Morellet, François
- Moulin, Raymonde
- Moureaux, Serge
- Mulkers, Urbain
- Musée Art et marges
- Musée d’Art Contemporain de la Communauté Germanophone de Belgique (IKOB)
- Musée d’Art Moderne
- Musée d’Ixelles
- Musées royaux des Beaux-Arts de Belgique
- Musée Lanchelevici de La Louvière 
- Musée jurassien des Arts de Moutier, en Suisse 
- Musée des Arts contemporains de la Fédération Wallonie-Bruxelles (MAC’s)
- Muylaert, Léo
- Muyle, Johan
- Naggar, Carole
- Nahas, Monique
- Nake, Freider
- Navez, Jean-Marc
- Nerincx, Olivia
- Neue Galerie
- New Smith Gallery
- New York
- Nees, Georg
- Nerviens, rue des
- Nord
- Nori, Claude
- Norvège
- Obêtre
- Octave, Jean-François
- Open Source Publishing (OSP)
- Oppenheim, Dennis
- Op de Beeck, Hans
- Orlan
- Palace
- Palais des Congrès de Bruxelles
- Palais des Beaux-Arts (BOZAR)
- Palais des Beaux-Arts de Charleroi
- Palais d’Egmont
- Palestine, Charlemagne
- Parc d’Egmont
- Parc du Cinquantenaire
- Parc Royal
- Paris
- Patinoire Royale
- Pen, Laurence
- Penailillo, Raul
- Penelle, Frédéric
- Peraya, Daniel
- Peremans, Françoise
- Persoons, François
- Petit, Guillaume
- Picabia, Francis
- Picasso, Pablo
- Picqué, Charles
- Pichrist, Sylvie
- Pierlet, Robert
- Piérart, Béatrice
- Pierre, Alain
- Pignon-Ernest, Ernest
- Pinckers, Veerle
- Pintus, Anne
- Piron, Jérôme
- Piron, Pascale
- Plan K
- Plaza (Mons)
- Plossu, Bernard
- Ponlot, Marianne
- Po$t Zozo$, 
- Poupko, Jean-Pierre
- Pousseur, Henri
- Porsperger, Jérôme
- Phabian
- Pratt, Hugo
- Prévost, Claude
- Présence et Action Culturelles (PAC)
- Prévost, Clovis
- Provoost, Ivo
- Prigogine, Ilya
- Puccini
- Quaregnon
- Quartier Européen
- Radar, Edmond
- Rahir, Yves
- Rainer
- Rainville, Mélanie
- Rancière, Jacques
- Randolet, Violaine
- Ragon, Michel
- Ravenstein (galerie)
- Recy-K
- Régence (rue de la)
- Région de Bruxelles-Capitale/Région Bruxelloise
- Reinhardt, Ad
- Remiche, Jean
- Renault, Aude
- Rensonnet, Pierre
- Reumont, Patrice
- Revault, Etienne
- Richez, Jacques
- Ricker, Marie-Emilie
- Richter
- Rigaut, Aurélie
- Roata, Toma
- Robert-Jones, Philippe
- Roche, Denis
- Rodriguez Escaleira, Sébastien
- Roland, José
- Rolet, Christian
- Ronse, Philippe
- Romanelli, Fabrizio
- Rond-Point Schuman
- Rosalie Pompon
- Rossi, Aldo
- Roulin, Philippe
- Rousseau, Isabelle
- Roux, Léopoldine
- RTBF/RTB
- Ruby, Christian
- Rückriem, Ulrich
- Rue aux Laines
- Ruhr
- Rulens, Jean (Rosier)
- Rustamova, Meggy
- Sablon
- Saint-Luc, Ecole supérieure des Arts
- Santocono, Adèle
- Sarah & Charles
- Sarkis
- Schein, Françoise
- Schepers, Marc
- Schoysman, Robert
- Schuyten, Monique
- Scott, Beverly Jo
- Segal, George
- Sens, Antoine
- Service des Arts Plastiques de la Communauté française
- Service Public francophone bruxellois
- Shell Auditorium
- Sherman, Cindy
- Siebig, Karl
- Sinnokort, Nida
- Six, Madame Paul
- SMAK
- Société des Expositions
- Sociétés
- Somville, Roger
- Sonnier, Keith
- Sorbonne
- Spillemaeckers, Fernand
- Sporting de Charleroi
- Spriet, Winston
- Stanescu, Michèle
- Stasiuk, Ela
- Stein, Gertrude
- Steinfort, Patrick
- Sterckx, Pierre
- Stevenheydens, Ive
- Stevens, Bruno
- Streamline
- Struycken, Peter
- Studenten, Eth
- Studio 51
- Suède
- Suntaxi Sanchez, Ivan
- Suys, Tilman-François
- Szternfeld, Barbara
- Sztulman, Paul
- Tapta
- Tanghe, Jean-Luc
- Takizawa, Reiko
- Tardi
- Team Habibi
- Temmerman, Clémy
- Terlinden, Christophe
- T’Kindt, Jacques
- Theis, Bert
- Thidet, Stéphane
- Thiry, Jean-Pierre
- Thomas, Michel
- Thomine, Clara
- Thys, Jean-Louis
- Tiberghien, Septembre
- Tirtiaux, Adrien
- Tordoir, Narcisse
- Tournai
- Tout (Asbl)
- Transcultures
- Truyols, Berta
- Trémeau, Tristan
- Tuerlinckx, Joëlle
- Tuymans, Luc
- Untel
- Urban
- Université de Strasbourg
- Université Libre de Bruxelles (ULB)
- Urvater, Joseph-Berthold
- Vanautgaerden, Alexandre
- Vancampenhoudt, Lyse
- Vance
- Vandenbroeck, Kevin
- Vandercam, Serge
- Vander Gucht, Daniel
- Van der Horst, Jozef
- Vanderstraeten, Pascal
- Van der Stichele, Michel
- Vandevelde, Hélène
- Vandevelde, Maurice
- Vandevivere, Ignace
- Van der Auwera, Bob
- Van der Auwera, Emmanuel
- Van Essche, Eric
- Van Geffel, Josue
- Van Geluwe, Johan
- Van Herck, Frank
- Van Hille, Alain
- Van Hoa, Pham
- Van Hout, Georges
- Van Hove, Eric
- Van Lier, Henri
- Van Malderen, Luc
- Van Noort, Marteen
- Van Parys, Yoann
- Van Roye, Michel
- Van Tieghem, Jean-Pierre
- Van Saltbommel, Sofi
- Varese, Christian
- Vaturi, Jessica
- Venet, Bernard
- Venise
- Verbeke, Thierry
- Verken, Monique
- Vercheval, Georges
- Vergara, Angel
- Verlant, Gilles
- Verschueren, Bob
- Vertessen, Liliane
- Vets, Irial
- Veys, Christophe
- Viallat
- Vienne, Michel
- Villa Hermosa
- Ville de Bruxelles
- Villers, Bernard
- Vidal, Nicole
- Vidal, Camille
- Vidéothèque de Jeunesse et Arts Plastiques
- Vilanova, Oriol
- Vinck, Caroline
- Vinck, John
- Visneyi, Eva
- Vissault, Maïté
- Vitkine, Alexandre
- Volders, Franck
- Vox/Polymorfilms
- Wanet, Sophia
- Warche, Robert
- Warhol, Andy
- West, Rebecca
- Wéry, Marthe
- Wiels
- Wille, Jonas
- Wuidar, Léon
- Xhafa, Bardul
- Yannakopoulos, Demetrios
- Yemanja
- Yerlès, Pierre-Paul
- Ysabeaux, Terry
- Zajec, Edward
- Zimmer, Paul
- Zurstrassen Yves







