(function () {
  function moveFootnotesInline () {
    var footnoteContainerSelector = 'div.footnote',
        footnoteSelector = 'div.footnote li',
        footnoteContentWrapperSelector = 'p';


    // find footnote container
    var footnoteContainer = document.querySelector(footnoteContainerSelector);

    if (footnoteContainer) {
      var footnotes = document.querySelectorAll(footnoteSelector);

      for (var i = 0; i < footnotes.length; i++) {
        var footnote = footnotes[i],
            backref = document.querySelector('[id="fnref:' + footnote.id.substr(3) + '"]'),
            backrefLink = document.querySelector('.footnote-backref'),
            footnoteContentWrapper = footnote.querySelector(footnoteContentWrapperSelector),
            inlineFootnote = document.createElement('span');

        inlineFootnote.classList.add('footnote');

        backrefLink.parentElement.removeChild(backrefLink);

        while (footnoteContentWrapper.childNodes.length > 0) {
          inlineFootnote.appendChild(footnoteContentWrapper.childNodes[0]);
        }

        backref.parentElement.replaceChild(inlineFootnote, backref);
      }

      footnoteContainer.parentElement.removeChild(footnoteContainer);
    }
  }

  window.moveFootnotesInline = moveFootnotesInline;
})();