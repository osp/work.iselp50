(function () {
  function setRealPageNumbers () {
    let pages = document.querySelectorAll('.pagedjs_page'),
        realPageCounter = 0;

    const pageCounterResetPatt = /page (\d+)/;
    // Set an extra data-attribute with the 'real' pagenumber
    // Loop through all the pages.
    for (let i = 0; i < pages.length; i++) {
      const page = pages[i],
            m = window.getComputedStyle(page).getPropertyValue('counter-reset').match(pageCounterResetPatt);
      // Test through getComputedStyles whether there is a counter reset.
      if (m && m.length) {
        // Set js counter to that value
        console.log(m[1])
        realPageCounter = parseInt(m[1]); 
      }
      
      // Increment js counter by one
      realPageCounter++;
      // Set it on the page.
      page.dataset.realPageNumber = realPageCounter;
    }
  }

  window.setRealPageNumbers = setRealPageNumbers;
})();