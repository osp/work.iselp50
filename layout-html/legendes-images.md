## Esprit 1970-1980
![](contenu-final/000-feuillet-esprit-70-80/001-GitaBrysSchatan-Portrait.jpg)
:    Gita Brys-Schatan, 1971

![](contenu-final/000-feuillet-esprit-70-80/003-Inforart-Oppenheim-1972.jpg)
:    Dennis Oppenheim, Conférence-débat Inforart, 1972.

![](contenu-final/000-feuillet-esprit-70-80/004-Inforart-Juan-Doultremont-1978-Ajouttardif.jpg)
:    Juan D’Oultremont, Conférence-débat Inforart D’art et décès, 9 février 1978.

![](contenu-final/000-feuillet-esprit-70-80/005-Infoart-JacquesLennep-1979.jpg)
:    Jacques Lennep, Conférence-débat Inforart L’homme exposé,8 février 1979.

![](contenu-final/000-feuillet-esprit-70-80/007-MyriamKervyn-1983.jpg)
:    Vernissage de l’exposition Myriam Kervyn. Coin d’œil dans l’Espace Ephémère, 23 juin 1983.

![](contenu-final/000-feuillet-esprit-70-80/008-Expériencesdegraphiste-JacquesRichez-GillesFiszman-MichelOlyff-7nov1983.jpg)
:    Séminaire Expériences de graphiste dispensé par Jacques Richez, avec la collaboration de Gilles Fiszman et Michel Olyff, 7 novembre 1983.

![](contenu-final/000-feuillet-esprit-70-80/009-UgoVolli-1983-Ajouttardif.jpg)
:    Conférence de Ugo Volli Pour un théâtre de l’expérience, 23 novembre1983.

![](contenu-final/000-feuillet-esprit-70-80/010-Cours.jpg)
:    Cours non-daté, probablement dans les années 1980.

![](contenu-final/000-feuillet-esprit-70-80/011-Portesouvertes-1985.jpg)
:    Portes ouvertes, octobre 1985.

![](contenu-final/000-feuillet-esprit-70-80/012-Portesouvertes-1985.jpg)
:    Portes ouvertes, octobre 1985.

![](contenu-final/000-feuillet-esprit-70-80/013-GBS02.jpg)
:    Portrait de Gita Brys-Schatan non-daté, probablement à l’ISELP dans les années 1980.


## Détails graphiques

![](contenu-final/004-feuillet-details-graphiques/001-1974-Artetordinateur.jpg)
:    Affiche de l’exposition Art et ordinateur, Forest National 1974.

![](contenu-final/004-feuillet-details-graphiques/002-ExpoArtetOrdinateur-1974.jpg)
:    Carton d’invitation de l’exposition Art et ordinateur, Forest National, 1974.

![](contenu-final/004-feuillet-details-graphiques/003-1978-Carton-Heros-Tics.jpg)
:    Carton d’invitation de l’exposition Heros-Tics, 1978.

![](contenu-final/004-feuillet-details-graphiques/004-Seminaire-MichelCeder-BD-Fignarrative-Héros-tics.jpg)
:    Affiche annonçant le séminaire Interprétations spatiales de la bande dessinée et/ou figuration narrative, dispensé par Michel Céder, dans le contexte de l’exposition Heros-Tics, 1978.

![](contenu-final/004-feuillet-details-graphiques/005-ExpoLeslivresdenfantsnesontpluscequilsetaient.jpg)
:    Carton d’invitation de l’exposition Les livres d’enfants ne sont plus ce qu’ils étaient, 1978.

![](contenu-final/004-feuillet-details-graphiques/006-1978-Dépliant.jpg)
:    Page-couverture d’une brochure, 1978.

![](contenu-final/004-feuillet-details-graphiques/007-1978-Affiche_promotionnelle.jpg)
:    Affiche annonçant le programme de cours de l’année académique 1977-1978.

![](contenu-final/004-feuillet-details-graphiques/008-1979-Affiche-1000bruxellois.jpg)
:    Affiche annonçant l’exposition 1000 Bruxellois et l’art, 1979.

:    
![](contenu-final/004-feuillet-details-graphiques/009-Brochure10ans.jpg)
:    Illustration de Lucien De Roeck reproduite sur la page-couverture de la publication ISELP. Institut supérieur pour l’étude du langage plastique. Dix années, 1981.

![](contenu-final/004-feuillet-details-graphiques/002-PossA-Brochure10ans.jpg)
:    Plan de Bruxelles produit par Lucien De Roeck et reproduit dans la publication ISELP. Institut supérieur pour l’étude du langage plastique. Dix années, 1981.
Affiche annonçant l’exposition Masque et autres masques, 1982.

![](contenu-final/004-feuillet-details-graphiques/010-1982-Affiche-Masque_et_autres_masques.JPG)
:    Document annonçant l’exposition Ne vous mettez pas en boule, mettez-nous en boîte, 1983.

![](contenu-final/004-feuillet-details-graphiques/011-A-Participatif-Nevousmettezpasenboulemettez-nousenboîte-1983.jpg)
:    Document annonçant l’exposition Ne vous mettez pas en boule, mettez-nous en boîte, 1983.

![](contenu-final/004-feuillet-details-graphiques/012-1983-Affiche_promotionnelle.JPG)
:    Affiche annonçant les séminaires proposés durant de l’année académique 1983-1984.

![](contenu-final/004-feuillet-details-graphiques/013-Affiche-JFO-1984.jpg)
:    Affiche annonçant l’exposition Jean-François Octave : Dessins d’architecte, 1984.

![](contenu-final/004-feuillet-details-graphiques/014-A-Participatif-Travauxencours-1985.jpg)
:    Carton d’invitation de l’exposition Travaux en cours, 1985.

![](contenu-final/004-feuillet-details-graphiques/014-B-Participatif-Travauxencours-1985.jpg)
:    Carton d’invitation de l’exposition Travaux en cours, 1985.

![](contenu-final/004-feuillet-details-graphiques/15-1986-Affiche_promotionnelle.JPG)
:    Affiche promouvant les champs d’activités, 1986.

![](contenu-final/004-feuillet-details-graphiques/016.jpeg)
:    Logos de l’ISELP et détails graphiques ayant ponctué les documents promotionnels de l’ISELP au fil des années, composition réalisée par OSP en 2020.


## Expositions fortes

![](contenu-final/006-feuillet-expos-fortes/001-1978-ConfdePaulDanblon-22mars1978.jpg)
:  Paul Danblon, conférence dans le contexte de l’exposition Héros-Tics, 22 mars 1978.

![](contenu-final/006-feuillet-expos-fortes/002-1981-Affiche-ExpoTampons.jpg)
:  Affiche de l’Exposition internationale de tampons d’artistes, 1981.

![](contenu-final/006-feuillet-expos-fortes/003-ExpoTampons-Carton.jpg)
:  Document présentant l’Exposition internationale de tampons d’artistes, 1981.

![](contenu-final/006-feuillet-expos-fortes/004-A-1981-Carton-ExpoTampons01.jpg)
:  Carton d’invitation de l’Exposition internationale de tampons d’artistes, 1981.

![](contenu-final/006-feuillet-expos-fortes/004-B-1981-Carton-ExpoTampons02.jpg)
:  Carton d’invitation de l’Exposition internationale de tampons d’artistes, 1981.

![](contenu-final/006-feuillet-expos-fortes/005-A-TamponCatalogue.jpg)
:  Formulaire de participation rempli par Robert Filliou, Exposition internationale de tampons d’artistes, 1981.

![](contenu-final/006-feuillet-expos-fortes/005-B-TamponCatalogue.jpg)
:  Formulaire de participation rempli par Robert Filliou, Exposition internationale de tampons d’artistes, 1981.

![](contenu-final/006-feuillet-expos-fortes/006-1981-ExpoTampons-1981.jpg)
:  Vue de l’Exposition internationale de tampons d’artistes, 1981.

![](contenu-final/006-feuillet-expos-fortes/007-1983-Affiche_Expo.JPG)
:  Affiche de l’exposition Pochettes de disques. Enluminures du XXe siècle, 1983.

![](contenu-final/006-feuillet-expos-fortes/008-1983-Pochettesdedisques-Invitation.jpg)
:  Carton d’invitation de l’exposition Pochettes de disques. Enluminures du XXe siècle,
1983.

![](contenu-final/006-feuillet-expos-fortes/009-1983-Pochettesdedisques-Vernissage.jpg)
:  Vue de l’exposition Pochettes de disques. Enluminures du XXe siècle, 1983.

![](contenu-final/006-feuillet-expos-fortes/010-1984-Neon-01-Affiche.jpg)
:  Affiche de l’exposition Néon, Fluor et cie,1984.

![](contenu-final/006-feuillet-expos-fortes/011-Neon.jpg)
:  Document de présentation de l’exposition Néon, Fluor et cie,1984.

![](contenu-final/006-feuillet-expos-fortes/012-1984-Neon-Cartoncouleur.jpg)
:  Carton d’invitation de l’exposition Néon, Fluor et cie,1984.

![](contenu-final/006-feuillet-expos-fortes/013-1984-Neon-Vernissage.jpg)
:  Fernand Flausch, Enseigne de la façade de la façade de l’ISELP, 1984. Exposition Néon, Fluor et cie, 1984.

![](contenu-final/006-feuillet-expos-fortes/013-1984-Neon-Vernissage.jpg)
:  Vernissage de l’exposition Néon, Fluor et Cie, 13 septembre 1984.

![](contenu-final/006-feuillet-expos-fortes/014-1984-Neon-04-Expo-ExtraitPublication.jpg)
:  Metallic Avau, Sans titre, 1984. Vue de l’exposition Néon, Fluor et Cie, installation dans le Passage de Milan, 1984.

![](contenu-final/006-feuillet-expos-fortes/014-1984-Neon-04-Expo-ExtraitPublication.jpg)
:  Leo Coppers, De Knoop / Le nœud, 1984. Vue de l’exposition Néon, Fluor et Cie, installation dans le parc d’Egmont, 1984.

![](contenu-final/006-feuillet-expos-fortes/015-Néon.jpg)
:  Henri Lambert, Voltiges… une étoile pour une nuit sans étoile, 1984. Vue de l’exposition Néon, Fluor et Cie, installation dans le parc d’Egmont, 1984.

![](contenu-final/006-feuillet-expos-fortes/015-Néon.jpg)
:  Antoine Laval/Streamline, Sans titre, 1984. Vue de l’exposition Néon, Fluor et Cie, installation dans le parc d’Egmont, 1984.

![](contenu-final/006-feuillet-expos-fortes/015-015-Néon.jpg)
:  Philippe Decelle, L’envol des échassiers, 1984. Vue de l’exposition Néon, Fluor et Cie, installation dans le parc d’Egmont, 1984.

![](contenu-final/006-feuillet-expos-fortes/016-Néon.jpg)
:  Danny Matthijs, La plus petite ambassade du monde, 1984. Vue de l’exposition Néon, Fluor et Cie, 1984.

![](contenu-final/006-feuillet-expos-fortes/016-Néon.jpg)
:  Franck Van Herck, Combat du jaune et du rouge, 1984. Vue de l’exposition Néon, Fluor et Cie, installation dans le parc d’Egmont, 1984.

![](contenu-final/006-feuillet-expos-fortes/017-Neon.tif)
:  TOUT (Paul Gonze), Rêve de voyance ou de cécité, 1984. Vue de l’exposition Néon, Fluor et Cie, installation dans le parc d’Egmont, 1984.

![](contenu-final/006-feuillet-expos-fortes/018-1984-Neon.jpeg)
:  Portes de l’entré administrative

![](contenu-final/006-feuillet-expos-fortes/020-Neon.jpeg)
:  Carré, triangle, cercle 

![](contenu-final/006-feuillet-expos-fortes/021-Neon.jpeg)
:  Grosse ampoule


## Scénographie des années 2000

![](contenu-final/008-feuillet-sceno-2000/002-Muyle-Sceno.jpg)
:  Johan Muyle, Plutôt la honte, 1996. Exposition Avoir lieu : Johan Muyle,
2001.

![](contenu-final/008-feuillet-sceno-2000/003-A-ISELPenfermement-2003.jpg)
:  Guillaume Liffran, Enfermement, Exposition Guillaume Lifran. Electric, 2003.

![](contenu-final/008-feuillet-sceno-2000/004-LaurenceDervaux.tif)
:  Laurence Dervaux, La quantité de sang pompée par le cœur humain en une heure et vingt-huit minutes, ceci à raison de sept mille litres de sang pompés en vingt-quatre heures, 2003. Exposition Du diaphane et de l’illusion, 2004.

![](contenu-final/008-feuillet-sceno-2000/005-PhilippeCardoen-2004.jpg)
:  Philippe Cardoen, Gises, 2004. Exposition Philippe Cardoen. Gises, 2004.

![](contenu-final/008-feuillet-sceno-2000/006-MichelClerbois-2005.jpg)
:  Michel Clerbois, Moments de vie, 1994-2005. Exposition De vous à moi, vous m’…, 2005.

![](contenu-final/008-feuillet-sceno-2000/007-SylvieMaciasDiaz-2009.jpg)
:  Sylvie Macias Diaz, Buildings Extension, 2009. Exposition Sylvie Macias Diaz : buildings extension, 2009.

![](contenu-final/008-feuillet-sceno-2000/008-exonymie2.jpg)
:  Eric Van Hove, Exonymie, 2010. Exposition Exonymie, 2010.

![](contenu-final/008-feuillet-sceno-2000/009-A-Flesh.jpg)
:  Sofi Van Saltbommel, Corps, 2004-2009, Exposition Flesh, 2009.

![](contenu-final/008-feuillet-sceno-2000/010-Duos-©JJSEROL0504.JPG)
:  Emilio López-Menchero, H2-H1 et Fortune et servitudes ou grandeur et décadence de CHFD du Jadis au Présent, 2012. Charles-François Duplain, CHFD 82 KG / ELM 79 KG, 2012. Exposition Duos. Deux poids, deux mesures, 2012.

![](contenu-final/008-feuillet-sceno-2000/011-©JJSEROL0563.JPG)
:  Lucile Bertrand, Titre, 2012. Exposition Duos. Deux poids, deux mesures, 2012.

![](contenu-final/008-feuillet-sceno-2000/012-Desorientés©JJSEROL0395-2013.JPG)
:  Peter Welz, Rotating figure inscribing a circle, 2006.  Exposition Dés-orienté(s), 2013.

![](contenu-final/008-feuillet-sceno-2000/013-Bonom-A-.JPG)
:  Vincent Glowinski et Ian Dykmans, vue de l’exposition Bonom, le singe boiteux, 2015.

![](contenu-final/008-feuillet-sceno-2000/014-_©jjserol_5098-Hostipitalité.jpg)
:  Patrick Bernier et Olive Martin, L’échiqueté et le Déparleur, 2012. Exposition Hostipitalité, 2015.

![](contenu-final/008-feuillet-sceno-2000/015-ManonBara-Candidat-2015.jpg)
:  Manon Bara, La parade des indésirables, 2015. Exposition Candidat, 2015.

![](contenu-final/008-feuillet-sceno-2000/016-Institut-Part2.JPG)
:  Yoann Van Parys, Sans titre, 2016. Exposition #Institut – Part 2 : De l’assemblée à l’imprimante, 2016.

![](contenu-final/008-feuillet-sceno-2000/017-Sync.JPG)
:  Adrien Tirtiaux, Les impostes, 2017.  Exposition Sync !, 2017.

![](contenu-final/008-feuillet-sceno-2000/018.JPG)
:  Adrien Tirtiaux, L’Impoutre, 2017 et Joachim Coucke, Frequently Answered Questions, 2015. Exposition L’art d’accommoder les restes, 2017.

![](contenu-final/008-feuillet-sceno-2000/019-.jpg)
:  Fanny Durand, installation dans le contexte d’une résidence, 2018.

![](contenu-final/008-feuillet-sceno-2000/020-comnuties054.jpg)
:  Scénographie conçue par Sarah et Charles, 2018. Exposition COM ∩∪ TIES, 2018.

![](contenu-final/008-feuillet-sceno-2000/021-iselpforest19_C.BEAU-toulet.jpg)
:  Cécile Beau, Cladonia, 2017. Exposition A Forest, 2018.

![](contenu-final/008-feuillet-sceno-2000/022-UNEXISTANT_ALECDEBUSCHERE-AGENCE-OKTO_JULES-TOULET-8.jpg)
:  Alec De Busschère, Exposition Unexistant, 2019.


## Participatif

![](contenu-final/010-feuillet-projets-participatifs/001.JPG)
:  Tell/Sell, a common story, performance de Sarah Van Lamsweerde, 2016. Exposition #Institut / Part 1 : Table of Content, 2016.

![](contenu-final/010-feuillet-projets-participatifs/002-BXL-IXL_Giller002.jpg)
:  Jérôme Giller, BXL-FRONTIERE-IXL, Marche sur les frontières des communes de Bruxelles et d’Ixelles, entre l'ISELP et le Buktapaktop, 3 juin 2017. Exposition Sync ! / Part 3 : Le Dispensaire, 2017. ©archives Jérôme Giller, 2017.

![](contenu-final/010-feuillet-projets-participatifs/003.jpg)
:  Sofia Caesar, Zero hour, 2018. Evènement Rites of Exchange, 12 mai 2018.

![](contenu-final/010-feuillet-projets-participatifs/004-A-ISELP_laure_cottin_stefanelli_MG_03280038.jpg)
:  Hannah Mevis, In salad we trust, 2018. Evènement Rites of Exchange, 12 mai 2018.

![](contenu-final/010-feuillet-projets-participatifs/005-B-.JPG)
:  Lisa Wilkens, The ping-pong talks, 2018. Evènement Rites of Exchange, 12 mai 2018.

![](contenu-final/010-feuillet-projets-participatifs/006.JPG)
:  Denicolai & Provoost, Tien Tarteen, 2018.

![](contenu-final/010-feuillet-projets-participatifs/06-02_PRESENTATIONTAPISSERIE_ISELP_____┬®JulesToulet-45.jpg)
:  Elen Braga, Manipulation de l’œuvre monumentale Elen Ou Hubris, 2020.

![](contenu-final/010-feuillet-projets-participatifs/007-GamesoPolitics.JPG)
:  Vue d’ensemble de l’exposition Games and Politics, 2019.

![](contenu-final/010-feuillet-projets-participatifs/008-Perf-MyriamPruvot.JPG)
:  Myriam Pruvot, performance 3x Tirésia, 2019. Exposition Babel, 12 septembre 2019.

![](contenu-final/010-feuillet-projets-participatifs/009-.jpg)
:  Drink’n’Draw dans le contexte de la journée d’étude L’édition indépendante : laboratoire de création & système D, 7 novembre 2019. Picture Festival, 2019.

![](contenu-final/010-feuillet-projets-participatifs/010-DSC07720.jpg)
:  Laetitia Bica regroupe une dizaine d’élèves de l’école primaire Baron Steens et de pensionnaires de la Maison de repos et de soins « Aux Ursulines » au sein d’ateliers réflexifs et créatifs, 2020.

![](contenu-final/010-feuillet-projets-participatifs/011-AtelierLaetitiaBica.JPG)
:  Exposition Point de croix, développée à partir des travaux d’une dizaine d’élèves de l’école primaire Baron Steens et de pensionnaires de la Maison de repos et de soins « Aux Ursulines ».


## Bâtiment

![](/contenu-final/XX-Feuillet-extra-Batiment/001-A-Locaux-4-novembre1953-Dobbels2.jpg)
:  Invitation à l’inauguration du nouvel atelier du sculpteur Georges Dobbels, installé dans les anciennes écuries du Palais d’Egmont, 4 novembre 1954.

![](contenu-final/XX-Feuillet-extra-Batiment/001-Locaux-Dobbels1.jpg)
:  Atelier du sculpteur Georges Dobbels, 1953.

![](contenu-final/XX-Feuillet-extra-Batiment/002-img074.jpg)
:  La salle polyvalente de l’ISELP avant les travaux de 1999.

![](contenu-final/XX-Feuillet-extra-Batiment/002-img075.jpg)
:  La salle polyvalente de l’ISELP avant les travaux de 1999.

![](contenu-final/XX-Feuillet-extra-Batiment/003-img072bis.jpg)
:  Fernand Flausch, Détail de l’Enseigne de la façade de l’ISELP, 1984. Porte d’arche permettant de quitter le boulevard de Waterloo et de s’engager dans le Passage de Milan, durant l’exposition Néon, Fluor et cie, 1984.

![](contenu-final/XX-Feuillet-extra-Batiment/004-Sallederédactionmars1996photosOlivierDupont.jpg)
:  Salle polyvalente de l’ISELP transformée en salle de rédaction en mars 1996, durant les travaux de rénovation.

![](contenu-final/XX-Feuillet-extra-Batiment/005-A-Bache-1999-2.jpg)
:  Bâche produite par Berta Truyols, Michaël Baltus et Charles Kaisin, recouvrant la façade de l’ISELP durant les travaux de rénovation, 1999.

![](contenu-final/XX-Feuillet-extra-Batiment/005-B-Bache-1999.jpg)
:  Bâche produite par Berta Truyols, Michaël Baltus et Charles Kaisin,  recouvrant la façade de l’ISELP durant les travaux de rénovation, 1999.

![](contenu-final/XX-Feuillet-extra-Batiment/005-C-Bache-1999.jpg)
:  Détail de la bâche produite par Berta Truyols, Michaël Baltus et Charles Kaisin, recouvrant la façade de l’ISELP durant les travaux de rénovation, 1999.

![](contenu-final/XX-Feuillet-extra-Batiment/006-FlyerNouveauxespaces.jpg)
:  Plan de l’ISELP suite aux travaux de rénovation des années 1990.

![](contenu-final/XX-Feuillet-extra-Batiment/007-img076.jpg)
:  Installation de la passerelle qui relie l’avant et l’arrière du bâtiment.

![](contenu-final/XX-Feuillet-extra-Batiment/008-img093.jpg)
:  Façade extérieure suite aux travaux de rénovation, début des années 2000.

![](contenu-final/XX-Feuillet-extra-Batiment/009-A-POEMESISELP_26_11_20___┬®JulesToulet-15.jpg)
:  Vue d’ensemble du Passage de Milan, vers le boulevard de Waterloo, suite à la production de l’œuvre publique et permanente de Joseph Kosuth, A Map with 13 Points, 1999.

![](contenu-final/XX-Feuillet-extra-Batiment/009-B-POEMESISELP_26_11_20___┬®JulesToulet-17.jpg)
:  Vue d’ensemble du Passage de Milan, depuis le boulevard de Waterloo, suite à la production de l’œuvre publique et permanente de Joseph Kosuth, A Map with 13 Points, 1999.

![](contenu-final/XX-Feuillet-extra-Batiment/009-C-POEMESISELP_26_11_20___┬®JulesToulet-16.jpg)
:  Vue de trois composantes de l’œuvre de Joseph Kosuth, A Map with 13 Points, 1999, gravées sur les fenêtres le long du passage de Milan : « Dans ce cas-là, quelle est la question ? » (Gertrude Stein), « L’approbation du public est à fuir par-dessus tout. » (André Breton), « Une vraie œuvre engendre un débat entre l’artiste et son public. (Rebecca West).

![](contenu-final/XX-Feuillet-extra-Batiment/009-D-POEMESISELP_26_11_20___┬®JulesToulet-4.jpg)
:  Détail de l’œuvre de Joseph Kosuth, A Map with 13 Points, 1999, gravées sur les fenêtres le long du passage de Milan : « Une vraie œuvre engendre un débat entre l’artiste et son public. (Rebecca West).

![](contenu-final/XX-Feuillet-extra-Batiment/009-E-POEMESISELP_26_11_20___┬®JulesToulet-12.jpg)
:  Détail de l’œuvre de Joseph Kosuth, A Map with 13 Points, 1999, gravées sur les fenêtres le long du passage de Milan : « L’art ne peut exister sans une condition permanente de remise en question. » (Ad Reinhardt).

![](contenu-final/XX-Feuillet-extra-Batiment/010-RayonArtLibrairie.jpg)
:  Vue d’ensemble de Rayon art / Librairie, années 2000.

![](contenu-final/XX-Feuillet-extra-Batiment/011-FredericPenelle-Horsdoeuvre1-2011.jpg)
:  Frédéric Penelle, Déviation, 2011. Concours Hors d’œuvre, Intégration artistique dans la cafétéria, 2011.

![](contenu-final/XX-Feuillet-extra-Batiment/012-A-BTQ008.jpg)
:  Réaménagement de Rayon Art / Objets, 2006.

![](contenu-final/XX-Feuillet-extra-Batiment/012-B-BTQ035-1.jpg)
:  Réaménagement de Rayon Art / Objets, 2006.

![](contenu-final/XX-Feuillet-extra-Batiment/013-Rayonartcafe.jpg)
:  Réaménagement de Rayon Art / Objets, 2006.

![](contenu-final/XX-Feuillet-extra-Batiment/014-ribg_iselp_espace_02.jpg)
:  Vue d’ensemble de la grande salle d’exposition avant les travaux de 2020.

![](contenu-final/XX-Feuillet-extra-Batiment/015-IMG_20200828_210812.jpg)
:  Vue du mur extérieur qui longe le Passage de Milan durant l’action Red Alert Belgium, 28 août 2020.


## Art public

![](contenu-final/012-visuels-texte-environnement/1A.jpg)
:  Installation de Jean Glibert à la Bourse dans le cadre de l'exposition La couleur et
la Ville, 1978.

![](contenu-final/012-visuels-texte-environnement/2A.jpg)
:  TOUT (Paul Gonze), Rêve encadré, installation au Mont des Arts dans le cadre de l'exposition La couleur et la Ville, 1978.

![](contenu-final/012-visuels-texte-environnement/3.jpg)
:  Affiche de la table ronde « Urbanistes et pouvoirs de décision » du cycle Environnemental, 23 avril 1981.

![](contenu-final/012-visuels-texte-environnement/4.jpg)
:  Affiche de la table ronde « Le graffiti : degré zéro de l'art ? », 27 novembre 1985.

![](contenu-final/012-visuels-texte-environnement/5A.jpg)
:  Manifeste pour l'environnemental, publié dans Environnemental, cahier n°1, 1983.

![](contenu-final/012-visuels-texte-environnement/6.jpg)
:  Couverture du premier numéro de la revue Environnemental, juin 1990.

![](contenu-final/012-visuels-texte-environnement/7A©JJ-SEROL2114.JPG)
:  Vue de l'installation La vie errante – La vie immobile – La prison de Tatiana Bohm dans le Parc d'Egmont, 2012.

![](contenu-final/012-visuels-texte-environnement/8©jjserol_1429.jpg)
:  Vue de l'installation Inside / OUT de Vincent Chenut et Alberto Ginepro dans le Parc d'Egmont, 2013.

![](contenu-final/012-visuels-texte-environnement/9A.jpg)
:  Une des discussions menées dans le Parlamento Flor conçu par Zuloark pour le laboratoire Being Urban, 2015.

![](contenu-final/012-visuels-texte-environnement/9B.jpg)
:  Un oiseau du projet Whistle City de Thomas Laureyssens, dans les mains de la documentaliste de l'ISELP (Being Urban, 2015).

![](contenu-final/012-visuels-texte-environnement/9C.jpg)
:  Vue de l'installation Whistle City de Thomas Laureyssens dans le Passage de Milan, dans le cadre du laboratoire Being Urban, 2015.

![](contenu-final/012-visuels-texte-environnement/9D.jpg)
:  Un agent de la section 14B de Bruxelles Propreté avec une des charrettes ornées par Julien Celdran dans le cadre du laboratoire Being Urban, 2015.

![](contenu-final/012-visuels-texte-environnement/10A.jpg)
:  Chiara Colombi, Monument à la jeunesse, campus de l'Institut Emile Gryson, 2017.

![](contenu-final/012-visuels-texte-environnement/11.jpg)
:  Œuvre de Thierry Verbeke conçue pour le site RECY-K de l'Agence Bruxelles Propreté, installée en 2020. 
