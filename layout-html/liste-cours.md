# 50
### ans
# 476
### cours
# 9
### coloques
# 183
### conférences
# 186
### expositions
# 48
### membres du CA
# 72
### membres de l’équipe

Liste des cours, conférences et colloques
{: .cartouche-titre}

Cette liste reprend les manifestations
dispensées à l’ISELP, dont nous avons retrouvé la trace. Elle exclut toutefois les rencontres, les
tables rondes, les masterclass, les workshops, les journées d’études,
qui sont trop nombreux.
{: .cartouche-role}

<div id="my-toc-content" style="display:none;"></div>



## 1971

#### Cours

- Le langage de l’affiche (Gita Brys-Schatan)

- Champs sémantiques dans l’art du XX<sup>e</sup> siècle&#x202F;: Idoles et démons dans
    l’art du XX<sup>e</sup> siècle (Gita Brys-Schatan)

- Sémiologie générale (Daniel Peraya)

- Approche structurale de l’art au XIX<sup>e</sup> siècle (Robert L. Delevoy)

- Technologie des arts plastiques (Ignace Vandevivere)

## 1972

#### Cours

- L’aventure provocante de l’art contemporain (Gita Brys-Schatan)

- L’architecture contre (Maurice Culot)

- Quelques approches des faits de communication (Daniel Peraya)

- Technologie des arts plastiques (Ignace Vandevivere)

- Approche sociologique de l’art actuel (Jean-Pierre Van Tieghem)

- Entre la structure et l’image (Pierre Sterckx)

- Sémiologie, langage cinématographique et critique idéologique.
    Lecture de films (Daniel Peraya)

## 1973

#### Cours

- Esthétique expérimentale (Jean Guiraud)

- Approches de l’art fantastique (René de Solier)

- Le monde des images et la fin de l’art autonome ou Découvrir un
    ordre du monde (L’image du monde, Le monde des images, La fin de
    l’art) (Léopold Flam)

- Linguistique générale. Notions fondamentales (Prospectives
    sémiotiques) (Daniel Peraya)

- Vers une signification de l’art dans la Chine ancienne et
    contemporaine (Marc Verscheure)

- Les avatars de la sculpture (Eugénie De Keyser)

- Sémiologie graphique élémentaire et portative (Luc Van Malderen)

- Dynamique de groupe et pédagogie rénovée (Suzanne Da Costa-Bollaert)

- Technologie des arts plastiques (Ignace Vandevivere)

- Langages&#x202F;: communication et/ou signification (Daniel Peraya)

- Faire et voir (Vision et connaissance, Problématique, Hypothèses de
    recherches) (Getulio Alviani)

#### Conférences

- L’image comme communication visuelle (Abraham Moles)

- La mutation des signes (René Berger)

## 1974 {: .septantequatre}

Les cours sont suspendus durant les travaux de rénovation des anciennes
dépendances du palais d’Egmont qui deviendront les nouveaux locaux de
l’Institut.


## 1975 {: .septantecinq}

Inauguration des nouveaux locaux le 15 octobre 1975.


## 1976 {: .septantesix}

#### Cours

- En Europe, l’art pour qui ? Pourquoi ? (professeur inconnu)

- Synthèse des arts plastiques. A) Des grottes de Lascaux aux palais
    indous (Luc Legrand, Rouve Hauser, Georges Rocque)

- Synthèse des arts plastiques. B) XX<sup>e</sup> siècle. De l’art au tableau au
    tableau de l’art (Rouve Hauser, Georges Rocque)

- Voir et regarder (Henri Van Lier)

- Technologie des arts plastiques (Paul Philippot)

- Langage de l’art en Asie Mineure et en Égypte (Luc Legrand)

- Pédagogie expérimentale (L. Vandevelde)

- Traduction et fixation de l’image dans l’art contemporain (Pierre
    Sterckx)

- Sociologiquement l’art et l’image (Claude Javeau)

- L’espace et l’image de la littérature (Henri Van Lier)

- Pourquoi Cézanne ? (Jean Guiraud)

- Le moment spatial (Jean Guiraud)

- Convention de l’image organisée, sémiologie du théâtre (Arsène
    Joukovsky)

- Images, trompe-l’œil, simulacres et déplacements d’identité (ou
    l’après Magritte) (Jacques Sojcher)

- Spécificité et fonction de l’image cinématographique (Boris Lehman)

- Langages de l’image&#x202F;: l’art public (Roger Somville)

- Perplexités&#x202F;: image et histoire/structure/symbole (G. Van Hout)

- Photo/Peinture, ou les avatars du Musée imaginaire (Thierry De Duve,
    avec la collaboration de Pierre Lahaut, Jean Glibert, Jean-Pierre
    Point)

#### Conférences

- Permanence et changement de l’art égyptien (Luc Legrand)

## 1977

#### Cours

- Initiation à l’histoire de la peinture/de la sculpture&#x202F;: étude des
    techniques (professeur inconnu)

- Panorama des arts plastiques (professeur inconnu)

- XX<sup>e</sup> siècle - Métamorphoses (professeur inconnu)

- Enquêtes&#x202F;: Renaissance, Inde, architecture (Viviane Guelfi, Rouve
    Hauser, Luc Legrand, Pierre Lenain, Bernard Marcelis, Armand Neven)

- Enquêtes&#x202F;: XX<sup>e</sup> siècle (Viviane Guelfi, Rouve Hauser, Luc Legrand,
    Pierre Lenain, Bernard Marcelis, Armand Neven)

- L’art dans les musées et les galeries (critique d’art inconnu)

- Technologie des arts plastiques (Paul Philippot)

- Le langage comme pratique sociale (I. Thomas)

- Duchamp m’harcèle (Thierry de Duve)

- Interprétations spatiales de la bande dessinée et/ou&nbsp;figuration
    narrative (Michel Céder)
    
- L’espace psychologique&#x202F;: l’ici et l’ailleurs (G. Declève) 

- Psychanalyse et langage plastique (Ginette Michaud)

- Réflexions sur la fabrication de l’image (Luc Van Malderen)

- Films d’animation et ordinateur (Stan Hayward, Abraham Moles,
    praticiens des techniques classiques d’animation inconnus)

#### Conférences

- Variations de l’art égyptien (Luc Legrand)

## 1978

#### Cours

- Initiation à l’histoire de la peinture et de la sculpture
    (professeur inconnu)

- Initiation à l’histoire de l’architecture (Viviane Guelfi, Luc
    Legrand, Pierre Lenain, Armand Neven, Marcel Pesleux, Luc Richir,
    Philippe-André Rihoux, Alain Van der Hofstadt, A. Jacou)

- Panorama des arts plastiques, XX<sup>e</sup> siècle - Métamorphoses
    (professeur inconnu)

- Enquêtes&#x202F;: Renaissance, Inde, XX<sup>e</sup> siècle (Viviane Guelfi, Luc
    Legrand, Pierre Lenain, Armand Neven, Marcel Pesleux, Luc Richir,
    Philippe-André Rihoux, Alain van der Hofstadt, A. Jacou)
    
- La logique des espaces urbains (Marcel Pesleux)

- Les styles et leur symbolique (Pierre Lenain)

- Psychanalyse de langage (Ginette Michaud)

- Le jazz&#x202F;? pour quoi&#x202F;? par qui&#x202F;? comment&#x202F;? (I. Thomas)

- Pour une nouvelle lecture de l’œuvre plastique (Daniel Laroche)

- L’image électronique (Paul Paquay, Jean-Paul Trefois)

#### Conférences

- Le borduro-syldave dans les aventures de Tintin (Paul Danblon)

##### Colloque

- La reconstruction de la ville européenne (Serge Moureaux, Pierre
    Laconte, Jacques Van Der Biest, René Schoonbrodt, Jacques Lucan,
    Jean Castex, Antoine Grumbach, François Loyer, Léon Krier, Fernando
    Montes, Bernard Huet, Pierluigi Nicolin, Maurice Culot, Robert L.
    Delevoy)


## 1979

#### Cours

- Initiation à l’histoire de la peinture/de la sculpture (Viviane
    Guelfi, Luc Legrand, Pierre Lenain, Philippe-André Rihoux, Alain van
    der Hofstadt)

- Initiation aux Mass Média (Renaud Denuit, Jacqueline Juin, Marc
    Lamensch, Jacques Meuris, J. P. Paquay, Sélim Sasson, J. P. Trefois,
    Gérard Valet, Alain Viray)

- Panorama des arts plastiques, XX<sup>e</sup> siècle&#x202F;: Métamorphoses et ruptures
    (professeurs inconnus)

- Hier, ici et ailleurs (L’artiste et sa société, L’art égyptien et
    langage plastique / XX<sup>e</sup> siècle) (E. de Volder, Viviane Guelfi, André
    Jocou, Luc Legrand, Philippe-André Rihoux, Pierre Lenain, Alain van
    der Hofstadt)

- L’art en Europe depuis 1968 (Jean-Pierre Van Tieghem)

- Les langages de la radio et de la télévision (Holde Lhoest)

- Cinéma narratif. Cycle cinéma indépendant (M. Lemaitre, M. Davorinne
    et Kolesar)

#### Conférences

- Les midis de l’art urbain (Gérard Singer, Shamaï Haber, Jean-Max
    Albert, avec la collaboration de Dominique Richir)

## 1980

#### Cours

- Panorama des arts anciens et non européens (France Borel)

- L’art au XX<sup>e</sup> siècle (Alain van der Hofstadt)

- Esthétique (Brueghel et son temps, L’Art nouveau) (Gaston Fernandez)

- Art égyptien (Anne Leurquin-Tefnin)

- Les arts en Europe depuis 10 ans (Philippe-André Rihoux)

- L’esthétique au XX<sup>e</sup> siècle (Brigitte Jousten)

- Mobiliers d’hier, décors d’aujourd’hui (Micheline Ruyssinck)

- From Brussels to Europe&#x202F;: History of Arts (Raymond Morini)

- L’art précolombien (Sergio Purin)

- Arts plastiques et systèmes d’autorité en Afrique Noire (Anne
    Leurquin-Tefnin)

- Étude analytique de l’art contemporain (L’image, l’espace et les
    médias) (Alain van der Hofstadt)

- Philosophie (Introduction historique à la philosophie, Actualité de
    la philosophie, Introduction à la philosophie de l’art) (Pierre
    Ansay)

- Spectacles au XX<sup>e</sup> siècle&#x202F;: de la gestuelle à l’opéra (Harry
    Halbreich)

- Les grands moments de la musique contemporaine depuis 1945 (Harry
    Halbreich)

- La création photographique (Pierre Cordier)

## 1981

#### Cours

- Arts anciens visuels, européens et non européens (France Borel)

- L’art moderne au XX<sup>e</sup> siècle (Alain van der Hofstadt)

- Initiation à la philosophie occidentale (Pierre Ansay)

- Cours et parcours de l’art actuel (Philippe-André Rihoux)

- Monde roman (Rouve Hauser)

- Univers gothique (Rouve Hauser)

- Métamorphoses du tableau au XX<sup>e</sup> siècle&#x202F;: une chronologie visuelle
    (professeur inconnu)

- Mobiliers d’hier, décors d’aujourd’hui (Françoise Remience)

- Découvertes&#x202F;: culture, art et sites - L’Inde (C. Picron), La Chine
    (Wen Li Kao), L’Égypte (Anne Tefnin), La Grèce (professeur inconnu),
    Les tapis (Khosrow Khazai)

- Étude analytique de l’art contemporain (Le blanc, le fragment, le
    vide; Le clos et l’ouvert - Alain van der Hofstadt)

- Une civilisation&#x202F;: l’Islam (Le monde islamique - Kosrow Khazai ;
    L’Orient islamique créé par l’Occident - Philippe-André Rihoux)

- Philosophie occidentale (La violence, la mort, l’animalité&#x202F;; La
    beauté, la démarche artistique; La ville, la campagne, Solitude et
    destin communautaire - Pierre Ansay)

- Villes et campagnes (Sociologie, Urbanisme, Ecologie, Systèmes
    économiques, Architecture, Paysagisme, Sociologie, Psychologie de
    l’espace - Intervenants inconnus)

- Au degré zéro de la vie quotidienne&#x202F;: les symboles de la
    banalisation (Claude Javeau)

- L’Orient islamique créé par l’Occident (Philippe-André Rihoux)

#### Conférences

- Matériaux et miroirs (conférencier inconnu)

- Demain, l’art ? (Henri Van Lier, Abraham Moles)

- Le problème de la fin de l’art (Kostas Axelos)

## 1982

#### Cours

- Arts visuels anciens. Européens et non-européens (Bernadette Thomas)

- Initiation à la philosophie occidentale (Luc Richir)

- Initiation&#x202F;: Lectures de l’art moderne (France Borel)

- L’Italie, la France, face à l’idéal grec (Philippe-André Rihoux)

- Art contemporain&#x202F;: l’objet sculptural de Matisse, Duchamp, à
    aujourd’hui (Philippe-André Rihoux)

- History of art (Dominique Emsens)

- Découvertes&#x202F;: cultures, arts et sites (professeur inconnu)

- La Russie et l’art byzantin (France Borel)

- Le Japon&#x202F;: Introduction au pays et à sa culture (Dirk Vonck)

- Les pays indianisés du Sud-Est asiatique (Claudine Picron)

- Les États-Unis et leurs grandes collections (France Borel)

- Le meuble&#x202F;: styles, modes et séduction (Synthèse du cours précédent,
    Le mobilier anglo-saxon, Vision panoramique du mobilier du XX<sup>e</sup>
    siècle) (Françoise Remience)

- Esthétique&#x202F;: lire, questionner et comprendre l’art (Yolande
    Duvivier)

- Une civilisation&#x202F;: La Grèce antique, byzantine et moderne (Ourania
    Tzen)

- Le trompe-l’œil - Une histoire grecque (Alain van der Hofstadt)

- Étude analytique de l’art contemporain&#x202F;: réalité et abstraction
    (Alain van der Hofstadt)

- Pour comprendre l’art grec (Alain van der Hofstadt)

- Abandon de la vision utopique du monde (Jan Hoet)

- Pour un théâtre de l’expérience (Ugo Volli)

## 1983

#### Cours

- Initiation aux arts visuels anciens&#x202F;: en Europe et dans les autres
    civilisations (Bernadette Thomas)

- Initiation à la sociologie&#x202F;: la société, l’art dans la société, la
    société dans l’art (Daniel Vander Gucht)

- Initiation à l’art du XX<sup>e</sup> siècle (France Borel)

- Rodin, Brancusi… des voies différentes… Pourquoi ? ou Rodin,
    Brancusi… et le langage de la sculpture (Alain van der Hofstadt)

- L’art contemporain&#x202F;: quel est le processus créateur chez les
    artistes aujourd’hui ? (Philippe-André Rihoux)

- Découvertes&#x202F;: cultures, arts et sites (L’Espagne-Andalousie - Sophie Orloff ; La Yougoslavie - France Borel ; L’Ile de Pâques - Micheline Ruyssinck)

- Le Triangle polynésien (Micheline Ruyssinck)

- Vivre le mobilier (Jacqueline Guisset)

- Pour comprendre l’art grec (Alain van der Hofstadt)

- Pleins feux sur… Venise, mythe et réalité dans l’Europe du XVIII<sup>e</sup>
    siècle (Yvonne Van Cutsem)

- Une relation double, Orient/Occident&#x202F;: l’estampe japonaise (Dirk
    Vonck)

- Comme si elle (la photographie) était née de la peinture (Alain van
    der Hofstadt)

- Dans le vif et dans le vent&#x202F;: Sur le vif, expériences de
    graphiste (Jacques Richez)

- À la mode… pour la mode (Marc Borgers)

- Opéra-ci, opéra-là (Isaïe Disenhaus)

- Clip vidéo, une vision pour demain, Qu’est-ce que le clip, quelle
    est sa finalité, son avenir, est-ce un art ? (Jean-Pierre Berckmans)

## 1984

#### Cours

- Initiation aux arts visuels anciens, en Europe et dans les
    civilisations extra-européennes (Jacqueline Guisset)

- Un siècle d’art moderne 1884-1984 (France Borel)

- Lectures approfondies de l’art contemporain (V. Daneels)

- Actualités/Virtualités&#x202F;: contacts avec l’œuvre (Alain van der
    Hofstadt)

- Esthétique (Luc Richir)

- Le temps de, le temps dans la peinture (Alain van der Hofstadt)

- De l’idéal de « beauté héroïque » à l’imagination perverse du
    visiteur des salons (Richard Kerremans)

- Promenades dans Rome (Brigitte d’Hainaut)

- Picasso, son héritage, ses enfants naturels (Philippe-André Rihoux)

- Vivre le mobilier à travers l’importance et la beauté des meubles
    italiens, flamands, anglais et français (Jacqueline Guisset)

- Spectacles de l’art, arts du spectacle (professeur inconnu)

- Le pinceau - l’aérographe - le tableau - le graffiti (Wolfgang
    Becker)

- Au bord des eaux de Babylone… (Isaïe Disenhaus)

- De Fritz Lang à Spielberg, le cinéma dans tous ses états (Luc
    Honorez)

- Cultures&#x202F;: du livre à l’art - Les maîtres de l’estampe japonaise
    (Dirk Vonck)

- Roots&#x202F;: L’Afrique (professeur inconnu)

## 1985

#### Cours

- Initiation aux arts visuels anciens (Jacqueline Guisset)

- La volonté d’originalité&#x202F;: une histoire de l’art moderne (Lillo
    Canta)

- Le corps dans tous ses états (France Borel)

- La couleur et le prisme de la peinture (K. Stalpaert)

- Europalia&#x202F;: de Vélasquez à la nouvelle génération (K. Stalpaert)

- Histoire de la photographie (Georges Vercheval)

- De Dürer à Baselitz&#x202F;: rêve et réalité d’une Allemagne en mutation
    (R. Michel)

- Andrea Palladio et le Palladianisme (Brigitte d’Hainaut)

- Du futurisme à la Transavanguardia, une approche des mouvements
    artistiques italiens contemporains (Lillo Canta)

- La mémoire des objets (Jacqueline Guisset)

- Erasmisme&#x202F;: réflexion sur les relations humanistes entre l’Espagne
    et les Pays-Bas (Jean-Pierre Vanden Branden)

- Aujourd’hui, l’art américain, Tableaux filmés (Jacqueline Aubenas)

- Poétique de la psychanalyse (Luc Richir)

- Lecture des mythes, lieu d’exemple avec le cosmos, avec la nature
    (Jean-Michel Turine)

## 1986

#### Cours

- Apprendre à voir&#x202F;: l’art dans le monde (Jacqueline Guisset)

- De 1945 à 1986&#x202F;: figures et vocabulaire de l’art d’aujourd’hui
    (Lillo Canta)

- Grèce antique&#x202F;: la nouvelle Illiade (Pierre Lenain)

- Initiation des enfants à l’histoire de l’art - Cours à l’Iselp !
    (Lillo Canta)

- Le banquet des anges&#x202F;: l’art baroque en Autriche aux XVII<sup>e</sup> et
    XVIII<sup>e</sup> siècle (Rose-Marie Michel)

- Valses joyeuses et danses macabres&#x202F;: Vienne entre 1850 et 1986 (R.
    Michel)

- Le corps dans la sculpture contemporaine (Jean-Lou Wastrat)

- La référence dans la peinture contemporaine&#x202F;: citation et
    interprétation (Jean-Lou Wastrat)

- L’art anglais contemporain (Phil Mertens)

- Mobilier&#x202F;: objet du XVIII<sup>e</sup> siècle à nos jours&#x202F;: un art de vivre
    (Jacqueline Guisset)

- C’était au temps où Bruxelles brusselait… (Jacqueline Guisset)

- Histoire de la photographie (Georges Vercheval)

- La peinture symboliste en Europe (Francine-Claire Legrand)

- Histoire de la pensée&#x202F;: Du mythos et du logos à la technoscience
    contemporaine (Gilbert Hottois)

- Gesualdo et la musique religieuse qui suit (Jean-Michel Turine)

## 1987

#### Cours

- Apprendre à voir&#x202F;: l’art dans le monde (Jacqueline Guisset)

- Mythes et réalités de l’ancienne Égypte (Catherine Rommelaere)

- Du chevalet à l’installation&#x202F;: liberté et interrogations de l’art du
    XX<sup>e</sup> siècle (E. Fierens)

- Artistes ou artisans&#x202F;: créateurs de meubles (Jacqueline Guiset)

- Ancienne Russie&#x202F;: cités impériales et monastères des Steppes
    (Jacqueline Guisset)

- De Marcel à Marcel&#x202F;: Anthologie de l’œuvre-objet (E. Fierens)

- Le sacre d’un printemps&#x202F;: l’art russe de 1863 à 1922 (Michel
    Draguet)

- Eléments pour une histoire de l’esthétique (Jean-Lou Wastrat)

- Visions romantiques&#x202F;: courants multiples d’un art en pleine mutation
    (Rose-Marie Michel)

- L’art face aux nouvelles technologies (Michèle Minne)

- Tentaculairement vôtre… Nouvelle mythologie… La publicité, la
    mode que diable comme beauté, Le meuble entre caverne et design
    (Emanuele Riccardi)

- Une forme ouverte&#x202F;: le jazz, l’art anglais (Steven Bann)

- Vrai et faux (Théorie et méthode&#x202F;: perspective philosophique,
    perspective lexicologique, historiographie et pratique du faux de
    l’Antiquité à nos jours, méthodologie de l’expertise, mythologie de
    l’expertise / Étude de cas concrets) (Ignace Vandevivere)

## 1988

#### Cours

- Apprendre à voir&#x202F;: l’art dans le monde (Jacqueline Guisset)

- Arts et métiers dans l’Egypte pharaonique (Catherine Rommelaere)

- L’art grec&#x202F;: naissance d’un modèle (Éric Van Essche)

- Technologie des arts visuels. Un axe chronologique des arts visuels
    (R. Debanterlé)

- La Renaissance italienne&#x202F;: fixation d’un modèle (Rose-Marie Michel)

- Impressions d’Orient, impressions d’Occident (Claudine Delecourt)

- L’œil et l’esprit - Maurice Merleau-Ponty (Jean-Lou Wastrat)

- Eléments d’une histoire de la perspective (Jean-Lou Wastrat)

- Approches de la condition matérielle de l’art (R. Debanterlé)

- Le mobilier et son époque&#x202F;: journal d’une femme de goût (Jacqueline
    Guisset)

- La Chine - Une autre humanité (Claudine Delecourt)

- Le nationalisme musical russe (Charles Philippon)

- La mode que diable comme beauté (Fr. Marot)

- Art et poésie (Michel Grodent)

- Le Japon des contrastes (Claudine Delecourt)

- Trente ans de musique rock (Dominique Lawalrée)

## 1989

#### Cours

- Apprendre à voir&#x202F;: l’art dans le monde (Graziella Martello)

- Le discours des images (Rose-Marie Michel)

- 20X20&#x202F;: tour de l’art du XX<sup>e</sup> siècle en 20 étapes (Bérangère
    Schietse)

- Impressions d’Orient, impressions d’Occident (Claudine Delecourt)

- Japon et Occident - Un dialogue artistique ou Japon des contrastes
    (Claudine Delecourt)

- Le Cubisme - Approches (Jean-Lou Wastrat)

- Se meubler&#x202F;: être ou paraître (Pierre Schreiden)

- Shinto, Bouddhisme, Bushido&#x202F;: un art de vivre dans l’ancien Japon
    (Claudine Delecourt)

## 1990

#### Cours

- Apprendre à voir&#x202F;: l’art dans le monde (Graziella Martello,
    Alexandre Vanautgaerden)

- L’art égyptien&#x202F;: ruptures et continuité (Éric Van Essche)

- Au XX<sup>e</sup> siècle&#x202F;: l’idée du paysage existe-t-elle encore&#x202F;? (Alexandre
    Vanautgaerden)

- Art vidéo (Michel Couturier)

- Art belge (Alexandre Vanautgaerden)

- Aux limites de l’objet sculptural (Philippe Leten)

- Élégance et prestige ou histoire du costume et du bijou (Clémy
    Temmerman) 

- Les châteaux oubliés de Bruxelles, témoins privilégiés d’un passé
    prestigieux (Clémy Temmerman)

- Regards sur la modernité&#x202F;: le Bauhaus (Jacques Aron)

- Art, ville et société (Pierre Ansay)

- Les grandes questions de l’archéologie péruvienne (Sergio Purin)

- Ars Musica&#x202F;: la création confrontée - La diffusion de la création
    musicale aujourd’hui (Christian Renard)

- L’Amérique et l’Europe après Cage et Boulez (Philippe Alle)

## 1991

#### Cours

- Apprendre à voir&#x202F;: l’art dans le monde (Alexandre Vanautgaerden)

- De Vasco de Gama à Tamerlan (Yves Robert)

- L’art belge (Alexandre Vanautgaerden)

- L’image peinte (Christophe Slagmuylder)

- Raffinement et recherche&#x202F;: le costume et le bijou du XVIII<sup>e</sup> siècle à
    nos jours (Clémy Temmerman)

- Les châteaux de Belgique, livres d’art et d’histoire (Clémy
    Temmerman)

- Aux limites de l’objet esthétique (Philippe Leten)

- L’art russe et ses spécificités au XIX<sup>e</sup> siècle (Nadeja Mankowskaya)

- Invention(s) et tradition(s) dans la peinture russe au XX<sup>e</sup> siècle
    (Nadeja Mankowskaya)

- Qu’entend-on par culture russe ? (Nadeja Mankowskaya)

## 1992

#### Cours

- Apprendre à voir&#x202F;: l’art dans le monde (Alexandre Vanautgaerden)

- À la découverte des Amérindiens (Clémy Temmerman)

- Art, films d’art, films d’artistes (Alexandre Vanautgaerden)

## 1993

#### Cours

- Apprendre à voir&#x202F;: la perspective (Alexandre Vanautgaerden)

- L’art dans la culture médiatique (Philippe Franck)

- La femme, miroir de la civilisation (Clémy Temmerman)

## 1994-1999 {: .intro}

Les cours sont suspendus en raison des travaux de rénovation et
d’agrandissement des locaux occupés par l’ISELP.


## 2000

#### Cours

- Réalité et simulation, une rupture avec l’art illusionniste (Etienne
    Tilman)

- Le marché de l’art, les cotes et l’art en situation (Ingrid van
    Langhendonck)

- La sculpture du XIX<sup>e</sup> siècle à nos jours, de Bourdelle à Segal, de
    Brancusi à Serra&#x202F;: un approfondissement de la tridimensionnalité
    (Catherine Leclercq)

- Les sentiers de la création revisités par la psychanalyse (Viviane
    Guelfi)

- De Virginia Woolf à Louise Bourgeois&#x202F;: la question du féminin au XX<sup>e</sup>
    siècle (Séphora Thomas)

- L’art actuel&#x202F;: expérimentations et découvertes au fil du temps. 1<sup>ère</sup>
    année&#x202F;: l’Europe (Christine de Lannoy-Clervaux)

- Peinture et architecture&#x202F;: de Brunelleschi à Dan Graham (Aram
    Mekhitarian)

- Faux et simulacre (Thierry Lenain)

- Le fantastique au cœur du quotidien&#x202F;: un itinéraire photographique
    (Tamara Kocheleff)

- Le surréalisme. Actualité et poursuites (Xavier Canonne)

- Psychanalyse et créativité (Jacqueline Harpman)

- De l’utopie à l’uchronie - Une réflexion esthétique sur la S.F.
    (Denis Gielen)

- L’urbanisme, l’artiste, le quartier et la ville (Pierre Puttemans)

- Art et écriture / du calligramme au logogramme (Xavier Canonne)

- Le design apprivoisé (Martine Boucher, Pol Quadens, Romano Scuvée)

- Clonage, problèmes et solutions (Robert Schoysman)

#### Conférences

- Le patrimoine majeur de la Belgique&#x202F;: Images patrimoniales et
    identités (Yves Robert, Gian-Giuseppe Simeone)

- Récents projets (Philippe Samyn)

- Les sculptures de Bruxelles (Catherine Leclercq, Jacques van Lennep,
    Patrick Derom)

- L’architecture moderne à Bruxelles (Pierre Puttemans, Jacques Aron,
    Patrick Burniat)

- Les origines de l’opéra-comique (François Ryelandt)

- À propos de la fugue (Walter Corten)

- Artiste&#x202F;: un statut mouvant. Lomé, Cotonou, Abidjan (Claire Poinas)

- Maîtres de rues. Les peintres populaires du Congo (Jean-Pierre
    Jacquemin)

- Migration et création (Valérie Brixhe)

## 2001

#### Cours

- Le marché de l’art&#x202F;: art d’aujourd’hui/argent de demain&#x202F;?
    (Christophe Veys)

- Trente ans de création artistique de 1970 à l’an 2000 ou de la
    dématérialisation au chaos (Étienne Tilman)

- De l’impressionnisme au Pop Art ou de l’émancipation du sujet à la
    remise en question du concept (Magali Parmentier)

- De Lascaux à Kiefer&#x202F;: La couleur dans l’art (Ben Durant)

- L’image du corps dans l’histoire de l’art classique. Première
    partie&#x202F;: de la représentation du corps en Égypte sous le règne
    d’Aménophis IV aux Maniéristes du XVII<sup>e</sup> siècle (Étienne Tilman)

- Les métropoles de l’art aux XX<sup>e</sup> et XXI<sup>e</sup> siècle (Jean-Philippe
    Theyskens)

- Les fils de la toile II (Jean-Loup Wastrat)

- Art et concept&#x202F;: penser l’art (Aram Mekhitarian)

- Les vases communicants - peinture et photographie (Xavier Canonne)

- Anthropologie de la conduite esthétique (Jean-Marie Schaeffer)

## 2002

#### Cours

- Du journal intime au mensonge&#x202F;: à la recherche des vraies/fausses
    identités de l’artiste en héros de l’art actuel (Christophe Veys)

- Des croisements interdisciplinaires aux arts électroniques&#x202F;:
    mutations, hybridations et interactions en question (Philippe
    Franck)

- L’art belge existe-t-il&#x202F;? Regards croisés sur la correspondance des
    arts en Belgique au XX<sup>e</sup> siècle (Magali Parmentier)

- Le musée comme muse&#x202F;: artistes et pratiques artistiques muséales, de
    Duchamp à nos jours (Jean-Philippe Theyskens)

- De Louis-Philippe à Dada&#x202F;: mais d’où vient donc l’art moderne&#x202F;?
    (Jean-Loup Wastrat)

- L’image du corps dans l’histoire de l’art classique&#x202F;: du XVI<sup>e</sup>
    siècle à la fin du XIX<sup>e</sup> siècle (Étienne Tilman)

- Regarde voir, c’est une image&#x202F;! Comment aborder les images d’hier et
    d’aujourd’hui (Vincent Cartuyvels)

- L’art qui ne ressemble plus à de l’art&#x202F;! (Étienne Tilman)

- Arts électroniques&#x202F;: des technologies nouvelles à la création
    multimédia (Philippe Franck)

- Dématérialisation de l’art, deuxième vague&#x202F;: critique sur
    l’importance de l’objet (Étienne Tilman)

#### Conférences

- De la naissance de la valeur d’exposition à la substitution de
    l’œuvre par l’exposition&#x202F;: questions esthétiques posées par une
    mutation, de Walter Benjamin à aujourd’hui (Tristan Trémeau)

- Pour en finir avec la querelle de l’art contemporain (Nathalie
    Heinich)

- Résonance dans l’image (Jean-Luc Nancy)

## 2003

#### Cours

- Nomadisme et transgressions&#x202F;: l’art contemporain en déplacements
    (Christophe Veys)

- Art et médias&#x202F;: stratégies d’infiltration et guérillas urbaines au
    XX<sup>e</sup> siècle (Catherine Mayeur)

- Abécédaire de l’art moderne et contemporain&#x202F;: approche chronologique
    et comparative des bouleversements de l’art de la fin du XIX<sup>e</sup> siècle
    à nos jours (Olivier Duquenne)

- L’art en liberté&#x202F;: de l’intervention dans le paysage aux
    déambulations urbaines (Magali Parmentier)

- La photographie plasticienne&#x202F;: sur les rapports croisés entre
    photographie et arts plastiques depuis 1960 (Natacha Derycke)

- Ar(t)chitectures&#x202F;: architectures aux confins des arts plastiques à
    la fin du XX<sup>e</sup> siècle (Jean-Philippe Theyskens)

- Le corps et la représentation du corps au XX<sup>e</sup> siècle (Étienne
    Tilman)

- Temps et récit dans l’image photographique contemporaine (Danielle
    Leenaerts)

- Onze éditions&#x202F;: bilans pluriels et états des lieux de la Documenta
    (Jean-Philippe Theyskens)

- Art et politique&#x202F;: figures de l’engagement et de la responsabilité
    de l’artiste dans l’art moderne et contemporain (Daniel Vander
    Gucht)

- L’art et le sexe dans la seconde moitié du XX<sup>e</sup> siècle&#x202F;: un
    parcours impudique (Étienne Tilman)

- Jean-Pierre Melville, un cycle de solitude&#x202F;: réflexion sur
    l’écriture cinématographique et les arts plastiques (Xavier Canonne)

- La part du jeu dans l’art&#x202F;: réflexions sur le sens du jeu à travers
    la peinture, la sculpture et le film (Catherine Leclercq)

##### Colloque

- Exposer l’image en mouvement&#x202F;: de la grammaire spatiale à la
    production de contenus (Muriel Andrin, Manuel Abendorth, Charles
    Angelroth, Hélène Agofroy, Michel Assenmaker, Jean-Louis Boissier,
    Régis Cotentin, Edmond Couchot, Alexandra Dementieva, Florence de
    Mèredieu, Alexis Destoop, Fred Forest, Philippe Franck, Bertrand
    Gadenne, Aram Mekhitarian, Jean-Michel Ribettes, Aldo Guillaume
    Turin, Éric Van Essche)

## 2004

#### Cours

- Iconographie&#x202F;: cinquante siècles d’images symboliques (Ben Durant)

- De l’artiste à nous&#x202F;: l’œuvre d’art comme espace de rencontre, de
    rupture et de réconciliation (Delphine Florence)

- Généalogies photographiques et stéréotypes visuels&#x202F;: la mise en
    place d’un imaginaire normé (Jean-Marc Bodson)

- Expérience et représentation chez Henri Bergson (Didier Debaise)

- Le monde imaginaire de Gilles Deleuze (Pascal Chabot)

- Walter Benjamin, passager de la modernité&#x202F;: enjeux esthétiques et
    héritages critiques (Philippe Franck)

#### Conférences

- Propos pour une organologie générale (Bernard Stiegler)

- Yves Klein contre C.G. Jung&#x202F;: l’immatériel contre l’obscurantisme
    (Jean-Marie Ribettes)

- Histoires de cartes. Cartographier la peinture (Pierre Sterckx)

- L’image entre l’instant et le temps (Alain Fleischer)

- Ouvrir ou couvrir&#x202F;: art contemporain et théâtralisation du corps
    (Paul Ardenne)

- Bijoux d’aujourd’hui, bijoux de demain&#x202F;? Les créateurs de bijoux
    contemporains en Communauté française (Pierre-Paul Dupont)

- L’art du clip&#x202F;: sa nouveauté, son univers et son langage (Régis
    Cotentin)

##### Colloque

- Le sens de l’indécence&#x202F;: la question de la censure à l’âge
    contemporain (Patrick Amine, Balbino Bautista, Olivier Blanckart,
    Eliane Brunet, Ralph Dekoninck, Joël Gilles, Carole Hoffmann,
    Danielle Leenaerts, Enrico Lunghi, Agnès Tricoire, Éric
    Vandecasteele)

## 2005

#### Cours

- L’objet dans l’art d’aujourd’hui&#x202F;: de son affirmation à sa
    disparition (Christophe Veys)

- Des interactions de l’art et de la télévision aux nouvelles formes
    documentaires&#x202F;: résistances ou confusions des genres&#x202F;? (Catherine
    Mayeur)

- Abécédaire de l’art contemporain&#x202F;: approche chronologique et
    thématique (Olivier Duquenne)

- L’art d’à côté&#x202F;: une nouvelle génération d’artistes français
    (Christophe Veys)

- Histoire(s) de vidéo&#x202F;: du grand et du petit écran aux cimaises des
    galeries (Colette Dubois)

- Les métamorphoses de la beauté&#x202F;: histoire et sens du Beau de
    l’Antiquité à nos jours (Olivier Duquenne)

- L’art de vendre ou le destin populaire de la modernité artistique
    (Jean-Loup Wastrat)

- L’artiste au pluriel&#x202F;: collectifs et convergences dans les pratiques
    artistiques émergentes (Delphine Florence)

- Questions à la sculpture&#x202F;: archaïsme et modernité de la statuaire
    (Pierre Sterckx)

- Arts primitifs et primitivisme&#x202F;: une histoire fusionnelle
    contemporaine (Ben Durant)

- Une histoire de la danse, de la Renaissance au début du XX<sup>e</sup> siècle&#x202F;:
    l’art, le pouvoir et la société (Jean-Philippe Van Aelbrouck)

- Et vous trouvez ça beau, vous&#x202F;? Une approche contemporaine de
    l’esthétique (Magali Parmentier)

- Le jeu vidéo&#x202F;: art ou culture&#x202F;? Réflexion, classification et
    généalogie des univers ludiques (Vincent Delvaux)

- William S. Burroughs, sur la route de l’interzone (Philippe Franck)

- Art et philosophie chez Michel Foucault&#x202F;: de la disposition des
    pensées à la pensée du dispositif (Gilles Collard)

- In-aesthesis&#x202F;: remarques sur l’inesthétique d’Alain Badiou (Daniel
    Franco)

- Art et politique chez Maurice Merleau-Ponty (Victor Hugo Riego)

- Cinéma, musique et arts plastiques (1)&#x202F;: l’avant-garde cinématographique des années 1920&#x202F;: Fritz Lang et Sergei M. Eisenstein (Muriel Andrin, Gilles Rémy)

- Cinéma, musique et arts plastiques (2)&#x202F;: Stanley Kubrick et Peter
    Greenaway&#x202F;: pour un syncrétisme auteuriste (Muriel Andrin, Gilles
    Rémy)

#### Conférences

- Le contemporain est-il soluble dans le champ de bataille
    moderne-post-moderne&#x202F;? (Christian Ruby)

- Quelle esthétique pour l’art d’aujourd’hui&#x202F;? (Marc Jimenez)

- Ai-je bien tout dit&#x202F;? (Catherine Millet)

- La beauté aujourd’hui&#x202F;: du transcendantal à l’omniprésence (Yves
    Michaud)

- Regarder une image, construire sa durée (Georges Didi-Huberman)

## 2006

#### Cours

- Figures émergentes&#x202F;: une chronique commentée de l’actualité
    artistique (Christophe Veys)

- Mot à main&#x202F;: image et écriture dans l’art moderne et contemporain en
    Belgique (Denis Laoureux)

- Les mythologies individuelles&#x202F;: histoire d’un art visionnaire
    (Olivier Duquenne)

- La querelle de l’art contemporain&#x202F;: une histoire critique des
    oppositions entre les Anciens et les Modernes (Jean-Loup Wastrat)

- La danse au XX<sup>e</sup> siècle, perspective diachronique&#x202F;: de la révolution
    moderne aux enjeux contemporains (Jean-Philippe Van Aelbrouck)

- Dix peintres exemplaires, de Paolo Ucello à Jean-Michel Basquiat&#x202F;:
    lectures croisées (Pierre Sterckx)

- Le maniérisme aujourd’hui&#x202F;: une esthétique du chaos (Laurent
    Courtens)

- De l’art au design et du design à l’art&#x202F;: production, consommation
    et critique sociale (Denis Laurent)

- Dramaturgie musicale et art contemporain&#x202F;: les arts visuels sur la
    scène de l’opéra (Raya Baudinet, Gilles Rémy)

- L’avant-garde musicale à Ars Musica&#x202F;: introduction aux œuvres de
    Boesmans, Eötvös, Lachenmann et Pintscher (Gilles Rémy)

- Musée imaginaire lacanien&#x202F;: de l’art et de la psychanalyse (Yves
    Depelsenaire)

- Collections et dépendances&#x202F;: de la passion à l’obsession du
    collectionneur (Ben Durant, Christophe Veys)

- Du fantasme à la projection&#x202F;: quand l’imaginaire se donne à voir, de
    Lascaux à Tony Oursler (Denis Gielen)

- De Marcel Broodthaers à Jean-Luc Godard&#x202F;: l’exposition du cinéma
    (Colette Dubois)

#### Conférences


- In situ, online&#x202F;: Les lieux de l’œuvre d’art à l’âge des réseaux
    (Norbert Hillaire)

- L’image au présent&#x202F;? Figure de l’événement et temporalités
    historiques (Michel Poivert)

- Bodies-cities&#x202F;: la ville comme terrain d’expérimentation pour l’art
    actuel (Thierry Davila)

- L’image installée&#x202F;: la vidéo en installation (Françoise Parfait)

- Une réflexion sur les pratiques artistiques contemporaines&#x202F;: enjeux
    et perspectives (Marc-Olivier Wahler)

##### Colloque

- De l’art contextuel aux nouvelles pratiques documentaires&#x202F;: les
    formes contemporaines de l’art engagé (Muriel Andrin, Paul Ardenne,
    Sébastien Biset, Aline Caillet, Hans Cova, Alexia Creusen, Yves
    Depelsenaire, Florence de Mèredieu, Thibaut de Ruyter, Virginie
    Devillez, Colette Dubois, Benoît Eugène, Eloy Feria, Philippe
    Franck, Denis Gielen, Paul Gonze, Frédéric Jacquemin, Jean-Marc
    Lachaud, Enrico Lunghi, Cécile Massart, Catherine Mayeur, Kader
    Mokaddem, Tania Nasielski, Els Opsomer, Carolina Serra, Nathalie
    Stefanov, Evelyne Toussaint, Jean-Philippe Uzel, Daniel Vander
    Gucht, Françoise Vincent, Didier Vivien)

## 2007

#### Cours

- Figures de proue&#x202F;: Miles Davis, Hergé, Marcel Proust, Cézanne et les
    autres (Pierre Sterckx)

- Textiles et fibres&#x202F;: un fil d’Ariane dans le dédale de l’art
    contemporain (Dominique Lamy)

- Les installations contemporaines&#x202F;: de la contemplation à la
    participation (Magali Parmentier)

- Quoi de neuf&#x202F;? L’art d’aujourd’hui au prisme de Documenta XII
    (Christophe Veys)

- L’art de l’« autre » Europe&#x202F;: regards sur la création contemporaine
    est-européenne (Olivier Vargin)

- Balades en Absurdie&#x202F;: non-sens, psychanalyse et féerie dans l’art
    (Olivier Duquenne)

- Beau comme l’Antique&#x202F;! Résonance de l’Antiquité dans l’art
    contemporain (Jean-Loup Wastrat)

- La scène théâtrale et chorégraphique&#x202F;: histoire d’un lieu
    d’expression et d’expérimentation pour les arts visuels
    (Jean-Philippe Van Aelbrouck, Vincent Delvaux)

- Musique contemporaine, que me veux-tu&#x202F;? À la découverte des paysages
    sonores actuels (Gilles Rémy)

- FREAKS&#x202F;: Monstres et hybrides dans l’art, des gorgones à Alien
    (Laurent Courtens)

- Relation/Création&#x202F;: le phénomène relationnel de l’art et de la
    culture d’aujourd’hui (Sébastien Biset)

- Projections et partitions&#x202F;: de la fonction à la composition de la
    musique au cinéma (Marc Hérouet, Gilles Rémy)

- La critique d’art comme genre littéraire&#x202F;: pour une esthétique de la
    réception (Patrick Amine)

- Qui surveille qui&#x202F;? L’art contemporain à l’épreuve de la
    vidéosurveillance (Nathalie Stefanov)

- Le faux dans l’art à travers les âges&#x202F;: enquête au musée des dupes
    (Ben Durant)

- Territoires du sonore&#x202F;: écouter le réel, les sons, les voix, les
    musiques, les constructions singulières (Daniel Deshays)

#### Conférences

- Le devenir cochon de Wim Delvoye (Pierre Sterckx)

## 2008

#### Cours

- Gilles Deleuze en dix concepts et quelques autres… Pour comprendre
    et vivre l’esthétique deleuzienne (Pierre Sterckx)

- De Jésus-Christ à Ben Laden&#x202F;: l’instrumentalisation des images par
    le pouvoir politique et religieux (Olivier Waedemon)

- De la création à l’exposition, Les nouvelles médiations de l’art
    contemporain (Emmanuel Lambion, Pierre-Yves Desaive)

- Chaque semaine, il y a quelque chose de différent&#x202F;: le kaléidoscope
    de l’art d’aujourd’hui (Christophe Veys)

- « Je(s) » est un autre&#x202F;: Identité(s) en mutation dans l’art
    contemporain est-européen (Olivier Vargin)

- Les arborescences contemporaines ou le végétal dans l’art actuel
    (Olivier Duquenne)

- Réflexions/Réflections&#x202F;: miroirs et reflets dans l’art contemporain
    (Dominique Lamy)

- Souffrance, mort et renaissance ; les métamorphoses du corps dans
    l’art (Ben Durant)

- Le dessin contemporain&#x202F;: un geste autographe dans le champ du
    multiple (Delphine Florence)

- Du pluriel à l’infini&#x202F;: Historie, cartographie et enjeux esthétiques
    des Multiples dans l’art contemporain (Marie Maeck)

- La vitrine de l’art&#x202F;: Marchands célèbres et grands galeristes du XXe
    siècle (Ben Durant, Christophe Veys)

- L’art et le temps&#x202F;: arts plastiques, musique, littérature et cinéma
    (Gilles Rémy)

- L’image projetée, exposée, médusée&#x202F;: métamorphoses du cinéma dans
    l’art contemporain (Muriel Andrin)

- Qu’est-ce que représenter&#x202F;? Réflexion sur le nouvel imaginaire
    théâtral et l’art contemporain (Raya Baudinet)

- Max Loreau&#x202F;: une esthétique à l’épreuve de la peinture contemporaine
    (Eric Clemens)

- Textes et toiles&#x202F;: Henri Bergson et Gilles Deleuze vont au cinéma
    (Jean-Loup Wastrat)

- Du sujet au subjectile&#x202F;: support et recouvrement en peinture, de
    Manet à Ryman (Raphaël Pirenne)

#### Conférences

- L’image séminale&#x202F;: vers une émancipation des limites biologiques
    (Eric Michaud)

- Impasses et impostures en art contemporain (Pierre Sterckx)

##### Colloque

- Spéculations spéculaires&#x202F;: le reflet du miroir dans l’image
    contemporaine (Olivier Belon, Pascale Borrel, Denis Briand, Eric
    Clemens, Thierry de Duve, Florence de Mèredieu, Olivier Duquenne,
    Sandrine Ferret, Alain Fleischer, Dominique Lamy, Denis Laoureux,
    Danielle Leenaerts, Marie-France Martin, Patricia Martin, Véronique
    Mauron, Magali Parmentier, Soko Phay-Vakalis, Éric Vandecasteele,
    Olivier Vargin, Christophe Viart, Christophe Veys, Paul Willemsen,
    Simon Welch)

## 2009

#### Cours

- 24 allers-retour/seconde&#x202F;: Le cinéma et l’imaginaire de la
    photographie/la photographie ou l’inconscient du cinéma (Emmanuel
    d’Autreppe)

- Entre absorption et distanciation&#x202F;: La place du spectateur face à
    l’œuvre d’art (Dominique Lamy)

- Nature et photographie/Nature de la photographie (Danielle
    Leenaerts)

- Matières et espaces&#x202F;: un siècle de sculpture contemporaine en
    Belgique, dans et hors les murs (Adrien Grimmeau)

- En avoir pour son ar(t)gent&#x202F;: l’art contemporain et ses rapports à
    l’économie (Christophe Veys)

- De Diderot à nos jours ; jalons d’une histoire de la critique d’art
    (Pierre Sterckx)

- Le propre de l’homme&#x202F;: rire, humour et dérision dans l’art
    contemporain (Laurent Courtens)

- L’art est-il soluble dans les industries culturelles&#x202F;? Enjeux
    esthétiques, économiques et politiques de l’art actuel, de ses
    institutions et de ses médiations (Tristan Trémeau)

- Illuminations esthétiques&#x202F;: Entre immanence et transcendance (Maud
    Assila)

- Pratiques sonores&#x202F;: le son installé dans la cité (Philippe Franck)

- Matière, procédures, lieu, structure&#x202F;: Sur quelques sculpteurs
    (post)modernistes (Anaël Lejeune)

- Made in/out Africa&#x202F;: la création africaine contemporaine aux prises
    du monde de l’art globalisé (Sarah Gilsoul)

- L’artiste commissaire&#x202F;: Entre jeu créatif, posture critique et
    valeur ajoutée (Julie Bawin)

#### Conférences

- Vêtements et arts plastiques&#x202F;: au croisement du corps et de la
    matière (Bernard Marcelis, Véronique Bouillez)

## 2010

#### Cours

- Matières à l’œuvre et formes nomades dans l’art contemporain&#x202F;:
    pendre, entasser, plier, nouer, etc. (Dominique Lamy)

- Sous le voile de la fable&#x202F;: démarches narratives dans l’art
    d’aujourd’hui (Delphine Florence)

- Vanité/Vanités dans l’art contemporain (Dominique Lamy)

- Les meilleures choses ont un début&#x202F;: initiation aux prologues
    cinématographiques (Adrien Grimmeau)

- Des gueules noires aux précaires&#x202F;: représentations du travail dans
    l’art (Laurent Courtens)

- Entre l’ombre et la lumière&#x202F;: la couleur dans l’art (Ben Durant),
    Pritzker Architecture Prize 1979-2010&#x202F;: un panorama critique de la
    création architecturale (Pablo Lhoas)

- Artistes contemporains&#x202F;: mode d’emploi&#x202F;! (Olivier Duquenne)

- Le rapport texte/image dans les religions du livre (Stefan
    Goltzberg)

- Graffitis bruxellois&#x202F;: trente ans d’art urbain dans la capitale
    (Adrien Grimmeau)

- Escales urbaines en Amérique Latine&#x202F;: quelques propositions
    artistiques générées par la ville (Anne-Esther Henao)

#### Conférences

- Poésie sonore/poésie numérique ou vers un élargissement du langage
    (Jacques Donguy)

- L’art (public) peut-il (ré) enchanter la ville&#x202F;? (Olivier Bastin)

- La céramique, un art actuel&#x202F;? (Ludovic Recchia)

- L’art et le critère de la responsabilité (Paul Audi)

##### Colloque

- Aborder les bordures&#x202F;: l’art contemporain et la question des
    frontières (Marie-Laure Allain, Patrick Amine, Pascale Ancel, Muriel
    Andrin, Raya Baudinet-Lindberg, Denis Briand, Aline Caillet,
    Marie-Haude Caraës, Laurent Courtens, Thierry Davila, Nathalie
    Delbard, Nelly Dobreva, Jérôme Dupeyrat, Anthony Fiant, Sarah
    Gilsoul, Morad Montazami, Anne Penders, Lydie Rekow-Fond, Christian
    Ruby, Évelyne Toussaint, Tristan Trémeau, Agnès Tricoire)

## 2011

#### Cours

- De la reine Mathilde à Louise Bourgeois&#x202F;: l’art au féminin (Ben
    Durant)

- Musiques et cinéma&#x202F;: dialogues entre sons et images (Gilles Rémy)

- Éloge de la lenteur&#x202F;: cinéma contemporain et narration minimale
    (Olivier Mignon)

- Arts numériques et nouvelles réalités (Vincent Delvaux)

- Célébrations&#x202F;: rites, résurgences, élans festifs dans la création
    contemporaine (Laurent Courtens, Pauline de La Boulaye, Adrien
    Grimmeau, Anne-Esther Henao, Catherine Henkinet)

- Simple/complexe/ludique/sévère/exaltant&#x202F;: art conceptuel, une
    invitation (Laurent Courtens)

- Introduction à l’architecture de 1945 à nos jours (Maurizio Cohen)

- L’architecture en Belgique&#x202F;: de l’art nouveau à nos jours (Maurizio
    Cohen)

- Fairy Tales&#x202F;: approche du merveilleux dans l’art (Olivier Duquenne)

- Affaires publiques&#x202F;: la production de l’espace public bruxellois
    (Rafaella Houlstan-Hasaerts)

- Art et technologie&#x202F;: une approche philosophique avec Gilbert
    Simondon (Ludovic Duhem)

- Un art adulescent&#x202F;: la culture nouvelle de ces éternels enfants
    (Adrien Grimmeau)

#### Conférences


- Le poème par-delà tout art poétique (Jean-Christophe Bailly)

- L’encyclopédie des guerres (Jean-Yves Jouannais)

## 2012

#### Cours

- Pour mémoire&#x202F;: l’art contemporain face à l’histoire (Dominique Lamy)

- La valeur documentaire de la photographie&#x202F;: du journalisme aux
    pratiques artistiques (Claire Labye)

- D’Alice à Alÿs&#x202F;: l’enfance comme paradigme de l’acte artistique
    (Delphine Florence)

- Play It Again Sam&#x202F;! Rejouer l’histoire dans l’art contemporain
    (Florence Cheval)

- Peindre&#x202F;: un état de la question (Laurent Courtens)

- Un B.A.-BA de l’art moderne. Rien ne se perd, rien ne se crée, tout
    se transforme (Dominique Lamy)

- Le sang est aussi une couleur (Ben Durant)

- Chaos-Cosmos&#x202F;: ordre et désordre dans la création (Pierre Sterckx)

- Le court-métrage, face cachée du cinéma (Géraldine Cierzniewski)

- Astro Black Mythology&#x202F;: culture et musiques afro-américaine (Pierre
    Deruisseau)

- Nouveau Cirque, Nouveau Siècle&#x202F;: quarante ans de mutation d’un art
    protéiforme (Pauline de La Boulaye)

- Musique et arts plastiques au XX<sup>e</sup> siècle&#x202F;: affinités et convergences
    (Gilles Rémy)

- Après le primitivisme&#x202F;: comment l’art contemporain s’approprie
    l’anthropologie (Tristan Mertens)

- Le concours en architecture&#x202F;: pratiques et études de cas (Pablo
    Lhoast)

#### Conférences

- Ciné et TV vont en vidéo (avis de tempête) (Jean-Paul Fargier)

- Le musée, une évolution fulgurante (François Mairesse)

- Des histoires, contre l’histoire (Guillaume Désanges)

##### Colloque

- HORS-CADRE/Peinture, couleur, lumière dans l’espace public
    contemporain (Pascale Ancel, Raymond Ballau, Hervé-Armand Béchy,
    Isabelle Bonzom, Marie-Louise Botella, Denis Briand, Michel
    Clerbois, Laurent Courtens, Félix D’Haeseleer, Franck Doriac, Arnaud
    Dubois, Marie Escorne, Adrien Grimmeau, Bénédicte Henderick, Pierre
    Henrion, Laurent Jacob, Ann-Veronica Janssens, Olga Kisseleva,
    Gaëtane Lamarche-Vadel, Emilio López-Menchero, Tristan Mertens,
    Jacqueline Mesmaeker, Caroline Mierop, Norbert Nelles, Gwendoline
    Robin, Bruno Trentini, Éric Van Essche, Christophe Viart, Christophe
    Veys)

## 2013

#### Cours

- Art et féminismes aux États-Unis (Véronique Danneels)

- La révolution des images, de Töpffer à Méliès (Benoît Peeters)

- Susciter l’invisible. Résurgences du mythe dans l’art contemporain
    (Adrien Grimmeau)

- Du beau dans l’art. Pour une approche esthétique du Beau, de la
    Grâce, du Laid et du Sublime (Gilles Rémy)

- Un B.A.-BA de l’art contemporain (Dominique Lamy)

- De la lumière à la couleur&#x202F;: une vision entre sciences et arts
    (Vinciane Lacroix)

- Revenir au masculin (Muriel Andrin)

- L’abject&#x202F;: fascination et révulsion du regard (Ludovic Duheim)

- L’art conceptuel américain&#x202F;: entre mots et images (Anaël Lejeune)

- Rien que la vérité. Le procès dans l’art contemporain (Florence
    Cheval)

- De l’art contemporain à l’art en réseau (Pierre-Yves Desaive)

- Cadrer, encadrer, désencadrer (Ben Durant)

- Art contemporain et globalisation&#x202F;: dans la jungle néolibérale
    (Tristan Mertens)

- Matérialité/immatérialité de la photographie contemporaine (Danielle
    Leenaerts)

- Un goût exquis - essai de pédesthétique (Antoine Pickels), Une
    passion dans le désert. L’animalité dans l’art d’aujourd’hui
    (Delphine Florence)

- Stephan Balleux, peintre (Stephan Balleux)

#### Conférences

- Maurizio Cattelan (Anne-Esther Henao)

- Robert Rauschenberg (Pierre Arese)

- Mark Rothko (Catherine Henkinet)

- Balthus (Delphine Florence)

- Bon goût et ordre moral (Évelyne Pieiller)

- Le corps entre ouverture et résistance (Daniel Blanga-Gubbay)

- L’art contemporain et la frontière (Collectif SIC)

## 2014 

#### Cours

- De la matière au code, du bit à l’atome. Les arts plastiques à la
    lumière du numérique (Adrien Lucca)

- La performance&#x202F;: une expérience cathartique&#x202F;? (Barbara Roland)

- Attention&#x202F;: lieux creux&#x202F;! (Pauline de La Boulaye)

- États nomades. L’art contemporain, territoire cosmopolite (Septembre
    Tiberghien)

- Inside and Outside Actors Studio (Astrid De Munter)

- Au-delà du cadre, la peinture en expansion (Catherine Henkinet)

- Allongés. Des cires anatomiques aux représentations contemporaines
    (Colette Dubois)

- ARCHIVES/déplier l’histoire - revue d’exposition (Laurent
    Courtens)

- Dans la marche. Des artistes se déplacent (Anne-Esther Henao)

- Dédoubler l’image. Faire et défaire un mythe (Michela Sacchetto)

- Alentours (de l’histoire) (Anne Penders)

- More things… pratiques sculpturales contemporaines (Delphine
    Florence)

- Walking on the moon. Arts et sciences, pièges et détournements au
    regard de l’espace (Jacques André)

- Du spirituel dans l’art (Nancy Casielles)

- Pour une approche vivante d’Ars Musica 2014 (Gilles Rémy)

- Le cinéma sans caméra. Origines et actualités du found footage (Yvan
    Flasse)

- Figures en quête d’identité (Dominique Lamy)

#### Conférences

- Bertolt Brecht (Florence Cheval)

- Félix Gonzàles-Torres (Christophe Veys)

- Wang Du (Nancy Casielles)

- Michaël Borremans (Delphine Florence)

- L’Internationale Situationniste (Pauline de La Boulaye)

- Max Ernst (Adrien Grimmeau)

- Maurice Pialat (Yvan Flasse)

- Gerhard Richter (Dominique Lamy)

- John Cage (Alan Speller)

- Thomas Hirschhorn (Tristan Mertens)

- Morton Feldman (Gilles Rémy)

- Rachel Whiteread (Axel Pleeck)

- Le Black Mountain College (Alan Speller)

- Barnett Newman/*Cathedra* (Jean-Philippe Theyskens)

- Édouard Manet (Delphine Florence)

- Un anniversaire. 1. Clair-obscur et noir et blanc. Du Caravage à
    David Claerbout (Pierre Sterckx) / 2. La chorale de Tintin.
    Exploration tintinophile à partir des voix dans l’univers d'Hergé
    (Pierre Sterckx)

- Bonom, Chamane (Adrien Grimmeau)

- Des gestes de la pensée - Echos (Guillaume Désanges)

- Topie impitoyable&#x202F;: les politiques corporelles du vêtement, du mur
    et de la rue (Léopold Lambert)

- Voir et ne pas voir la révolution (Dork Zabunyan)

##### Colloque {: .break-before}

- ARCHIVES/déplier l’histoire (Mathieu Kleyebe Abonnenc, Claire
    Angelini, Agence, Laurent Courtens, Ets. Decoux, Wil Mathijs,
    Vincent Meessen, Anne Penders, RED/Laboratoire Pédagogique, Pieter
    Uyttenhove, Jasper Rigole, Pedro G. Romero, Evelyne Toussaint, Sarah
    Vanagt, Eric Van Essche)

## 2015

#### Cours

- Le rock, tout un art… (Christophe Pirenne)

- Art as Art as Art. L’empire de l’autoréférence (Eric Angelini)

- Modernité fétiche. Le fétiche et l’exposition d’art contemporain
    (Anna Seiderer)

- Art in Amerina. Conditions d’émergence de l’art américain (Alan
    Speller), Conservation. Une histoire de l’art performée
    (Jean-Philippe Convert)

- 9<sup>e</sup> art&#x202F;: la BD dans et hors cases (Jan Baetens et Olivier Deprez)

- Architecture, villes et cinéma (Maurizio Cohen)

- Concevoir et aménager les espaces publics à Bruxelles&#x202F;: description de pratiques différenciées (Benoit Moritz)

- L’art public à l’épreuve de la réalité urbaine&#x202F;: discussions sur les pratiques bruxelloises (Nicolas Hemeleers et Mathieu Berger)

#### Conférences

- Walker Evans (Jean-Marc Bodson)

- La fin des contemplations de René Magritte (Jean-Philippe Theyskens)

- Hans Haacke (Pauline Hatzigeorgiou)

- Land Art, nature et paysage (Yogan Muller)

- Marcel Duchamp (Hans De Wolf)

- Robert Morris (Dominique Lamy)

- Kader Attia (Nancy Casielles)

- James Turrell (Septembre Tiberghien)

- Chris Burden (Anaël Lejeune)

- Gaston Bachelard (Anthony Spiegeler)

- Le Corbusier (Maurizio Cohen)

- La théorie des systèmes et l’art américain (1965-1975) (Pauline
    Hatzigeorgiou)

- John Cassavetes (Barbara Roland)

- Le(s) musée(s) d’art moderne de Marcel Broodthaers (Yves
    Depelsenaire)

- Pascale Martine Tayou (Nancy Casielles)

- Ettore Sottsass & Memphis, repenser l’objet (Pierre Bouchat)

- Donald Judd architecte (Pierre Arese)

- Pierre Bourdieu et les règles de l’art (Benoit Dusart)

- Harald Szeemann (Maïté Vissault)

- Giuseppe Penone (Dominique Lamy)

- Le Pop Art en Belgique (Ben Durant)

- Chris Marker (Tristan Mertens)

- Chauvet à l’époque de sa reproductibilité publique (Delphine
    Florence)

- Adolf Wölfli (Vanessa Noizet)

- Kasimir Malevitch (Wendy Toussaint)

- Doris Salcedo (Anne-Esther Henao), Pablo Picasso, jeune peintre
    (Jean-Philippe Theyskens)

- Pratiques artistiques et démocratie agonistique (Chantal Mouffe)

- La part de l’œil (Emmanuel Lambion et Agnès Geoffray)

- L’espace entre les images&#x202F;: la douleur des autres dans l’art
    contemporain (Caroline Recher)

- Qu’avons-nous fait du paysage (Yogan Muller)

## 2016

#### Cours

- Les abstractions en Belgique. Éveils, déclins et renaissances
    (Anthony Spiegeler)

- Aragon au fil des arts et des textes (Delphine Florence)

- Le goût des autres. Sens et usages de la culture à travers
    l’héritage de Pierre Bourdieu (Benoit Dusart, Laurie Hankinet,
    Jean-Louis Fabiani)

#### Conférences

- Focus/Le temps des biennales (Maïté Vissault)

- Croiser les arts, un horizon du siècle (Pierre Arese)

- Oriol Vilanova (Christophe Veys)

- Marcel Proust (Gilles Rémy), Eduardo Kac (Jacques André)

- La jeune scène espagnole (Christophe Veys)

- B. M. P. T. (Marine Lagasse)

- Johan Muyle (Nancy Casielles)

- Karen Knorr (Danielle Leenaerts)

- On Kawara (Christophe Veys)

- Piero Manzoni (Immacolata Tralli)

- Kiki Smith (Dominique Lamy)

- Susan Sontag (Laurent Courtens)

- Sol LeWitt (Pierre Arese)

- L’œuvre-fantôme / Untitled Ceremony \#08 (Béatrice Gross)

- La passivité&#x202F;: un concept révisé et augmenté ? (Vanessa Desclaux)

- Q comme Question. L’Abécédaire de Gilles Deleuze (1988) (Clémence
    Mercier)

- Ce qu’exposer veut dire / ce que parler peut faire (Guillaume
    Désanges)

- Aesthetics and Systems Theory (Luke Skrebowski)

- L’esthétique des systèmes de Jack Burnham (1968) (Pauline
    Hatzigeorgiou)

## 2017

#### Cours

- RAW/WAR. Comment la guerre hante les images (Laurent Courtens)

- Une brève histoire des collectifs d’artistes (Émilie Berger, Alice
    Horlait, Sébastien Bizet, Mathilde Sauzet Mattei)

- Les formes de collaborations entre artistes et scientifiques
    (Nathalie Stefanov)

- La mise en scène cinématographique (Olivier Lecomte)

- Faire œuvre, créer du commun&#x202F;: d’un art qui mobilise (Delphine
    Florence)

- Par-delà les formes&#x202F;: agir, prendre art (Catherine Mayeur, Sonia
    Dermience)

## 2018

#### Cours

- Nouvelles perspectives sur la photographie africaine contemporaine
    (Estelle Lecaille)

#### Conférences

- Le Bauhaus en jeux (Pierre Arese)

- Cobra & l’enfance de l’art. De la spontanéité à l’enseignement
    (Anthony Spiegeler)

- Le Black Mountain College (Alan Speller)

- Mike Kelley (Delphine Florence)

- Yto Barrada (Laurent Courtens)

- Fernand Deligny (Sandra Alvarez de Toledo, Marlon Miguel)

- Matsuzawa Yutaka (Olivier Mignon)

- Roman Ondak. L’art de mesurer l’univers (Christophe Veys)

- Jim Shaw ou l’Amérique mythomane (Delphine Florence)

- William Kentridge. Le monde comme processus (Tania Nasielski)

- Sonore, visuel, poétique & vivant&#x202F;: Christian Marclay en quatre
    épithètes (Axel Pleeck)

- Le groupe E.G.A.U. et les artistes. Une histoire liégeoise (Maurizio
    Cohen)

- Vodun (Theodor Atropko)

- Zéro de conduite (Olivier Lecomte)

- La forêt nouvelle (Paul Sztulman)

- Staying Wild (Yogan Muller)

- Femmes et forêts, noces vertes (Caroline Goldblum)

- L’écosystème forestier, mode d’emploi (Caroline Vincke)

- Hortus Minor (Isabelle Dumont)

- La forêt est un jardin (Marco Martella)

- Forme, art et environnement (Nathalie Blanc)

- À l’ombre des grands arbres (Delphine Florence)

- Être forêts (Jean-Baptiste Vidalou)

##### Colloque

- COM∩∪TIES. Seuils/Drempels/Thresholds (Yves Bernard, Elise v.
    Bernstorff, Lieven Boelaert, Eric Corijn, Afra Dekie, Pierre-Yves
    Desaive, Gwenola Drillet, Benoît Dusart, Delphine Forestier, Carl
    Fraser, Françoise Gallez, Azzedine Hajji, Iris Lafon, jan Locus,
    Mabiala Mbeka, Antoine Pickels, Rolf Quaqhebeur, Eric Russon,
    Madeleine Sallustio, Pascal Smet, Dirk Snauwaert, Ive Stevenheydens,
    Jeffrey Tallane, Fanny Tang, Abdelfattah Touzri, Jacques Urbanska,
    Christian Vandermotten, Mezli Vega Osorno, Maïté Vissault, Estelle
    Zhong)

## 2019

#### Cours

- Une histoire de l’art contemporain. 1<sup>ère</sup> partie 1955-1989 (Delphine
    Florence)

- Une histoire de l’art contemporain. 2<sup>e</sup> partie 1989-2001 (Delphine
    Florence)

- L’esthétique est-elle une politique du possible&#x202F;? Penser à partir de
    Sartre et Giacometti (Clémence Mercier)

- Est-ce bien un public&#x202F;? Formes participatives dans l’espace public
    (Antoine Pickels)

- L’inaudible en peinture (Rosanna Gangemi)

- L’affaire Brancusi (Benoît Dusart)

- A Matter of Life and Death. Voyage photographique (Paul Willemsen)

- Histoire culturelle et politique du jeu vidéo (Julien Annart)

- La fiction du JE, chronique d’une mort annoncée (Marion Zilio)

- La nostalgie de Babel, ou le rêve d’une langue universelle
    (Jean-Philippe Convert)

#### Conférences

- Hito Steyerl. The Economy of Poor Images (Iris Lafon)

- Culture numérique et actions politiques. Les artistes hacktivistes
    (Pierre-Yves Desaive)

- Anthony Gormley. Subjectivité collective (Tarquin Sinan)

- Sanja Iveković. Une œuvre engagée (Anaëlle Prêtre)

- L’art baroque de David Lynch. Entre immersion et métathéâtralité
    (Karel Vanhaesebrouck)

- Tilda Swinton. Performances (Muriel Andrin)

- Jeff Koons. Pour en finir avec la critique (Laurent de Sutter)

- André Robillard. Un artiste au monde (Christophe Boulanger, Savine
    Faupin)

- Thomas Hirschhorn. Musée précaire Albinet (Guillaume Désanges)

- Jef Geys (Philip Van den Bossche)

- Wim Delvoye (Pierre-Yves Desaive)

- Richard Long. In effort we trust (Tarquin Sinan)

- Les jeux d’arcade. Quand un modèle industriel et économique
    représente le réel (Julien Annart)

- Le Bauhaus au féminin (Véronique Bouillez)

- Giovanni Anselmo. Mano che indica… (Laurent Courtens)

- Des simulateurs de meurtres, les jeux vidéo ? Pourquoi éduquer aux
    jeux vidéo (Daniel Bonvoisin, Martin Culot)

- Saint Pac-Mac, priez pour nous. Religiosité et spiritualité dans les
    jeux vidéo en ligne (Olivier Servais)

- Je joue donc je suis ? Rôles et sens de l’avatar dans le jeu vidéo
    (Julie Delbouille)

- Des Machinimas au speedrun. Les détournements de jeu vidéo par les
    joueurs (Fanny Barnabé)

- Alec De Busschère (Joël Benzakin)

- Vie et mort du visage (Laurent Courtens)

- Brusselsspeeches (Bruxelles nous appartient / Brussel behoort ons
    toe)

- Snobs ou populaires, moqués ou admirés, appris ou oubliés… d’où
    viennent les accents du français ? (Maria Candea)

- Les langues, une espèce en voie de disparition ? (Hugues Sheeren)

## 2020

#### Cours

- Persistance de l’éphémère. Regards croisés sur les arts
    contemporains (Gilles Rémy)

- Helio Oiticica. Modernités et contre-cultures brésiliennes (Tristan
    Trémeau)

- Faire image malgré tout. Comment cinéma d’animation, images de
    synthèse et dispositifs de mise en scène habitent le documentaire
    (Yvan Flasse)

#### Conférences

- Vivir ! Eduardo Arroyo ou la peinture à se rompre (Delphine
    Florence)

- La question coloniale au Pavillon belge de Venise. Les expositions
    de Luc Tuymans et de Vincent Meesen (Alisson Bisschop)

- Ai Weiwei. L’engagement (réseau) social (Pierre-Yves Desaive)

- Femmes surréalistes. Au-delà des muses (Lyse Vancampenhoudt)

- L’art de la lenteur (Rosanna Gangemi)

- S’attarder, fluer, suspendre. Quelques expériences spirituelles dans
    les arts contemporains (Philippe Filliot)

De nombreux événements ont dû être annulés ou reportés durant le
confinement dû à la crise sanitaire du coronavirus.
