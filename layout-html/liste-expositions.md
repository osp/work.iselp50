<div id="my-toc-content" style="display:none;"></div>


Liste des expositions
{: .cartouche-titre}

Cette liste reprend les expositions organisées à l’ISELP ou hors les
murs dont nous avons retrouvé la trace, à l’exception de celles ayant pris place dans les espaces de
vente (Galerie-Découverte, Rayon Art/Objets, La boutique).
{: .cartouche-role}



1974
====

*Art et ordinateur* (Forest National)
----------------------------------

Klaus Basset, Pierre Cordier, Hein Gravenhorst, Karl Holzaeuser,
Gottfried Jaeger, Auro Lecci, Herbert W. Franke, Manfred Mohr, Frieder
Nake, Georg Nees, Karl Siebig, Eth Studenten, Peter Struyken, Alexander
Vitkine, Demetrios Yannakopoulos, Edward Zajec

1975
====

*Patrimoine bruxellois : reportages*
----------------------------------

*Art en série provenant d’intérieurs ouvriers du pays de la Ruhr*
---------------------------------------------------------------

1976
====

*Humour, caricature, miroir du monde*
---------------------------------

1977
====

*Hans Richter&#x202F;: Peinture, photo, film*
--------------------------------

1978
====

*Héros-Tics*
----------

Philippe Druillet, Moebius, Jacques Tardi, Hugo Pratt, Gir, F’Murr, Jean-Claude Forest, Franquin,
Gottlieb, Hergé, Macherot, Maurice Tillieux, William Vance

*Milieu rural*
------------

Pierre Courtois, Jean Dechene, Jacques Etienne, Guy Horenbach, Pierre
Hubert, Bernadette Lambrecht, Jacques Lennep, Bernard Lorge, Michel
Mineur, Michel Moffarts et Georges Schurgers, Jean-Marc Navez, Jacques
Louis Nyst, Pierre Pasteels, Jean-Pierre Point, Jean-Pierre Ransonnet,
Juliette Roussef, Serge Vandercam, Max Van Der Linden, Georges
Vercheval, Guy Wery

*Les Livres d’enfants ne sont plus ce qu’ils étaient*
---------------------------------------------------

*La Couleur et la ville* (Espaces publics bruxellois)
---------------------------------------------------

Philippe De Gobert, Jean Glibert, Jano, André Mees, Jacqueline
Mesmaeker, « TOUT » (Paul Gonze), Serge Vandercam, collectif 50/04,
Maurice Maelderlick, Alain Pierre


### Espace Éphémère

*Bernard Descamps*
----------------

*Jean-Luc Tanghe*
---------------

1979
====

*Dieter Magnus. L’art de l’environnement contre un environnement artificiel*
--------------------------------------------------------------------------

*La Ville dans la ville*
----------------------

*Les Bâtisseurs de l’imaginaire*
------------------------------

Documents collectionnés par Claude et Clovis Prévost : Raymond Isidore
(Picassiette), Irial Vets, Camille Vidal, Monsieur G., etc.

*1000 Bruxellois et l’art*
------------------------

### Espace Éphémère

*Bernard Plossu*
--------------

*Evelyne Gondry*
--------------

*John Vinck*
----------

*Bernard Gomez*
-------------

*Christian De Freyman*
--------------------

*Maarten Van Noort*
-----------------

1980
====

*La Photographie et la peinture aujourd’hui*
------------------------------------------

d'Alessandro, Monique Alluin, Andre, Astman, Ben, Fred Forest, Hannah Collins, Alexandre Delay, Philippe Dubit, Erró, Umberto Mariani, Jochen Gerz, Jean Le Gac, Jacques Lennep, Jacques Lizène, Annette Messager, Rainer, Urban, Untel, Claude Viallat et Andy Warhol

*Soldes de fins de séries*
------------------------

### Espace Éphémère

*Francis Coutellier*
------------------

*Alain Fleig*
-----------

*Claude Nori*
-----------

*Patrice Reumont*
---------------

*Phabian*
-------

1981
====

*Exposition internationale de tampons d’artistes*
-----------------------------------------------

Ben, Jacques Lennep, Metallic Avau, Luc Van Malderen, etc.

*L’Action de la couleur*
----------------------

*Groupe « Couleur » de l’Association Fibre et Fil*
--------------------------------------------------------------------------

*Conservation du patrimoine monumental en République démocratique allemande*
--------------------------------------------------------------------------

### Espace Éphémère

*Denis Roche*
-----------

*Pascal Vanderstraeten*
---------------------

*Caroline Ghyselen*
-----------------

1982
====

*Masque et autres masques*
------------------------

### Espace Éphémère

*Alain Van Hille*
---------------

*Étienne Revault*
---------------

1983
====

*Pochettes de disques, enluminures du XX<sup>e</sup> siècle*
-----------------------------------------------

*Méliès l’enchanteur*
-------------------

### Espace Éphémère

*Myriam Kervyn. Coin d’œil*
-------------------

*Jacques Richez*
-------------------

1984
====

*Doublures de Paris*
------------------


*Néon, Fluor et Cie*
------------------

**ISELP** : Yanis Bouteas, Toni Burgering, Varda Chryssa, Jean-François De
Witte, Dan Flavin, Joseph Kosuth, Piotr Kowalski, Mario Merz, François
Morellet, Pierre Rensonnet, Sarkis, Keith Sonnier, Patrick Steinfort,
Bruno Stevens, Jozef van Der Horst, Liliane Vertessen, Bernard Villers. **Parc d’Egmont** : Albert et Guido, Alain Buscarlet, Leo Coppers, Philippe Decelle, Fernand Flausch, Pablo
Gasparotto, Henri Lambert, Antoine Laval, Streamline, Danny Matthys,
Metallic Avau, Navez et Rolet, TOUT (Paul Gonze), Frank van Herck.
Designers et enseignes : Rosalie Pompon, Studio 51, Streamline, Mason

### Espace Éphémère

*Dessins d'architecte. Jean-François Octave*
-------------------

*José Roland*
-------------------


1985
====

*Travaux en cours*
----------------

Étudiant·e·s des écoles d’art bruxelloises

*Le Bombeur fou*
--------------

### Espace Éphémère

*Félix Roulin*
-------------------


1986 {: .quatre-vingt-six}
====

*Premier Festival international du dessin politique*
--------------------------------------------------

1988  
====

*Pages d’artistes hors mesure*
----------------------------

Regroupe les œuvres de plus de 100 artistes.

*Graveurs d’aujourd’hui*  {: .quatre-vingt-huit}
----------------------

1990 {: .nonante}
====

*La Roumanie salue l’Europe*
--------------------------

1991 {: .nonante-un}
====

*Travaux d’architectes paysagistes danois contemporains*
----------------------------

1992-1998 {: .intro}
====

Les expositions sont suspendues en raison des travaux de rénovation et d’agrandissement des locaux occupés par l’ISELP.


1999
====

*Liberté, libertés chéries ou l’art comme résistance… à l’art : un regard posé sur dix années d’acquisitions de la Communauté française de Belgique (1989-1998)*
----------------------------------------------------------------------------------------------------------------------------------------------------------------

Regroupe les œuvres de plus de 120 artistes.

*Côté parc, côté boulevard, l’ISELP un chemin pour les arts*
----------------------------------------------------------

(Documents produits par l’ISELP au fil des années)

*Le réservoir d’idées*
--------------------

Elisabeth Barmarin, Benoît Van Innis, Guillaume Bijl, Yves Bosquet,
Thérèse Chotteau, Christian Claus, Florence Claus, Pierre Culot,
Berlinde De Bruyckère, Paul De Gobert, Bert De Keyser, Marc De Roover,
Dennis De Rudder, Camille De Taeye, Jephan De Villiers, Philippe
Decelle, Wim Delvoye, Émile Desmedt, Jean-François Diord, Francis
Dusépuchre, Francis Feidler, Richard Flament, Jean-Pierre Ghysels,
Rainer Gross, Marie-Paule Haar, Boubeker Hamsi, Pal Horvath, Nathalie
Joiris, Dani Karavan, Marin Kasimir, Per Kirkeby, Joseph Kosuth,
Jean-Paul Laenen, Charlotte Marchal, Jacques Moeschal, Juan Paparella,
Luca Maria Patella, Recyclart ASBL, Aldo Rossi, Félix Roulin, Ulrich
Rückriem, Françoise Schein, Luc Schuiten, Roger Somville, Vincent
Stretbell, Koen Theys, Francis Tondeur, TOUT asbl, Bob Van der Auwera,
Philip Van Isacker, Bob Van Reeth, Luk Van Soom, Charles Vandenhove,
Dimitri Vangrunderbeek, Jan Vanriet, Gérard Wibin, Pascaline Wollast

2000
====

*FIND. Design finlandais*
-----------------------

*Évelyne Axell. Mémoire de Bacchante*
-----------------------------------


*Pierre Culot. Jardins-Sculptures. Sculptures-Jardins*
----------------------------------------------------

*Messagerie de l’art contemporain. Le train de l’art contemporain roule en francophonie. Deuxième étape*
----------------------------------------------------------------------------------------------------

Hamsi Boubeker, Djamel Merbah, Romuald Hazoumé, Johan Muyle, André
Lambotte, Fernando Alvim, Hamed Ouattara, Beybson, Hervé Youmbi, Michèle
Waquant, Françoise Sullivan, Louise Masson, Chéri Samba, Chéri Chérin,
Aimé Mpane, Berse Grandsinge, Aboudramane, Hervé Di Rosa, Xavier
Veilhan, David Nal-Vad, Gabriel Bien-Aimé, Kis’Keya, Pascale Ayoub,
Debbie Fleming Caffery, Jean de la Fontaine, Abou Diallo, Rachid
Benissa, Abdi Errami, Malam Zabeïrou, Ibrahima Kébé, Serigne Mbaye
Camara, Bernard Voïta, Do Mesrine, Ei Loko, Samira Lourini, Mohamed ben
Hamadi, Van Hunh, Nguyen Cam


*En quête d’étoiles* (La Monnaie, Bruxelles)
------------------

Étudiants en graphisme de l’erg : Raquel Alves, Ingrid Brasseur, Céline Deprez, Virginie Feltz, Thomas Gilson, Laurent Habran, Alexandre Marly, David Otero Y Alonso, Esther Rappé, Vera Scholtes, Johan Serck, Nam-Mathieu Simonis, Caroline Toutounji

2001
====

*Avoir lieu : Bob Verschueren*
----------------------------

*Avoir lieu : Johan Muyle*
------------------------

*Avoir lieu : Philippe Decelle*
-----------------------------

*Le Clonage d’Adam*
------------------

*La Galerie ’Les années 50’ expose du mobilier de Jules Wabbes*
-------------------------------------------------------------

*Leica, Magic Moments II (Leica, my point of view)*
-------------------------------------------------

2002
====

*Être au fond : Aurore d’Utopie*, Paul Gonze
-------------------------------------------

*Être au fond : Michel Mouffe s’empare de l’ISELP*
------------------------------------------------

*Être au fond : Christian Carez. Le jour se lève*
----------------------------------------------

*Netd@ys Protocol*, Alexis Destoop
----------------------------------

*Visions de 4 photographes belges (Leica, my point of view)*
----------------------------------------------------------

Pascale Delfosse, Frans Pyck, Herman Selleslaghs, Gaël Turine


2003
====

*Guillaume Liffran. Electric*
---------------------------

*Du diaphane et de l’illusion : une pluralité d’apparences*
-----------------------------------------------------------

Catherine Amathéü, Lucile Bertrand, Marie-Ange Cambruzzi, Marie
Delfosca, Laurence Dervaux, Bénédicte Henderick, Aïda Kazarian, Niki
Kokkinos, Sylvie Pichrist, Marianne Ponlot, Aude Renault, Isabelle
Rousseau, Léopoldine Roux, Ela Stasiuk, Reiko Takizawa

*Une sélection de sièges américains des années 50*
------------------------------------------------

*Apparitions(s). Alexandra Dementieva et Bertrand Gadenne*
--------------------------------------------------------

*Magic Hands. Elliott Erwitt (Leica, my point of view)*
-----------------------------------------------------

2004
====

*Mass Moving : un aspect de l’art contemporain en Belgique 1969-1976*
-------------------------------------------------------------------

*Demain j’irai mieux. Dessins et textes d’enfants de l’unité de cancer de l’hôpital des enfants de Bruxelles*
------------------------------------------------------------------------------------------------------------

*Le dessus des cartes : art et cartographie*
------------------------------------------

Gianni Caravaggio, Jocelyne Coster, Wim Delvoye, Eric Derac, Roger
Dewint, Peter Fend, Christophe Fink, Nicole Haurez, Changha Hwang,
Marie-Christine Katz, William Kentridge, Moshekwa Langa, Capucine Levie,
Mateo Maté, Michel Mineur, David Renaud, Françoise Schein, Panagiotis
Siatidis, Thierry Tillier, Jessica Vaturi, Angel Vergara

*Philippe Cardoen. Gises*
-----------------------

*Le Bijou contemporain en Communauté française*
---------------------------------------------

*Marcel Veelo. Magic Visions (Leica, my point of view)*
-----------------------------------------------------

2005
====

*De vous à moi, vous m’…*, Michel Clerbois
-------------------------------------------

*FREMOK, vie et mort de Fréon triomphante*
----------------------------------------

*Mes tendres années*. Christian Rolet
------------------------------------

*24x36 — 50 ans de photographie Leica (Leica, my point of view)*
---------------------------------------------------------------

2006
====

*Alain Bornain : Prorata temporis…*
------------------------------------

*Picto(s)*
--------

Jofroi Amaral, Lisa Brice, Pierre Charpin, Jérôme Considérant, Grégory
Decock, Edith Dekyndt, Messieurs Delmotte, Roland Denaeyer, Aurélien
Dendoncker, Daniël Dewaele, Nicolas Grimaud, Christoph Hildebrand, Steve
Jacobs, Ryan McGinness, Jean-Yves Magnay, Lucie Malou, Cécile Massart,
Matt Miullican, Jean-François Octave, Narcisse Tordoir

*Véronique Ellena. Les grands moments de la vie*
----------------------------------------------

*Artistes pour Amnesty*
---------------------

*Laetitia B : autopsie*, Bénédicte Henderick
--------------------------------------------

*Martin Santander. Magic Moments. Iceland (Leica, my point of view)*
------------------------------------------------------------------

2007
====

*Hommage à Marthe Wéry*
---------------------

*Paysages / visions paradoxales*
------------------------------

Robert Arnold, Isabelle Arthuis, Pablo Avendaño, Philibert Delécluse,
Laetitia Delafontaine-Grégory Niel, Felten-Massinger, Laurent Friob,
Nathalie Garot, Beate Gütschow, Laura Henno, Steven Houins,
Claire-Jeanne Jézéquel, Mark Lewis, Hans Op de Beek, Qubo Gas, Robert
Quint, Philippe Ramette, Sébastien Reuzé, Bernard Tullen

*Didier Mahieu. Eve(s) Phase I : le complot*
------------------------------------------

*Magic Nature — German National Parks (Leica, my point of view)*
---------------------------------------------------------------

2008
====

*Bernard Gaube. 26, rue de la Comtesse de Flandre*
------------------------------------------------

*De Narcisse à Alice : miroirs et reflets en question*
----------------------------------------------------

Carla Arocha, Stéphane Schraenen, Patty Chang, Nicola Evangelisti,
Alain Fleischer, Pablo Garcia Rubio, Emmanuel Dundic, Dan Graham, Thomas
Huygue, Yves Lecomte, Marie-France et Patricia Martin, Giulio Paolini,
Michelangelo Pistoletto, Ela Stasiuk

*White Noise. Carte blanche à la céramiste Caroline Andrin*
---------------------------------------------------------

Philippe Barde, Ivan Citelli, Éric Chevalier, Ann Delaite, Guillaume
Delvigne, Patricia Glave, Sophie Hanagarth, Rumi Kobayashi, Lucile
Soufflet, Clémence Van Lunen, Ionna Vautrin

*Claude Lévêque. Hymne*
---------------------

*Africa Report. Photographies de Cédric Gerbehaye et Gaël Turine (Leica, my point of view)*
-----------------------------------------------------------------------------------------

2009
====

*Sylvie Macías Díaz : Buildings Extension*
----------------------------------------

*Dress Code : ces vêtements qui nous collent à la peau*
-----------------------------------------------------

Elodie Antoine, By-Product, Géraldine Chevenier, Carina Diepens, Lucie
Duval, Charles Freger, Samuel François, Margherita Isola, Miss Roberta,
Julien Meert, Jonathan Poliart, Lycy+Jorge Orta, Christèle Simonard,
Marie Van Roey, Nicole Tran Ba Vang, Dominique Vandenbergh, Liana
Zanfrisco

*Flesh*
----------------------------------------------------------
Dany Danino, Hughes Dubuisson, Sofi Van Saltbommel

*Mishmash ou la confusion. Photographies de Christian Carez*
----------------------------------------------------------

2010
====

*En quelques traits. Un cabinet de dessins*
-----------------------------------------

Sofia Boubolis, Charley Case, Pauline Cornu, Amélie De Beaufort,
Virginie de Limbourg, Petrus De Man, Philippe Dubit, Ephameron, Benoît
Félix, Vincent Fortemps, Annabelle Guetatra, Félix Hannaert, Hell’o
Monsters, Charlotte Marchand, Michaël Matthys, Jean-Luc Moerman, Tristan
Robin, Guðný Rósa Ingimarsdóttir, Boris Thiebaut, Tinus Vermeersch

*Exonymie*
-----------------------

Éric Van Hove, avec la collaboration de Mohamed Bourouissa, Véronique Caye, Antoine de Vilmorin, Reem Fadda,
Thomas Gevaert, Emmanuel Lambion, Jean-Christophe Lanquetin, Antonia
Majaca, Nástio Mosquito, Pathy Tshindele, Nina Støttrup Larsen, Jérémy
Tomczak, Maarten Vanden Eynde, etc.

*MEDIUM. Photographies de Vincen Beeckman*
----------------------------------------

*Sonopoetics. De la parole à l’image, de la poésie au son*
-------------------------------------------------------

Henri Chopin, John Giorno, Brion Gysin, Bernard Heidsieck, Maurice
Lemaître, Roland Sabatier, Tris Vonna-Mitchell, Gil Joseph Wolman

*Singuliers / pluriels. Huit artistes s’installent. Carte blanche à Marie Chantelot*
----------------------------------------------------------------------------------

Marie Chantelot, Laurence Deweer, Nathalie Doyen, Claire Kirkpatrick,
Akashi Murakami, Sophie Ronse, Anima Roos, Lydia Wauters

2011
====

*Ceci n’est pas Beyrouth. Photographies de Fouad Elkoury*
-------------------------------------------------------

*Laurence Dervaux*
----------------

*Célébration(s)*
--------------

**Galerie** : Béatrice Bailet, Tessy Bauer, Anne Brégeaut, Lisa
Carletta, Dialogist-Kantor, Jeremy Deller, Filip Gilissen, Charlemagne
Palestine, Stéphane Thidet. **Atelier** : Samuel Coisne, Xavier Mary,
Jonathan De Winter, Fabien Giraud, Jérôme Porsperger. **Studio** : Hans Op
de Beek. **Passage de Milan** : Isabelle Bonté. **Parc d’Egmont** : Olivier
Bovy, Marion Fabien, Christophe Terlinden, Bert Theis

*Duos d’artistes : un échange* (Musée jurassien des arts, Moutier, Suisse)
----------------

Léonard Félix, Charlotte Beaudry, Elodie Antoine, Gabrielle Voisard

*Flesh II* (Musée Ianchelevici, La Louvière) 
----------------

Dany Danino, Hugues Dubuisson, Jacques Dujardin, Karine
Jollet, Claude Panier, Sofi van Saltbommel


### Concours Hors d’œuvre 

*Déviation*, Frédéric Penelle
----------------

2012
====

*Duos d’artistes : un échange*
----------------------------

Lucile Bertrand, Mireille Henry, Emilio López-Menchero,
Charles-François Duplain

*McLuhan 101* (dans le contexte de Transnumériques)
-----------------------------------------------

*Armes blanches pour nuits noires. De la lame à l’incise de la chair*
-------------------------------------------------------------------

Marina Abramovic, Carlos Aires, Éric Angenot, Tatiana et Pierre-Yves
Bohm, Giuseppina Caci, Charley Case, Mariel Clayton, Leo Copers, Eduard
Hermans, Sigalit Landau, Selçuk Mutlu, Erwin Olaf, Gina Pane, Marc
Riboud, Rémy Russotto, Jan Saudek, Andres Serrano, Joël Peter Witkin

*L’Oreille interne*, Julien Sirjacq
----------------------------------

*Et le monde est plongé dans la pénombre*, Lise Duclaux
-------------------------------------------------------

### Concours Hors d’œuvre

*Graf&shy;fitecture*, Obêtre
-------------------------------------------------------

### Atelier

*La Noë*, Sébastien Reuzé
-------------------------------------------------------

*To Come*, Fabrice Samyn
-------------------------------------------------------


### Parc d’Egmont

*La Vie errante — la vie immobile — la prison*, Tatiana Bohm
-------------------------------------------------------

*En Suspension*, Julien Khanh Vong
-------------------------------------------------------


2013
====

*Dés-orienté(s)*
--------------

3 DRAW — João Freitas et Lea Belooussovitch, Julien Bruneau
et l’équipe des Phréatiques, Henk Delabie, Clorinde Durand, Dorian
Gaudin, Barbara Geraci, Elise Leboutte, Markus Schinwald, Emmanuel
Vantillard, Jérôme Thomas, t.r.a.n.s.i.t.s.c.a.p.e (Emmanuelle Vincent,
Pierre Larauza), Peter Welz. Étudiants de l’ISAC, ArBA-EsA : Benoît
Armange, Eglantine Bironneau, Mohamed Boujarra, Brune Campos, Thomas
Dessein, Carole Jimenez, Nasrine Kheltent, Luisa Prias, Francesca
Saraullo, Theodosia Stathi, Carolina Van Eps

*Stephan Balleux. Mème*
---------------------

*Corps Commun* (Anciens Abattoirs, Mons)
-------------------------------------------------------

Collectifs : Cuesmes 68, Carré d’Art et Maka. Artistes : Akim, Jeroen
Jongeleen, Obêtre, Mathieu Tremblin.


*Célébration(s). Renouveau du festif dans la création contemporaine à Bruxelles* (Centre Wallonie-Bruxelles, Paris)
--------------------------------------------------------------------------------------------------------------

Tessy Bauer, Lisa Carletta, Samuel Coisne, Dialogist-Kantor, DSCTHK,
Filip Gilissen, André Goldberg, Hell’O Monsters, Lucie Lanzini, Hans op
de Beeck, Charlemagne Palestine, Charles Paulicevich

### Concours Hors d’œuvre

*Le vaisselier*, Sofi Van Saltbommel
-------------------------------------------------------

### Atelier

*Le regard exercé/50 ans de recherches photographiques à l’INSAS*
-------------------------------------------------------

*Die Welt ist schön* 
-------------------------------------------------------

Elèves de 7<sup>e</sup> technique de l’Institut Don Bosco : Ismaïl Avci, Erwin
Blaszczuk, Lionel Carette, Kevin Collinet, Sébastien Ergo, Sébastien
Rodriguez Escaleira, Sébastien Firre, Morteza Khavand Ghassroldashti,
Valentin Hanssens, Kevin Lapage, Mario Marasco, Guillaume Petit, Antoine
Sens, Kevin Vandenbroeck


*O Superman*, Emmanuel Van der Auwera
-------------------------------------------------------


*Correspondances*, Aurore Dal Mas & Sandrine Morgante
-------------------------------------------------------

### Parc d’Egmont

*Réseau*, Gaëlle Caplet
-------------------------------------------------------


*Inside/OUT*, Vincent Chenut et Alberto Ginepro
-------------------------------------------------------


2014
====

*Bonom, le singe boiteux*, Vincent Glowinski, Ian Dykmans
--------------------------------------------------------

*ARCHIVES/déplier l’histoire*
----------------------------

**ISELP** : Mathieu Kleyebe Abonnenc, Wil Mathijs, Etablissements Decoux,
Anne Penders, Gintaras Didziapetris, Deimantas Narkevicius, Jasper
Rigole, José Alejandro Restrepo, Stefanos Tsivopoulos, Pedro G. Romero.
**L’Escaut architectures** : Claire Angelini

*Christophe Terlinden. No sugar*
------------------------------

### Concours Hors d’œuvre

*No to Contemporary Art*, Patrick Guns
-------------------------------------------------------


### Atelier

*Transformateur de Lumière/prototype 4.0*, Adrien Lucca
-------------------------------------------------------

*de Chine____________ ici/ou à côté*, Anne Penders
-------------------------------------------------------

*Un joli nom d’oiseau*, Priscilla Beccarri
-------------------------------------------------------

*L’Atelier*, Vincent Chenut
-------------------------------------------------------


2015
====

*Hostipitalité*
-------------

Effi & Amir, Elise Florenty & Marcel Türkowsky, Olive Martin & Patrick
Bernier, Éleonore Saintagnan & Grégoire Motte

*BEING URBAN. Laboratoire pour l’art dans la ville*
-------------------------------------------------

The Mental Masonry Lab, Jérôme Giller, Denicolai & Provoost, Julien
Celdran, Thomas Laureyssens, Stephan Goldrajch, Zuloark, Robert
Milin, Xurxo Durán Sineiro, Sans-Titres, Outings project, Recyclart, Benoît Moritz, Mathieu Berger, Carine Potvin, Nicolas Hemeleers,
Laurence Jenard, Les Nouveaux Commanditaires, Olivier Bastin, Tiffany
Hernalesteen, Antoine Pickels, IDM, Wim Embrechts, Contrats de
quartiers durables, Vincent Degrune, artconnexion, vous


*Failure Falling Figure*, Agnès Geoffray
-----------------------------------------


*Transitions*, Mira Sanders (Eté 78, Bruxelles)
-------------------------------------------------------



### Concours Hors d’œuvre

*Others May Follow*, Amélie Bouvier
-------------------------------------------------------

### Atelier

*Candidat*, Manon Bara
-------------------------------------------------------


*Street / Book Art*, Charley Case, Dany Danino
-------------------------------------------------------


*(Ponos/Ergon)graphie*, Loïc Desroeux
-------------------------------------------------------


*Les Disparues*
-------------------------------------------------------

Aleksandra Chaushova, Judith Duchêne, Teodora Cosman, Clara Sobrino


*Hraun*, Yogan Muller
-------------------------------------------------------


*Belle comme une image*, Émilie Danchin
----------------------------------------

*Conversations \#1*, Olivier Dekegel, Emmanuel Van Der Auwera
-------------------------------------------------------


2016
====

*L’Image qui vient*
-----------------

Marcel Broodthaers, Marco De Sanctis, Sophie Langohr, Yves Lecomte,
Chantal Maes, Pauline M’barek, Léa Mayer, Cédric Noël, Oriol Vilanova

*Béatrice Balcou/Kazuko Miyamoto*
-------------------------------

\#INSTITUT
----------

**Part 1 : Table of content** : Agence, Stefan Brüggeman, Helga Dejaegher, Mario Garcia Torres,
Cristina Garrido, Joséphine Kaeppelin, Joseph Kosuth, Léa Mayer, Sarah
van Lamsweerde, Freek Wambacq

**Part 2 : De l’assemblée à l’imprimante** : Yoann Van Parys

**Part 3 : ERG** : Catharina Van Eetvelde


*Double room*, Richard Venlet
-------------------------------

**Épisode 1** : Pieter Vermeersch - Denicolai & Provoost


**Épisode 2** : Carte blanche aux étudiants du MASDEX (Master en design d’exposition, Arts2, Mons)


**Épisode 3** : Angela Detanico & Rafael Lain, Aurélien Froment (Collection Veys-Verhaevert)


**Épisode 4** : Claire Ducène


*Gisements* (Recy-K, Bruxelles)
-------------------------------------------------------
Joachim Coucke, Adrien Tirtiaux, Thierry Verbeke

### Atelier

*Reprise. Cécile Ibarra*
-------------------------------------------------------

*A Prisoner’s Cinema*, Olivier Dekegel & Emmanuel Van der Auwera
-------------------------------------------------------


2017
====

*Some arguments later*
-----------------------------------------------------------------------------------------------------
MESSIDOR : Eitan Efrat, Sirah Foighel Brutmann, Pieter Geenen, Meggy Rustamova

*SYNC !*
-----

**Part 1 : Métalangue**, Les Commissaires anonymes

Camille Bondon, Sofia Caesar & Luiza Crosman, Clôde Coulpier, Juliette
Goiffon & Charles Beauté, Lola Pertsowsky, Joséphine Kaeppelin,
Lich/sprot 2000, Maud Marique, Eona Mc Callum, Grégoire Motte & Gabriel
Mattei, Yoann Thommerel et Vikhi Vahavek

**Part 2 : Hannah Hoffman**, Clovis XV


**Part 3 : Le dispensaire**, PEZCORP


*L’art d’accommoder les restes*
------------------------------------

Joachim Coucke, Adrien Tirtiaux, Thierry Verbeke

*COM∩∪TIES. Seuils/Drempels/Thresholds*
------------------------------------

**URBANITÉ** : Efi & Amir, Dagmar Keller & Martin Wittwer, Jan Locus, Nicolas Provost

**SOCIABILITÉ** : Harald Thys, Jos De Gruyter & Harald Thys, Raphaël Cuomo & Maria lorio,
Els Dietvorst

**POLITIQUE** : Ruben Bellinks, Teresa Cos, Ira A. Goryainova, Hans Op de Beek

**ENVIRONNEMENT** : Els Dietvorst, Esther Johnson, Hans Op de Beek

*Biennale Watch this space* \> Ariane Loze & Justine Pluvinage
--------------------------------------------

2018
====

*On sort en bande, Labor of Love. Salad jours*
--------------------------------------------

Béatrice Delcorde, avec la contribution d'Aude Anquetil, Léa Beaubois,
David Evrard, Nicolas Jorio, Misssouri, Nicolas Valckenaere, les
étudiants du Master sculpture de l’erg 

*State of Statelessness* (*Nos états apatrides*)
--------------------------------------------

Artistes/Étudiants du HISK, Gand : Sabrina Chou, Marijke De Roover,
Office for Joint Administrative Intelligence (OJAI : Chris Dreier & Gary Farrelly), Yuki Okumura, Meggy Rustamova, Eléonore Saintagnan, Gen Ueda,
Jonathan Vinel, Beny Wagner, Justyna Wierzchowiecka. Curateurs/Étudiants
de la KASK, Gand : Hilde Borgermans, Alicja Melzacka, Maxime Gourdon,
Romuald Demidenko

*A Forest*
--------

Maria Thereza Alves, Danièle Aron, Félicia Atkinson, Cécile Beau, Lise
Duclaux, Diana Duplakova, Paul Duvigneaud (archives), Olga Kisseleva,
Deborah Levy & Antoine Wang, Yogan Muller, Various Artists, Pep Vidal

2019
====

*Games and Politics*
------------------

*Unexistant*, Alec De Busschère
------------------------------

*Sly Composition*
---------------

Exposition de fin d’études du master Pratiques de l’exposition — CARE
de l’ArBA-EsA

Artistes&#x202F;: Bára Barbora Hřebačková, Robin Beauvais, Max Blotas,
Faustine Boissery, Marine Boissières, Antonin Broquedis, Tom Callens,
Camille Coléon, Daniele Coppola, Sylvère Cypriani, Delphine De Baere,
Benoît de Dreuille, Rodolphe Delacourt, Designwithgenius, Pierre
Deruisseau, Mikael Di Marzo, Alexandre Duvinage, Rafaël Elders, Florin
Filleul, Greta Fjellman, Carlos Gaston, Benoît Gérard, Fanny Godefroid,
Geoffrey Grumelli, Ophélie Honoré, Guðný Rósa Ingimarsdóttir, Héloïse
Jadoul, Mikail Koçak, Charlotte Leblé, Gabrielle Lerch, Les Sangliers
Lâchés, Mehdi Mojahid, Andreia Morado, Anne-Laure Mouchette, Katia
Nouchi, Vincent Overath, Edouard Pagant, Eva Papegeorgiou, Clémence
Peyroche d’Arnaud, Maxime Pichon, Charlotte Simon, Julie Thibault,
Anouchka Walewyk. Étudiants : Sung Yoon Ahn, Thomas De Sousa, Yseult
Gay, Romane Gérard, Carole Lallemand, Camille Lemille, Pauline Salinas

*Babel*
-----

Lawrence Abu Hamdan, Marc Buchy, Erik Bullot, Brendan Fernandes, Michel
François, Rainer Ganahl, Susan Hiller, Marianne Mispelaëre, Myriam
Pruvot, Meggy Rustamova, Zineb Sedira

*Lignes de fuite*, Jérôme Giller (Maison des artistes d’Anderlecht, Bruxelles)
-----------------------------------

### Espace Satellite

*Junk Office*
--------------------------------------------------
Chloé Schuiten et Clément Thiry

*Waiting to Blossom + Constallations*
-------------------------------------------------------
Pascale Barret, 3G(eneration) (Annie Abrahams, Pascale Barret, Alix Désaubliaux)

*Entre intérêts*, Maëlle Dufour
------------------------------

*Offshore Garden*, Surya Ibrahim
------------------------------

2020
====

*some things…*, Guðný Rósa Ingimarsdóttir
------------------------------------------

*Inspire*
-------

F&D cartier, David Claerbout, Manon de Boer, Edith Dekyndt, Noémie
Goldberg/Nogold, Suchan Kinoshita, Wolfgang Laib, collectif muesli,
Elise Peroi, Fabrice Samyn, Maarten Vanden Eynde

### Espace Satellite

*Point de croix*, Laetitia Bica
------------------------------
