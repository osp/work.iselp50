#! /usr/bin/env python


'''
Tags Extension for Python-Markdown
===============================================

Adds tags. Replaces [[tag]] for <span class="classname" data-index="">tag</span>.

Customizable with `make_link` option as to what the actual element is.

%%label%tag%%
%%tag|label%%
[[tag]]
[[tag|label]]

Usage
-----

    >>> text = "Some text with a [[WikiLink]]."
    >>> html = markdown.markdown(text, ['semanticwikilinks'])
    >>> print(html)
    <p>Some text with a <a href="WikiLink">WikiLink</a>.</p>

    >>> text = "[[http://activearchives.org/]], [[#id|anchor]], [[../index.html|a relative link]], [[/|an absolute link]], [[/index.html|another absolute link]]"
    >>> html = markdown.markdown(text, ['semanticwikilinks'])
    >>> print(html)
    <p><a href="http://activearchives.org/">http://activearchives.org/</a>, <a href="#id">anchor</a>, <a href="../index.html">a relative link</a>, <a href="/">an absolute link</a>, <a href="/index.html">another absolute link</a></p>

Define a custom URL builder:

    >>> def make_rdfa(md, rel, target, label):
    ...     # `md` is the Markdown instance
    ...     elt = etree.Element("span")
    ...     elt.set("property", rel)
    ...     elt.set("value", target)
    ...     elt.text = label or target
    ...     return elt

    >>> md = markdown.Markdown(extensions=['semanticwikilinks'],
    ...         extension_configs={'semanticwikilinks' : [('make_link', make_rdfa)]})
    >>> html = md.convert('[[ Speaker :: Sherry Turkle | Second Self ]]')
    >>> print(html)
    <p><span property="aa:Speaker" value="Sherry Turkle">Second Self</span></p>

Change the default namespace (which is "aa"):

    >>> md = markdown.Markdown(extensions=['semanticwikilinks'],
    ...         extension_configs={'semanticwikilinks' : [('namespace', 'mynamespace')]})
    >>> html = md.convert('[[ Speaker :: Sherry Turkle | Second Self ]]')
    >>> print(html)
    <p><a href="Sherry Turkle" rel="mynamespace:Speaker">Second Self</a></p>

To do
-----

- An optional function to wikify names? (It is already possible to achieve
this with the custom `make_link` function).


Dependencies
------------

* [Markdown 2.0+](http://www.freewisdom.org/projects/python-markdown/)


Copyright
---------

2011, 2012 [The active archives contributors](http://activearchives.org/)
2011, 2012 [Michael Murtaugh](http://automatist.org/)
2011, 2012 [Alexandre Leray](http://stdin.fr/)

All rights reserved.

This software is released under the modified BSD License. 
See LICENSE.md for details.
'''


import markdown
try: from markdown import etree
except ImportError: from markdown.util import etree
import re


__version__ = "1.1.1"


INLINE_TAGS_RE = """
%%\s*
    (?P<indexedValue>.+?)
    (?:\s* \| \s* (?P<label>.+?) \s*)?
%%(?!%)
"""


def make_inline_tag(md, indexedValue, label, className, indexAttribute):
    span = etree.Element('span')
    span.set('class', className)
    span.set(indexAttribute, indexedValue)
    span.text = label or indexedValue
    return span


class InlineTagExtension(markdown.Extension):
    def __init__(self, configs):
        self.config = {
            'make_inline_tag': [make_inline_tag, 'Callback to convert link parts into an HTML/etree element'],
            'index_attribute': ['data-book-index', 'Attribute used for storing the indexed value'],
            'className': ['book-index', 'Class added to wrapper around indexed elements']
        }

        # Override defaults with user settings
        for key, value in configs:
            self.setConfig(key, value)

    def extendMarkdown(self, md, md_globals):
        self.md = md

        # append to end of inline patterns
        ext = InlineTagPattern(self.config, md)
        md.inlinePatterns.add('inlinetag', ext, "<not_strong")


class InlineTagPattern(markdown.inlinepatterns.Pattern):
    def __init__(self, config, md=None):
        markdown.inlinepatterns.Pattern.__init__(self, '', md)
        self.compiled_re = re.compile("^(.*?){}(.*?)$".format(INLINE_TAGS_RE), re.DOTALL | re.X)
        self.config = config

    def getCompiledRegExp(self):
        return self.compiled_re

    def handleMatch(self, m):
        """ Returns etree """
        d = m.groupdict()
        fn = self.config['make_inline_tag'][0]

        return fn(self.markdown, d.get("indexedValue"), d.get("label"), self.config['className'][0], self.config['index_attribute'][0])


def makeExtension(configs={}):
    return InlineTagExtension(configs=configs)


if __name__ == "__main__":
    import doctest
    doctest.testmod()
