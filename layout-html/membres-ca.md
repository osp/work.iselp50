Membres du Conseil d’administration depuis 1971 
{: .cartouche-titre} 
 
Présidents
:   Joseph-Berthold %%Urvater, Joseph-Berthold|Urvater%% (1970-1971), Robert L. %%Delevoy, Robert L.|Delevoy%% (1972-1980), %%Prigogine, Ilya|Ilya Prigogine%% (1980-2003), %%Van der Stichele, Michel|Michel Van der Stichele%% (2003 à aujourd’hui)

Administrateurs et administratrices
:   Giulio Carlo %%Argan, Giulio Carlo|Argan%%, René %%Berger, René|Berger%%, Marcel %%Berlanger, Marcel|Berlanger%%, France %%Borel, France|Borel%%, Marcel %%Bussers, Marcel|Bussers%%, Gita Brys-Schatan, Anne %%Chaponan-Garcia, Anne|Chaponan-Garcia%%, Jean %%Coquelet, Jean|Coquelet%%, Maurice Culot, Herman %%Daled, Herman|Daled%%, Patrick %%Debouverie, Patrick|Debouverie%%, Francis %%De Lulle, Francis|De Lulle%%, Robert L. Delevoy, André Delvaux, Jacques %%Dewit, Jacques|Dewit%%, %%Dumont, Georges-Henri|Georges-Henri Dumont%%, Albert %%Goffart, Albert|Goffart%%, %%Hassel-Grosjean, Catherine|Catherine Hassel-Grosjean%%, Barbara %%Hassel-Szternfeld, Barbara|Hassel-Szternfeld%%, %%Herman, Ariane|Ariane Herman%%, Nicole %%d’Huart, Nicole d’|d’Huart%%, Hans L. C. %%Jaffé, Hans L. C.|Jaffé%%, Raoul %%Jakou, Raoul|Jakou%%, Laurence %%Jenard, Laurence|Jenard%%, Wen Li %%Kao, Wen Li|Kao%%, Martine %%La Haye, Martine|La Haye%%, Jacques %%Lassaigne, Jacques|Lassaigne%%, Christian %%Lasserre, Christian|Lasserre%%, Jean %%Lefébure, Jean|Lefébure%%, Luc %%Legrand, Luc|Legrand%%, Philippe le %%Hodey, Philippe le|Hodey%%, Marion %%Lemesre, Marion|Lemesre%%, René Léonard, Herman %%Liebaers, Herman|Liebaers%%, Jean-Émile %%Mesot, Jean-Émile|Mesot%%, Johan Muyle, Robert %%Pierlet, Robert|Pierlet%%, %%Poupko, Jean-Pierre|Jean-Pierre Poupko%%, Jean %%Remiche, Jean|Remiche%%, Georges %%Van Hout, Georges|Van Hout%%, Michel %%Van Roye, Michel|Van Roye%%, Joseph-Berthold %%Urvater, Joseph-Berthold|Urvater%%, %%Verschueren, Bob|Bob Verschueren%%, Robert %%Warche, Robert|Warche%%  

<div id="my-toc-content" style="display:none;"></div>

