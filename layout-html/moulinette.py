import sys
import markdown

with open("header.html", "r", encoding="utf-8") as input_file:
    h = input_file.read()
    # Insert id in header based on filename
    h = h.replace("{{ id }}", sys.argv[1].replace(".md", ""))

with open(sys.argv[1], "r", encoding="utf-8") as input_file:
    md = input_file.read()

with open("footer.html", "r", encoding="utf-8") as input_file:
    f = input_file.read()

o = open(sys.argv[1].replace(".md", ".html"), "w", encoding="utf-8")

md_html = markdown.markdown(md, extensions = ["extra", "mdx_figcaption", "mdx_inline_tags"])

o.write("%s \n %s \n %s" % (h, md_html, f))
o.close()

