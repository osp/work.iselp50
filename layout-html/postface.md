 
<div class="cartouche">

<p class="cartouche-titre">Postface</p>
<p class="cartouche-signature">
<span class="book-index" data-book-index="Grimmeau, Adrien">Adrien Grimmeau</span></p>
<p class="cartouche-role">
Directeur de l’ISELP</p>

</div>

<div id="my-toc-content" style="display:none;"></div>

Les recherches menées pour ce livre nous ont permis de découvrir une
histoire que nous-mêmes, dans l’équipe, ne connaissions pas.
L’historique reconstitué témoigne des changements successifs de
l’Institut au fil des décennies, des directions, et d’un contexte
bruxellois en mutation. Riches d’une meilleure connaissance de nos
racines, nous pouvons définir plus précisément dans quelle direction
nous voulons voir pousser nos branches.

En lisant cette histoire, il me semble que l’ISELP est à l’intersection
fertile de trois ensembles qui ont joué un rôle avec une intensité
variable au fil du temps. À mes yeux, ils ont tous trois autant
d’importance, et je les évoque donc sans ordre spécifique. Il s’agit du
public, des artistes et des chercheurs.

Le public. L’ISELP a été créé pour répondre à une demande&#x202F;: combler un
manque dans la compréhension du monde artistique par un public curieux
mais non expert. Un demi-siècle plus tard, la donne a changé&#x202F;: un
amateur cherche désormais plus facilement des informations sur internet
que dans un programme de cours. Cependant, je suis persuadé que nous
existons pour le public, aussi large que possible. C’est même
probablement plus nécessaire aujourd’hui qu’auparavant. Les avis se
multiplient, les discours se subjectivisent, le concept de vérité
devient flou. En tant qu’institut, nous devons respecter le public et
lui offrir une approche la plus juste, la plus sincère possible. En
parallèle, il me semble central d’initier les plus jeunes à la culture
au sens large, pas tant dans l’idée d’inculquer une culture générale que
dans celle de développer au maximum leur capacité d’analyse, et donc
leur esprit critique. L’œuvre d’art est un regard sur le monde&#x202F;: la
décoder, c’est confronter son opinion, élargir son horizon. Il ne s’agit
pas de convaincre de la qualité de l’art contemporain, mais de
convaincre qu’il y a là matière à se développer. Très certainement, au
moment où les contenus deviennent largement virtuels, penser un institut
comme un forum, un espace de rencontre et de dialogue réel entre public,
artistes, chercheurs, fait sens.

Le deuxième ensemble est constitué par les artistes. Ici aussi, cela
paraît être une évidence. Cependant, les recherches menées pour cet
ouvrage ont montré que l’ISELP n’est pas historiquement lié aux artistes
contemporains. Le rapprochement s’est effectué progressivement, avec un
tournant marqué au début des années 2000. Aujourd’hui, nous sommes l’un
des principaux lieux subventionnés de soutien aux plasticiens belges.
Cela signifie qu’au-delà du discours sur l’art que nous souhaitons
accueillir, nous accordons une attention particulière à présenter la
scène belge, spécialement émergente.

Ces derniers mois ont prouvé la précarité de ce secteur, et il apparaît
comme une évidence de renforcer le soutien et la visibilité de cette
scène. Sans les artistes, l’ISELP n’a pas de sens. Ce soutien ne
consiste pas simplement à montrer. Il est aussi nécessaire en termes de
production, de réflexion, de diffusion de la pensée, de médiation… Si,
il y a quelques décennies, nous travaillions principalement avec des
œuvres, aujourd’hui nous travaillons avec des artistes. Il s’agit de
productions *in situ*, de collaborations avec des écoles, de résidences de
recherche, de productions dans l’espace public… Cela s’accompagne de
doutes, d’unions pas toujours évidentes entre art et administration,
entre art et public.

Si nous souhaitons nous affirmer comme lieu de lien entre création et
société, nous devons être à la hauteur de cet engagement vis-à-vis des
artistes. En termes de temps, d’argent, de ressources.

Le troisième ensemble est constitué par les chercheurs. Il s’agit
probablement de l’ensemble le moins identifié. Historiquement, il
renvoie à la fois aux conférences données par des professeurs, à
l’action menée par l’Institut dans le champ de l’art public par le biais
de tables rondes puis d’expertise au sein de commissions, et aux
colloques organisés dans les années 2000. Aujourd’hui, je souhaite
concevoir notre rapport à la recherche exactement comme notre rapport
aux artistes. Il s’agit d’accompagner une scène, de lui donner les
moyens d’exister, et de rencontrer une audience. Cela signifie qu’il est
autant question de diffuser une recherche établie, par le biais de
conférences et cours, que d’encourager des questionnements neufs. Les
résidences scientifiques que nous initions dans le cadre des cinquante ans de
l’ISELP en sont le premier témoignage&#x202F;: permettre à un historien de
l’art de venir construire son propos en nos murs puis le
diffuser témoigne de notre volonté d’encourager les réflexions
nouvelles. Très clairement, nous souhaitons accompagner une pluralité de
discours sur les rapports entre art et société contemporaine, et des
conceptions les plus larges possibles de l’art actuel. Nous sommes
demandeurs d’une scène d’historiens qui soit diversifiée, en phase avec
la société. De la même façon, les sujets évoqués doivent être
décloisonnés. Même si les vingt dernières années de l’ISELP ont assis
une action dans le champ des arts plastiques contemporains, les
décennies plus anciennes doivent continuer de nous encourager à nous
intéresser à des formes moins reconnues comme ont pu l’être le clip
vidéo, la caricature ou la bande dessinée, pour ne citer que quelques
exemples accueillis ici. Les arts plastiques et l’histoire culturelle
doivent se rencontrer.

Ces trois ensembles forment un tout dont la force réside dans la
rencontre. L’ISELP est un carrefour, un forum, le lieu d’une
co-construction de notre rapport au monde. L’échange est le maître mot.
Quel est l’objectif de cette rencontre&#x202F;? En joignant une recherche
artistique ou scientifique en cours à un / des publics, initiés ou non,
il s’agit de participer à la démocratie culturelle. Cela ne peut se
faire qu’horizontalement&#x202F;: en croisant les discours, en croisant les
disciplines artistiques. Le décloisonnement qui a marqué la création de
l’Institut me sert encore de modèle aujourd’hui. Plutôt que de tenter de
convaincre de la pertinence d’une scène artistique (il s’agit alors de
démocratisation de la culture), il importe de créer et défendre ensemble
une vision de l’art qui fasse sens dans le monde qui nous entoure. Qui
permette à chacun de s’approprier un peu mieux son quotidien, grâce à la
multitude d’approches de l’art&#x202F;: regard critique, émotion, pas de côté,
focalisation accrue, humour… L’art n’existe que comme objet de
société, comme lieu collectif, aussi intime que puisse en être sa
perception. En ce sens, nous souhaitons être un lieu de société.
