 <div class="cartouche">
 
<p class="cartouche-partie">Préface</p>
<p class="sous-titre">Pourquoi et comment<p>
<p class="cartouche-signature">
<span class="book-index" data-book-index="Brys-Schatan, Gita">Gita Brys-Schatan</span></p>
<p class="cartouche-role">
Fondatrice de l’ISELP
</p>

</div> 


<div id="my-toc-content" style="display:none;"></div>

J’ai grandi avec l’Institut   
En toute simplicité  
C’est vertigineux&#x202F;!   
Un si long temps.
Cinquante ans&#x202F;!

Jamais je n’aurais pu supposer qu’un tel projet puisse réellement voir
le jour&#x202F;!

Bien entendu, je me connaissais de solides connaissances en histoire de
l’art…

Bien entendu, mais je voulais essayer, entreprendre, offrir une clé pour
entrer dans un monde d’une richesse peu commune, mais suffisamment
diversifié pour apporter à chacun un accroissement de sa sensibilité.

Dans une réalité plurielle et *multi-personnelle.*

J’utilise volontairement ces termes. 

L’énervement et le malaise en furent sans doute la raison.

Ce qui me causa ce malaise, profond comme peut l’être celui d’une
révolte intérieure, fut de m’apercevoir qu’un grand nombre de personnes
étaient persuadées, voire affirmaient&#x202F;: «&#x202F;Ce n’est pas pour moi&#x202F;» ou&#x202F;: «&#x202F;Ce n’est qu’une image… je ne comprends pas l’importance », ou
encore&#x202F;: «&#x202F;Qu’est-ce que ça veut dire…&#x202F;?&#x202F;». 

Alors je me rendis compte combien observation, découverte, éducation,
*perception*, jouaient un rôle important pour le sens de la vie.

Ai-je mal joué ma vie&#x202F;?

Bien au contraire, celle-ci s’est amplifiée, certes coupée sans cesse
par des questionnements, des doutes&#x202F;: fallait-il continuer&#x202F;?

Sans me pencher plus avant sur un aspect très personnel, j’avais tenu
à me rappeler que j’étais chargée d’autres responsabilités, lesquelles
m’étaient infiniment chères&#x202F;: une famille en plein épanouissement.

Et le temps, le temps qui courait, qui courait…

Quel basculement&#x202F;!

Une première crainte m’avait envahie&#x202F;: pourrais-je tenir sans être
épaulée&#x202F;? Où et comment, sans budget et sans lieu déterminé&#x202F;?

En effet, je n’avais aucun budget personnel à consacrer à ce beau et
mystérieux projet, non plus que les possibilités de trouver un
collaborateur pour m’aider à mettre en marche cette action.

Et dans quel espace fonctionner&#x202F;?

Néanmoins, y a cinquante ans, avec espoir, volonté et enthousiasme
j’entrepris de mener à bien, d’inventer plus spécifiquement les prémices
de ce qui allait devenir une véritable aventure. Une ouverture pour qui
le souhaitait, à l’Art, l’art, aux arts.

Cette étrangeté dans le monde des vivants.

Souvenir, souvenirs…

Un antiquaire actif au célèbre marché des antiquaires du %%Sablon%%,
s’intéressa au projet et m’annonça qu’il lui était possible de nous
prêter momentanément un rez-de-chaussée situé rue aux %%Laines (rue aux)|Laines%%, à la
condition que le projet ne tarde pas trop à se réaliser.

Impressionnée par la proposition, j’acceptai immédiatement.

Ce fut le premier lieu où se pratiquèrent les cours&#x202F;! Modestement mais
avec quelques excellents historiens de l’art enthousiasmés par le
projet.

Les premiers «&#x202F;étudiants&#x202F;» en emmenèrent d’autres, ce qui me donna une
immense satisfaction et me poussa à continuer.

Plusieurs professeurs séduits par l’initiative furent chargés de cette
mission.

Il est intéressant aussi de savoir qu’en ces années, l’intérêt et la
curiosité pour les arts étaient nettement moindres qu’ils ne se sont
démontrés depuis lors.

Il fallut néanmoins, après quelque temps, chercher un autre lieu plus
adéquat, plus vaste.

Au bout de quelques mois, un petit miracle s’accomplit.

Je découvris alors un lieu très poétique et fort séduisant&#x202F;: c’est-à-dire un chemin très discret qui menait au %%Egmont, Parc d’|parc d’Egmont%% et son château.

L’entrée principale en était située %%Waterloo (boulevard de)|boulevard de Waterloo%%. On y pénétrait déjà à cette époque par un
chemin latéral, d’un aspect particulièrement calme, très lointain des
nombreuses boutiques qui animent aujourd’hui ledit boulevard.

L’allée était pourvue d’un bâtiment fermé sur le côté droit. Une série
de grandes et larges fenêtres arrondies laissaient entrevoir de vastes
espaces intérieurs…
{: .enchevetrement}

Lesquels étaient apparemment vides&#x202F;! Stop et arrêt de ma part…

Le lecteur comprendra sans doute, qu’usant de mon courage, j’allai
demander audience au ministère de la Culture, puis à la %%Commission communautaire française (COCOF)|COCOF%%.

Au cours de plusieurs entretiens avec les ministres responsables, je
leur ai soumis en détails le projet tout en soulignant l’importance de
stimuler la connaissance des arts plastiques pour le plus grand public
possible&#x202F;: de permettre ainsi à la population d’entrer dans le monde de
l’art, lequel pouvait devenir *étonnement, plaisir, jouissance.*

Ce qui n’excluait absolument pas la présence de connaisseurs et tout
simplement de curieux qui, comme on le constatera, devinrent rapidement
des habitués.

Il s’agissait d’ouvrir ces lieux pour stimuler l’approche et la
connaissance d’un savoir passionnant.

Les espaces étant vides, les ministres, en soutenant ce projet, menaient
une action culturelle et sociale&#x202F;: permettre à la population intéressée
d’entrer ainsi dans le monde de l’art, ce qui devint de plus en plus
fréquent&#x202F;!

Car contrairement au non-sens, le monde des arts est un monde de sens,
du sens et des sens&#x202F;!

Le projet prit corps quelques mois plus tard.

Enfin ces locaux nous furent attribués. Bientôt l’ISELP en prit
possession.

Après un certain laps de temps, ces locaux furent restaurés&#x202F;: au
rez-de-chaussée une première salle d’exposition, un auditoire et un
secrétariat ; au premier étage, un bureau et une salle de documentation.

Il fallut alors penser à chercher une ou deux personnes aidantes pour
les tâches à venir&#x202F;: la solution apparut notamment grâce au concours de
deux objecteurs de conscience.

La suite se construisit petit à petit.

Certains cours se donnèrent l’après-midi, d’autres dans la soirée… ce
qui représentait pour notre toute petite équipe une véritable difficulté
que nous surmontions vaillamment.

Au bout de plusieurs années, une certaine évolution se produisant dans
une partie des différents centres d’intérêt de la société, le public se
montra de plus en plus intéressé par tous les aspects artistiques, dont
les arts plastiques.

Le langage lui-même évolua, les termes souvent se réinventèrent.
L’intérêt pour les arts se fit plus intense.

Pendant ce temps l’équipe de l’ISELP s’était agrandie petit à petit. Des
historiens de l’art, enthousiastes et imaginatifs, s’attachèrent à
ouvrir de vastes pans de l’art, de ses aspects parfois étranges…

Mais comment et pourquoi&#x202F;?

Quelle est donc cette sorte de «&#x202F;nourriture terrestre&#x202F;» (André %%Gide, André|Gide%%) qui nous
entraîne à convaincre, à partager&#x202F;?

Cette vision de l’Art, de l’art, des arts.

L’invention, la sensibilité, la curiosité, le rêve&#x202F;: cette autre
sensibilité existentielle de ce que nous nommons l’imagination&#x202F;! Et
pourtant, la matérialité y joue un grand rôle.

En faisant abstraction des arts anciens tels que la peinture, la
sculpture, la gravure, de notre passé, désormais l’audace des artistes
contemporains se sert d’une temporalité totalement inventive. Qui
affronte et le passé et le futur dans un présent à la fois infini et
fuyant.

Il n’est pas aisé de prendre la liberté de créer.

Dans le même temps quelle sorte d’ivresse.

Laquelle peut torturer l’artiste, le créateur, et le plomber jusqu’à ce
que celui-ci passe dans une sorte d’inconscience, pourtant riche.

Il y a de multiples portes d’entrées pour ces intentions artistiques.

On peut évaluer l’art ancien, l’apprécier ou y rester indifférent.

On peut tout aussi bien repousser l’art contemporain… pour trouver
l’art actuel bizarre, tordu, délirant, tourné vers la «&#x202F;trahison des
images&#x202F;» (René %%Magritte, René|Magritte%%).

En somme, il faut se lancer dans une sorte d’apprentissage de lecture
particulière où le quant-à-soi privé, personnel, accepte de se libérer
du regard plâtré jeté sur l’image encore inconnue.

Oser essayer non pas de comprendre ou d’adhérer *ipso facto* lorsque
l’objet présenté ne vous éveille pas (quelle est la langue qu’il n’a
pas fallu apprendre dans le courant de la vie&#x202F;; même l’enfant jeune,
sauf s’il est doté de parents polyglottes, passe par cet
apprentissage), mais se laisser aller à la curiosité active de celui
qui le désire.

Se mettre à la hauteur du message visuel.

Ne pourrait-on pas avancer — sauf en certaines circonstances
douloureuses et pesantes — que le seul langage capable de faire penser
à cet infini étrange, qui passe dans toutes les langues, est l’art&#x202F?

(Les notes de l'*Ode à la joie* de %%Beethoven, Ludwig van|Beethoven%%, par exemple, qui
délivrent un sentiment d’émotion très profonde, même si l’écoute en est involontaire, les vibrations musicales de Keith
%%Jarrett, Keith|Jarrett%% ou la voix de Leonard %%Cohen, Léonard|Cohen%% d’une gravité rugueuse, entrent dans
la même problématique.)

N’avons-nous pas tous en notre mémoire intérieure, par moments — très
rarement ou souvent — plus que le souvenir, la sensation de retourner
dans un passé qui nous offre une mélodie laquelle s’était un jour ancrée
en nous. Il y a sans doute un lien intérieur, fluide ou pesant, qui
reconstruit dans une partie de notre cerveau le besoin profond de
reconnaître (ou d’apprendre, de découvrir) tel ou tel passage musical.
Vous pouvez vous arrêter au milieu de votre activité, écouter et passer
en un éclair, de la vie courante à une forte émotion intérieure.

Nous pouvons ressentir le même type d’émotion devant un tableau qu'à
première vue je qualifierais d'«&#x202F;hirsute&#x202F;», et non, je n’accepte pas les
termes désapprobateurs du soi-disant « &#x202F;n’importe quoi&#x202F; &#x202F;» ! Cela ne veut
rien dire et pourtant, ces impressions négatives peuvent alors fortement
s’inscrire dans votre regard lors de votre désapprobation première.

Dites-vous qu’à chaque moment de la vie nous sommes obligés d’accepter
une découverte ou l’autre&#x202F;; cela s’appelle *apprendre.*

Nous sommes tous obligés à un moment ou un autre d’y faire face.

Cet aspect sans doute un peu difficile ne s’adresse pas seulement ou
nécessairement aux divers aspects de la vie matérielle.

Cela signifie aussi que votre sensibilité existe.

Nous voilà ici avec le désir de découvrir, d’apprendre, d’être proche ou
non d’une œuvre.

Quelles en sont les critères et les qualités&#x202F;?

Les mots changent, nous le savons bien. La génération actuelle, qui
plonge sur des termes appartenant à diverses langues, pour en quelque
sorte s’amuser à marquer ses racines personnelles dans un champ
international, semble presque signifier une accointance avec une certaine
mondialisation.

Certes on ne peut sans doute être perceptif à chaque aspect possible des
arts en général&#x202F;: il y a sans doute des affinités particulières.

Donner cours, chacun le sait, est une sorte de transmission entre le
transmetteur et l’écoutant.

Ici, à l’ISELP, chaque professeur me paraît animé d’un souhait bien
ancré en lui. Ouvrir un champ particulier constitué par la liaison entre
les courants contemporains, par exemple, signifie que l’image en soi est
mise en cause&#x202F;: il s’agit alors de percer le mur de l’incompréhension
première.

Les questions fusant, l’animation s’empare de l’auditoire.

Une partie des auditeurs est nettement en activation&#x202F;: l’œuvre est mise
en question, puis calmement restituée. Les auditeurs ressortiront en
discutant entre eux, et généralement ravis, intrigués ou perplexes&#x202F;!

Un premier but est atteint.
{: .enchevetrement}

L’équipe de l’Institut entre dans le groupe, et l’animation, juste
photographie des buts poursuivis, prend tout son impact.

Il ne faut pas oublier que, de dimensions fort importantes, la salle
d’exposition joue son rôle.

Pour beaucoup d’auditeurs la tension y est forte&#x202F;: ces derniers, devenus
spectateurs, sont avides d’adhérer (ou non) au sens interne de la toute
nouvelle compréhension de l’œuvre visualisée. Le regard nouveau qu’ils
adressent à cette dernière suscite des commentaires, qui à leur tour,
prolongent le cours donné et renforcent de futures amitiés.

Je ressens, à certains moments, en parcourant ces lieux divers, une
sorte de plénitude qui me transporte intérieurement.

Des milliers d’auditeurs ont accédé à des connaissances multiples, se
sont volontairement passionnés pour les arts plastiques contemporains,
actuels ou… déjà futurs&#x202F;!

Des créateurs y ont participé en donnant des éclaircissements, ou en
provoquant le dialogue avec les auditeurs.

Au fond, on peut même imaginer que la génération suivante suivra leur
exemple… mais ne divaguons pas…

À présent partons ensemble dans l’imaginaire&#x202F;: un imaginaire multiple&#x202F;!

Puisque l’objet, ou la matière, ou la couleur, ou le format (nous n’en
sommes pas encore à l’odeur, mais qui sait&#x202F;!), a réussi à plomber ce que
nous appelons la réalité, pour en faire *une œuvre*, allons jusqu’au
bout.

Quelle différence y a-t-il entre la sensation produite par l’œuvre, vue
et revue par l’artiste, et la sensation du regard qui sera celui du
lecteur&#x202F;?

Un monde d’émotions différentes…

En premier lieu rappelons que pour l’artiste, la situation est
différente&#x202F;: il aura toujours le désir de changer quelque chose dans
l’œuvre qu’il a créée. Ce qui signifie qu’il ressentira une double
sensation en la regardant après finition.

Pour sa part, le spectateur y entrera d’abord sans doute par étapes, même s’il a
bien analysé l’œuvre en question. Ou bien le travail artistique fera
partie de sa sensibilité intrinsèque, et il accèdera intérieurement à
cet univers pictural. Alors une émotion très intime peut s’insinuer en
lui… Une émotion particulière qui l’enrichira.

Nous sommes devant un double mystère, pour ainsi dire.

Le créateur, l’artiste, le peintre, le sculpteur… mais aussi les
blancs et les noirs, les vides et les pleins de %%Malevitch, Kasimir|Malevitch%%, les visages
et les nus de %%Picasso, Pablo|Picasso%%, les actuelles désarticulations et les
constructions de sable et gravats, ne sont que tentatives…
d’aller plus loin encore…

(Pour autant que je sache, néanmoins, la capture d’un vide par un vide,
n’a pas encore été obtenue. Sauf peut-être dans l’espace cosmique, mais
à peine un espace aurait-il été enfermé que cet enfermement lui-même
détruirait la tentative&#x202F;! Inutile d’aller plus avant&#x202F;!! Dans cette
perspective à tout le moins…)
{: .enchevetrement}

Sauf à rêver, mais justement&#x202F;: la création artistique n’est-elle pas un
rêve réalisé&#x202F;?

La beauté, certes, est prise à partie. Si ce n’est, que nous, êtres
vivants, devons et pouvons nous élever dans notre propre imaginaire. Et
constater par exemple que tout sujet vivant est lié à un regard déjà
formaté. Par un regard qui veut à la fois adhérer au spectacle du monde,
et, ce faisant, accroître la liberté intime de pouvoir transformer,
voire libérer ce même monde…

Je souhaite à présent remercier chaleureusement le conseil
d’administration de notre Institut, le Président et les administrateurs
successifs, d’avoir contribué à protéger et à consolider notre aventure.
Il me tient à cœur de remercier également l’ensemble des professeurs et
conférenciers, ainsi que les équipes administratives et techniques, qui
ont permis le rayonnement de l’Institut.

Passons du rêve à une réalité. L’ISELP existe.

En fait, c’est sans doute pour éclaircir tout ceci que l’ISELP est né.

Il y a cinquante ans.
{: .enchevetrement}
