# De l’initiative privée à l’aventure collective {: .cartouche-titre} 
 
 
* %%Bara, Manon|Manon Bara%%,
* %%Boulares, Sami|Sami Boulares%%,
* %%Dervaux, Laurence|Laurence Dervaux%%,
* %%Desaive, Pierre-Yves|Pierre-Yves Desaive%%,
* %%Even-Adin, Bernadette|Bernadette Even-Adin%%,
* %%Genicot, Thierry|Thierry Genicot%%,
* %%Lennep, Jacques|Jacques Lennep%%,
* %%Octave, Jean-François|Jean-François Octave%%,
* %%Pintus, Anne|Anne Pintus%%,
* %%Romanelli, Fabrizio|Fabrizio Romanelli%%,
* %%Santocono, Adèle|Adèle Santocono%%,
* %%Stanescu, Michèle|Michèle Stanescu%%,
* %%Vancampenhoudt, Lyse|Lyse Vancampenhoudt%%,
* %%Veys, Christophe|Christophe Veys%%

<div id="my-toc-content" style="display:none;"></div>

> Thierry %%Genicot, Thierry|Genicot%%, journaliste et réalisateur de documentaires radiophoniques engagé au sein de différentes émissions de la %%RTBF/RTB|RTBF%%
{: .signature}
>
> ### Refus initial
>
>Il y presque un demi-siècle. À l’époque le service militaire,
obligatoire pour les hommes, pouvait être remplacé par un service civil
de deux ans presté (entre autres) au sein d’une institution
culturelle… L’ISELP était du nombre. J’avais, dans ce but, pris
rendez-vous avec sa directrice, %%Brys-Schatan, Gita|Gita Brys-Schatan%%. Je me trouvais dans son bureau, à l’étage de l’Institut. J’avais face à moi la silhouette – je m’en souviens – d’une femme très élégante au tailleur blanc et à la
chevelure épaisse et noire. J’ai la mémoire de son visage à la peau sombre.
Une femme très vive, et qui m’interrogeait…
>
>J’imagine que ma maladresse me valut un laisser pour compte endéans les
quelques minutes d’échanges. 
>
>Ce refus, je le porte toujours comme une porte d’entrée éblouissante.
>
>Il s’en est suivi, au fil du temps, nombre d’entretiens radiophoniques
que j’ai menés, non seulement avec Gita, mais également toutes celles et
ceux qui sont attachés à cet espace de recherches…
>
> Bref, c’était un refus déclencheur, une sorte de non qui dit&#x202F;: «&#x202F;Viens&#x202F;!&#x202F;»          

***

> Jean-François %%Octave, Jean-François|Octave%%, artiste et architecte
{: .signature}
>
> La toute première fois, l’ISELP, c’est %%Brys-Schatan, Gita|Gita Brys-Schatan%% qui m’en parle. Elle est ma prof d’histoire de l’art à %%École nationale supérieure d’architecture et des arts visuels de La Cambre (La Cambre)| La Cambre%% Architecture, en cette fin très *new wave* des années 1970. Gita veut m’acheter des dessins à mon jury, je ne comprends pas comment on peut vouloir m’acheter des dessins, je ris, un peu nerveux. Il y a plus de cent dessins, que j’ai accrochés au mur, pour mon projet de fin d’études. Déjà cette obsession d’accumulation. Elle dit qu’elle me recontactera.
>
> Très vite, Gita me propose une exposition à l’ISELP, ma toute première expo perso : *Dessins d’architecte*. Dans un cycle appelé *Dessins et autres desseins*. En 1984. J’y montre des perspectives imaginaires, proches de mes projets d’études. Loin encore de mes futures peintures. Puis, en 1985, elle m’invite à intervenir dans l’exposition *Travaux en cours*. J’y montre une grande mosaïque de gouaches intitulée *From BArBAry to Here*. Un hommage à John %%Cale, John|Cale%%. Un jour, Gita me dit qu’elle voudrait bien me proposer un projet d’art public. Elle me confie même&#x202F;: «&#x202F;Et pourquoi pas une station de métro&#x202F;?&#x202F;» Je suis assez surpris. Puis tout s’emballe autour de moi. %%Busine, Laurent|Laurent Busine%% m’expose, puis Chris %%Dercon, Chris|Dercon%%, c’est la %%Biennale de Venise%% en 1986…
>
> Dix ans ont passé. Un beau jour de 1995, Gita me téléphone et me demande si je ne veux pas remettre une esquisse pour la station de métro %%Heysel%%. Très vite : la %%Commission artistique des infrastructures de déplacement (CAID)|CAID%% (Commission artistique des infrastructures de déplacement de la %%Région de Bruxelles-Capitale/Région Bruxelloise|Région de Bruxelles-Capitale%%) se réunit quinze jours plus tard. Je remets une proposition. Le lendemain de la réunion, Gita m’annonce que mon esquisse a été acceptée. Je lui dis qu’elle vient de m’offrir le plus beau des cadeaux d’anniversaire pour le jour de mes 40 ans. Puis le projet va s’enliser, presque un an passe, la commission a été dissoute, puis reformée avec de nouveaux membres. Le %%Heysel%% ressurgit. Mais les originaux du projet ont été égarés dans un bureau quelconque. J’ai des copies (pas d’ordi à l’époque), mais je dois quand même tout recommencer. Quelques batailles politico-linguistiques plus tard, le projet est finalisé et définitivement accepté. Mille mètres carrés de sérigraphie réalisés sur tôle émaillée. Quand Gita et moi découvrons la station après le placement de l’œuvre, nous avons presque les larmes aux yeux. Un jeune mec avec son skate, descend les escaliers, regarde les murs et s’écrie : «&#x202F;Wouaw il faudrait faire une grande teuf ici&#x202F;!&#x202F;» Inauguration par la reine %%Fabiola (reine)|Fabiola%% en 1998. La reine veut absolument que je lui explique le sens de la gigantesque ampoule reproduite sur un des murs. Nous sommes sans cesse séparés par un ministre ou l’autre ; tout le monde veut à tout prix la saluer. Au moment de nous quitter, je lui dis en riant que je ne lui ai toujours pas expliqué. «&#x202F;Écrivez-le-moi !&#x202F;», déclare-t-elle en montant dans sa limousine. Je ne le ferai hélas jamais.


***

> Bernadette %%Even-Adin, Bernadette|Even-Adin%%, auditrice régulière de l’ISELP depuis le début des années 1980
{: .signature}
>
> Il y a une quarantaine d’années, décennie 1980, lors d’une émission
radio %%RTBF/RTB|RTB%%, interview de M<sup>me</sup> %%Brys-Schatan, Gita|Gita Brys-Schatan%%&#x202F;: elle parle d’un lieu
accessible à tous, où l’on s’ouvre à la culture. Pas de diplôme
prérequis, mais une curiosité, une envie d’apprendre&#x202F;!
>
> Voilà… en route pour, en effet, un voyage d’apprentissage : on arrive
par l’arrière du bâtiment actuel, petit bureau où la directrice entendue
à la %%RTBF/RTB|RTB%%, accompagnée de sa belle chienne Tervueren gris-argent, nous
accueille chaleureusement. Et on commence : premier programme,
«&#x202F;Histoire du mobilier&#x202F;» (sujet universel qui plaît à tous), autre
cycle, «&#x202F;Le Baroque&#x202F;» (pas le poussiéreux, mais celui qui va nous mener
plus tard vers l’expo anversoise *Sanguine* de Luc %%Tuymans, Luc|Tuymans%% ou le
polyptique de Narcisse %%Tordoir, Narcisse|Tordoir%% aux %%Musées royaux des Beaux-Arts de Belgique%%, enfin l’ABC de
l’art contemporain autant pour les novices que pour ceux qui «&#x202F;savent&#x202F;».
>
> Ces débuts illustrent parfaitement l’esprit de l’ISELP : ouverture et
clés de compréhension pour les arts plastiques. Brassage où des
fondements solides sont donnés : architecture, cinéma, mouvements
d’idées, expos, street art, rencontres de collectionneurs ou d’artistes&#x202F;!
>
> Que dire, pour terminer, des voyages inoubliables ; ceux qui y ont
participé en parlent encore… Pas le genre « agence de voyage&#x202F;» où on
mange en troupeau… non&#x202F;! Libres midis et soirs, mais journées guidées
d’intenses découvertes.
>
> Merci à l’ISELP, à sa fondatrice, M<sup>me</sup> %%Brys-Schatan, Gita|Brys-Schatan%%, et aussi à ses
successeurs qui insufflent constamment à l’institution un idéal de vraie
culture, abordable pour tous, avec un domino d’horaires variés où chacun
peut trouver son bonheur.

***

> Jacques %%Lennep, Jacques|Lennep%%, artiste et historien de l’art
{: .signature}
>
> Le 8 février 1979, j’ai exécuté à l’ISELP, une performance-conférence
consacrée à mon «&#x202F;Musée de l’homme&#x202F;» sous le titre *L’Homme exposé.*
J’avais été invité par Jean-Pierre %%Van Tieghem, Jean-Pierre|Van Tieghem%% alors que, du 24 janvier
au 24 février, la galerie %%Brachot, Isy|Isy Brachot%% exposait une des personnes de ce
musée. Il s’agissait de *Tania, modèle pour photos de charme,* présente
dans le public. Celle-ci, comme toutes les personnes de ce musée,
considérait son existence comme un espace de création – ce que j’ai
expliqué au public. J’étais en veston cravate mais à la fin, les ayant
enlevés y compris la chemise, je suis apparu vêtu du maillot zébré blanc
et noir des footballeurs du %%Sporting de Charleroi|Sporting%% de %%Charleroi%%. C’était un hommage à
l’un de ses supporters les plus exaltés, %%Bucci, Ezio|Ezio Bucci%%, que j’avais exposé
au %%Palais des Beaux-Arts (BOZAR)|palais des Beaux-Arts%%, deux ans plus tôt.
>
> Une autre personne de mon musée avait été évoquée à l’ISELP lors d’une
exposition collective&#x202F;: *Milieu rural* (27 septembre-15 octobre
1978)&#x202F;: *%%Six, Madame Paul|Madame Paul Six%%*, une fermière flamande qui s’évertuait à
obtenir des autographes de personnalités du monde entier.
>
> Mes manifestations à l’ISELP répondaient, je crois, à son programme
orienté vers l’art contemporain émergent dans ses dimensions
expérimentales, alternatives et pluridisciplinaires. Par cet objectif,
l’ISELP occupa une position stratégique dès les années 1970. En témoigne encore ce colloque *Exposer l'image en mouvement&#x202F;: de la grammaire spatiale à la production de contenu* des 28 et 29 novembre 2003,
organisé par Éric %%Van Essche, Éric|Van Essche%%. J’y avais retrouvé %%Forest, Fred|Fred Forest%% avec
lequel j’étais en relation depuis de nombreuses années. Tous deux, nous étions les pionniers d’une esthétique relationnelle, dans sa portée sociologique.

***

> Christophe %%Veys, Christophe|Veys%%, historien de l’art, enseignant, commissaire d’exposition, collectionneur
{: .signature}
>
> À l’époque de mon arrivée à l’ISELP, les cours prenaient la forme d’un cycle de vingt rendez-vous de deux heures. Mes week-ends étaient dédiés aux préparations ainsi qu’à la fastidieuse réalisation de diapositives.
Chaque lundi, je cherchais à transmettre ma passion avec le souhait
d’être à la fois accessible, profond et joyeux. En 2002, comme second cycle, j’ai proposé un module intitulé «&#x202F;Du journal intime au mensonge&#x202F;». C’est probablement de cette époque que date mon anecdote. Les séances de fin de journée étaient les plus mixtes à l’Institut. Elles regroupaient des personnes de tous âges. Parmi elles se dégageait la petite silhouette voûtée d’une dame à l’accent venu de %%Hongrie%%. J’ai autrefois connu son prénom qui m’échappe hélas au moment d’écrire ces lignes.
>
>Le sort du conférencier tient, souvent, au fait qu’il ignore l’identité des personnes qui accompagnent cet exercice collectif mais paradoxalement solitaire.
{: .exergue}
>
> Elle devait être la doyenne de notre assemblée. D’une curiosité insatiable, elle n’hésitait jamais à prendre la parole ce qui faisait d’elle une alliée pour le jeune conférencier timide que j’étais. Même si elle n’était parfois pas simple à canaliser. Elle avait pour habitude d’arriver en retard. Cela donnait à ses entrées un effet comique digne d'une pièce de boulevards. Je soupçonnais qu’elle
 le fit un rien exprès afin qu’on ne l’oublie pas. Au terme d’un
 chapitre consacré à %%Cahun, Claude|Claude Cahun%%, Cindy %%Sherman, Cindy|Sherman%% et Pierre %%Molinier, Pierre|Molinier%%, elle
 vint me trouver en me disant qu’elle viendrait avec un cadeau la semaine
 suivante. Ainsi, le lundi suivant, elle me tendit un sac en me disant&#x202F;:
 «&#x202F;J’ai depuis des années une revue ‘légère’ que je dois cacher à
 chaque fois que vient ma femme de ménage. Il y a plusieurs pages
 consacrées à Pierre %%Molinier, Pierre|Molinier%%. Je vous l’offre.&#x202F;» Je repartis donc avec
 une revue coquine offerte par l’une de mes plus espiègles auditrices. Je
 l’ai à mon tour cachée (je ne sais plus très bien où). Je ne résiste
 jamais au plaisir de raconter cette anecdote à mes étudiants. Leur
 souhaitant, par là même, une existence durant laquelle le feu de la
 curiosité ne s’éteindrait jamais.

***

> Fabrizio %%Romanelli, Fabrizio|Romanelli%%, technicien, responsable du bâtiment depuis 2011
{: .signature}
>
> Lorsque j’ai commencé à travailler à l’ISELP, les montages demandaient un vrai engagement au niveau technique (beaucoup de cimaises, de constructions…). Les dernières retouches se faisaient à quelques secondes du début du vernissage. J’avais l’habitude d’entendre les artistes et membres de l’équipe prêts à célébrer dans l’espace bar pendant que je terminais mon travail. Le soir du vernissage du volet belge de l’exposition *Duos d’artistes*, en 2012, alors que les dernières retouches étaient finalisées et que je me changeais, j’entends frapper à la porte de mon local. Devant moi se trouve l’artiste Charles-François %%Duplain, Charles-François|Duplain%%, deux bières à la main. Je lui demande s’il y a un souci technique. Sa réponse a été&#x202F;: «&#x202F;Je viens te dire un tout grand merci et boire un verre avec toi&#x202F;». Son attention m’a touché au cœur&#x202F;! Il a non seulement pris le temps de me remercier mais aussi de partager un moment ensemble. Sur dix années de travail dans les coulisses de l’ISELP, je ne me suis jamais senti aussi valorisé.

***

> Laurence %%Dervaux, Laurence|Dervaux%%, artiste, responsable de l’atelier peinture de l’ESA – %%Académie des Beaux-Arts de Tournai%%
{: .signature}
>
> L’ISELP et les différentes équipes qui y ont travaillé durant toutes ces
 années ont ponctué mon parcours de plasticienne.
>
> Jeune artiste, je me souviens avoir présenté, avec une certaine émotion,
 un projet relatif à l’exposition *Mémoire de l’histoire de l’art* devant
 toute l’équipe dirigée à l’époque par %%Brys-Schatan, Gita|Gita Brys-Schatan%% et devant des
 artistes dont certains connaissaient déjà une belle notoriété.
>
> Une dizaine d’années plus tard, c’est avec assurance que j’ai décrit,
 devant une assemblée similaire, le cahier des charges pour la
 réalisation d’une installation relative à la quantité de sang pompée par
 le cœur humain. Et c’est grâce à la confiance et à la complicité du
 staff et de sa directrice, Arlette %%Lemonnier, Arlette|Lemonnier%%, que cette installation
 d’envergure a vu le jour. C’est sous cette même direction que mon
 exposition monographique à l’ISELP a été programmée et c’est sous la
 direction d’Éric Van Essche qu’elle a eu lieu.
>
 >Que de souvenirs de périodes de travail diurne mais aussi nocturne,
 notamment avec la commissaire Catherine %%Henkinet, Catherine|Henkinet%% pour la mise en place
 des installations *in situ* mais aussi pour la mise en œuvre d’un
 catalogue paru lors de cette exposition monographique. Des souvenirs,
 également, d’échanges riches avec les historiens de l’art de l’ISELP afin
 de mener à bien les textes de cette parution.
>
 >Aujourd’hui, cette complicité avec l’ISELP perdure, notamment au travers
 de mes invitations adressées à l’équipe actuelle et à son directeur,
 Adrien %%Grimmeau, Adrien|Grimmeau%%, afin qu’ils participent aux jurys que j’organise en ma
 qualité de responsable de l’atelier peinture de l’ESA – %%Académie des Beaux-Arts de Tournai%%. Ces invitations permettent à certains étudiants
 de se faire remarquer et, parfois, leur donne l’occasion de participer à
 des projets de soutien à des jeunes créateurs organisés par l’ISELP.
>
 >Ces soutiens sont une belle continuité de cette complicité.

***

> Adèle %%Santocono, Adèle|Santocono%%, historienne de l'art, responsable du secteur des arts plastiques de la Province de %%Hainaut%%
{: .signature}
>
> Cette rencontre s’est déroulée probablement lors d’une de mes dernières
 visites d’ateliers d’artistes en tant que collaboratrice à l’ISELP. Nous
 nous y sommes rendus pour ramener une installation présentée dans
 l’exposition qui justement célébrait les quarante ans de l’Institut. Nous
 appréciions particulièrement ces moments privilégiés durant lesquels,
 avec ma collègue de l’époque, et mon amie aujourd’hui, nous pénétrions
 dans l’intimité des artistes et de leurs créations. Il y en a des récits
 dans ces lieux envahis de matières, de formes à explorer, de substances
 à odorer, d’émotions à découvrir, d’histoires à se raconter, de thé ou
 de café à partager&#x202F;!
>
 > L’atelier en question était un hangar très aseptisé et très vaste avec
 une grande porte de garage. Nous avions plutôt l’habitude de pièces plus
 petites et agréables mais, cette fois, l’artiste étant de renommée
 internationale, la taille imposante et surprenante du lieu pouvait
 s’expliquer. Je me rappelle des hautes poutres métalliques, de la
 mezzanine et des bureaux vitrés comme ceux occupés par le responsable
 pour surveiller le travail de ses employés. Ici, il n’y avait ni
 mobilier, ni personnel au travail mais des milliers de peluches colorées
 qui étaient entassées, couchées, suspendues à des câbles, classées par
 teintes, par tailles, par textures dans tout le local. Des ballons
 étaient retenus dans leurs filets, des textiles de chambre d’enfant
 recouvraient le sol. C’était un décor de conte enfantin ou de fête
 foraine imaginaire qui se présentait derrière la porte à moteur que nous
 venions de franchir. L’artiste et sa femme nous attendaient dans cet univers immense, au centre de cette installation artistique
 perpétuelle dans laquelle nous semblions tout petits.
>
> Quelques semaines plus tôt, lors du montage, l’artiste m’avait en effet
 raconté, avec une certaine tendresse, le souvenir du jour où sa mère
 avait mis aux ordures, sans le prévenir, sa collection de l’époque.
 Cette disparition l’avait amené à devenir un plasticien «&#x202F;sauveur
 d’oursons abandonnés&#x202F;». Cette histoire et cette démarche qui me touchent
 beaucoup, je la raconte à mes enfants et, depuis, jamais je n’ai osé
 toucher à une oreille de leurs doudous, même les plus poussiéreux. Cette anecdote reflète ce que je conserve de mon expérience à l’ISELP, les
 belles rencontres, mais surtout le fait de percevoir la pratique
 artistique comme une nécessité pour changer le regard de la société,
 pour nous emmener ailleurs et ce même à partir d’un objet apparemment
 anodin.


***

> %%Boulares, Sami|Sami Boulares%%, technicien, régisseur à l’ISELP depuis 2015
{: .signature}
>
> Je suis arrivé à l’ISELP comme technicien audiovisuel en 2015, mais j’ai
 toujours réalisé plein de tâches techniques en parallèle. Je prends
 essentiellement part aux montages d’expositions, ainsi qu’à tous les
 événements dans l’auditorium.
>
 > Au printemps 2017, le montage de l’exposition *SYNC&#x202F;!* a été un moment
 marquant bien qu’éprouvant. Le défi était énorme&#x202F;: une exposition en
 trois volets, une multitude d’événements simultanés. Le projet traitait
 du travail réalisé en collectif. Il devait être dynamique, composé de
 performances, de lectures, de concerts ainsi que de cours et
 d’interventions dans l’auditorium.
 >
 > Le soir du vernissage, une atmosphère particulière se dégageait. Le
 public était plus mixte que d’habitude&#x202F;: une armée d’étudiants, une
 pléiade d’amateurs d’art contemporain, de membres de l’ISELP, de
 visiteurs réguliers, ainsi que de gens de passage&#x202F;! Ce long projet
 impliquant beaucoup de régie et plus de public que d’habitude
 représentait une grande première pour moi.     
 Je me suis retrouvé assis au centre d’un groupe d’étudiants qui
 écoutaient un concert, pour mixer le son. À ce moment, j’ai ressenti un
 mélange de satisfaction, de joie et de fierté. On y était&#x202F;! J’étais
 heureux d’avoir pu donner corps, avec toute l'équipe, à cette idée qui
 touchait autant de personnes différentes et présentes. Je prenais
 conscience de la pertinence de ce que nous avions réalisé et de
 l’utilité des efforts fournis. 

 ***

 ![](contenu-final/14-B-Temoignage-ManonBara-Manuscrit.png)
 :    

%%Bara, Manon|Manon Bara%%
{: .signature .cacher}

 ***

> Anne %%Pintus, Anne|Pintus%%, historienne de l’art, enseignante à %%Saint-Luc — ESA|Saint-Luc%% ESA
{: .signature}
>
> J’ai à peine 18 ans quand je découvre l’ISELP pour la première fois.
 Dans le cadre d’un cours d’esthétique (en 6<sup>e</sup> secondaire), je dois
 réaliser une présentation aux Musées royaux des %%Musées royaux des Beaux-Arts de Belgique|Beaux-Arts de Belgique%%
 (Bruxelles) à propos d’une sculpture de Vic %%Gentils, Vic|Gentils%%, et notre professeur
 exige que les recherches aient lieu à la bibliothèque du Centre d’art
 contemporain, alors située rue des %%Nerviens, avenue des|Nerviens%%, le long du parc du
 %%Parc du Cinquantenaire|Cinquantenaire%%. Je n’ai jamais entendu parler de ce centre. Je m’y
 rends, et j’y reste une journée entière à lire, à photocopier
 catalogues, livres, et revues spécialisées, à recenser les sources, et à
 questionner le sujet. Mon énergie passe du bouillonnement à l’angoisse.
 Je n’ai jamais analysé d’œuvre, je n’ai jamais rédigé de bibliographie,
 et je suis terrorisée à l’idée de m’exprimer oralement face à mes
 camarades de classe et à mon professeur, que je ne veux surtout pas
 décevoir. Je découvre au même moment l’histoire de l’art et le syndrome
 de l’imposteur. Je ne dois pas être démasquée. Après une journée
 supplémentaire de travail assidu et nerveux, et pour me préparer au
 mieux, je décide de présenter la sculpture à une amie de mon frère, qui
 travaille déjà au musée en tant que guide. Face à elle, et devant la
 sculpture, mes craintes d’échec et d’humiliation se transforment en
 réalité&#x202F;: plus de voix, idées troubles, je suis complètement paralysée.
 Avec beaucoup de finesse et d’empathie (un peu comme un ange), elle
 parvient à mettre de l’ordre dans mes idées et à organiser ma réflexion.
 Quelques jours plus tard, je retrouve mes copains et mon professeur au
 musée pour la présentation. Une sensation de joie et de sérénité
 m’envahit dès le moment où je m’exprime. J’ai presque l’impression que
 ce n’est pas moi qui parle. Je sais dès la fin de l’exercice que
 j’étudierai l’histoire de l’art.


***

> Michèle %%Stanescu, Michèle|Stanescu%%, documentaliste responsable du centre de documentation du %%Centre d’art contemporain%% de 1984 à 2004, et du centre de documentation de l’ISELP depuis 2005
{: .signature}
>
> J’ai vu défiler des dizaines d’étudiants en histoire de l’art dans la salle de lecture depuis que je suis documentaliste. Au fil des années, j’ai observé une évolution dans leur méthodologie de recherche. Avant l’application du traité de %%Bologne%% au sein des institutions d’enseignement, les étudiants suivaient des programmes de cours continus et fréquentaient le centre de documentation depuis leur deuxième année d’université jusqu’à la remise de leur mémoire. Je les aidais parfois à préciser leur sujet de recherche, je collaborais à leur processus de réflexion. Grâce à la régularité de leurs visites, j’avais l’occasion de développer des liens stimulants. La restructuration des études, la croissance d’Internet, la mobilité permise par les programmes %%Erasmus%% ont eu des répercussions sur les modes de recherche et la fréquentation du centre de documentation. Je me suis jointe à l’équipe de l’ISELP en 2005, un an après la publication d’une version augmentée de *L’Histoire matérielle et immatérielle de l’art moderne & contemporain* de Florence de %%De Mèredieu, Florence de|De Mèredieu%%, qui a eu un impact sur la recherche en histoire de l’art en Belgique. Par la suite, j’ai observé que les sujets de mémoires, auparavant monographiques, étaient de plus en plus concentrés autour de problématiques diverses.
>
> Après toutes ces années d’observation de la recherche réalisée par les étudiants et les professionnels, le plus touchant, pour moi, demeure sans doute d’assister à l’évolution professionnelle de mes lecteurs, des années après leur passage au centre de documentation.
{: .exergue}

***

> Lyse %%Vancampenhoudt, Lyse|Vancampenhoudt%%, historienne de l’art, chercheuse, guide et conférencière
{: .signature}
>
> Dès la fin de mon Master en histoire de l’art, je souhaitais faire de la
 recherche. Après avoir postulé à trois reprises au %%Fonds (national) de la recherche scientifique (FNRS)|FNRS%%, mais aussi
 après avoir tenté de décrocher un poste de chercheuse aux %%Musées royaux des Beaux-Arts de Belgique|Musées royaux des Beaux-Arts%%, en vain, je finis par me décourager quelque peu. Et puis
 un jour, confinée dans mon petit appartement, je reçois trois mails, de
 connaissances et ami·e·s différent·e·s. Tous me transmettent l’appel à
 candidatures pour une résidence scientifique qui s’ouvre bientôt à
 l’ISELP. Qui ne tente rien n’a rien, je décide d’y postuler, qui sait,
 tout n’est peut-être pas perdu. Et l’été passe, masqué et
 mystérieux. Alors que je faisais la sieste, mon téléphone sonne, je décroche,
 ronchonne. Le réveil en valait la chandelle, mon dossier a été choisi à
 l’unanimité pour la résidence scientifique à l’ISELP. L’année sombre et
 étrange qui m’attendait s’éclaire d’un éclat nouveau. À mon petit
 bureau, au centre de doc, bien cachée derrière les rayonnages, en
 compagnie d’une grande plante amicale, je me risque à une tentative de
 réécriture de l’histoire de l’art. Les mots et les images tiennent une
 place importante dans l’histoire de l’art belge. Mais les artistes
 femmes ne semblent pas y trouver leur place. Où sont-elles, les
 contemporaines de %%Magritte, René|Magritte%%, %%Dotremont, Christian|Dotremont%% ou %%Broodthaers, Marcel|Broodthaers%%&#x202F;? Ce sont-elles
 intéressées aux mots et aux images, et si non, pourquoi&#x202F;? Michèle
 %%Stanescu, Michèle|Stanescu%% sort de grandes piles de livres que j’épluche patiemment. Les
 idées fusent avec Mélanie %%Rainville, Mélanie|Rainville%%, autour d’un café, lorsqu’on pouvait
 encore boire des cafés autour d’une table. Lors d’une réunion virtuelle,
 Adrien %%Grimmeau, Adrien|Grimmeau%% me conseille le catalogue d’une exposition…
 >
 > La recherche est une aventure solitaire, une solitude collective dans
 l’architecture labyrinthique de l’ISELP où les idées se croisent et se
 rencontrent malgré la crise que nous traversons…
 {: .exergue}

***

>  Pierre-Yves %%Desaive, Pierre-Yves|Desaive%%, historien de l’art et conservateur pour l’art contemporain aux Musées royaux des %%Musées royaux des Beaux-Arts de Belgique|Beaux-Arts de Belgique%%, Bruxelles
{: .signature}
>
> Un an quasi jour pour jour sépare la conférence que j’ai donnée à
 l’ISELP, consacrée à Wim %%Delvoye, Wim|Delvoye%%, de l’enregistrement que j’ai réalisé en
 juin 2020 pour le %%SoundCloud%% nouvellement créé. Au risque d’user d’une
 formule un rien stéréotypée, cette année ressemble à une éternité, un
 écart abyssal entre le monde d’avant et celui dans lequel nous vivons
 aujourd’hui, miné par la pandémie. J’avais choisi pour le %%SoundCloud%% de
 parler d’une œuvre d’Emmanuel %%Van der Auwera, Emmanuel|Van der Auwera%%, *VideoSculpture XXI
 (Vegas)*. Elle traite de surveillance de masse via des caméras
 thermiques militaires, d’une redoutable précision. Nul doute que ce
 choix m’ait été inconsciemment dicté par le climat anxiogène dans lequel
 nous baignions alors – et qui n’a malheureusement guère changé au
 moment où j’écris ces lignes.
